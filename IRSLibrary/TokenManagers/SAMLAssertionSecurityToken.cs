namespace IRSLibrary
{
    using Microsoft.Web.Services3.Security.Cryptography;
    using Microsoft.Web.Services3.Security.Tokens;
    using System;
    using System.Security.Permissions;
    using System.Xml;

    [SecurityPermission(SecurityAction.Demand, UnmanagedCode=true)]
    public class SAMLAssertionSecurityToken : SecurityToken
    {
        private XmlDocument assertion;

        public SAMLAssertionSecurityToken() : base("urn:oasis:names:tc:SAML:1.0:assertion")
        {
        }

        public override bool Equals(SecurityToken token)
        {
            if ((token == null) || !(token is SAMLAssertionSecurityToken))
            {
                return false;
            }
            SAMLAssertionSecurityToken token2 = (SAMLAssertionSecurityToken) token;
            return ((token2.assertion == this.assertion) || (((token2.assertion != null) && (this.assertion != null)) && token2.assertion.OuterXml.Equals(this.assertion.OuterXml)));
        }

        public override int GetHashCode()
        {
            if (this.assertion == null)
            {
                return 0;
            }
            return this.assertion.OuterXml.GetHashCode();
        }

        public override XmlElement GetXml(XmlDocument document)
        {
            if (this.assertion == null)
            {
                return null;
            }
            return (XmlElement) document.ImportNode(this.assertion.DocumentElement, true);
        }

        public override void LoadXml(XmlElement element)
        {
            this.assertion = new XmlDocument();
            this.assertion.PreserveWhitespace = true;
            this.assertion.AppendChild(this.assertion.ImportNode(element, true));
        }

        public override bool IsCurrent
        {
            get
            {
                return true;
            }
        }

        public override KeyAlgorithm Key
        {
            get
            {
                return null;
            }
        }

        public override bool SupportsDataEncryption
        {
            get
            {
                return false;
            }
        }

        public override bool SupportsDigitalSignature
        {
            get
            {
                return false;
            }
        }
    }
}

