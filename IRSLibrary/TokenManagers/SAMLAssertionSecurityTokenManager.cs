namespace IRSLibrary
{
    using Microsoft.Web.Services3.Security.Tokens;
    using System;
    using System.Xml;

    public class SAMLAssertionSecurityTokenManager : SecurityTokenManager
    {
        public override SecurityToken LoadTokenFromXml(XmlElement element)
        {
           SAMLAssertionSecurityToken token = new SAMLAssertionSecurityToken();
            token.LoadXml(element);
            return token;
        }

        public override void VerifyToken(SecurityToken token)
        {
            if (!(token is SAMLAssertionSecurityToken))
            {
                throw new ArgumentException("The token is the wrong type.");
            }
            SAMLAssertionSecurityToken token1 = (SAMLAssertionSecurityToken) token;
        }

        public override string TokenType
        {
            get
            {
                return "urn:oasis:names:tc:SAML:1.0:assertion";
            }
        }
    }
}

