﻿using System;
using System.Configuration;

public static class Variables
{
	public static string ETIN = ConfigurationSettings.AppSettings["ETIN"];
	public static string MeF_AppSysID=ConfigurationSettings.AppSettings["MeF_AppSysID"];
		
	public static string EFIN=ConfigurationSettings.AppSettings["EFIN"];
	public static string PIN=ConfigurationSettings.AppSettings["PIN"];

	public static string TestIndicator=ConfigurationSettings.AppSettings["TestIndicator"];
	
	public static string StatusNodeName=ConfigurationSettings.AppSettings["StatusNodeName"];

	public static string path2CreateZipFile=@"C:\";

    public static string MailServer = ConfigurationSettings.AppSettings["MailServer"];
    public static string AuthenticationEmail = ConfigurationSettings.AppSettings["AuthenticationEmail"];
    public static string MailPassword = ConfigurationSettings.AppSettings["MailPassword"];
    public static int MailPort =Convert.ToInt32(ConfigurationSettings.AppSettings["MailPort"]);
    public static string MailBcc = ConfigurationSettings.AppSettings["MailBcc"];
    public static string Environment = ConfigurationSettings.AppSettings["Environment"];

    public static string DBConnectionString = ConfigurationSettings.AppSettings["smConnectionString"];
   
    public static string CertificateKey = "Fl0e4AX+KzCC6x8KdguOfjEoKmo=";
    public static string WSDLVerNum = "10.4";

}