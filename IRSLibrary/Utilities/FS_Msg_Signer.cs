using System.Collections;
using Microsoft.Web.Services3;
using Microsoft.Web.Services3.Security;
using Microsoft.Web.Services3.Security.Tokens;
using Microsoft.Web.Services3.Security.X509;
using Microsoft.Web.Services3.Mime;
using System.Security.Cryptography.X509Certificates;
using System.Diagnostics;
using IRSLibrary.Utilities;
using System;

public class FS_Msg_Signer
{
	public FS_Msg_Signer()
	{
	}

    private static X509SecurityToken GetSigningToken_SubName(string keyIdentifier)
    {
        X509SecurityToken token = null;
        X509Store store = new X509Store(System.Security.Cryptography.X509Certificates.StoreName.My, StoreLocation.LocalMachine);
        try
        {

            store.Open(OpenFlags.OpenExistingOnly | OpenFlags.ReadOnly);

            X509Certificate2Collection certs = X509Util.FindCertificateBySubjectName(keyIdentifier, StoreLocation.LocalMachine, "MY");

            IEnumerator certEnum;
            certEnum = certs.GetEnumerator();
            certEnum.MoveNext();

            if (certs.Count > 0)
            {
                token = new X509SecurityToken(certs[0]);

            }
        }
        finally
        {
            if (store != null)
                store.Close();
        }
        return token;
    } 

    private static X509SecurityToken GetSigningToken(string keyIdentifier)
    {
        X509SecurityToken token =null;
        X509Store store=new X509Store(System.Security.Cryptography.X509Certificates.StoreName.My,StoreLocation.LocalMachine);
        
        try
        {
        
        store.Open(OpenFlags.OpenExistingOnly | OpenFlags.ReadOnly);
        
        X509Certificate2Collection certs = X509Util.FindCertificateByKeyIdentifier(System.Convert.FromBase64String(keyIdentifier), StoreLocation.LocalMachine, "My");

		IEnumerator certEnum ;
        certEnum = certs.GetEnumerator();
        certEnum.MoveNext();

        if (certs.Count > 0)
        {
            token = new X509SecurityToken(certs[0]);
			
        }
        }
        finally
        {
            if (store != null)
                store.Close();
        }
        return token;
    } 


    private static byte[] ConvertHexToByteArray(string hexString)
    {
        int i = 0;
        int j = 0;
        byte[] bytes = new byte[(hexString.Length / 2)];
        string hex = null;
        char[] hexCharArray = null;

        hexCharArray = hexString.ToCharArray();
        j = 0;
        for (i = 0; i <= (bytes.Length - 1); i++)
        {
            hex = new string(new char[] { hexCharArray[j], hexCharArray[j + 1] });
            bytes[i] = byte.Parse(hex, System.Globalization.NumberStyles.HexNumber);
            j = j + 2;
        }
        return bytes;
    } 



    public static void Sign(ref Microsoft.Web.Services3.WebServicesClientProtocol proxy, string sigRef, string KeyIdentifier)
    {
        try
        {
            X509SecurityToken token;
            MessageSignature sig;
            SignatureReference soapRef = new SignatureReference(sigRef);

            soapRef.AddTransform(new Microsoft.Web.Services3.Security.Xml.XmlDsigExcC14NTransform());
            token = GetSigningToken(KeyIdentifier);

            proxy.RequestSoapContext.Security.Tokens.Add(token);

            sig = new MessageSignature(token);

            sig.SignatureOptions = SignatureOptions.IncludeNone;

            sig.SignatureOptions = SignatureOptions.IncludeSoapBody;

            sig.AddReference(soapRef);

            proxy.RequestSoapContext.Security.Elements.Add(sig);
        }
        catch (Exception ex)
        {
            WriteLog.Log(ex.Message, EventLogEntryType.Error);
            throw;
        }
    }

    public static void Sign_SubName(ref Microsoft.Web.Services3.WebServicesClientProtocol proxy, string sigRef, string KeyIdentifier)
    {
        try
        {
            X509SecurityToken token;
            MessageSignature sig;
            SignatureReference soapRef = new SignatureReference(sigRef);

            soapRef.AddTransform(new Microsoft.Web.Services3.Security.Xml.XmlDsigExcC14NTransform());
            token = GetSigningToken_SubName(KeyIdentifier);

            proxy.RequestSoapContext.Security.Tokens.Add(token);

            sig = new MessageSignature(token);

            sig.SignatureOptions = SignatureOptions.IncludeNone;

            sig.SignatureOptions = SignatureOptions.IncludeSoapBody;

            sig.AddReference(soapRef);

            proxy.RequestSoapContext.Security.Elements.Add(sig);
        }
        catch (Exception ex)
        {
            WriteLog.Log(ex.Message, EventLogEntryType.Error);
            throw;
        }
    } 

}
