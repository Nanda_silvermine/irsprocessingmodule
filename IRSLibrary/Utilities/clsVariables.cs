using System;
using System.Configuration;


public class clsVariables
{
    public string TestIndicator;
    public string ETIN;
    public string PIN;
    public string MeF_AppSysID;
    public string EFIN;
    public string SoftwareID;
    public string SoftwareVersion;

    public string path2CreateZipFile;

    public clsVariables(string taxYear)
    {
        TestIndicator = ConfigurationSettings.AppSettings["TestIndicator"].ToString();
        //SILVERMINE GROUP OF WILTON, LLC EIN : 27-4394630

        MeF_AppSysID = "06434601";
        EFIN = "064346";
        path2CreateZipFile = @"C:\";

        if (TestIndicator == "T")
        {
            ETIN = "24665";
            PIN = "24665";
        }
        else if (TestIndicator == "P")
        {
            ETIN = "25093";
            PIN = "25093";
        }

        if (taxYear == "2021")
        {
            SoftwareID = "21012807";
            SoftwareVersion = "2021v1.0";
        }

        if (taxYear == "2020")
        {
            SoftwareID = "20010866";
            SoftwareVersion = "2020v4.0";
        }

        if (taxYear == "2019")
        {
            SoftwareID = "19008988";
            SoftwareVersion = "2019v1.1"; 
        }

        if (taxYear == "2018")
        {
            SoftwareID = "18006648";
            SoftwareVersion = "2018v2.0";
        }

        if(taxYear == "2017")
        {
            SoftwareID = "17000103";
            SoftwareVersion = "2017v1.1";
        }

    }
}