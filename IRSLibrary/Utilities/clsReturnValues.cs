using System;
using System.IO;


    [Serializable]
    public class clsReturnValues
    {
        public clsReturnValues()
        {
        }

        private string _status;
        private string _returnMsg;
        private byte[] _schedule1;
        private string _submissionID;
        private string _submissionIndicator;//T=Testing; P=Production

        public string status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }

        public string returnMsg
        {
            get
            {
                return _returnMsg;
            }
            set
            {
                _returnMsg = value;
            }
        }

        public string submissionID
        {
            get
            {
                return _submissionID;
            }
            set
            {
                _submissionID = value;
            }
        }
        /* Vishwaw 9.6 
        public string submissionIDType
        {
            get
            {
                return _submissionID;
            }
            set
            {
                _submissionID = value;
            }
        }
         */

        public byte[] schedule1
        {
            get
            {
                return _schedule1;
            }
            set
            {
                _schedule1 = value;
            }
        }


        public string submissionIndicator
        {
            get
            {
                return _submissionIndicator;
            }
            set
            {
                _submissionIndicator = value;
            }
        }
    }

