using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Web.Services;
using Microsoft.Web.Services3;
using Microsoft.Web.Services3.Security;
using Microsoft.Web.Services3.Security.Tokens;
using Microsoft.Web.Services3.Security.X509;
using Microsoft.Web.Services3.Mime;
using ICSharpCode.SharpZipLib.Zip;
using IRSLibrary.MefMsiServices;
using IRSLibrary.Utilities;
using System.Diagnostics;
using System.Configuration;
using System.Collections.Generic;

namespace IRSLibrary
{
    public class Return2290Attachment
    {
        BaseMethods BM = new BaseMethods();
        static clsVariables Variables;

        public static string SubmissionType = "2290";
        public static string submissionID;

        static string path2CreateFile_4SendSubmission = @"C:\";
        static string path2CreateZipFile = @"C:\";

        public static byte[] schedule1;

        public Return2290Attachment(string taxyear)
        {
            Variables = new clsVariables(taxyear);
        }

        public clsReturnValues SEND_APPLICATION_TO_IRS
         (
                #region Parameters
         string ref_no,
         string subID,
         string frm_id,
         char status,

         bool IsAddressChange,
         bool IsAmendedReturn,
         bool IsVINCorrection,
         bool IsFinalReturn,
         bool IsNotSubjectToTaxChecked,
         bool IsConsentToDiscloseYes,
         string AmendedMonth,
         string TaxYear,
         string First_Used_Date,

         string Filer_AddressLine1,
         string Filer_City,
         string Filer_EIN,
         string Filer_Foreign_Country,
         string Filer_Name,
         string Filer_NameControl,
         string Filer_State,
         string Filer_ZIPCode,

         string Officer_Name,
         string Officer_Phone,
         string Officer_PIN,
         string Officer_Title,
         string Officer_email,

         string ThirdPartyDesigne_Name,
         string ThirdPartyDesigne_Phone,
         string ThirdPartyDesigne_PIN,

         string Preparer_Firm,
         string Preparer_EIN,
         string Preparer_City,
         string Preparer_State,
         string Preparer_Zip,
         string Preparer_Country,
         string Preparer_Address,
         string Preparer_ForeignPhone,
         string Preparer_Phone,
         string Preparer_Email,
         string Preparer_Name,
         string Preparer_PTIN_SSN,
         bool IsPreparerHasPTIN,
         bool IsPreparerSelfEmployed,

         string paymentType,
         string Payment_Acc_No,
         string Payment_Acc_Type,
         string Payment_ReqPayDate,
         string Payment_RoutingTransitNo,
         string Payment_Txpyer_Ph,

         DataSet dsTaxableVehicles,
         DataSet dsCreditVehicles,
         DataSet dsSuspendedVehicles,
         DataSet dsSoldSuspendedVehicles,
         DataSet dsPriorYearMileageExceededVehicles,
         DataSet dsTGWIncreaseWorksheet,
         DataSet dsTaxComputation,
         string soldSuspendedVehicles,
         decimal TaxFromTaxComputation,
         decimal AdditionalTaxAmount,
         decimal CreditAmount,
         string TotalVehicles
        #endregion
         )
        {
            submissionID = subID;

            clsReturnValues rv = new clsReturnValues();
            string returnMsg = "";

            try
            {
                if(SecToken.isSessionPresent)
                {
                    WriteLog.Log("Login Success", EventLogEntryType.Information);
                    returnMsg = "Success";
                }
                else
                {
                    returnMsg = BM._Login();
                }

                if (returnMsg != "Success")
                {
                    rv.returnMsg = returnMsg;
                    rv.status = "";
                    rv.submissionID = "";
                    rv.schedule1 = null;
                    rv.submissionIndicator = ConfigurationManager.AppSettings["TestIndicator"].ToString();
                    BM._Logout();
                    return rv;
                }

                #region SEND_SUBMISSION

                if (status == 'N' || status == 'R')
                {
                    submissionID = "";

                    returnMsg = Create_Attachment_For_f2290
                    (
                        #region Parameters
                        IsAddressChange,
                        IsAmendedReturn,
                        IsVINCorrection,
                        IsFinalReturn,
                        IsNotSubjectToTaxChecked,
                        IsConsentToDiscloseYes,
                        AmendedMonth,
                        TaxYear,
                        First_Used_Date,

                        Filer_AddressLine1,
                        Filer_City,
                        Filer_EIN,
                        Filer_Foreign_Country,
                        Filer_Name,
                        Filer_NameControl,
                        Filer_State,
                        Filer_ZIPCode,

                        Officer_Name,
                        Officer_Phone,
                        Officer_PIN,
                        Officer_Title,
                        Officer_email,

                        ThirdPartyDesigne_Name,
                        ThirdPartyDesigne_Phone,
                        ThirdPartyDesigne_PIN,

                        Preparer_Firm,
                        Preparer_EIN,
                        Preparer_City,
                        Preparer_State,
                        Preparer_Zip,
                        Preparer_Country,
                        Preparer_Address,
                        Preparer_ForeignPhone,
                        Preparer_Phone,
                        Preparer_Email,
                        Preparer_Name,
                        Preparer_PTIN_SSN,
                        IsPreparerHasPTIN,
                        IsPreparerSelfEmployed,

                        paymentType,
                        Payment_Acc_No,
                        Payment_Acc_Type,
                        Payment_ReqPayDate,
                        Payment_RoutingTransitNo,
                        Payment_Txpyer_Ph,

                        dsTaxableVehicles,
                        dsCreditVehicles,
                        dsSuspendedVehicles,
                        dsSoldSuspendedVehicles,
                        dsPriorYearMileageExceededVehicles,
                        dsTGWIncreaseWorksheet,
                        dsTaxComputation,

                        soldSuspendedVehicles,
                        TaxFromTaxComputation,
                        AdditionalTaxAmount,
                        CreditAmount,
                        TotalVehicles
                    #endregion
                    );

                    if (returnMsg != "Success")
                    {
                        rv.returnMsg = returnMsg;
                        rv.status = "";
                        rv.submissionID = "";
                        rv.schedule1 = null;
                        rv.submissionIndicator = ConfigurationManager.AppSettings["TestIndicator"].ToString();
                        return rv;
                    }

                    try
                    {
                        System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\Live Submissions\2290 Log\RefSubmission.txt",true);
                        file.WriteLine(ref_no + " :::> " + submissionID);
                        file.Close();
                    }
                    catch
                    {
                    }
                   
                    returnMsg = BM._SendSubmissions(submissionID);

                    if (returnMsg != "Success")
                    {

                        rv.returnMsg = returnMsg;
                        rv.status = "N";
                        rv.submissionID = submissionID;
                        //rv.submissionIDType = submissionID; // Vishwa 9.6

                        rv.submissionIndicator = ConfigurationManager.AppSettings["TestIndicator"].ToString();
                        rv.schedule1 = null;

                        if (rv.submissionIndicator == "P")
                            MoveFiles(submissionID, ref_no);

                        return rv;
                    }

                    status = 'S';
                }

                #endregion

                #region GET_ACKNOWLEDGEMENT

                if (status == 'S')
                {
                    returnMsg = BM._GetAck(submissionID);

                    if (returnMsg != "Success")
                    {
                        rv.returnMsg = returnMsg;

                        if (returnMsg.IndexOf("::::") > -1)
                            rv.status = "S";
                        else
                            rv.status = "R";

                        if (returnMsg.Contains("Acknowledgement"))
                            rv.status = "A";

                        rv.submissionID = submissionID;
                        //rv.submissionIDType = submissionID; // Vishwa 9.6

                        rv.schedule1 = null;
                        rv.submissionIndicator = ConfigurationManager.AppSettings["TestIndicator"].ToString();

                        if (rv.submissionIndicator == "P")
                            MoveFiles(submissionID, ref_no);

                        return rv;
                    }

                    status = 'A';
                }

                #endregion

                #region ACCEPTED

                if (status == 'A')
                {
                    schedule1 = null;
                    returnMsg = BM._GetSchedule1(submissionID);

                    if (returnMsg != "Success")
                    {
                        rv.returnMsg = returnMsg;
                        rv.status = "A";
                        rv.submissionID = submissionID;
                        //rv.submissionIDType = submissionID; // Vishwa 9.6

                        rv.schedule1 = null;
                        rv.submissionIndicator = ConfigurationManager.AppSettings["TestIndicator"].ToString();
                        if (rv.submissionIndicator == "P")
                            MoveFiles(submissionID, ref_no);
                       
                        return rv;
                    }
                    else
                    {
                        rv.returnMsg = returnMsg;
                        rv.status = "C";
                        rv.submissionID = submissionID;
                        //rv.submissionIDType = submissionID; // Vishwa 9.6

                        rv.schedule1 = schedule1;
                        rv.submissionIndicator = ConfigurationManager.AppSettings["TestIndicator"].ToString();
                        if (rv.submissionIndicator == "P")
                            MoveFiles(submissionID, ref_no);
                        
                        return rv;
                    }
                }
                #endregion

            }
            catch (Exception ex)
            {
                WriteLog.Log(ex.Message, EventLogEntryType.Error);
            }

            return rv;
        }

        #region 2290 Attachment

        public string Create_Attachment_For_f2290(
        #region Parameters
            bool IsAddressChange,
            bool IsAmendedReturn,
            bool IsVINCorrection,
            bool IsFinalReturn,
            bool IsNotSubjectToTaxChecked,
            bool IsConsentToDiscloseYes,
            string AmendedMonth,
            string TaxYear,
            string First_Used_Date,

            string Filer_AddressLine1,
            string Filer_City,
            string Filer_EIN,
            string Filer_Foreign_Country,
            string Filer_Name,
            string Filer_NameControl,
            string Filer_State,
            string Filer_ZIPCode,

            string Officer_Name,
            string Officer_Phone,
            string Officer_PIN,
            string Officer_Title,
            string Officer_Email,

            string ThirdPartyDesigne_Name,
            string ThirdPartyDesigne_Phone,
            string ThirdPartyDesigne_PIN,

            string Preparer_Firm,
            string Preparer_EIN,
            string Preparer_City,
            string Preparer_State,
            string Preparer_Zip,
            string Preparer_Country,
            string Preparer_Address,
            string Preparer_ForeignPhone,
            string Preparer_Phone,
            string Preparer_Email,
            string Preparer_Name,
            string Preparer_PTIN_SSN,
            bool IsPreparerHasPTIN,
            bool IsPreparerSelfEmployed,

            string paymentType,
            string Payment_Acc_No,
            string Payment_Acc_Type,
            string Payment_ReqPayDate,
            string Payment_RoutingTransitNo,
            string Payment_Txpyer_Ph,

            DataSet dsTaxableVehicles,
            DataSet dsCreditVehicles,
            DataSet dsSuspendedVehicles,
            DataSet dsSoldSuspendedVehicles,
            DataSet dsPriorYearMileageExceededVehicles,
            DataSet dsTGWIncreaseWorksheet,
            DataSet dsTaxComputation,
            string soldSuspendedVehicles,
            decimal TaxFromTaxComputation,
            decimal AdditionalTaxAmount,
            decimal CreditAmount,
            string TotalVehicles
        #endregion
)
        {
            try
            {  
                
                int temp_TaxYr = 0;
                temp_TaxYr = Convert.ToInt32(TaxYear);
                DateTime TaxPeriodEndDate = Convert.ToDateTime("06-30-" + TaxYear);;
                if (temp_TaxYr > 0)
                {
                    temp_TaxYr = temp_TaxYr + 1;
                    TaxPeriodEndDate = Convert.ToDateTime("06-30-" + temp_TaxYr.ToString());
                }

                DateTime TaxPeriodBeginDate = Convert.ToDateTime("07-01-" + TaxYear);
                //DateTime TaxPeriodEndDate = Convert.ToDateTime("06-30-" + TaxYear);

                createManifestXML(TaxYear, TaxPeriodEndDate , TaxPeriodBeginDate, Filer_EIN, "2290");                

                createZip();

                return "Success";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        private int ToInt32(string TaxYear)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region createReturnXml_2290_2021

        /* Vishwa New 2021 Changes, for the XML building for 2021 */
        public void createReturnXml_2290_2021
        (
        #region Parameters
                bool IsAddressChange, bool IsAmendedReturn, bool IsVINCorrection, bool IsFinalReturn,
                bool IsNotSubjectToTaxChecked, bool IsConsentToDiscloseYes, string AmendedMonth, string TaxYear,
                string First_Used_Date, string Filer_AddressLine1, string Filer_City, string Filer_EIN,
                string Filer_Foreign_Country, string Filer_Name, string Filer_NameControl, string Filer_State,
                string Filer_ZIPCode, string Officer_Name, string Officer_Phone, string Officer_PIN,
                string Officer_Title, string Officer_email, string ThirdPartyDesigne_Name, string ThirdPartyDesigne_Phone,
                string ThirdPartyDesigne_PIN, string Preparer_Firm, string Preparer_EIN, string Preparer_City,
                string Preparer_State, string Preparer_Zip, string Preparer_Country, string Preparer_Address,
                string Preparer_ForeignPhone, string Preparer_Phone, string Preparer_Email, string Preparer_Name,
                string Preparer_PTIN_SSN, bool IsPreparerHasPTIN, bool IsPreparerSelfEmployed, string paymentType,
                string Payment_Acc_No, string Payment_Acc_Type, string Payment_ReqPayDate, string Payment_RoutingTransitNo,
                string Payment_Txpyer_Ph, DataSet dsTaxableVehicles, DataSet dsCreditVehicles, DataSet dsSuspendedVehicles,
                DataSet dsSoldSuspendedVehicles, DataSet dsPriorYearMileageExceededVehicles, DataSet dsTGWIncreaseWorksheet, DataSet dsTaxComputation,
                string soldSuspendedVehicles, decimal TaxFromTaxComputation, decimal AdditionalTaxAmount, decimal CreditAmount,
                string TotalVehicles
        #endregion
        )
        {
            int documentCount = 0;
            FileStream fStream = null;
            FileStream fStream2 = null;
            MemoryStream mStream = null;

            int numTotalTaxableVeh = 0;
            int numTaxSuspended_LoggingVeh = 0;
            int numTaxSuspended_NonLoggingVeh = 0;

            int numNonLoggingVehicleCnt = 0;
            int numLoggingVehicleCnt = 0;

            try
            {
                if (!Directory.Exists(path2CreateZipFile + @"\xml"))
                    Directory.CreateDirectory(path2CreateZipFile + @"\xml");

                fStream2 = new FileStream(path2CreateZipFile + @"\xml\submission.xml", FileMode.Create);

                byte[] byteArray = new byte[90000];
                mStream = new MemoryStream(byteArray);
                XmlSerializer s = new XmlSerializer(typeof(Return2290_2021.Return));

                #region RETRUN_HEADER

                Return2290_2021.ReturnHeaderType returnHeaderType = new Return2290_2021.ReturnHeaderType();

                returnHeaderType.ReturnTs = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz");

                returnHeaderType.FirstUsedDt = First_Used_Date;

                returnHeaderType.MultSoftwarePackagesUsedInd = false;
                returnHeaderType.PINEnteredByCd = Return2290_2021.ReturnHeaderTypePINEnteredByCd.Taxpayer;
                returnHeaderType.PINEnteredByCdSpecified = true;
                returnHeaderType.ReturnTypeCd = Return2290_2021.ReturnHeaderTypeReturnTypeCd.Item2290;
                returnHeaderType.SignatureOptionCd = Return2290_2021.ReturnHeaderTypeSignatureOptionCd.PINNumber;
                returnHeaderType.SignatureOptionCdSpecified = true;

                #region ORIGINATOR

                #region PRACTIONER PIN

                Return2290_2021.ReturnHeaderTypeOriginatorGrpPractitionerPINGrp objPractitionerPIN =
                    new Return2290_2021.ReturnHeaderTypeOriginatorGrpPractitionerPINGrp();
                objPractitionerPIN.EFIN = Variables.EFIN;
                objPractitionerPIN.PIN = Variables.PIN;

                #endregion

                Return2290_2021.ReturnHeaderTypeOriginatorGrp originator = new Return2290_2021.ReturnHeaderTypeOriginatorGrp();
                originator.EFIN = Variables.EFIN;
                originator.OriginatorTypeCd = Return2290_2021.OriginatorType.ERO;
                originator.PractitionerPINGrp = objPractitionerPIN;
                returnHeaderType.OriginatorGrp = originator;

                #endregion

                #region FILER
                Return2290_2021.ReturnHeaderTypeFiler objFiler = new Return2290_2021.ReturnHeaderTypeFiler();

                objFiler.EIN = Filer_EIN;
                objFiler.BusinessNameLine1Txt = Filer_Name; //max_length=60
                objFiler.BusinessNameControlTxt = Filer_NameControl;

                if (Filer_Foreign_Country == "" || Filer_Foreign_Country == "US")
                {
                    #region FILER US ADDRESS

                    Return2290_2021.USAddressType usAdd_Filer = new Return2290_2021.USAddressType();

                    if (Filer_AddressLine1.Length > 35)
                    {
                        string Filer_AddressLine2 = string.Empty;
                        usAdd_Filer.AddressLine1 = Filer_AddressLine1.Substring(0, 35).Trim();
                        Filer_AddressLine2 = Filer_AddressLine1.Substring(35);
                        usAdd_Filer.AddressLine2 = Filer_AddressLine2.Trim();

                    }
                    else
                    {
                        usAdd_Filer.AddressLine1 = Filer_AddressLine1.Trim();
                    }
                    usAdd_Filer.City = Filer_City.Trim();

                    switch (Filer_State)
                    {
                        #region Filer_State
                        case "AA": usAdd_Filer.State = Return2290_2021.StateType.AA; break;
                        case "AE": usAdd_Filer.State = Return2290_2021.StateType.AE; break;
                        case "AK": usAdd_Filer.State = Return2290_2021.StateType.AK; break;
                        case "AL": usAdd_Filer.State = Return2290_2021.StateType.AL; break;
                        case "AP": usAdd_Filer.State = Return2290_2021.StateType.AP; break;
                        case "AR": usAdd_Filer.State = Return2290_2021.StateType.AR; break;
                        case "AS": usAdd_Filer.State = Return2290_2021.StateType.AS; break;
                        case "AZ": usAdd_Filer.State = Return2290_2021.StateType.AZ; break;
                        case "CA": usAdd_Filer.State = Return2290_2021.StateType.CA; break;
                        case "CO": usAdd_Filer.State = Return2290_2021.StateType.CO; break;
                        case "CT": usAdd_Filer.State = Return2290_2021.StateType.CT; break;
                        case "DC": usAdd_Filer.State = Return2290_2021.StateType.DC; break;
                        case "DE": usAdd_Filer.State = Return2290_2021.StateType.DE; break;
                        case "FL": usAdd_Filer.State = Return2290_2021.StateType.FL; break;
                        case "FM": usAdd_Filer.State = Return2290_2021.StateType.FM; break;
                        case "GA": usAdd_Filer.State = Return2290_2021.StateType.GA; break;
                        case "GU": usAdd_Filer.State = Return2290_2021.StateType.GU; break;
                        case "HI": usAdd_Filer.State = Return2290_2021.StateType.HI; break;
                        case "IA": usAdd_Filer.State = Return2290_2021.StateType.IA; break;
                        case "ID": usAdd_Filer.State = Return2290_2021.StateType.ID; break;
                        case "IL": usAdd_Filer.State = Return2290_2021.StateType.IL; break;
                        case "IN": usAdd_Filer.State = Return2290_2021.StateType.IN; break;
                        case "KS": usAdd_Filer.State = Return2290_2021.StateType.KS; break;
                        case "KY": usAdd_Filer.State = Return2290_2021.StateType.KY; break;
                        case "LA": usAdd_Filer.State = Return2290_2021.StateType.LA; break;
                        case "MA": usAdd_Filer.State = Return2290_2021.StateType.MA; break;
                        case "MD": usAdd_Filer.State = Return2290_2021.StateType.MD; break;
                        case "ME": usAdd_Filer.State = Return2290_2021.StateType.ME; break;
                        case "MH": usAdd_Filer.State = Return2290_2021.StateType.MH; break;
                        case "MI": usAdd_Filer.State = Return2290_2021.StateType.MI; break;
                        case "MN": usAdd_Filer.State = Return2290_2021.StateType.MN; break;
                        case "MO": usAdd_Filer.State = Return2290_2021.StateType.MO; break;
                        case "MP": usAdd_Filer.State = Return2290_2021.StateType.MP; break;
                        case "MS": usAdd_Filer.State = Return2290_2021.StateType.MS; break;
                        case "MT": usAdd_Filer.State = Return2290_2021.StateType.MT; break;
                        case "NC": usAdd_Filer.State = Return2290_2021.StateType.NC; break;
                        case "ND": usAdd_Filer.State = Return2290_2021.StateType.ND; break;
                        case "NE": usAdd_Filer.State = Return2290_2021.StateType.NE; break;
                        case "NH": usAdd_Filer.State = Return2290_2021.StateType.NH; break;
                        case "NJ": usAdd_Filer.State = Return2290_2021.StateType.NJ; break;
                        case "NM": usAdd_Filer.State = Return2290_2021.StateType.NM; break;
                        case "NV": usAdd_Filer.State = Return2290_2021.StateType.NV; break;
                        case "NY": usAdd_Filer.State = Return2290_2021.StateType.NY; break;
                        case "OH": usAdd_Filer.State = Return2290_2021.StateType.OH; break;
                        case "OK": usAdd_Filer.State = Return2290_2021.StateType.OK; break;
                        case "OR": usAdd_Filer.State = Return2290_2021.StateType.OR; break;
                        case "PA": usAdd_Filer.State = Return2290_2021.StateType.PA; break;
                        case "PR": usAdd_Filer.State = Return2290_2021.StateType.PR; break;
                        case "PW": usAdd_Filer.State = Return2290_2021.StateType.PW; break;
                        case "RI": usAdd_Filer.State = Return2290_2021.StateType.RI; break;
                        case "SC": usAdd_Filer.State = Return2290_2021.StateType.SC; break;
                        case "SD": usAdd_Filer.State = Return2290_2021.StateType.SD; break;
                        case "TN": usAdd_Filer.State = Return2290_2021.StateType.TN; break;
                        case "TX": usAdd_Filer.State = Return2290_2021.StateType.TX; break;
                        case "UT": usAdd_Filer.State = Return2290_2021.StateType.UT; break;
                        case "VA": usAdd_Filer.State = Return2290_2021.StateType.VA; break;
                        case "VI": usAdd_Filer.State = Return2290_2021.StateType.VI; break;
                        case "VT": usAdd_Filer.State = Return2290_2021.StateType.VT; break;
                        case "WA": usAdd_Filer.State = Return2290_2021.StateType.WA; break;
                        case "WI": usAdd_Filer.State = Return2290_2021.StateType.WI; break;
                        case "WV": usAdd_Filer.State = Return2290_2021.StateType.WV; break;
                        case "WY": usAdd_Filer.State = Return2290_2021.StateType.WY; break;
                            #endregion
                    }

                    usAdd_Filer.ZIPCode = Filer_ZIPCode;

                    objFiler.Item = usAdd_Filer;
                    #endregion
                }
                else
                {
                    #region FILER FOREIGN ADDRESS
                    Return2290_2021.ForeignAddressType foreignAdd_Filer = new Return2290_2021.ForeignAddressType();

                    if (Filer_AddressLine1.Length > 35)
                    {
                        string Filer_AddressLine2 = string.Empty;
                        foreignAdd_Filer.AddressLine1 = Filer_AddressLine1.Substring(0, 35).Trim();
                        Filer_AddressLine2 = Filer_AddressLine1.Substring(35);
                        foreignAdd_Filer.AddressLine2 = Filer_AddressLine2.Trim();
                    }
                    else
                    {
                        foreignAdd_Filer.AddressLine1 = Filer_AddressLine1.Trim();
                    }

                    foreignAdd_Filer.City = Filer_City.Trim();

                    if (Filer_State.Length > 0)
                        foreignAdd_Filer.ProvinceOrState = Filer_State;

                    switch (Filer_Foreign_Country)
                    {
                        #region Filer_Foreign_Country
                        case "AA": foreignAdd_Filer.Country = Return2290_2021.CountryType.AA; break;
                        case "AC": foreignAdd_Filer.Country = Return2290_2021.CountryType.AC; break;
                        case "AE": foreignAdd_Filer.Country = Return2290_2021.CountryType.AE; break;
                        case "AF": foreignAdd_Filer.Country = Return2290_2021.CountryType.AF; break;
                        case "AG": foreignAdd_Filer.Country = Return2290_2021.CountryType.AG; break;
                        case "AJ": foreignAdd_Filer.Country = Return2290_2021.CountryType.AJ; break;
                        case "AL": foreignAdd_Filer.Country = Return2290_2021.CountryType.AL; break;
                        case "AM": foreignAdd_Filer.Country = Return2290_2021.CountryType.AM; break;
                        case "AN": foreignAdd_Filer.Country = Return2290_2021.CountryType.AN; break;
                        case "AO": foreignAdd_Filer.Country = Return2290_2021.CountryType.AO; break;
                        case "AR": foreignAdd_Filer.Country = Return2290_2021.CountryType.AR; break;
                        case "AS": foreignAdd_Filer.Country = Return2290_2021.CountryType.AS; break;
                        case "AT": foreignAdd_Filer.Country = Return2290_2021.CountryType.AT; break;
                        case "AU": foreignAdd_Filer.Country = Return2290_2021.CountryType.AU; break;
                        case "AV": foreignAdd_Filer.Country = Return2290_2021.CountryType.AV; break;
                        case "AX": foreignAdd_Filer.Country = Return2290_2021.CountryType.AX; break;
                        case "AY": foreignAdd_Filer.Country = Return2290_2021.CountryType.AY; break;
                        case "BA": foreignAdd_Filer.Country = Return2290_2021.CountryType.BA; break;
                        case "BB": foreignAdd_Filer.Country = Return2290_2021.CountryType.BB; break;
                        case "BC": foreignAdd_Filer.Country = Return2290_2021.CountryType.BC; break;
                        case "BD": foreignAdd_Filer.Country = Return2290_2021.CountryType.BD; break;
                        case "BE": foreignAdd_Filer.Country = Return2290_2021.CountryType.BE; break;
                        case "BF": foreignAdd_Filer.Country = Return2290_2021.CountryType.BF; break;
                        case "BG": foreignAdd_Filer.Country = Return2290_2021.CountryType.BG; break;
                        case "BH": foreignAdd_Filer.Country = Return2290_2021.CountryType.BH; break;
                        case "BK": foreignAdd_Filer.Country = Return2290_2021.CountryType.BK; break;
                        case "BL": foreignAdd_Filer.Country = Return2290_2021.CountryType.BL; break;
                        case "BM": foreignAdd_Filer.Country = Return2290_2021.CountryType.BM; break;
                        case "BN": foreignAdd_Filer.Country = Return2290_2021.CountryType.BN; break;
                        case "BO": foreignAdd_Filer.Country = Return2290_2021.CountryType.BO; break;
                        case "BP": foreignAdd_Filer.Country = Return2290_2021.CountryType.BP; break;
                        case "BQ": foreignAdd_Filer.Country = Return2290_2021.CountryType.BQ; break;
                        case "BR": foreignAdd_Filer.Country = Return2290_2021.CountryType.BR; break;
                        case "BT": foreignAdd_Filer.Country = Return2290_2021.CountryType.BT; break;
                        case "BU": foreignAdd_Filer.Country = Return2290_2021.CountryType.BU; break;
                        case "BV": foreignAdd_Filer.Country = Return2290_2021.CountryType.BV; break;
                        case "BX": foreignAdd_Filer.Country = Return2290_2021.CountryType.BX; break;
                        case "BY": foreignAdd_Filer.Country = Return2290_2021.CountryType.BY; break;
                        case "CA": foreignAdd_Filer.Country = Return2290_2021.CountryType.CA; break;
                        case "CB": foreignAdd_Filer.Country = Return2290_2021.CountryType.CB; break;
                        case "CD": foreignAdd_Filer.Country = Return2290_2021.CountryType.CD; break;
                        case "CE": foreignAdd_Filer.Country = Return2290_2021.CountryType.CE; break;
                        case "CF": foreignAdd_Filer.Country = Return2290_2021.CountryType.CF; break;
                        case "CG": foreignAdd_Filer.Country = Return2290_2021.CountryType.CG; break;
                        case "CH": foreignAdd_Filer.Country = Return2290_2021.CountryType.CH; break;
                        case "CI": foreignAdd_Filer.Country = Return2290_2021.CountryType.CI; break;
                        case "CJ": foreignAdd_Filer.Country = Return2290_2021.CountryType.CJ; break;
                        case "CK": foreignAdd_Filer.Country = Return2290_2021.CountryType.CK; break;
                        case "CM": foreignAdd_Filer.Country = Return2290_2021.CountryType.CM; break;
                        case "CN": foreignAdd_Filer.Country = Return2290_2021.CountryType.CN; break;
                        case "CO": foreignAdd_Filer.Country = Return2290_2021.CountryType.CO; break;
                        case "CR": foreignAdd_Filer.Country = Return2290_2021.CountryType.CR; break;
                        case "CS": foreignAdd_Filer.Country = Return2290_2021.CountryType.CS; break;
                        case "CT": foreignAdd_Filer.Country = Return2290_2021.CountryType.CT; break;
                        case "CU": foreignAdd_Filer.Country = Return2290_2021.CountryType.CU; break;
                        case "CV": foreignAdd_Filer.Country = Return2290_2021.CountryType.CV; break;
                        case "CW": foreignAdd_Filer.Country = Return2290_2021.CountryType.CW; break;
                        case "CY": foreignAdd_Filer.Country = Return2290_2021.CountryType.CY; break;
                        case "DA": foreignAdd_Filer.Country = Return2290_2021.CountryType.DA; break;
                        case "DJ": foreignAdd_Filer.Country = Return2290_2021.CountryType.DJ; break;
                        case "DO": foreignAdd_Filer.Country = Return2290_2021.CountryType.DO; break;
                        case "DR": foreignAdd_Filer.Country = Return2290_2021.CountryType.DR; break;
                        case "DX": foreignAdd_Filer.Country = Return2290_2021.CountryType.DX; break;
                        case "EC": foreignAdd_Filer.Country = Return2290_2021.CountryType.EC; break;
                        case "EG": foreignAdd_Filer.Country = Return2290_2021.CountryType.EG; break;
                        case "EI": foreignAdd_Filer.Country = Return2290_2021.CountryType.EI; break;
                        case "EK": foreignAdd_Filer.Country = Return2290_2021.CountryType.EK; break;
                        case "EN": foreignAdd_Filer.Country = Return2290_2021.CountryType.EN; break;
                        case "ER": foreignAdd_Filer.Country = Return2290_2021.CountryType.ER; break;
                        case "ES": foreignAdd_Filer.Country = Return2290_2021.CountryType.ES; break;
                        case "ET": foreignAdd_Filer.Country = Return2290_2021.CountryType.ET; break;
                        case "EZ": foreignAdd_Filer.Country = Return2290_2021.CountryType.EZ; break;
                        case "FI": foreignAdd_Filer.Country = Return2290_2021.CountryType.FI; break;
                        case "FJ": foreignAdd_Filer.Country = Return2290_2021.CountryType.FJ; break;
                        case "FK": foreignAdd_Filer.Country = Return2290_2021.CountryType.FK; break;
                        case "FO": foreignAdd_Filer.Country = Return2290_2021.CountryType.FO; break;
                        case "FP": foreignAdd_Filer.Country = Return2290_2021.CountryType.FP; break;
                        case "FQ": foreignAdd_Filer.Country = Return2290_2021.CountryType.FQ; break;
                        case "FR": foreignAdd_Filer.Country = Return2290_2021.CountryType.FR; break;
                        case "FS": foreignAdd_Filer.Country = Return2290_2021.CountryType.FS; break;
                        case "GA": foreignAdd_Filer.Country = Return2290_2021.CountryType.GA; break;
                        case "GB": foreignAdd_Filer.Country = Return2290_2021.CountryType.GB; break;
                        case "GG": foreignAdd_Filer.Country = Return2290_2021.CountryType.GG; break;
                        case "GH": foreignAdd_Filer.Country = Return2290_2021.CountryType.GH; break;
                        case "GI": foreignAdd_Filer.Country = Return2290_2021.CountryType.GI; break;
                        case "GJ": foreignAdd_Filer.Country = Return2290_2021.CountryType.GJ; break;
                        case "GK": foreignAdd_Filer.Country = Return2290_2021.CountryType.GK; break;
                        case "GL": foreignAdd_Filer.Country = Return2290_2021.CountryType.GL; break;
                        case "GM": foreignAdd_Filer.Country = Return2290_2021.CountryType.GM; break;
                        case "GR": foreignAdd_Filer.Country = Return2290_2021.CountryType.GR; break;
                        case "GT": foreignAdd_Filer.Country = Return2290_2021.CountryType.GT; break;
                        case "GV": foreignAdd_Filer.Country = Return2290_2021.CountryType.GV; break;
                        case "GY": foreignAdd_Filer.Country = Return2290_2021.CountryType.GY; break;
                        case "HA": foreignAdd_Filer.Country = Return2290_2021.CountryType.HA; break;
                        case "HK": foreignAdd_Filer.Country = Return2290_2021.CountryType.HK; break;
                        case "HM": foreignAdd_Filer.Country = Return2290_2021.CountryType.HM; break;
                        case "HO": foreignAdd_Filer.Country = Return2290_2021.CountryType.HO; break;
                        case "HQ": foreignAdd_Filer.Country = Return2290_2021.CountryType.HQ; break;
                        case "HR": foreignAdd_Filer.Country = Return2290_2021.CountryType.HR; break;
                        case "HU": foreignAdd_Filer.Country = Return2290_2021.CountryType.HU; break;
                        case "IC": foreignAdd_Filer.Country = Return2290_2021.CountryType.IC; break;
                        case "ID": foreignAdd_Filer.Country = Return2290_2021.CountryType.ID; break;
                        case "IM": foreignAdd_Filer.Country = Return2290_2021.CountryType.IM; break;
                        case "IO": foreignAdd_Filer.Country = Return2290_2021.CountryType.IO; break;
                        case "IP": foreignAdd_Filer.Country = Return2290_2021.CountryType.IP; break;
                        case "IR": foreignAdd_Filer.Country = Return2290_2021.CountryType.IR; break;
                        case "IS": foreignAdd_Filer.Country = Return2290_2021.CountryType.IS; break;
                        case "IT": foreignAdd_Filer.Country = Return2290_2021.CountryType.IT; break;
                        case "IV": foreignAdd_Filer.Country = Return2290_2021.CountryType.IV; break;
                        case "IZ": foreignAdd_Filer.Country = Return2290_2021.CountryType.IZ; break;
                        case "JA": foreignAdd_Filer.Country = Return2290_2021.CountryType.JA; break;
                        case "JE": foreignAdd_Filer.Country = Return2290_2021.CountryType.JE; break;
                        case "JM": foreignAdd_Filer.Country = Return2290_2021.CountryType.JM; break;
                        case "JN": foreignAdd_Filer.Country = Return2290_2021.CountryType.JN; break;
                        case "JO": foreignAdd_Filer.Country = Return2290_2021.CountryType.JO; break;
                        case "JQ": foreignAdd_Filer.Country = Return2290_2021.CountryType.JQ; break;
                        case "KE": foreignAdd_Filer.Country = Return2290_2021.CountryType.KE; break;
                        case "KG": foreignAdd_Filer.Country = Return2290_2021.CountryType.KG; break;
                        case "KN": foreignAdd_Filer.Country = Return2290_2021.CountryType.KN; break;
                        case "KQ": foreignAdd_Filer.Country = Return2290_2021.CountryType.KQ; break;
                        case "KR": foreignAdd_Filer.Country = Return2290_2021.CountryType.KR; break;
                        case "KS": foreignAdd_Filer.Country = Return2290_2021.CountryType.KS; break;
                        case "KT": foreignAdd_Filer.Country = Return2290_2021.CountryType.KT; break;
                        case "KU": foreignAdd_Filer.Country = Return2290_2021.CountryType.KU; break;
                        case "KV": foreignAdd_Filer.Country = Return2290_2021.CountryType.KV; break;
                        case "KZ": foreignAdd_Filer.Country = Return2290_2021.CountryType.KZ; break;
                        case "LA": foreignAdd_Filer.Country = Return2290_2021.CountryType.LA; break;
                        case "LE": foreignAdd_Filer.Country = Return2290_2021.CountryType.LE; break;
                        case "LG": foreignAdd_Filer.Country = Return2290_2021.CountryType.LG; break;
                        case "LH": foreignAdd_Filer.Country = Return2290_2021.CountryType.LH; break;
                        case "LI": foreignAdd_Filer.Country = Return2290_2021.CountryType.LI; break;
                        case "LO": foreignAdd_Filer.Country = Return2290_2021.CountryType.LO; break;
                        case "LQ": foreignAdd_Filer.Country = Return2290_2021.CountryType.LQ; break;
                        case "LS": foreignAdd_Filer.Country = Return2290_2021.CountryType.LS; break;
                        case "LT": foreignAdd_Filer.Country = Return2290_2021.CountryType.LT; break;
                        case "LU": foreignAdd_Filer.Country = Return2290_2021.CountryType.LU; break;
                        case "LY": foreignAdd_Filer.Country = Return2290_2021.CountryType.LY; break;
                        case "MA": foreignAdd_Filer.Country = Return2290_2021.CountryType.MA; break;
                        case "MC": foreignAdd_Filer.Country = Return2290_2021.CountryType.MC; break;
                        case "MD": foreignAdd_Filer.Country = Return2290_2021.CountryType.MD; break;
                        case "MG": foreignAdd_Filer.Country = Return2290_2021.CountryType.MG; break;
                        case "MH": foreignAdd_Filer.Country = Return2290_2021.CountryType.MH; break;
                        case "MI": foreignAdd_Filer.Country = Return2290_2021.CountryType.MI; break;
                        case "MJ": foreignAdd_Filer.Country = Return2290_2021.CountryType.MJ; break;
                        case "MK": foreignAdd_Filer.Country = Return2290_2021.CountryType.MK; break;
                        case "ML": foreignAdd_Filer.Country = Return2290_2021.CountryType.ML; break;
                        case "MN": foreignAdd_Filer.Country = Return2290_2021.CountryType.MN; break;
                        case "MO": foreignAdd_Filer.Country = Return2290_2021.CountryType.MO; break;
                        case "MP": foreignAdd_Filer.Country = Return2290_2021.CountryType.MP; break;
                        case "MQ": foreignAdd_Filer.Country = Return2290_2021.CountryType.MQ; break;
                        case "MR": foreignAdd_Filer.Country = Return2290_2021.CountryType.MR; break;
                        case "MT": foreignAdd_Filer.Country = Return2290_2021.CountryType.MT; break;
                        case "MU": foreignAdd_Filer.Country = Return2290_2021.CountryType.MU; break;
                        case "MV": foreignAdd_Filer.Country = Return2290_2021.CountryType.MV; break;
                        case "MX": foreignAdd_Filer.Country = Return2290_2021.CountryType.MX; break;
                        case "MY": foreignAdd_Filer.Country = Return2290_2021.CountryType.MY; break;
                        case "MZ": foreignAdd_Filer.Country = Return2290_2021.CountryType.MZ; break;
                        case "NC": foreignAdd_Filer.Country = Return2290_2021.CountryType.NC; break;
                        case "NE": foreignAdd_Filer.Country = Return2290_2021.CountryType.NE; break;
                        case "NF": foreignAdd_Filer.Country = Return2290_2021.CountryType.NF; break;
                        case "NG": foreignAdd_Filer.Country = Return2290_2021.CountryType.NG; break;
                        case "NH": foreignAdd_Filer.Country = Return2290_2021.CountryType.NH; break;
                        case "NI": foreignAdd_Filer.Country = Return2290_2021.CountryType.NI; break;
                        case "NL": foreignAdd_Filer.Country = Return2290_2021.CountryType.NL; break;
                        case "NN": foreignAdd_Filer.Country = Return2290_2021.CountryType.NN; break;
                        case "NO": foreignAdd_Filer.Country = Return2290_2021.CountryType.NO; break;
                        case "NP": foreignAdd_Filer.Country = Return2290_2021.CountryType.NP; break;
                        case "NR": foreignAdd_Filer.Country = Return2290_2021.CountryType.NR; break;
                        case "NS": foreignAdd_Filer.Country = Return2290_2021.CountryType.NS; break;
                        case "NU": foreignAdd_Filer.Country = Return2290_2021.CountryType.NU; break;
                        case "NZ": foreignAdd_Filer.Country = Return2290_2021.CountryType.NZ; break;
                        case "PA": foreignAdd_Filer.Country = Return2290_2021.CountryType.PA; break;
                        case "PC": foreignAdd_Filer.Country = Return2290_2021.CountryType.PC; break;
                        case "PE": foreignAdd_Filer.Country = Return2290_2021.CountryType.PE; break;
                        case "PF": foreignAdd_Filer.Country = Return2290_2021.CountryType.PF; break;
                        case "PG": foreignAdd_Filer.Country = Return2290_2021.CountryType.PG; break;
                        case "PK": foreignAdd_Filer.Country = Return2290_2021.CountryType.PK; break;
                        case "PL": foreignAdd_Filer.Country = Return2290_2021.CountryType.PL; break;
                        case "PM": foreignAdd_Filer.Country = Return2290_2021.CountryType.PM; break;
                        case "PO": foreignAdd_Filer.Country = Return2290_2021.CountryType.PO; break;
                        case "PP": foreignAdd_Filer.Country = Return2290_2021.CountryType.PP; break;
                        case "PU": foreignAdd_Filer.Country = Return2290_2021.CountryType.PU; break;
                        case "QA": foreignAdd_Filer.Country = Return2290_2021.CountryType.QA; break;
                        case "RI": foreignAdd_Filer.Country = Return2290_2021.CountryType.RI; break;
                        case "RN": foreignAdd_Filer.Country = Return2290_2021.CountryType.RN; break;
                        case "RO": foreignAdd_Filer.Country = Return2290_2021.CountryType.RO; break;
                        case "RP": foreignAdd_Filer.Country = Return2290_2021.CountryType.RP; break;
                        case "RS": foreignAdd_Filer.Country = Return2290_2021.CountryType.RS; break;
                        case "RW": foreignAdd_Filer.Country = Return2290_2021.CountryType.RW; break;
                        case "SA": foreignAdd_Filer.Country = Return2290_2021.CountryType.SA; break;
                        case "SB": foreignAdd_Filer.Country = Return2290_2021.CountryType.SB; break;
                        case "SC": foreignAdd_Filer.Country = Return2290_2021.CountryType.SC; break;
                        case "SE": foreignAdd_Filer.Country = Return2290_2021.CountryType.SE; break;
                        case "SF": foreignAdd_Filer.Country = Return2290_2021.CountryType.SF; break;
                        case "SG": foreignAdd_Filer.Country = Return2290_2021.CountryType.SG; break;
                        case "SH": foreignAdd_Filer.Country = Return2290_2021.CountryType.SH; break;
                        case "SI": foreignAdd_Filer.Country = Return2290_2021.CountryType.SI; break;
                        case "SL": foreignAdd_Filer.Country = Return2290_2021.CountryType.SL; break;
                        case "SM": foreignAdd_Filer.Country = Return2290_2021.CountryType.SM; break;
                        case "SN": foreignAdd_Filer.Country = Return2290_2021.CountryType.SN; break;
                        case "SO": foreignAdd_Filer.Country = Return2290_2021.CountryType.SO; break;
                        case "SP": foreignAdd_Filer.Country = Return2290_2021.CountryType.SP; break;
                        case "ST": foreignAdd_Filer.Country = Return2290_2021.CountryType.ST; break;
                        case "SU": foreignAdd_Filer.Country = Return2290_2021.CountryType.SU; break;
                        case "SV": foreignAdd_Filer.Country = Return2290_2021.CountryType.SV; break;
                        case "SW": foreignAdd_Filer.Country = Return2290_2021.CountryType.SW; break;
                        case "SX": foreignAdd_Filer.Country = Return2290_2021.CountryType.SX; break;
                        case "SY": foreignAdd_Filer.Country = Return2290_2021.CountryType.SY; break;
                        case "SZ": foreignAdd_Filer.Country = Return2290_2021.CountryType.SZ; break;
                        case "TB": foreignAdd_Filer.Country = Return2290_2021.CountryType.TB; break;
                        case "TD": foreignAdd_Filer.Country = Return2290_2021.CountryType.TD; break;
                        case "TH": foreignAdd_Filer.Country = Return2290_2021.CountryType.TH; break;
                        case "TI": foreignAdd_Filer.Country = Return2290_2021.CountryType.TI; break;
                        case "TK": foreignAdd_Filer.Country = Return2290_2021.CountryType.TK; break;
                        case "TL": foreignAdd_Filer.Country = Return2290_2021.CountryType.TL; break;
                        case "TN": foreignAdd_Filer.Country = Return2290_2021.CountryType.TN; break;
                        case "TO": foreignAdd_Filer.Country = Return2290_2021.CountryType.TO; break;
                        case "TP": foreignAdd_Filer.Country = Return2290_2021.CountryType.TP; break;
                        case "TS": foreignAdd_Filer.Country = Return2290_2021.CountryType.TS; break;
                        case "TT": foreignAdd_Filer.Country = Return2290_2021.CountryType.TT; break;
                        case "TU": foreignAdd_Filer.Country = Return2290_2021.CountryType.TU; break;
                        case "TV": foreignAdd_Filer.Country = Return2290_2021.CountryType.TV; break;
                        case "TW": foreignAdd_Filer.Country = Return2290_2021.CountryType.TW; break;
                        case "TX": foreignAdd_Filer.Country = Return2290_2021.CountryType.TX; break;
                        case "TZ": foreignAdd_Filer.Country = Return2290_2021.CountryType.TZ; break;
                        case "UC": foreignAdd_Filer.Country = Return2290_2021.CountryType.UC; break;
                        case "UG": foreignAdd_Filer.Country = Return2290_2021.CountryType.UG; break;
                        case "UK": foreignAdd_Filer.Country = Return2290_2021.CountryType.UK; break;
                        case "UP": foreignAdd_Filer.Country = Return2290_2021.CountryType.UP; break;
                        case "UV": foreignAdd_Filer.Country = Return2290_2021.CountryType.UV; break;
                        case "UY": foreignAdd_Filer.Country = Return2290_2021.CountryType.UY; break;
                        case "UZ": foreignAdd_Filer.Country = Return2290_2021.CountryType.UZ; break;
                        case "VC": foreignAdd_Filer.Country = Return2290_2021.CountryType.VC; break;
                        case "VE": foreignAdd_Filer.Country = Return2290_2021.CountryType.VE; break;
                        case "VI": foreignAdd_Filer.Country = Return2290_2021.CountryType.VI; break;
                        case "VM": foreignAdd_Filer.Country = Return2290_2021.CountryType.VM; break;
                        case "VT": foreignAdd_Filer.Country = Return2290_2021.CountryType.VT; break;
                        case "WA": foreignAdd_Filer.Country = Return2290_2021.CountryType.WA; break;
                        case "WF": foreignAdd_Filer.Country = Return2290_2021.CountryType.WF; break;
                        case "WI": foreignAdd_Filer.Country = Return2290_2021.CountryType.WI; break;
                        case "WQ": foreignAdd_Filer.Country = Return2290_2021.CountryType.WQ; break;
                        case "WS": foreignAdd_Filer.Country = Return2290_2021.CountryType.WS; break;
                        case "WZ": foreignAdd_Filer.Country = Return2290_2021.CountryType.WZ; break;
                        case "YM": foreignAdd_Filer.Country = Return2290_2021.CountryType.YM; break;
                        case "ZA": foreignAdd_Filer.Country = Return2290_2021.CountryType.ZA; break;
                        case "ZI": foreignAdd_Filer.Country = Return2290_2021.CountryType.ZI; break;
                            #endregion
                    }

                    if (Filer_ZIPCode.Length > 0)
                        foreignAdd_Filer.PostalCode = Filer_ZIPCode;

                    objFiler.Item = foreignAdd_Filer;
                    #endregion
                }

                returnHeaderType.Filer = objFiler;
                #endregion

                #region OFFICER
                if (Officer_Name != "")
                {
                    Return2290_2021.ReturnHeaderTypeBusinessOfficerGrp objOfficer = new Return2290_2021.ReturnHeaderTypeBusinessOfficerGrp();

                    objOfficer.PersonNm = Officer_Name;
                    objOfficer.PersonTitleTxt = Officer_Title;

                    if (Officer_PIN.Length > 0)
                        objOfficer.TaxpayerPIN = Officer_PIN;

                    if (Officer_Phone.Length > 0)
                    {
                        objOfficer.Item = Officer_Phone;
                        if (Filer_Foreign_Country == "US" || Filer_Foreign_Country == "")
                            objOfficer.ItemElementName = Return2290_2021.ItemChoiceType.PhoneNum;
                        else
                            objOfficer.ItemElementName = Return2290_2021.ItemChoiceType.ForeignPhoneNum;
                    }

                    if (Officer_email.Length > 0)
                        objOfficer.EmailAddressTxt = Officer_email;

                    objOfficer.SignatureDt = DateTime.Now.ToUniversalTime();

                    returnHeaderType.BusinessOfficerGrp = objOfficer;
                }
                #endregion

                #region THIRD PARTY DESIGNEE

                Return2290_2021.ReturnHeaderTypeThirdPartyDesignee objThirdPrtyDesignee = new Return2290_2021.ReturnHeaderTypeThirdPartyDesignee();

                object[] itemsField = new object[4];
                Return2290_2021.ItemsChoiceType[] itemsElementNameField = new Return2290_2021.ItemsChoiceType[4];

                itemsField[0] = Return2290_2021.CheckboxType.X;

                if (ThirdPartyDesigne_Name != "")
                {
                    itemsField[1] = ThirdPartyDesigne_Name;
                    itemsField[2] = ThirdPartyDesigne_Phone;
                    itemsField[3] = ThirdPartyDesigne_PIN;

                    itemsElementNameField[0] = Return2290_2021.ItemsChoiceType.DiscussWithThirdPartyYesInd;
                    itemsElementNameField[1] = Return2290_2021.ItemsChoiceType.ThirdPartyDesigneeNm;
                    itemsElementNameField[2] = Return2290_2021.ItemsChoiceType.ThirdPartyDesigneePhoneNum;
                    itemsElementNameField[3] = Return2290_2021.ItemsChoiceType.ThirdPartyDesigneePIN;
                }
                else
                {
                    itemsElementNameField[0] = Return2290_2021.ItemsChoiceType.DiscussWithThirdPartyNoInd;
                }
                objThirdPrtyDesignee.Items = itemsField;
                objThirdPrtyDesignee.ItemsElementName = itemsElementNameField;

                returnHeaderType.ThirdPartyDesignee = objThirdPrtyDesignee;

                #endregion

                #region PREPARER SIGNINGOFFICER PREPARERPERSONGROUP

                if (Preparer_Name != "")
                {
                    Return2290_2021.ReturnHeaderTypePreparerFirmGrp objPreparerFirm = new Return2290_2021.ReturnHeaderTypePreparerFirmGrp();
                    Return2290_2021.ReturnHeaderTypePreparerPersonGrp objPreparer = new Return2290_2021.ReturnHeaderTypePreparerPersonGrp();

                    //Vishwa 2021 - Neeed to add here the SigningOffice details
                    //Return2290_2021.ReturnHeaderTypeSigningOfficerGrp objOfficer = new Return2290_2021.ReturnHeaderTypeSigningOfficerGrp();

                    objPreparer.PreparerPersonNm = Preparer_Name;

                    if (IsPreparerHasPTIN)
                        objPreparer.PTIN = Preparer_PTIN_SSN;

                    if (Preparer_Phone.Length > 0)
                    {
                        objPreparer.Item = Preparer_Phone;
                        objPreparer.ItemElementName = Return2290_2021.ItemChoiceType1.PhoneNum;
                    }
                    else if (Preparer_ForeignPhone.Length > 0)
                    {
                        objPreparer.Item = Preparer_Phone;
                        objPreparer.ItemElementName = Return2290_2021.ItemChoiceType1.ForeignPhoneNum;
                    }
                    objPreparer.EmailAddressTxt = Preparer_Email;
                    objPreparer.PreparationDt = DateTime.Now.ToUniversalTime();
                    objPreparer.PreparationDtSpecified = true;

                    if (IsPreparerSelfEmployed)
                    {
                        objPreparer.SelfEmployedInd = Return2290_2021.CheckboxType.X;
                        objPreparer.SelfEmployedIndSpecified = IsPreparerSelfEmployed;
                    }

                    returnHeaderType.PreparerPersonGrp = objPreparer;
                }
                //2021 Preparer_Address not empty check 
                if (!IsPreparerSelfEmployed && Preparer_Name != "" && Preparer_Address != "")
                {
                    Return2290_2021.ReturnHeaderTypePreparerFirmGrp objPreparerFirm = new Return2290_2021.ReturnHeaderTypePreparerFirmGrp();
                    Return2290_2021.BusinessNameType PreparerBusinessName = new Return2290_2021.BusinessNameType();
                    PreparerBusinessName.BusinessNameLine1Txt = Preparer_Firm;

                    objPreparerFirm.PreparerFirmEIN = Preparer_EIN;
                    objPreparerFirm.PreparerFirmName = PreparerBusinessName;
                    if (Preparer_Country == "" || Preparer_Country == "US")
                    {
                        #region Preparer US ADDRESS

                        Return2290_2021.USAddressType usAdd_Preparer = new Return2290_2021.USAddressType();

                        if (Preparer_Address.Length > 35)
                        {
                            string Preparer_AddressLine2 = string.Empty;
                            usAdd_Preparer.AddressLine1 = Preparer_Address.Substring(0, 35).Trim();
                            Preparer_AddressLine2 = Preparer_Address.Substring(35);
                            usAdd_Preparer.AddressLine2 = Preparer_AddressLine2.Trim();
                        }
                        else
                        {
                            usAdd_Preparer.AddressLine1 = Preparer_Address.Trim();
                        }

                        usAdd_Preparer.City = Preparer_City.Trim();

                        switch (Preparer_State)
                        {
                            #region Filer_State
                            case "AA": usAdd_Preparer.State = Return2290_2021.StateType.AA; break;
                            case "AE": usAdd_Preparer.State = Return2290_2021.StateType.AE; break;
                            case "AK": usAdd_Preparer.State = Return2290_2021.StateType.AK; break;
                            case "AL": usAdd_Preparer.State = Return2290_2021.StateType.AL; break;
                            case "AP": usAdd_Preparer.State = Return2290_2021.StateType.AP; break;
                            case "AR": usAdd_Preparer.State = Return2290_2021.StateType.AR; break;
                            case "AS": usAdd_Preparer.State = Return2290_2021.StateType.AS; break;
                            case "AZ": usAdd_Preparer.State = Return2290_2021.StateType.AZ; break;
                            case "CA": usAdd_Preparer.State = Return2290_2021.StateType.CA; break;
                            case "CO": usAdd_Preparer.State = Return2290_2021.StateType.CO; break;
                            case "CT": usAdd_Preparer.State = Return2290_2021.StateType.CT; break;
                            case "DC": usAdd_Preparer.State = Return2290_2021.StateType.DC; break;
                            case "DE": usAdd_Preparer.State = Return2290_2021.StateType.DE; break;
                            case "FL": usAdd_Preparer.State = Return2290_2021.StateType.FL; break;
                            case "FM": usAdd_Preparer.State = Return2290_2021.StateType.FM; break;
                            case "GA": usAdd_Preparer.State = Return2290_2021.StateType.GA; break;
                            case "GU": usAdd_Preparer.State = Return2290_2021.StateType.GU; break;
                            case "HI": usAdd_Preparer.State = Return2290_2021.StateType.HI; break;
                            case "IA": usAdd_Preparer.State = Return2290_2021.StateType.IA; break;
                            case "ID": usAdd_Preparer.State = Return2290_2021.StateType.ID; break;
                            case "IL": usAdd_Preparer.State = Return2290_2021.StateType.IL; break;
                            case "IN": usAdd_Preparer.State = Return2290_2021.StateType.IN; break;
                            case "KS": usAdd_Preparer.State = Return2290_2021.StateType.KS; break;
                            case "KY": usAdd_Preparer.State = Return2290_2021.StateType.KY; break;
                            case "LA": usAdd_Preparer.State = Return2290_2021.StateType.LA; break;
                            case "MA": usAdd_Preparer.State = Return2290_2021.StateType.MA; break;
                            case "MD": usAdd_Preparer.State = Return2290_2021.StateType.MD; break;
                            case "ME": usAdd_Preparer.State = Return2290_2021.StateType.ME; break;
                            case "MH": usAdd_Preparer.State = Return2290_2021.StateType.MH; break;
                            case "MI": usAdd_Preparer.State = Return2290_2021.StateType.MI; break;
                            case "MN": usAdd_Preparer.State = Return2290_2021.StateType.MN; break;
                            case "MO": usAdd_Preparer.State = Return2290_2021.StateType.MO; break;
                            case "MP": usAdd_Preparer.State = Return2290_2021.StateType.MP; break;
                            case "MS": usAdd_Preparer.State = Return2290_2021.StateType.MS; break;
                            case "MT": usAdd_Preparer.State = Return2290_2021.StateType.MT; break;
                            case "NC": usAdd_Preparer.State = Return2290_2021.StateType.NC; break;
                            case "ND": usAdd_Preparer.State = Return2290_2021.StateType.ND; break;
                            case "NE": usAdd_Preparer.State = Return2290_2021.StateType.NE; break;
                            case "NH": usAdd_Preparer.State = Return2290_2021.StateType.NH; break;
                            case "NJ": usAdd_Preparer.State = Return2290_2021.StateType.NJ; break;
                            case "NM": usAdd_Preparer.State = Return2290_2021.StateType.NM; break;
                            case "NV": usAdd_Preparer.State = Return2290_2021.StateType.NV; break;
                            case "NY": usAdd_Preparer.State = Return2290_2021.StateType.NY; break;
                            case "OH": usAdd_Preparer.State = Return2290_2021.StateType.OH; break;
                            case "OK": usAdd_Preparer.State = Return2290_2021.StateType.OK; break;
                            case "OR": usAdd_Preparer.State = Return2290_2021.StateType.OR; break;
                            case "PA": usAdd_Preparer.State = Return2290_2021.StateType.PA; break;
                            case "PR": usAdd_Preparer.State = Return2290_2021.StateType.PR; break;
                            case "PW": usAdd_Preparer.State = Return2290_2021.StateType.PW; break;
                            case "RI": usAdd_Preparer.State = Return2290_2021.StateType.RI; break;
                            case "SC": usAdd_Preparer.State = Return2290_2021.StateType.SC; break;
                            case "SD": usAdd_Preparer.State = Return2290_2021.StateType.SD; break;
                            case "TN": usAdd_Preparer.State = Return2290_2021.StateType.TN; break;
                            case "TX": usAdd_Preparer.State = Return2290_2021.StateType.TX; break;
                            case "UT": usAdd_Preparer.State = Return2290_2021.StateType.UT; break;
                            case "VA": usAdd_Preparer.State = Return2290_2021.StateType.VA; break;
                            case "VI": usAdd_Preparer.State = Return2290_2021.StateType.VI; break;
                            case "VT": usAdd_Preparer.State = Return2290_2021.StateType.VT; break;
                            case "WA": usAdd_Preparer.State = Return2290_2021.StateType.WA; break;
                            case "WI": usAdd_Preparer.State = Return2290_2021.StateType.WI; break;
                            case "WV": usAdd_Preparer.State = Return2290_2021.StateType.WV; break;
                            case "WY": usAdd_Preparer.State = Return2290_2021.StateType.WY; break;
                                #endregion
                        }

                        usAdd_Preparer.ZIPCode = Preparer_Zip;

                        objPreparerFirm.Item = usAdd_Preparer;

                        #endregion
                    }
                    else
                    {
                        #region Preparer FOREIGN ADDRESS
                        Return2290_2021.ForeignAddressType foreignAdd_Preparer = new Return2290_2021.ForeignAddressType();

                        if (Preparer_Address.Length > 35)
                        {
                            string Preparer_AddressLine2 = string.Empty;
                            foreignAdd_Preparer.AddressLine1 = Preparer_Address.Substring(0, 35).Trim();
                            Preparer_AddressLine2 = Preparer_Address.Substring(35);
                            foreignAdd_Preparer.AddressLine2 = Preparer_AddressLine2.Trim();

                        }
                        else
                        {
                            foreignAdd_Preparer.AddressLine1 = Preparer_Address.Trim();
                        }
                        foreignAdd_Preparer.City = Preparer_City.Trim();

                        if (Preparer_State.Length > 0)
                            foreignAdd_Preparer.ProvinceOrState = Preparer_State;

                        switch (Preparer_Country)
                        {
                            #region Preparer_Foreign_Country
                            case "AA": foreignAdd_Preparer.Country = Return2290_2021.CountryType.AA; break;
                            case "AC": foreignAdd_Preparer.Country = Return2290_2021.CountryType.AC; break;
                            case "AE": foreignAdd_Preparer.Country = Return2290_2021.CountryType.AE; break;
                            case "AF": foreignAdd_Preparer.Country = Return2290_2021.CountryType.AF; break;
                            case "AG": foreignAdd_Preparer.Country = Return2290_2021.CountryType.AG; break;
                            case "AJ": foreignAdd_Preparer.Country = Return2290_2021.CountryType.AJ; break;
                            case "AL": foreignAdd_Preparer.Country = Return2290_2021.CountryType.AL; break;
                            case "AM": foreignAdd_Preparer.Country = Return2290_2021.CountryType.AM; break;
                            case "AN": foreignAdd_Preparer.Country = Return2290_2021.CountryType.AN; break;
                            case "AO": foreignAdd_Preparer.Country = Return2290_2021.CountryType.AO; break;
                            case "AR": foreignAdd_Preparer.Country = Return2290_2021.CountryType.AR; break;
                            case "AS": foreignAdd_Preparer.Country = Return2290_2021.CountryType.AS; break;
                            case "AT": foreignAdd_Preparer.Country = Return2290_2021.CountryType.AT; break;
                            case "AU": foreignAdd_Preparer.Country = Return2290_2021.CountryType.AU; break;
                            case "AV": foreignAdd_Preparer.Country = Return2290_2021.CountryType.AV; break;
                            case "AX": foreignAdd_Preparer.Country = Return2290_2021.CountryType.AX; break;
                            case "AY": foreignAdd_Preparer.Country = Return2290_2021.CountryType.AY; break;
                            case "BA": foreignAdd_Preparer.Country = Return2290_2021.CountryType.BA; break;
                            case "BB": foreignAdd_Preparer.Country = Return2290_2021.CountryType.BB; break;
                            case "BC": foreignAdd_Preparer.Country = Return2290_2021.CountryType.BC; break;
                            case "BD": foreignAdd_Preparer.Country = Return2290_2021.CountryType.BD; break;
                            case "BE": foreignAdd_Preparer.Country = Return2290_2021.CountryType.BE; break;
                            case "BF": foreignAdd_Preparer.Country = Return2290_2021.CountryType.BF; break;
                            case "BG": foreignAdd_Preparer.Country = Return2290_2021.CountryType.BG; break;
                            case "BH": foreignAdd_Preparer.Country = Return2290_2021.CountryType.BH; break;
                            case "BK": foreignAdd_Preparer.Country = Return2290_2021.CountryType.BK; break;
                            case "BL": foreignAdd_Preparer.Country = Return2290_2021.CountryType.BL; break;
                            case "BM": foreignAdd_Preparer.Country = Return2290_2021.CountryType.BM; break;
                            case "BN": foreignAdd_Preparer.Country = Return2290_2021.CountryType.BN; break;
                            case "BO": foreignAdd_Preparer.Country = Return2290_2021.CountryType.BO; break;
                            case "BP": foreignAdd_Preparer.Country = Return2290_2021.CountryType.BP; break;
                            case "BQ": foreignAdd_Preparer.Country = Return2290_2021.CountryType.BQ; break;
                            case "BR": foreignAdd_Preparer.Country = Return2290_2021.CountryType.BR; break;
                            case "BT": foreignAdd_Preparer.Country = Return2290_2021.CountryType.BT; break;
                            case "BU": foreignAdd_Preparer.Country = Return2290_2021.CountryType.BU; break;
                            case "BV": foreignAdd_Preparer.Country = Return2290_2021.CountryType.BV; break;
                            case "BX": foreignAdd_Preparer.Country = Return2290_2021.CountryType.BX; break;
                            case "BY": foreignAdd_Preparer.Country = Return2290_2021.CountryType.BY; break;
                            case "CA": foreignAdd_Preparer.Country = Return2290_2021.CountryType.CA; break;
                            case "CB": foreignAdd_Preparer.Country = Return2290_2021.CountryType.CB; break;
                            case "CD": foreignAdd_Preparer.Country = Return2290_2021.CountryType.CD; break;
                            case "CE": foreignAdd_Preparer.Country = Return2290_2021.CountryType.CE; break;
                            case "CF": foreignAdd_Preparer.Country = Return2290_2021.CountryType.CF; break;
                            case "CG": foreignAdd_Preparer.Country = Return2290_2021.CountryType.CG; break;
                            case "CH": foreignAdd_Preparer.Country = Return2290_2021.CountryType.CH; break;
                            case "CI": foreignAdd_Preparer.Country = Return2290_2021.CountryType.CI; break;
                            case "CJ": foreignAdd_Preparer.Country = Return2290_2021.CountryType.CJ; break;
                            case "CK": foreignAdd_Preparer.Country = Return2290_2021.CountryType.CK; break;
                            case "CM": foreignAdd_Preparer.Country = Return2290_2021.CountryType.CM; break;
                            case "CN": foreignAdd_Preparer.Country = Return2290_2021.CountryType.CN; break;
                            case "CO": foreignAdd_Preparer.Country = Return2290_2021.CountryType.CO; break;
                            case "CR": foreignAdd_Preparer.Country = Return2290_2021.CountryType.CR; break;
                            case "CS": foreignAdd_Preparer.Country = Return2290_2021.CountryType.CS; break;
                            case "CT": foreignAdd_Preparer.Country = Return2290_2021.CountryType.CT; break;
                            case "CU": foreignAdd_Preparer.Country = Return2290_2021.CountryType.CU; break;
                            case "CV": foreignAdd_Preparer.Country = Return2290_2021.CountryType.CV; break;
                            case "CW": foreignAdd_Preparer.Country = Return2290_2021.CountryType.CW; break;
                            case "CY": foreignAdd_Preparer.Country = Return2290_2021.CountryType.CY; break;
                            case "DA": foreignAdd_Preparer.Country = Return2290_2021.CountryType.DA; break;
                            case "DJ": foreignAdd_Preparer.Country = Return2290_2021.CountryType.DJ; break;
                            case "DO": foreignAdd_Preparer.Country = Return2290_2021.CountryType.DO; break;
                            case "DR": foreignAdd_Preparer.Country = Return2290_2021.CountryType.DR; break;
                            case "DX": foreignAdd_Preparer.Country = Return2290_2021.CountryType.DX; break;
                            case "EC": foreignAdd_Preparer.Country = Return2290_2021.CountryType.EC; break;
                            case "EG": foreignAdd_Preparer.Country = Return2290_2021.CountryType.EG; break;
                            case "EI": foreignAdd_Preparer.Country = Return2290_2021.CountryType.EI; break;
                            case "EK": foreignAdd_Preparer.Country = Return2290_2021.CountryType.EK; break;
                            case "EN": foreignAdd_Preparer.Country = Return2290_2021.CountryType.EN; break;
                            case "ER": foreignAdd_Preparer.Country = Return2290_2021.CountryType.ER; break;
                            case "ES": foreignAdd_Preparer.Country = Return2290_2021.CountryType.ES; break;
                            case "ET": foreignAdd_Preparer.Country = Return2290_2021.CountryType.ET; break;
                            case "EZ": foreignAdd_Preparer.Country = Return2290_2021.CountryType.EZ; break;
                            case "FI": foreignAdd_Preparer.Country = Return2290_2021.CountryType.FI; break;
                            case "FJ": foreignAdd_Preparer.Country = Return2290_2021.CountryType.FJ; break;
                            case "FK": foreignAdd_Preparer.Country = Return2290_2021.CountryType.FK; break;
                            case "FO": foreignAdd_Preparer.Country = Return2290_2021.CountryType.FO; break;
                            case "FP": foreignAdd_Preparer.Country = Return2290_2021.CountryType.FP; break;
                            case "FQ": foreignAdd_Preparer.Country = Return2290_2021.CountryType.FQ; break;
                            case "FR": foreignAdd_Preparer.Country = Return2290_2021.CountryType.FR; break;
                            case "FS": foreignAdd_Preparer.Country = Return2290_2021.CountryType.FS; break;
                            case "GA": foreignAdd_Preparer.Country = Return2290_2021.CountryType.GA; break;
                            case "GB": foreignAdd_Preparer.Country = Return2290_2021.CountryType.GB; break;
                            case "GG": foreignAdd_Preparer.Country = Return2290_2021.CountryType.GG; break;
                            case "GH": foreignAdd_Preparer.Country = Return2290_2021.CountryType.GH; break;
                            case "GI": foreignAdd_Preparer.Country = Return2290_2021.CountryType.GI; break;
                            case "GJ": foreignAdd_Preparer.Country = Return2290_2021.CountryType.GJ; break;
                            case "GK": foreignAdd_Preparer.Country = Return2290_2021.CountryType.GK; break;
                            case "GL": foreignAdd_Preparer.Country = Return2290_2021.CountryType.GL; break;
                            case "GM": foreignAdd_Preparer.Country = Return2290_2021.CountryType.GM; break;
                            case "GR": foreignAdd_Preparer.Country = Return2290_2021.CountryType.GR; break;
                            case "GT": foreignAdd_Preparer.Country = Return2290_2021.CountryType.GT; break;
                            case "GV": foreignAdd_Preparer.Country = Return2290_2021.CountryType.GV; break;
                            case "GY": foreignAdd_Preparer.Country = Return2290_2021.CountryType.GY; break;
                            case "HA": foreignAdd_Preparer.Country = Return2290_2021.CountryType.HA; break;
                            case "HK": foreignAdd_Preparer.Country = Return2290_2021.CountryType.HK; break;
                            case "HM": foreignAdd_Preparer.Country = Return2290_2021.CountryType.HM; break;
                            case "HO": foreignAdd_Preparer.Country = Return2290_2021.CountryType.HO; break;
                            case "HQ": foreignAdd_Preparer.Country = Return2290_2021.CountryType.HQ; break;
                            case "HR": foreignAdd_Preparer.Country = Return2290_2021.CountryType.HR; break;
                            case "HU": foreignAdd_Preparer.Country = Return2290_2021.CountryType.HU; break;
                            case "IC": foreignAdd_Preparer.Country = Return2290_2021.CountryType.IC; break;
                            case "ID": foreignAdd_Preparer.Country = Return2290_2021.CountryType.ID; break;
                            case "IM": foreignAdd_Preparer.Country = Return2290_2021.CountryType.IM; break;
                            case "IO": foreignAdd_Preparer.Country = Return2290_2021.CountryType.IO; break;
                            case "IP": foreignAdd_Preparer.Country = Return2290_2021.CountryType.IP; break;
                            case "IR": foreignAdd_Preparer.Country = Return2290_2021.CountryType.IR; break;
                            case "IS": foreignAdd_Preparer.Country = Return2290_2021.CountryType.IS; break;
                            case "IT": foreignAdd_Preparer.Country = Return2290_2021.CountryType.IT; break;
                            case "IV": foreignAdd_Preparer.Country = Return2290_2021.CountryType.IV; break;
                            case "IZ": foreignAdd_Preparer.Country = Return2290_2021.CountryType.IZ; break;
                            case "JA": foreignAdd_Preparer.Country = Return2290_2021.CountryType.JA; break;
                            case "JE": foreignAdd_Preparer.Country = Return2290_2021.CountryType.JE; break;
                            case "JM": foreignAdd_Preparer.Country = Return2290_2021.CountryType.JM; break;
                            case "JN": foreignAdd_Preparer.Country = Return2290_2021.CountryType.JN; break;
                            case "JO": foreignAdd_Preparer.Country = Return2290_2021.CountryType.JO; break;
                            case "JQ": foreignAdd_Preparer.Country = Return2290_2021.CountryType.JQ; break;
                            case "KE": foreignAdd_Preparer.Country = Return2290_2021.CountryType.KE; break;
                            case "KG": foreignAdd_Preparer.Country = Return2290_2021.CountryType.KG; break;
                            case "KN": foreignAdd_Preparer.Country = Return2290_2021.CountryType.KN; break;
                            case "KQ": foreignAdd_Preparer.Country = Return2290_2021.CountryType.KQ; break;
                            case "KR": foreignAdd_Preparer.Country = Return2290_2021.CountryType.KR; break;
                            case "KS": foreignAdd_Preparer.Country = Return2290_2021.CountryType.KS; break;
                            case "KT": foreignAdd_Preparer.Country = Return2290_2021.CountryType.KT; break;
                            case "KU": foreignAdd_Preparer.Country = Return2290_2021.CountryType.KU; break;
                            case "KZ": foreignAdd_Preparer.Country = Return2290_2021.CountryType.KZ; break;
                            case "LA": foreignAdd_Preparer.Country = Return2290_2021.CountryType.LA; break;
                            case "LE": foreignAdd_Preparer.Country = Return2290_2021.CountryType.LE; break;
                            case "LG": foreignAdd_Preparer.Country = Return2290_2021.CountryType.LG; break;
                            case "LH": foreignAdd_Preparer.Country = Return2290_2021.CountryType.LH; break;
                            case "LI": foreignAdd_Preparer.Country = Return2290_2021.CountryType.LI; break;
                            case "LO": foreignAdd_Preparer.Country = Return2290_2021.CountryType.LO; break;
                            case "LQ": foreignAdd_Preparer.Country = Return2290_2021.CountryType.LQ; break;
                            case "LS": foreignAdd_Preparer.Country = Return2290_2021.CountryType.LS; break;
                            case "LT": foreignAdd_Preparer.Country = Return2290_2021.CountryType.LT; break;
                            case "LU": foreignAdd_Preparer.Country = Return2290_2021.CountryType.LU; break;
                            case "LY": foreignAdd_Preparer.Country = Return2290_2021.CountryType.LY; break;
                            case "MA": foreignAdd_Preparer.Country = Return2290_2021.CountryType.MA; break;
                            case "MC": foreignAdd_Preparer.Country = Return2290_2021.CountryType.MC; break;
                            case "MD": foreignAdd_Preparer.Country = Return2290_2021.CountryType.MD; break;
                            case "MG": foreignAdd_Preparer.Country = Return2290_2021.CountryType.MG; break;
                            case "MH": foreignAdd_Preparer.Country = Return2290_2021.CountryType.MH; break;
                            case "MI": foreignAdd_Preparer.Country = Return2290_2021.CountryType.MI; break;
                            case "MJ": foreignAdd_Preparer.Country = Return2290_2021.CountryType.MJ; break;
                            case "MK": foreignAdd_Preparer.Country = Return2290_2021.CountryType.MK; break;
                            case "ML": foreignAdd_Preparer.Country = Return2290_2021.CountryType.ML; break;
                            case "MN": foreignAdd_Preparer.Country = Return2290_2021.CountryType.MN; break;
                            case "MO": foreignAdd_Preparer.Country = Return2290_2021.CountryType.MO; break;
                            case "MP": foreignAdd_Preparer.Country = Return2290_2021.CountryType.MP; break;
                            case "MQ": foreignAdd_Preparer.Country = Return2290_2021.CountryType.MQ; break;
                            case "MR": foreignAdd_Preparer.Country = Return2290_2021.CountryType.MR; break;
                            case "MT": foreignAdd_Preparer.Country = Return2290_2021.CountryType.MT; break;
                            case "MU": foreignAdd_Preparer.Country = Return2290_2021.CountryType.MU; break;
                            case "MV": foreignAdd_Preparer.Country = Return2290_2021.CountryType.MV; break;
                            case "MX": foreignAdd_Preparer.Country = Return2290_2021.CountryType.MX; break;
                            case "MY": foreignAdd_Preparer.Country = Return2290_2021.CountryType.MY; break;
                            case "MZ": foreignAdd_Preparer.Country = Return2290_2021.CountryType.MZ; break;
                            case "NC": foreignAdd_Preparer.Country = Return2290_2021.CountryType.NC; break;
                            case "NE": foreignAdd_Preparer.Country = Return2290_2021.CountryType.NE; break;
                            case "NF": foreignAdd_Preparer.Country = Return2290_2021.CountryType.NF; break;
                            case "NG": foreignAdd_Preparer.Country = Return2290_2021.CountryType.NG; break;
                            case "NH": foreignAdd_Preparer.Country = Return2290_2021.CountryType.NH; break;
                            case "NI": foreignAdd_Preparer.Country = Return2290_2021.CountryType.NI; break;
                            case "NL": foreignAdd_Preparer.Country = Return2290_2021.CountryType.NL; break;
                            case "NO": foreignAdd_Preparer.Country = Return2290_2021.CountryType.NO; break;
                            case "NP": foreignAdd_Preparer.Country = Return2290_2021.CountryType.NP; break;
                            case "NR": foreignAdd_Preparer.Country = Return2290_2021.CountryType.NR; break;
                            case "NS": foreignAdd_Preparer.Country = Return2290_2021.CountryType.NS; break;
                            case "NU": foreignAdd_Preparer.Country = Return2290_2021.CountryType.NU; break;
                            case "NZ": foreignAdd_Preparer.Country = Return2290_2021.CountryType.NZ; break;
                            case "PA": foreignAdd_Preparer.Country = Return2290_2021.CountryType.PA; break;
                            case "PC": foreignAdd_Preparer.Country = Return2290_2021.CountryType.PC; break;
                            case "PE": foreignAdd_Preparer.Country = Return2290_2021.CountryType.PE; break;
                            case "PF": foreignAdd_Preparer.Country = Return2290_2021.CountryType.PF; break;
                            case "PG": foreignAdd_Preparer.Country = Return2290_2021.CountryType.PG; break;
                            case "PK": foreignAdd_Preparer.Country = Return2290_2021.CountryType.PK; break;
                            case "PL": foreignAdd_Preparer.Country = Return2290_2021.CountryType.PL; break;
                            case "PM": foreignAdd_Preparer.Country = Return2290_2021.CountryType.PM; break;
                            case "PO": foreignAdd_Preparer.Country = Return2290_2021.CountryType.PO; break;
                            case "PP": foreignAdd_Preparer.Country = Return2290_2021.CountryType.PP; break;
                            case "PU": foreignAdd_Preparer.Country = Return2290_2021.CountryType.PU; break;
                            case "QA": foreignAdd_Preparer.Country = Return2290_2021.CountryType.QA; break;
                            case "RO": foreignAdd_Preparer.Country = Return2290_2021.CountryType.RO; break;
                            case "RP": foreignAdd_Preparer.Country = Return2290_2021.CountryType.RP; break;
                            case "RS": foreignAdd_Preparer.Country = Return2290_2021.CountryType.RS; break;
                            case "RW": foreignAdd_Preparer.Country = Return2290_2021.CountryType.RW; break;
                            case "SA": foreignAdd_Preparer.Country = Return2290_2021.CountryType.SA; break;
                            case "SB": foreignAdd_Preparer.Country = Return2290_2021.CountryType.SB; break;
                            case "SC": foreignAdd_Preparer.Country = Return2290_2021.CountryType.SC; break;
                            case "SE": foreignAdd_Preparer.Country = Return2290_2021.CountryType.SE; break;
                            case "SF": foreignAdd_Preparer.Country = Return2290_2021.CountryType.SF; break;
                            case "SG": foreignAdd_Preparer.Country = Return2290_2021.CountryType.SG; break;
                            case "SH": foreignAdd_Preparer.Country = Return2290_2021.CountryType.SH; break;
                            case "SI": foreignAdd_Preparer.Country = Return2290_2021.CountryType.SI; break;
                            case "SL": foreignAdd_Preparer.Country = Return2290_2021.CountryType.SL; break;
                            case "SM": foreignAdd_Preparer.Country = Return2290_2021.CountryType.SM; break;
                            case "SN": foreignAdd_Preparer.Country = Return2290_2021.CountryType.SN; break;
                            case "SO": foreignAdd_Preparer.Country = Return2290_2021.CountryType.SO; break;
                            case "SP": foreignAdd_Preparer.Country = Return2290_2021.CountryType.SP; break;
                            case "ST": foreignAdd_Preparer.Country = Return2290_2021.CountryType.ST; break;
                            case "SU": foreignAdd_Preparer.Country = Return2290_2021.CountryType.SU; break;
                            case "SV": foreignAdd_Preparer.Country = Return2290_2021.CountryType.SV; break;
                            case "SW": foreignAdd_Preparer.Country = Return2290_2021.CountryType.SW; break;
                            case "SX": foreignAdd_Preparer.Country = Return2290_2021.CountryType.SX; break;
                            case "SY": foreignAdd_Preparer.Country = Return2290_2021.CountryType.SY; break;
                            case "SZ": foreignAdd_Preparer.Country = Return2290_2021.CountryType.SZ; break;
                            case "TD": foreignAdd_Preparer.Country = Return2290_2021.CountryType.TD; break;
                            case "TH": foreignAdd_Preparer.Country = Return2290_2021.CountryType.TH; break;
                            case "TI": foreignAdd_Preparer.Country = Return2290_2021.CountryType.TI; break;
                            case "TK": foreignAdd_Preparer.Country = Return2290_2021.CountryType.TK; break;
                            case "TL": foreignAdd_Preparer.Country = Return2290_2021.CountryType.TL; break;
                            case "TN": foreignAdd_Preparer.Country = Return2290_2021.CountryType.TN; break;
                            case "TO": foreignAdd_Preparer.Country = Return2290_2021.CountryType.TO; break;
                            case "TP": foreignAdd_Preparer.Country = Return2290_2021.CountryType.TP; break;
                            case "TS": foreignAdd_Preparer.Country = Return2290_2021.CountryType.TS; break;
                            case "TT": foreignAdd_Preparer.Country = Return2290_2021.CountryType.TT; break;
                            case "TU": foreignAdd_Preparer.Country = Return2290_2021.CountryType.TU; break;
                            case "TV": foreignAdd_Preparer.Country = Return2290_2021.CountryType.TV; break;
                            case "TW": foreignAdd_Preparer.Country = Return2290_2021.CountryType.TW; break;
                            case "TX": foreignAdd_Preparer.Country = Return2290_2021.CountryType.TX; break;
                            case "TZ": foreignAdd_Preparer.Country = Return2290_2021.CountryType.TZ; break;
                            case "UG": foreignAdd_Preparer.Country = Return2290_2021.CountryType.UG; break;
                            case "UK": foreignAdd_Preparer.Country = Return2290_2021.CountryType.UK; break;
                            case "UP": foreignAdd_Preparer.Country = Return2290_2021.CountryType.UP; break;
                            case "UV": foreignAdd_Preparer.Country = Return2290_2021.CountryType.UV; break;
                            case "UY": foreignAdd_Preparer.Country = Return2290_2021.CountryType.UY; break;
                            case "UZ": foreignAdd_Preparer.Country = Return2290_2021.CountryType.UZ; break;
                            case "VC": foreignAdd_Preparer.Country = Return2290_2021.CountryType.VC; break;
                            case "VE": foreignAdd_Preparer.Country = Return2290_2021.CountryType.VE; break;
                            case "VI": foreignAdd_Preparer.Country = Return2290_2021.CountryType.VI; break;
                            case "VM": foreignAdd_Preparer.Country = Return2290_2021.CountryType.VM; break;
                            case "VT": foreignAdd_Preparer.Country = Return2290_2021.CountryType.VT; break;
                            case "WA": foreignAdd_Preparer.Country = Return2290_2021.CountryType.WA; break;
                            case "WF": foreignAdd_Preparer.Country = Return2290_2021.CountryType.WF; break;
                            case "WI": foreignAdd_Preparer.Country = Return2290_2021.CountryType.WI; break;
                            case "WQ": foreignAdd_Preparer.Country = Return2290_2021.CountryType.WQ; break;
                            case "WS": foreignAdd_Preparer.Country = Return2290_2021.CountryType.WS; break;
                            case "WZ": foreignAdd_Preparer.Country = Return2290_2021.CountryType.WZ; break;
                            case "YM": foreignAdd_Preparer.Country = Return2290_2021.CountryType.YM; break;
                            case "ZA": foreignAdd_Preparer.Country = Return2290_2021.CountryType.ZA; break;
                            case "ZI": foreignAdd_Preparer.Country = Return2290_2021.CountryType.ZI; break;
                                #endregion
                        }

                        if (Filer_ZIPCode.Length > 0)
                            foreignAdd_Preparer.PostalCode = Filer_ZIPCode;

                        objFiler.Item = foreignAdd_Preparer;
                        #endregion
                    }

                    returnHeaderType.PreparerFirmGrp = objPreparerFirm;
                }
                #endregion

                #region CONSENT_TO_VIN_DATA_DISCLOSURE

                object[] itemConsent = new object[4];
                Return2290_2021.ItemsChoiceType1[] itemElements = new Return2290_2021.ItemsChoiceType1[4];

                itemConsent[0] = Return2290_2021.CheckboxType.X;

                if (IsConsentToDiscloseYes)
                {
                    itemElements[0] = Return2290_2021.ItemsChoiceType1.ConsentToDiscloseYesInd;

                    Return2290_2021.ReturnHeaderTypeConsentToVINDataDisclosureGrpDisclosureFormSignatureInfo objDisclosureFormSignInfo =
                        new Return2290_2021.ReturnHeaderTypeConsentToVINDataDisclosureGrpDisclosureFormSignatureInfo();
                    objDisclosureFormSignInfo.EIN = Filer_EIN;
                    Return2290_2021.BusinessNameType disclosureFormSignInfo_business = new Return2290_2021.BusinessNameType();
                    disclosureFormSignInfo_business.BusinessNameLine1Txt = Filer_Name;
                    objDisclosureFormSignInfo.BusinessName = disclosureFormSignInfo_business;
                    objDisclosureFormSignInfo.SignatureDt = DateTime.Now.ToUniversalTime();

                    itemConsent[1] = (objDisclosureFormSignInfo);
                    itemElements[1] = (Return2290_2021.ItemsChoiceType1.DisclosureFormSignatureInfo);

                    itemConsent[2] = (Return2290_2021.ReturnHeaderTypeConsentToVINDataDisclosureGrpSignatureOptionCd.PINNumber);
                    itemElements[2] = (Return2290_2021.ItemsChoiceType1.SignatureOptionCd);

                    itemConsent[3] = (Officer_PIN);
                    itemElements[3] = (Return2290_2021.ItemsChoiceType1.PIN);
                }
                else
                {
                    itemElements[0] = (Return2290_2021.ItemsChoiceType1.ConsentToDiscloseNoInd);
                }

                Return2290_2021.ReturnHeaderTypeConsentToVINDataDisclosureGrp objConsent2DataDisclosure =
                    new Return2290_2021.ReturnHeaderTypeConsentToVINDataDisclosureGrp();
                objConsent2DataDisclosure.Items = itemConsent;
                objConsent2DataDisclosure.ItemsElementName = itemElements;

                returnHeaderType.ConsentToVINDataDisclosureGrp = objConsent2DataDisclosure;

                #endregion

                returnHeaderType.TaxYr = TaxYear;

                returnHeaderType.binaryAttachmentCnt = "0";

                returnHeaderType.SoftwareId = Variables.SoftwareID;
                returnHeaderType.SoftwareVersionNum = Variables.SoftwareVersion;
                #endregion

                #region RETURN DATA

                Return2290_2021.ReturnData returnData = new Return2290_2021.ReturnData();

                #region IRS2290

                Return2290_2021.IRS2290 objIRS2290 = new Return2290_2021.IRS2290();

                #region TAX COMPUTATION
                // 2021 changes 
                if (dsTaxComputation.Tables.Count > 0)
                {
                    if (dsTaxComputation.Tables[0].Rows.Count > 0)
                    {
                        List<Return2290_2021.IRS2290TypeHighwayMtrVehTxComputationGrp> objTaxComputationList = new
                            List<Return2290_2021.IRS2290TypeHighwayMtrVehTxComputationGrp>();

                        foreach (DataRow ldr in dsTaxComputation.Tables[0].Rows)
                        {
                            Return2290_2021.IRS2290TypeHighwayMtrVehTxComputationGrp objTaxComputation = new
                                Return2290_2021.IRS2290TypeHighwayMtrVehTxComputationGrp();
                            objTaxComputation.VehicleCategoryCd = Convert.ToString(ldr["Category"]);

                            Return2290_2021.HighwayMtrVehTxCmptColumnsGrpType objVeh = new Return2290_2021.HighwayMtrVehTxCmptColumnsGrpType();

                            objVeh.TaxAmt = Convert.ToDecimal(ldr["Tax_Amount"]);
                            objVeh.TaxAmtSpecified = true;



                            objVeh.LoggingVehPartialTaxAmtSpecified = false;
                            objVeh.NonLoggingVehPartialTaxAmtSpecified = false;

                            if (Convert.ToString(ldr["Logging_Vehicle_Count"]) != "0")
                            {
                                if (Convert.ToString(ldr["Logging_Vehicle_Count"]) != "")
                                {
                                    numLoggingVehicleCnt = numLoggingVehicleCnt + Convert.ToInt16(ldr["Logging_Vehicle_Count"]);
                                    objVeh.LoggingVehicleCnt = Convert.ToString(ldr["Logging_Vehicle_Count"]);
                                }
                                if (Convert.ToString(ldr["Logging_Vehicle_Count"]) != "")
                                    numTotalTaxableVeh += Convert.ToInt16(ldr["Logging_Vehicle_Count"]);
                            }
                            if (Convert.ToString(ldr["Non_Logging_Vehicle_Count"]) != "0")
                            {
                                if (Convert.ToString(ldr["Non_Logging_Vehicle_Count"]) != "")
                                {
                                    numNonLoggingVehicleCnt = numNonLoggingVehicleCnt + Convert.ToInt16(ldr["Non_Logging_Vehicle_Count"]);
                                    objVeh.NonLoggingVehicleCnt = Convert.ToString(ldr["Non_Logging_Vehicle_Count"]);
                                }
                                if (Convert.ToString(ldr["Non_Logging_Vehicle_Count"]) != "")
                                    numTotalTaxableVeh += Convert.ToInt16(ldr["Non_Logging_Vehicle_Count"]);
                            }
                            objTaxComputation.HighwayMtrVehTxCmptColumnsGrp = objVeh;
                            objTaxComputationList.Add(objTaxComputation);
                        }
                        objIRS2290.HighwayMtrVehTxComputationGrp = objTaxComputationList.ToArray();

                        objIRS2290.TotalTaxAmt = Convert.ToDecimal(TaxFromTaxComputation.ToString("N2"));
                        objIRS2290.TotalTaxAmtSpecified = true;

                        objIRS2290.TotalVehicleCnt = numTotalTaxableVeh.ToString();

                    }
                }

                #endregion

                #region IS APPLICATION FOR....

                if (IsAddressChange)
                    objIRS2290.AddressChangeInd = Return2290_2021.CheckboxType.X;

                if (IsAmendedReturn)
                {
                    objIRS2290.AmendedReturnInd = Return2290_2021.CheckboxType.X;
                    objIRS2290.AmendedReturnIndSpecified = true;
                    objIRS2290.AmendedMonthNum = AmendedMonth; //format is MM
                }

                if (IsVINCorrection)
                {
                    Return2290_2021.IRS2290TypeVINCorrInd objVINCor = new Return2290_2021.IRS2290TypeVINCorrInd();
                    objVINCor.Value = "X";
                    List<string> docId_VINCorrDetails = new List<string>();
                    docId_VINCorrDetails.Add("VINCorrectionExplan");
                    objVINCor.referenceDocumentId = docId_VINCorrDetails.ToArray();
                    objVINCor.referenceDocumentName = "VINCorrectionExplanationStatement";

                    objIRS2290.VINCorrectionInd = objVINCor;


                    //Return2290_2021.VINCorrectionExplanationStmtType VinCorExp = new Return2290_2021.VINCorrectionExplanationStmtType();

                    //VinCorExp.ExplanationTxt = "VIN Correction update request";
                    //objIRS2290.VINCorrectionExplanationStmt = VinCorExp;

                }
                //2021 VIN Correction
                if (IsFinalReturn && IsVINCorrection == false)
                {
                    objIRS2290.FinalReturnInd = Return2290_2021.CheckboxType.X;
                    objIRS2290.FinalReturnIndSpecified = true;
                }

                #endregion

                #region ADDITIONAL TAX
                if (AdditionalTaxAmount > 0)
                {
                    Return2290_2021.IRS2290TypeAdditionalTaxAmt objAdditionalTax = new Return2290_2021.IRS2290TypeAdditionalTaxAmt();
                    objAdditionalTax.Value = Convert.ToDecimal(AdditionalTaxAmount.ToString("N2"));
                    List<string> docId_TGWIncreaseWorksheet = new List<string>();
                    docId_TGWIncreaseWorksheet.Add("TGWIncreaseWorksheet");
                    objAdditionalTax.referenceDocumentId = docId_TGWIncreaseWorksheet.ToArray();
                    objAdditionalTax.referenceDocumentName = "TGWIncreaseWorksheet";

                    objIRS2290.AdditionalTaxAmt = objAdditionalTax;
                }
                #endregion

                objIRS2290.TotalTaxAmt = Convert.ToDecimal(TaxFromTaxComputation.ToString("N2")) + Convert.ToDecimal(AdditionalTaxAmount.ToString("N2")); ;
                objIRS2290.TotalTaxAmtSpecified = true;

                #region CREDIT AMOUNT
                if (CreditAmount > 0)
                {
                    Return2290_2021.IRS2290TypeTaxCreditsAmt objTaxCreditsAmt = new Return2290_2021.IRS2290TypeTaxCreditsAmt();
                    List<string> docId_CreditsAmountStatement = new List<string>();
                    docId_CreditsAmountStatement.Add("CreditsAmountStatement");
                    objTaxCreditsAmt.referenceDocumentId = docId_CreditsAmountStatement.ToArray();
                    objTaxCreditsAmt.referenceDocumentName = "CreditsAmountStatement";
                    objTaxCreditsAmt.Value = Convert.ToDecimal(CreditAmount.ToString("N2"));

                    objIRS2290.TaxCreditsAmt = objTaxCreditsAmt;
                }
                #endregion

                objIRS2290.BalanceDueAmt = (Convert.ToDecimal(TaxFromTaxComputation.ToString("N2")) + Convert.ToDecimal(AdditionalTaxAmount.ToString("N2"))) - Convert.ToDecimal(CreditAmount.ToString("N2"));

                if (objIRS2290.BalanceDueAmt < 0)
                    objIRS2290.BalanceDueAmt = 0.00M;

                #region IRS PAYMENT2

                Decimal BalanceDue = objIRS2290.BalanceDueAmt;

                if (BalanceDue > 0)
                {
                    if (paymentType != null)
                    {
                        /* Now Both Money Order or EFTPS will be same */
                        if (paymentType == "CMO" || paymentType == "EFTPS")
                        {
                            objIRS2290.EFTPSPaymentInd = Return2290_2021.CheckboxType.X;
                            objIRS2290.EFTPSPaymentIndSpecified = true;
                        }
                        /* Credit / Debit card payments */
                        else if (paymentType == "CDC")
                        {
                            objIRS2290.CreditDebitCardPaymentInd = Return2290_2021.CheckboxType.X;
                            objIRS2290.CreditDebitCardPaymentIndSpecified = true;

                        }
                        else if (paymentType == "DirectDebit" || paymentType == "EFW")
                        {
                            List<Return2290_2021.IRSPayment2> irsPmt2Collection = new List<Return2290_2021.IRSPayment2>();

                            Return2290_2021.IRSPayment2 objPayType = new Return2290_2021.IRSPayment2();

                            objPayType.PaymentAmt = Convert.ToDecimal(BalanceDue.ToString("N2"));
                            objPayType.RoutingTransitNum = Payment_RoutingTransitNo;
                            objPayType.BankAccountNum = Payment_Acc_No;

                            if (Payment_Acc_Type == "Checking" || Payment_Acc_Type == "0")
                                objPayType.BankAccountTypeCd = Return2290_2021.BankAccountType.Item1;
                            else if (Payment_Acc_Type == "Saving" || Payment_Acc_Type == "1")
                                objPayType.BankAccountTypeCd = Return2290_2021.BankAccountType.Item2;

                            objPayType.RequestedPaymentDt = Convert.ToDateTime(Payment_ReqPayDate);

                            objPayType.TaxpayerDaytimePhoneNum = Payment_Txpyer_Ph;

                            objPayType.documentId = "IRSPayment2";
                            //objPayType.documentName = "IRSPayment2";
                            objPayType.softwareId = Variables.SoftwareID;
                            objPayType.softwareVersion = Variables.SoftwareVersion;

                            irsPmt2Collection.Add(objPayType);

                            returnData.IRSPayment2 = irsPmt2Collection.ToArray();
                            documentCount += 1;
                        }
                    }
                }
                #endregion

                #region SUSPENDED VIN

                if (dsSuspendedVehicles.Tables[0].Rows.Count > 0)
                {
                    List<Return2290_2021.SuspendedVINInfoTypeVINDetail> objSusVehList = new List<Return2290_2021.SuspendedVINInfoTypeVINDetail>();

                    foreach (DataRow ldr_suspended in dsSuspendedVehicles.Tables[0].Rows)
                    {
                        Return2290_2021.SuspendedVINInfoTypeVINDetail objSusVeh = new Return2290_2021.SuspendedVINInfoTypeVINDetail();
                        objSusVeh.VIN = Convert.ToString(ldr_suspended["VIN"]);
                        objSusVehList.Add(objSusVeh);

                        #region ....
                        if (!Convert.ToBoolean(ldr_suspended["IsAgricultural"]))
                        {
                            objIRS2290.AgricMileageUsed7500OrLessInd = Return2290_2021.CheckboxType.X;
                            objIRS2290.AgricMileageUsed7500OrLessIndSpecified = true;
                        }
                        else if (Convert.ToBoolean(ldr_suspended["IsAgricultural"]))
                        {
                            objIRS2290.MileageUsed5000OrLessInd = Return2290_2021.CheckboxType.X;
                            objIRS2290.MileageUsed5000OrLessIndSpecified = true;
                        }

                        if (Convert.ToString(ldr_suspended["IsLogging"]) != "")
                        {
                            if (Convert.ToBoolean(ldr_suspended["IsLogging"]))
                                numTaxSuspended_LoggingVeh += 1;
                            else if (!Convert.ToBoolean(ldr_suspended["IsLogging"]))
                                numTaxSuspended_NonLoggingVeh += 1;
                        }
                        else
                        {
                            numTaxSuspended_NonLoggingVeh += 1;
                        }

                        #endregion
                    }

                    Return2290_2021.SuspendedVINStatementType objSusVehStmtType = new Return2290_2021.SuspendedVINStatementType();
                    objSusVehStmtType.SuspendedVINInfo = objSusVehList.ToArray();

                    if (numTaxSuspended_LoggingVeh != 0)
                        objIRS2290.TaxSuspendedLoggingVehCnt = numTaxSuspended_LoggingVeh.ToString();

                    if (numTaxSuspended_NonLoggingVeh != 0 && numTaxSuspended_LoggingVeh != 0)
                        objIRS2290.TaxSuspendedLoggingVehCnt = (numTaxSuspended_LoggingVeh + numTaxSuspended_NonLoggingVeh).ToString();
                    else
                    {
                        if (numTaxSuspended_NonLoggingVeh != 0)
                            objIRS2290.TaxSuspendedNonLoggingVehCnt = numTaxSuspended_NonLoggingVeh.ToString();
                    }

                    Return2290_2021.IRS2290TypeSuspendedVINReferenceTyp objSuspendedVINRef =
                        new Return2290_2021.IRS2290TypeSuspendedVINReferenceTyp();
                    List<string> docID_SuspendedVINStmt = new List<string>();
                    docID_SuspendedVINStmt.Add("SuspendedVINStatement");
                    objSuspendedVINRef.referenceDocumentId = docID_SuspendedVINStmt.ToArray();
                    objSuspendedVINRef.referenceDocumentName = "SuspendedVINStatement";


                    objIRS2290.SuspendedVINReferenceTyp = objSuspendedVINRef;
                }

                #endregion

                if (IsNotSubjectToTaxChecked)
                    objIRS2290.NotSubjectToTaxInd = Return2290_2021.CheckboxType.X;


                if (dsTGWIncreaseWorksheet.Tables.Count > 0 && IsAmendedReturn)
                {
                    /*
                    if (Convert.ToDecimal(AdditionalTaxAmount.ToString("N2")) > 0)
                    {
                        objIRS2290.TotalTaxComputationAmt = Convert.ToDecimal(AdditionalTaxAmount.ToString("N2"));
                        objIRS2290.TotalTaxComputationAmtSpecified = true;
                    }*/
                    if (Convert.ToDecimal(AdditionalTaxAmount.ToString("N2")) > 0)
                    {
                        objIRS2290.TotalTaxComputationAmt = 0.01m;//Convert.ToDecimal(TaxFromTaxComputation.ToString("N2"));
                        objIRS2290.TotalTaxComputationAmtSpecified = true;
                    }
                    else
                    {
                        objIRS2290.TotalTaxComputationAmtSpecified = false;
                    }
                }
                else
                {
                    if (Convert.ToDecimal(TaxFromTaxComputation.ToString("N2")) > 0)
                    {
                        objIRS2290.TotalTaxComputationAmt = Convert.ToDecimal(TaxFromTaxComputation.ToString("N2"));
                        objIRS2290.TotalTaxComputationAmtSpecified = true;
                    }
                    else
                    {
                        objIRS2290.TotalTaxComputationAmtSpecified = false;
                    }
                }
                objIRS2290.softwareId = Variables.SoftwareID;
                objIRS2290.softwareVersion = Variables.SoftwareVersion;
                objIRS2290.documentId = "IRS2290";
                objIRS2290.documentName = "IRS2290";

                returnData.IRS2290 = objIRS2290;
                documentCount += 1;

                #endregion

                #region IRS 2290 SCHEDULE1

                Return2290_2021.IRS2290Schedule1 irs2290Schedule1 = new Return2290_2021.IRS2290Schedule1();

                if (Convert.ToInt16(numTotalTaxableVeh) > 0)
                    irs2290Schedule1.TaxableVehicleCnt = numTotalTaxableVeh.ToString();

                int Count_taxSuspendedLogVeh = Convert.ToInt16(numTaxSuspended_LoggingVeh);
                int Count_taxSuspendedNonLogVeh = Convert.ToInt16(numTaxSuspended_NonLoggingVeh);
                if (Count_taxSuspendedLogVeh + Count_taxSuspendedNonLogVeh != 0)
                    irs2290Schedule1.TotalSuspendedVehicleCnt = Convert.ToString(Count_taxSuspendedLogVeh + Count_taxSuspendedNonLogVeh);


                if ((numTotalTaxableVeh + Count_taxSuspendedLogVeh + Count_taxSuspendedNonLogVeh) > 0)
                    irs2290Schedule1.VehicleCnt = Convert.ToString(numTotalTaxableVeh + Count_taxSuspendedLogVeh + Count_taxSuspendedNonLogVeh);
                else
                {
                    if (IsAmendedReturn)
                    {
                        if ((numTotalTaxableVeh + Count_taxSuspendedLogVeh + Count_taxSuspendedNonLogVeh) > 0)
                        {
                            irs2290Schedule1.VehicleCnt = Convert.ToString(numTotalTaxableVeh + Count_taxSuspendedLogVeh + Count_taxSuspendedNonLogVeh);
                        }
                        else
                            irs2290Schedule1.VehicleCnt = "1";
                    }
                }

                List<Return2290_2021.IRS2290Schedule1TypeVehicleReportTaxItem> objVehRptTaxItem =
                    new List<Return2290_2021.IRS2290Schedule1TypeVehicleReportTaxItem>();

                if (dsTaxableVehicles.Tables.Count > 0)
                    foreach (DataRow ldr in dsTaxableVehicles.Tables[0].Rows)
                    {
                        Return2290_2021.IRS2290Schedule1TypeVehicleReportTaxItem vrti =
                            new Return2290_2021.IRS2290Schedule1TypeVehicleReportTaxItem();
                        vrti.VehicleCategoryCd = Convert.ToString(ldr["WeightCategory"]);
                        vrti.VIN = Convert.ToString(ldr["VIN"]);
                        objVehRptTaxItem.Add(vrti);
                    }

                if (dsTGWIncreaseWorksheet.Tables.Count > 0)
                    foreach (DataRow ldr in dsTGWIncreaseWorksheet.Tables[0].Rows)
                    {
                        Return2290_2021.IRS2290Schedule1TypeVehicleReportTaxItem vrti =
                            new Return2290_2021.IRS2290Schedule1TypeVehicleReportTaxItem();
                        vrti.VehicleCategoryCd = Convert.ToString(ldr["WeightCategoryNew"]);
                        vrti.VIN = Convert.ToString(ldr["VIN"]);
                        objVehRptTaxItem.Add(vrti);
                    }

                if (dsSuspendedVehicles.Tables.Count > 0)
                {
                    foreach (DataRow ldr in dsSuspendedVehicles.Tables[0].Rows)
                    {
                        Return2290_2021.IRS2290Schedule1TypeVehicleReportTaxItem vrti = new
                            Return2290_2021.IRS2290Schedule1TypeVehicleReportTaxItem();
                        vrti.VehicleCategoryCd = "W";
                        vrti.VIN = Convert.ToString(ldr["VIN"]);
                        objVehRptTaxItem.Add(vrti);
                    }
                }

                irs2290Schedule1.VehicleReportTaxItem = objVehRptTaxItem.ToArray();

                irs2290Schedule1.documentId = "IRS2290Schedule1";
                irs2290Schedule1.documentName = "IRS2290Schedule1";
                irs2290Schedule1.softwareId = Variables.SoftwareID;
                irs2290Schedule1.softwareVersion = Variables.SoftwareVersion;

                returnData.IRS2290Schedule1 = irs2290Schedule1;
                documentCount += 1;

                #endregion

                //2021 VIN Correction 
                #region VIN CORRECTION  Explanation Statement
                if (IsVINCorrection)
                {
                    //Return2290_2021.IRS2290Schedule1 irs2290Schedule1 = new Return2290_2021.IRS2290Schedule1();

                    Return2290_2021.VINCorrectionExplanationStmtType VinCorExp = new Return2290_2021.VINCorrectionExplanationStmtType();

                    VinCorExp.ExplanationTxt = "VINCorrection update request. Update the previosuly accepted Submission to the NEW VIN attached with the IRS2290Schedule1";
                    //objIRS2290.VINCorrectionExplanationStmt = VinCorExp;

                    VinCorExp.documentId = "VINCorrectionExplan";
                    //VinCorExp.documentName = "VINCorrectionExplanationStmtType";
                    VinCorExp.softwareId = Variables.SoftwareID;
                    VinCorExp.softwareVersion = Variables.SoftwareVersion;

                    returnData.VINCorrectionExplanationStmt = VinCorExp;
                    documentCount += 1;
                }
                #endregion   // VIN CORRECTION  Explanation Statement

                #region CREDITS AMOUNT STATEMENT

                if (dsCreditVehicles.Tables[0].Rows.Count > 0)
                {
                    int rowCount = dsCreditVehicles.Tables[0].Rows.Count;
                    List<Return2290_2021.CreditsAmountInfoTypeDisposalReportingItem> dric =
                        new List<Return2290_2021.CreditsAmountInfoTypeDisposalReportingItem>();

                    foreach (DataRow ldr in dsCreditVehicles.Tables[0].Rows)
                    {
                        Return2290_2021.CreditsAmountInfoTypeDisposalReportingItem dri =
                            new Return2290_2021.CreditsAmountInfoTypeDisposalReportingItem();
                        dri.CreditsAmountExplanationTxt = Convert.ToString(ldr["Reason"]);
                        dri.DisposalReportingAmt = Convert.ToString(ldr["TaxAmount"]);
                        dri.DisposalReportingDt = Convert.ToDateTime(ldr["EffectiveDate"]);
                        dri.DisposalReportingVIN = Convert.ToString(ldr["VIN"]);

                        dric.Add(dri);
                    }

                    Return2290_2021.CreditsAmountStatement cas = new Return2290_2021.CreditsAmountStatement();
                    cas.CreditsAmountInfo = dric.ToArray();
                    cas.documentId = "CreditsAmountStatement";
                    cas.documentName = "CreditsAmountStatement";
                    cas.softwareId = Variables.SoftwareID;
                    cas.softwareVersion = Variables.SoftwareVersion;

                    Return2290_2021.CreditsAmountStatement[] casc = new Return2290_2021.CreditsAmountStatement[1];
                    casc[0] = cas;

                    returnData.CreditsAmountStatement = casc;
                    documentCount += 1;
                }
                #endregion

                #region STATEMENT IN SUPPORT OF SUSPENSION //Sold Suspended

                if (dsSoldSuspendedVehicles.Tables[0].Rows.Count > 0)
                {
                    List<Return2290_2021.StmtInSupportOfSuspensionInfoTypeVehicleSuspensionDetail> ic =
                        new List<Return2290_2021.StmtInSupportOfSuspensionInfoTypeVehicleSuspensionDetail>();

                    foreach (DataRow ldr in dsSoldSuspendedVehicles.Tables[0].Rows)
                    {
                        Return2290_2021.StmtInSupportOfSuspensionInfoTypeVehicleSuspensionDetail i =
                            new Return2290_2021.StmtInSupportOfSuspensionInfoTypeVehicleSuspensionDetail();
                        i.Dt = Convert.ToDateTime(ldr["DateSold"]);
                        Return2290_2021.BusinessNameType bnt = new Return2290_2021.BusinessNameType();
                        bnt.BusinessNameLine1Txt = Convert.ToString(ldr["Buyer"]);
                        i.BusinessName = bnt;
                        i.VIN = Convert.ToString(ldr["VIN"]);
                        ic.Add(i);
                    }

                    Return2290_2021.StmtInSupportOfSuspension sisosit = new Return2290_2021.StmtInSupportOfSuspension();
                    sisosit.StmtInSupportOfSuspensionInfo = ic.ToArray();

                    List<Return2290_2021.StmtInSupportOfSuspension> sisostc = new List<Return2290_2021.StmtInSupportOfSuspension>();
                    sisostc.Add(sisosit);

                    sisosit.documentId = "StatementInSupportOfSuspension";
                    sisosit.documentName = "StatementInSupportOfSuspension";
                    sisosit.softwareId = Variables.SoftwareID;
                    sisosit.softwareVersion = Variables.SoftwareVersion;

                    returnData.StmtInSupportOfSuspension = sisostc.ToArray();
                    documentCount += 1;
                }
                #endregion

                #region SUSPENDED VIN STATEMENT

                if (dsPriorYearMileageExceededVehicles.Tables[0].Rows.Count > 0)
                {
                    List<Return2290_2021.SuspendedVINInfoTypeVINDetail> objSVITVDList =
                        new List<Return2290_2021.SuspendedVINInfoTypeVINDetail>();
                    foreach (DataRow ldr in dsPriorYearMileageExceededVehicles.Tables[0].Rows)
                    {
                        Return2290_2021.SuspendedVINInfoTypeVINDetail objSVITVD = new Return2290_2021.SuspendedVINInfoTypeVINDetail();
                        objSVITVD.VIN = Convert.ToString(ldr["VIN"]);
                        objSVITVDList.Add(objSVITVD);
                    }

                    Return2290_2021.SuspendedVINStatement objSVS = new Return2290_2021.SuspendedVINStatement();
                    objSVS.SuspendedVINInfo = objSVITVDList.ToArray();
                    objSVS.documentId = "SuspendedVINStatement";
                    objSVS.documentName = "SuspendedVINStatement";
                    objSVS.softwareId = Variables.SoftwareID;
                    objSVS.softwareVersion = Variables.SoftwareVersion;

                    Return2290_2021.SuspendedVINStatement[] suspendedVINStmt = new Return2290_2021.SuspendedVINStatement[1];
                    suspendedVINStmt[0] = objSVS;

                    returnData.SuspendedVINStatement = suspendedVINStmt;
                    documentCount += 1;
                }
                #endregion

                #region TGW INCREASE WORKSHEET

                if (dsTGWIncreaseWorksheet.Tables[0].Rows.Count > 0)
                {
                    List<Return2290_2021.TGWIncreaseInfoType> tgwiitc = new List<Return2290_2021.TGWIncreaseInfoType>();
                    foreach (DataRow ldr in dsTGWIncreaseWorksheet.Tables[0].Rows)
                    {
                        Return2290_2021.TGWIncreaseInfoType tgwiit = new Return2290_2021.TGWIncreaseInfoType();

                        tgwiit.AdditionalTaxAmt = Convert.ToDecimal(ldr["TaxAmount"]);
                        tgwiit.AdditionalTaxAmtSpecified = true;
                        tgwiit.TGWIncreaseMonthNum = Convert.ToString(ldr["IncreasedMonthValue"]);
                        tgwiit.NewTaxAmt = Convert.ToDecimal(ldr["NewTaxAmount"]);
                        tgwiit.NewTaxAmtSpecified = true;
                        tgwiit.PreviousTaxAmt = Convert.ToDecimal(ldr["PreviousTaxAmount"]);
                        tgwiit.PreviousTaxAmtSpecified = true;
                        tgwiit.TGWCategoryCd = Convert.ToString(ldr["WeightCategoryNew"]);
                        tgwiitc.Add(tgwiit);
                    }

                    Return2290_2021.TGWIncreaseWorksheet tgwIncreaseWorksheet = new Return2290_2021.TGWIncreaseWorksheet();
                    tgwIncreaseWorksheet.TGWIncreaseInfo = tgwiitc.ToArray();
                    tgwIncreaseWorksheet.documentId = "TGWIncreaseWorksheet";
                    //tgwIncreaseWorksheet.__documentName = "TGWIncreaseWorksheet";
                    tgwIncreaseWorksheet.softwareId = Variables.SoftwareID;
                    tgwIncreaseWorksheet.softwareVersion = Variables.SoftwareVersion;

                    List<Return2290_2021.TGWIncreaseWorksheet> tgwiwc = new List<Return2290_2021.TGWIncreaseWorksheet>();
                    tgwiwc.Add(tgwIncreaseWorksheet);

                    returnData.TGWIncreaseWorksheet = tgwiwc.ToArray();
                    documentCount += 1;
                }
                #endregion

                returnData.__documentCount = documentCount.ToString();

                #endregion

                #region RETURN

                Return2290_2021.Return return2290 = new Return2290_2021.Return();
                return2290.ReturnData = returnData;
                return2290.ReturnHeader = returnHeaderType;
                return2290.returnVersion = Variables.SoftwareVersion;

                #endregion

                #region Create XML file

                IFormatter ft = new BinaryFormatter();

                ft.Serialize(mStream, return2290);

                mStream.Position = 0;

                Return2290_2021.Return irsForm2 = (Return2290_2021.Return)ft.Deserialize(mStream);

                s.Serialize(fStream2, irsForm2);

                if (mStream != null) mStream.Close();

                if (fStream2 != null) fStream2.Close();

                if (fStream != null) fStream.Close();

                try
                {
                    XmlDocument xdoc = new XmlDocument();
                    xdoc.PreserveWhitespace = false;
                    xdoc.Load("C:\\xml\\submission.xml");
                    xdoc.DocumentElement.SetAttribute("xmlns:efile", "http://www.irs.gov/efile");
                    XmlTextWriter xmlWriter = new XmlTextWriter("C:\\" + @"\xml\submission.xml", new UTF8Encoding(false));
                    xdoc.Save(xmlWriter);
                    xmlWriter.Flush();
                    xmlWriter.Close();
                }
                catch { }

                #endregion

            }
            catch (Exception ex)
            {
                if (fStream != null) fStream.Close();
                if (fStream2 != null) fStream2.Close();
                if (mStream != null) mStream.Close();
                throw ex;
            }
        }
        #endregion

        #region createReturnXml_2290_2020

        /* Vishwa New 2020 Changes, for the XML building for 2020 */
        public void createReturnXml_2290_2020
        (
        #region Parameters
                bool IsAddressChange, bool IsAmendedReturn, bool IsVINCorrection, bool IsFinalReturn,
                bool IsNotSubjectToTaxChecked, bool IsConsentToDiscloseYes, string AmendedMonth, string TaxYear,
                string First_Used_Date, string Filer_AddressLine1, string Filer_City, string Filer_EIN,
                string Filer_Foreign_Country, string Filer_Name, string Filer_NameControl, string Filer_State,
                string Filer_ZIPCode, string Officer_Name, string Officer_Phone, string Officer_PIN,
                string Officer_Title, string Officer_email, string ThirdPartyDesigne_Name, string ThirdPartyDesigne_Phone,
                string ThirdPartyDesigne_PIN, string Preparer_Firm, string Preparer_EIN, string Preparer_City,
                string Preparer_State, string Preparer_Zip, string Preparer_Country, string Preparer_Address,
                string Preparer_ForeignPhone, string Preparer_Phone, string Preparer_Email, string Preparer_Name,
                string Preparer_PTIN_SSN, bool IsPreparerHasPTIN, bool IsPreparerSelfEmployed, string paymentType,
                string Payment_Acc_No, string Payment_Acc_Type, string Payment_ReqPayDate, string Payment_RoutingTransitNo,
                string Payment_Txpyer_Ph, DataSet dsTaxableVehicles, DataSet dsCreditVehicles, DataSet dsSuspendedVehicles,
                DataSet dsSoldSuspendedVehicles, DataSet dsPriorYearMileageExceededVehicles, DataSet dsTGWIncreaseWorksheet, DataSet dsTaxComputation,
                string soldSuspendedVehicles, decimal TaxFromTaxComputation, decimal AdditionalTaxAmount, decimal CreditAmount,
                string TotalVehicles
        #endregion
        )
        {
            int documentCount = 0;
            FileStream fStream = null;
            FileStream fStream2 = null;
            MemoryStream mStream = null;

            int numTotalTaxableVeh = 0;
            int numTaxSuspended_LoggingVeh = 0;
            int numTaxSuspended_NonLoggingVeh = 0;

            int numNonLoggingVehicleCnt = 0;
            int numLoggingVehicleCnt = 0;

            try
            {
                if (!Directory.Exists(path2CreateZipFile + @"\xml"))
                    Directory.CreateDirectory(path2CreateZipFile + @"\xml");

                fStream2 = new FileStream(path2CreateZipFile + @"\xml\submission.xml", FileMode.Create);

                byte[] byteArray = new byte[90000];
                mStream = new MemoryStream(byteArray);
                XmlSerializer s = new XmlSerializer(typeof(Return2290_2020.Return));

                #region RETRUN_HEADER

                Return2290_2020.ReturnHeaderType returnHeaderType = new Return2290_2020.ReturnHeaderType();

                returnHeaderType.ReturnTs = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz");

                returnHeaderType.FirstUsedDt = First_Used_Date;

                returnHeaderType.MultSoftwarePackagesUsedInd = false;
                returnHeaderType.PINEnteredByCd = Return2290_2020.ReturnHeaderTypePINEnteredByCd.Taxpayer;
                returnHeaderType.PINEnteredByCdSpecified = true;
                returnHeaderType.ReturnTypeCd = Return2290_2020.ReturnHeaderTypeReturnTypeCd.Item2290;
                returnHeaderType.SignatureOptionCd = Return2290_2020.ReturnHeaderTypeSignatureOptionCd.PINNumber;
                returnHeaderType.SignatureOptionCdSpecified = true;

                #region ORIGINATOR

                #region PRACTIONER PIN

                Return2290_2020.ReturnHeaderTypeOriginatorGrpPractitionerPINGrp objPractitionerPIN =
                    new Return2290_2020.ReturnHeaderTypeOriginatorGrpPractitionerPINGrp();
                objPractitionerPIN.EFIN = Variables.EFIN;
                objPractitionerPIN.PIN = Variables.PIN;

                #endregion

                Return2290_2020.ReturnHeaderTypeOriginatorGrp originator = new Return2290_2020.ReturnHeaderTypeOriginatorGrp();
                originator.EFIN = Variables.EFIN;
                originator.OriginatorTypeCd = Return2290_2020.OriginatorType.ERO;
                originator.PractitionerPINGrp = objPractitionerPIN;
                returnHeaderType.OriginatorGrp = originator;

                #endregion

                #region FILER
                Return2290_2020.ReturnHeaderTypeFiler objFiler = new Return2290_2020.ReturnHeaderTypeFiler();

                objFiler.EIN = Filer_EIN;
                objFiler.BusinessNameLine1Txt = Filer_Name; //max_length=60
                objFiler.BusinessNameControlTxt = Filer_NameControl;

                if (Filer_Foreign_Country == "" || Filer_Foreign_Country == "US")
                {
                    #region FILER US ADDRESS

                    Return2290_2020.USAddressType usAdd_Filer = new Return2290_2020.USAddressType();

                    if (Filer_AddressLine1.Length > 35)
                    {
                        string Filer_AddressLine2 = string.Empty;
                        usAdd_Filer.AddressLine1 = Filer_AddressLine1.Substring(0, 35).Trim();
                        Filer_AddressLine2 = Filer_AddressLine1.Substring(35);
                        usAdd_Filer.AddressLine2 = Filer_AddressLine2.Trim();

                    }
                    else
                    {
                        usAdd_Filer.AddressLine1 = Filer_AddressLine1.Trim();
                    }
                    usAdd_Filer.City = Filer_City.Trim();

                    switch (Filer_State)
                    {
                        #region Filer_State
                        case "AA": usAdd_Filer.State = Return2290_2020.StateType.AA; break;
                        case "AE": usAdd_Filer.State = Return2290_2020.StateType.AE; break;
                        case "AK": usAdd_Filer.State = Return2290_2020.StateType.AK; break;
                        case "AL": usAdd_Filer.State = Return2290_2020.StateType.AL; break;
                        case "AP": usAdd_Filer.State = Return2290_2020.StateType.AP; break;
                        case "AR": usAdd_Filer.State = Return2290_2020.StateType.AR; break;
                        case "AS": usAdd_Filer.State = Return2290_2020.StateType.AS; break;
                        case "AZ": usAdd_Filer.State = Return2290_2020.StateType.AZ; break;
                        case "CA": usAdd_Filer.State = Return2290_2020.StateType.CA; break;
                        case "CO": usAdd_Filer.State = Return2290_2020.StateType.CO; break;
                        case "CT": usAdd_Filer.State = Return2290_2020.StateType.CT; break;
                        case "DC": usAdd_Filer.State = Return2290_2020.StateType.DC; break;
                        case "DE": usAdd_Filer.State = Return2290_2020.StateType.DE; break;
                        case "FL": usAdd_Filer.State = Return2290_2020.StateType.FL; break;
                        case "FM": usAdd_Filer.State = Return2290_2020.StateType.FM; break;
                        case "GA": usAdd_Filer.State = Return2290_2020.StateType.GA; break;
                        case "GU": usAdd_Filer.State = Return2290_2020.StateType.GU; break;
                        case "HI": usAdd_Filer.State = Return2290_2020.StateType.HI; break;
                        case "IA": usAdd_Filer.State = Return2290_2020.StateType.IA; break;
                        case "ID": usAdd_Filer.State = Return2290_2020.StateType.ID; break;
                        case "IL": usAdd_Filer.State = Return2290_2020.StateType.IL; break;
                        case "IN": usAdd_Filer.State = Return2290_2020.StateType.IN; break;
                        case "KS": usAdd_Filer.State = Return2290_2020.StateType.KS; break;
                        case "KY": usAdd_Filer.State = Return2290_2020.StateType.KY; break;
                        case "LA": usAdd_Filer.State = Return2290_2020.StateType.LA; break;
                        case "MA": usAdd_Filer.State = Return2290_2020.StateType.MA; break;
                        case "MD": usAdd_Filer.State = Return2290_2020.StateType.MD; break;
                        case "ME": usAdd_Filer.State = Return2290_2020.StateType.ME; break;
                        case "MH": usAdd_Filer.State = Return2290_2020.StateType.MH; break;
                        case "MI": usAdd_Filer.State = Return2290_2020.StateType.MI; break;
                        case "MN": usAdd_Filer.State = Return2290_2020.StateType.MN; break;
                        case "MO": usAdd_Filer.State = Return2290_2020.StateType.MO; break;
                        case "MP": usAdd_Filer.State = Return2290_2020.StateType.MP; break;
                        case "MS": usAdd_Filer.State = Return2290_2020.StateType.MS; break;
                        case "MT": usAdd_Filer.State = Return2290_2020.StateType.MT; break;
                        case "NC": usAdd_Filer.State = Return2290_2020.StateType.NC; break;
                        case "ND": usAdd_Filer.State = Return2290_2020.StateType.ND; break;
                        case "NE": usAdd_Filer.State = Return2290_2020.StateType.NE; break;
                        case "NH": usAdd_Filer.State = Return2290_2020.StateType.NH; break;
                        case "NJ": usAdd_Filer.State = Return2290_2020.StateType.NJ; break;
                        case "NM": usAdd_Filer.State = Return2290_2020.StateType.NM; break;
                        case "NV": usAdd_Filer.State = Return2290_2020.StateType.NV; break;
                        case "NY": usAdd_Filer.State = Return2290_2020.StateType.NY; break;
                        case "OH": usAdd_Filer.State = Return2290_2020.StateType.OH; break;
                        case "OK": usAdd_Filer.State = Return2290_2020.StateType.OK; break;
                        case "OR": usAdd_Filer.State = Return2290_2020.StateType.OR; break;
                        case "PA": usAdd_Filer.State = Return2290_2020.StateType.PA; break;
                        case "PR": usAdd_Filer.State = Return2290_2020.StateType.PR; break;
                        case "PW": usAdd_Filer.State = Return2290_2020.StateType.PW; break;
                        case "RI": usAdd_Filer.State = Return2290_2020.StateType.RI; break;
                        case "SC": usAdd_Filer.State = Return2290_2020.StateType.SC; break;
                        case "SD": usAdd_Filer.State = Return2290_2020.StateType.SD; break;
                        case "TN": usAdd_Filer.State = Return2290_2020.StateType.TN; break;
                        case "TX": usAdd_Filer.State = Return2290_2020.StateType.TX; break;
                        case "UT": usAdd_Filer.State = Return2290_2020.StateType.UT; break;
                        case "VA": usAdd_Filer.State = Return2290_2020.StateType.VA; break;
                        case "VI": usAdd_Filer.State = Return2290_2020.StateType.VI; break;
                        case "VT": usAdd_Filer.State = Return2290_2020.StateType.VT; break;
                        case "WA": usAdd_Filer.State = Return2290_2020.StateType.WA; break;
                        case "WI": usAdd_Filer.State = Return2290_2020.StateType.WI; break;
                        case "WV": usAdd_Filer.State = Return2290_2020.StateType.WV; break;
                        case "WY": usAdd_Filer.State = Return2290_2020.StateType.WY; break;
                        #endregion
                    }

                    usAdd_Filer.ZIPCode = Filer_ZIPCode;

                    objFiler.Item = usAdd_Filer;
                    #endregion
                }
                else
                {
                    #region FILER FOREIGN ADDRESS
                    Return2290_2020.ForeignAddressType foreignAdd_Filer = new Return2290_2020.ForeignAddressType();

                    if (Filer_AddressLine1.Length > 35)
                    {
                        string Filer_AddressLine2 = string.Empty;
                        foreignAdd_Filer.AddressLine1 = Filer_AddressLine1.Substring(0, 35).Trim();
                        Filer_AddressLine2 = Filer_AddressLine1.Substring(35);
                        foreignAdd_Filer.AddressLine2 = Filer_AddressLine2.Trim();
                    }
                    else
                    {
                        foreignAdd_Filer.AddressLine1 = Filer_AddressLine1.Trim();
                    }

                    foreignAdd_Filer.City = Filer_City.Trim();

                    if (Filer_State.Length > 0)
                        foreignAdd_Filer.ProvinceOrState = Filer_State;

                    switch (Filer_Foreign_Country)
                    {
                        #region Filer_Foreign_Country
                        case "AA": foreignAdd_Filer.Country = Return2290_2020.CountryType.AA; break;
                        case "AC": foreignAdd_Filer.Country = Return2290_2020.CountryType.AC; break;
                        case "AE": foreignAdd_Filer.Country = Return2290_2020.CountryType.AE; break;
                        case "AF": foreignAdd_Filer.Country = Return2290_2020.CountryType.AF; break;
                        case "AG": foreignAdd_Filer.Country = Return2290_2020.CountryType.AG; break;
                        case "AJ": foreignAdd_Filer.Country = Return2290_2020.CountryType.AJ; break;
                        case "AL": foreignAdd_Filer.Country = Return2290_2020.CountryType.AL; break;
                        case "AM": foreignAdd_Filer.Country = Return2290_2020.CountryType.AM; break;
                        case "AN": foreignAdd_Filer.Country = Return2290_2020.CountryType.AN; break;
                        case "AO": foreignAdd_Filer.Country = Return2290_2020.CountryType.AO; break;
                        case "AR": foreignAdd_Filer.Country = Return2290_2020.CountryType.AR; break;
                        case "AS": foreignAdd_Filer.Country = Return2290_2020.CountryType.AS; break;
                        case "AT": foreignAdd_Filer.Country = Return2290_2020.CountryType.AT; break;
                        case "AU": foreignAdd_Filer.Country = Return2290_2020.CountryType.AU; break;
                        case "AV": foreignAdd_Filer.Country = Return2290_2020.CountryType.AV; break;
                        case "AX": foreignAdd_Filer.Country = Return2290_2020.CountryType.AX; break;
                        case "AY": foreignAdd_Filer.Country = Return2290_2020.CountryType.AY; break;
                        case "BA": foreignAdd_Filer.Country = Return2290_2020.CountryType.BA; break;
                        case "BB": foreignAdd_Filer.Country = Return2290_2020.CountryType.BB; break;
                        case "BC": foreignAdd_Filer.Country = Return2290_2020.CountryType.BC; break;
                        case "BD": foreignAdd_Filer.Country = Return2290_2020.CountryType.BD; break;
                        case "BE": foreignAdd_Filer.Country = Return2290_2020.CountryType.BE; break;
                        case "BF": foreignAdd_Filer.Country = Return2290_2020.CountryType.BF; break;
                        case "BG": foreignAdd_Filer.Country = Return2290_2020.CountryType.BG; break;
                        case "BH": foreignAdd_Filer.Country = Return2290_2020.CountryType.BH; break;
                        case "BK": foreignAdd_Filer.Country = Return2290_2020.CountryType.BK; break;
                        case "BL": foreignAdd_Filer.Country = Return2290_2020.CountryType.BL; break;
                        case "BM": foreignAdd_Filer.Country = Return2290_2020.CountryType.BM; break;
                        case "BN": foreignAdd_Filer.Country = Return2290_2020.CountryType.BN; break;
                        case "BO": foreignAdd_Filer.Country = Return2290_2020.CountryType.BO; break;
                        case "BP": foreignAdd_Filer.Country = Return2290_2020.CountryType.BP; break;
                        case "BQ": foreignAdd_Filer.Country = Return2290_2020.CountryType.BQ; break;
                        case "BR": foreignAdd_Filer.Country = Return2290_2020.CountryType.BR; break;
                        case "BT": foreignAdd_Filer.Country = Return2290_2020.CountryType.BT; break;
                        case "BU": foreignAdd_Filer.Country = Return2290_2020.CountryType.BU; break;
                        case "BV": foreignAdd_Filer.Country = Return2290_2020.CountryType.BV; break;
                        case "BX": foreignAdd_Filer.Country = Return2290_2020.CountryType.BX; break;
                        case "BY": foreignAdd_Filer.Country = Return2290_2020.CountryType.BY; break;
                        case "CA": foreignAdd_Filer.Country = Return2290_2020.CountryType.CA; break;
                        case "CB": foreignAdd_Filer.Country = Return2290_2020.CountryType.CB; break;
                        case "CD": foreignAdd_Filer.Country = Return2290_2020.CountryType.CD; break;
                        case "CE": foreignAdd_Filer.Country = Return2290_2020.CountryType.CE; break;
                        case "CF": foreignAdd_Filer.Country = Return2290_2020.CountryType.CF; break;
                        case "CG": foreignAdd_Filer.Country = Return2290_2020.CountryType.CG; break;
                        case "CH": foreignAdd_Filer.Country = Return2290_2020.CountryType.CH; break;
                        case "CI": foreignAdd_Filer.Country = Return2290_2020.CountryType.CI; break;
                        case "CJ": foreignAdd_Filer.Country = Return2290_2020.CountryType.CJ; break;
                        case "CK": foreignAdd_Filer.Country = Return2290_2020.CountryType.CK; break;
                        case "CM": foreignAdd_Filer.Country = Return2290_2020.CountryType.CM; break;
                        case "CN": foreignAdd_Filer.Country = Return2290_2020.CountryType.CN; break;
                        case "CO": foreignAdd_Filer.Country = Return2290_2020.CountryType.CO; break;
                        case "CR": foreignAdd_Filer.Country = Return2290_2020.CountryType.CR; break;
                        case "CS": foreignAdd_Filer.Country = Return2290_2020.CountryType.CS; break;
                        case "CT": foreignAdd_Filer.Country = Return2290_2020.CountryType.CT; break;
                        case "CU": foreignAdd_Filer.Country = Return2290_2020.CountryType.CU; break;
                        case "CV": foreignAdd_Filer.Country = Return2290_2020.CountryType.CV; break;
                        case "CW": foreignAdd_Filer.Country = Return2290_2020.CountryType.CW; break;
                        case "CY": foreignAdd_Filer.Country = Return2290_2020.CountryType.CY; break;
                        case "DA": foreignAdd_Filer.Country = Return2290_2020.CountryType.DA; break;
                        case "DJ": foreignAdd_Filer.Country = Return2290_2020.CountryType.DJ; break;
                        case "DO": foreignAdd_Filer.Country = Return2290_2020.CountryType.DO; break;
                        case "DR": foreignAdd_Filer.Country = Return2290_2020.CountryType.DR; break;
                        case "DX": foreignAdd_Filer.Country = Return2290_2020.CountryType.DX; break;
                        case "EC": foreignAdd_Filer.Country = Return2290_2020.CountryType.EC; break;
                        case "EG": foreignAdd_Filer.Country = Return2290_2020.CountryType.EG; break;
                        case "EI": foreignAdd_Filer.Country = Return2290_2020.CountryType.EI; break;
                        case "EK": foreignAdd_Filer.Country = Return2290_2020.CountryType.EK; break;
                        case "EN": foreignAdd_Filer.Country = Return2290_2020.CountryType.EN; break;
                        case "ER": foreignAdd_Filer.Country = Return2290_2020.CountryType.ER; break;
                        case "ES": foreignAdd_Filer.Country = Return2290_2020.CountryType.ES; break;
                        case "ET": foreignAdd_Filer.Country = Return2290_2020.CountryType.ET; break;
                        case "EZ": foreignAdd_Filer.Country = Return2290_2020.CountryType.EZ; break;
                        case "FI": foreignAdd_Filer.Country = Return2290_2020.CountryType.FI; break;
                        case "FJ": foreignAdd_Filer.Country = Return2290_2020.CountryType.FJ; break;
                        case "FK": foreignAdd_Filer.Country = Return2290_2020.CountryType.FK; break;
                        case "FO": foreignAdd_Filer.Country = Return2290_2020.CountryType.FO; break;
                        case "FP": foreignAdd_Filer.Country = Return2290_2020.CountryType.FP; break;
                        case "FQ": foreignAdd_Filer.Country = Return2290_2020.CountryType.FQ; break;
                        case "FR": foreignAdd_Filer.Country = Return2290_2020.CountryType.FR; break;
                        case "FS": foreignAdd_Filer.Country = Return2290_2020.CountryType.FS; break;
                        case "GA": foreignAdd_Filer.Country = Return2290_2020.CountryType.GA; break;
                        case "GB": foreignAdd_Filer.Country = Return2290_2020.CountryType.GB; break;
                        case "GG": foreignAdd_Filer.Country = Return2290_2020.CountryType.GG; break;
                        case "GH": foreignAdd_Filer.Country = Return2290_2020.CountryType.GH; break;
                        case "GI": foreignAdd_Filer.Country = Return2290_2020.CountryType.GI; break;
                        case "GJ": foreignAdd_Filer.Country = Return2290_2020.CountryType.GJ; break;
                        case "GK": foreignAdd_Filer.Country = Return2290_2020.CountryType.GK; break;
                        case "GL": foreignAdd_Filer.Country = Return2290_2020.CountryType.GL; break;
                        case "GM": foreignAdd_Filer.Country = Return2290_2020.CountryType.GM; break;
                        case "GR": foreignAdd_Filer.Country = Return2290_2020.CountryType.GR; break;
                        case "GT": foreignAdd_Filer.Country = Return2290_2020.CountryType.GT; break;
                        case "GV": foreignAdd_Filer.Country = Return2290_2020.CountryType.GV; break;
                        case "GY": foreignAdd_Filer.Country = Return2290_2020.CountryType.GY; break;
                        case "HA": foreignAdd_Filer.Country = Return2290_2020.CountryType.HA; break;
                        case "HK": foreignAdd_Filer.Country = Return2290_2020.CountryType.HK; break;
                        case "HM": foreignAdd_Filer.Country = Return2290_2020.CountryType.HM; break;
                        case "HO": foreignAdd_Filer.Country = Return2290_2020.CountryType.HO; break;
                        case "HQ": foreignAdd_Filer.Country = Return2290_2020.CountryType.HQ; break;
                        case "HR": foreignAdd_Filer.Country = Return2290_2020.CountryType.HR; break;
                        case "HU": foreignAdd_Filer.Country = Return2290_2020.CountryType.HU; break;
                        case "IC": foreignAdd_Filer.Country = Return2290_2020.CountryType.IC; break;
                        case "ID": foreignAdd_Filer.Country = Return2290_2020.CountryType.ID; break;
                        case "IM": foreignAdd_Filer.Country = Return2290_2020.CountryType.IM; break;
                        case "IO": foreignAdd_Filer.Country = Return2290_2020.CountryType.IO; break;
                        case "IP": foreignAdd_Filer.Country = Return2290_2020.CountryType.IP; break;
                        case "IR": foreignAdd_Filer.Country = Return2290_2020.CountryType.IR; break;
                        case "IS": foreignAdd_Filer.Country = Return2290_2020.CountryType.IS; break;
                        case "IT": foreignAdd_Filer.Country = Return2290_2020.CountryType.IT; break;
                        case "IV": foreignAdd_Filer.Country = Return2290_2020.CountryType.IV; break;
                        case "IZ": foreignAdd_Filer.Country = Return2290_2020.CountryType.IZ; break;
                        case "JA": foreignAdd_Filer.Country = Return2290_2020.CountryType.JA; break;
                        case "JE": foreignAdd_Filer.Country = Return2290_2020.CountryType.JE; break;
                        case "JM": foreignAdd_Filer.Country = Return2290_2020.CountryType.JM; break;
                        case "JN": foreignAdd_Filer.Country = Return2290_2020.CountryType.JN; break;
                        case "JO": foreignAdd_Filer.Country = Return2290_2020.CountryType.JO; break;
                        case "JQ": foreignAdd_Filer.Country = Return2290_2020.CountryType.JQ; break;
                        case "KE": foreignAdd_Filer.Country = Return2290_2020.CountryType.KE; break;
                        case "KG": foreignAdd_Filer.Country = Return2290_2020.CountryType.KG; break;
                        case "KN": foreignAdd_Filer.Country = Return2290_2020.CountryType.KN; break;
                        case "KQ": foreignAdd_Filer.Country = Return2290_2020.CountryType.KQ; break;
                        case "KR": foreignAdd_Filer.Country = Return2290_2020.CountryType.KR; break;
                        case "KS": foreignAdd_Filer.Country = Return2290_2020.CountryType.KS; break;
                        case "KT": foreignAdd_Filer.Country = Return2290_2020.CountryType.KT; break;
                        case "KU": foreignAdd_Filer.Country = Return2290_2020.CountryType.KU; break;
                        case "KV": foreignAdd_Filer.Country = Return2290_2020.CountryType.KV; break;
                        case "KZ": foreignAdd_Filer.Country = Return2290_2020.CountryType.KZ; break;
                        case "LA": foreignAdd_Filer.Country = Return2290_2020.CountryType.LA; break;
                        case "LE": foreignAdd_Filer.Country = Return2290_2020.CountryType.LE; break;
                        case "LG": foreignAdd_Filer.Country = Return2290_2020.CountryType.LG; break;
                        case "LH": foreignAdd_Filer.Country = Return2290_2020.CountryType.LH; break;
                        case "LI": foreignAdd_Filer.Country = Return2290_2020.CountryType.LI; break;
                        case "LO": foreignAdd_Filer.Country = Return2290_2020.CountryType.LO; break;
                        case "LQ": foreignAdd_Filer.Country = Return2290_2020.CountryType.LQ; break;
                        case "LS": foreignAdd_Filer.Country = Return2290_2020.CountryType.LS; break;
                        case "LT": foreignAdd_Filer.Country = Return2290_2020.CountryType.LT; break;
                        case "LU": foreignAdd_Filer.Country = Return2290_2020.CountryType.LU; break;
                        case "LY": foreignAdd_Filer.Country = Return2290_2020.CountryType.LY; break;
                        case "MA": foreignAdd_Filer.Country = Return2290_2020.CountryType.MA; break;
                        case "MC": foreignAdd_Filer.Country = Return2290_2020.CountryType.MC; break;
                        case "MD": foreignAdd_Filer.Country = Return2290_2020.CountryType.MD; break;
                        case "MG": foreignAdd_Filer.Country = Return2290_2020.CountryType.MG; break;
                        case "MH": foreignAdd_Filer.Country = Return2290_2020.CountryType.MH; break;
                        case "MI": foreignAdd_Filer.Country = Return2290_2020.CountryType.MI; break;
                        case "MJ": foreignAdd_Filer.Country = Return2290_2020.CountryType.MJ; break;
                        case "MK": foreignAdd_Filer.Country = Return2290_2020.CountryType.MK; break;
                        case "ML": foreignAdd_Filer.Country = Return2290_2020.CountryType.ML; break;
                        case "MN": foreignAdd_Filer.Country = Return2290_2020.CountryType.MN; break;
                        case "MO": foreignAdd_Filer.Country = Return2290_2020.CountryType.MO; break;
                        case "MP": foreignAdd_Filer.Country = Return2290_2020.CountryType.MP; break;
                        case "MQ": foreignAdd_Filer.Country = Return2290_2020.CountryType.MQ; break;
                        case "MR": foreignAdd_Filer.Country = Return2290_2020.CountryType.MR; break;
                        case "MT": foreignAdd_Filer.Country = Return2290_2020.CountryType.MT; break;
                        case "MU": foreignAdd_Filer.Country = Return2290_2020.CountryType.MU; break;
                        case "MV": foreignAdd_Filer.Country = Return2290_2020.CountryType.MV; break;
                        case "MX": foreignAdd_Filer.Country = Return2290_2020.CountryType.MX; break;
                        case "MY": foreignAdd_Filer.Country = Return2290_2020.CountryType.MY; break;
                        case "MZ": foreignAdd_Filer.Country = Return2290_2020.CountryType.MZ; break;
                        case "NC": foreignAdd_Filer.Country = Return2290_2020.CountryType.NC; break;
                        case "NE": foreignAdd_Filer.Country = Return2290_2020.CountryType.NE; break;
                        case "NF": foreignAdd_Filer.Country = Return2290_2020.CountryType.NF; break;
                        case "NG": foreignAdd_Filer.Country = Return2290_2020.CountryType.NG; break;
                        case "NH": foreignAdd_Filer.Country = Return2290_2020.CountryType.NH; break;
                        case "NI": foreignAdd_Filer.Country = Return2290_2020.CountryType.NI; break;
                        case "NL": foreignAdd_Filer.Country = Return2290_2020.CountryType.NL; break;
                        case "NN": foreignAdd_Filer.Country = Return2290_2020.CountryType.NN; break;
                        case "NO": foreignAdd_Filer.Country = Return2290_2020.CountryType.NO; break;
                        case "NP": foreignAdd_Filer.Country = Return2290_2020.CountryType.NP; break;
                        case "NR": foreignAdd_Filer.Country = Return2290_2020.CountryType.NR; break;
                        case "NS": foreignAdd_Filer.Country = Return2290_2020.CountryType.NS; break;
                        case "NU": foreignAdd_Filer.Country = Return2290_2020.CountryType.NU; break;
                        case "NZ": foreignAdd_Filer.Country = Return2290_2020.CountryType.NZ; break;
                        case "PA": foreignAdd_Filer.Country = Return2290_2020.CountryType.PA; break;
                        case "PC": foreignAdd_Filer.Country = Return2290_2020.CountryType.PC; break;
                        case "PE": foreignAdd_Filer.Country = Return2290_2020.CountryType.PE; break;
                        case "PF": foreignAdd_Filer.Country = Return2290_2020.CountryType.PF; break;
                        case "PG": foreignAdd_Filer.Country = Return2290_2020.CountryType.PG; break;
                        case "PK": foreignAdd_Filer.Country = Return2290_2020.CountryType.PK; break;
                        case "PL": foreignAdd_Filer.Country = Return2290_2020.CountryType.PL; break;
                        case "PM": foreignAdd_Filer.Country = Return2290_2020.CountryType.PM; break;
                        case "PO": foreignAdd_Filer.Country = Return2290_2020.CountryType.PO; break;
                        case "PP": foreignAdd_Filer.Country = Return2290_2020.CountryType.PP; break;
                        case "PU": foreignAdd_Filer.Country = Return2290_2020.CountryType.PU; break;
                        case "QA": foreignAdd_Filer.Country = Return2290_2020.CountryType.QA; break;
                        case "RI": foreignAdd_Filer.Country = Return2290_2020.CountryType.RI; break;
                        case "RN": foreignAdd_Filer.Country = Return2290_2020.CountryType.RN; break;
                        case "RO": foreignAdd_Filer.Country = Return2290_2020.CountryType.RO; break;
                        case "RP": foreignAdd_Filer.Country = Return2290_2020.CountryType.RP; break;
                        case "RS": foreignAdd_Filer.Country = Return2290_2020.CountryType.RS; break;
                        case "RW": foreignAdd_Filer.Country = Return2290_2020.CountryType.RW; break;
                        case "SA": foreignAdd_Filer.Country = Return2290_2020.CountryType.SA; break;
                        case "SB": foreignAdd_Filer.Country = Return2290_2020.CountryType.SB; break;
                        case "SC": foreignAdd_Filer.Country = Return2290_2020.CountryType.SC; break;
                        case "SE": foreignAdd_Filer.Country = Return2290_2020.CountryType.SE; break;
                        case "SF": foreignAdd_Filer.Country = Return2290_2020.CountryType.SF; break;
                        case "SG": foreignAdd_Filer.Country = Return2290_2020.CountryType.SG; break;
                        case "SH": foreignAdd_Filer.Country = Return2290_2020.CountryType.SH; break;
                        case "SI": foreignAdd_Filer.Country = Return2290_2020.CountryType.SI; break;
                        case "SL": foreignAdd_Filer.Country = Return2290_2020.CountryType.SL; break;
                        case "SM": foreignAdd_Filer.Country = Return2290_2020.CountryType.SM; break;
                        case "SN": foreignAdd_Filer.Country = Return2290_2020.CountryType.SN; break;
                        case "SO": foreignAdd_Filer.Country = Return2290_2020.CountryType.SO; break;
                        case "SP": foreignAdd_Filer.Country = Return2290_2020.CountryType.SP; break;
                        case "ST": foreignAdd_Filer.Country = Return2290_2020.CountryType.ST; break;
                        case "SU": foreignAdd_Filer.Country = Return2290_2020.CountryType.SU; break;
                        case "SV": foreignAdd_Filer.Country = Return2290_2020.CountryType.SV; break;
                        case "SW": foreignAdd_Filer.Country = Return2290_2020.CountryType.SW; break;
                        case "SX": foreignAdd_Filer.Country = Return2290_2020.CountryType.SX; break;
                        case "SY": foreignAdd_Filer.Country = Return2290_2020.CountryType.SY; break;
                        case "SZ": foreignAdd_Filer.Country = Return2290_2020.CountryType.SZ; break;
                        case "TB": foreignAdd_Filer.Country = Return2290_2020.CountryType.TB; break;
                        case "TD": foreignAdd_Filer.Country = Return2290_2020.CountryType.TD; break;
                        case "TH": foreignAdd_Filer.Country = Return2290_2020.CountryType.TH; break;
                        case "TI": foreignAdd_Filer.Country = Return2290_2020.CountryType.TI; break;
                        case "TK": foreignAdd_Filer.Country = Return2290_2020.CountryType.TK; break;
                        case "TL": foreignAdd_Filer.Country = Return2290_2020.CountryType.TL; break;
                        case "TN": foreignAdd_Filer.Country = Return2290_2020.CountryType.TN; break;
                        case "TO": foreignAdd_Filer.Country = Return2290_2020.CountryType.TO; break;
                        case "TP": foreignAdd_Filer.Country = Return2290_2020.CountryType.TP; break;
                        case "TS": foreignAdd_Filer.Country = Return2290_2020.CountryType.TS; break;
                        case "TT": foreignAdd_Filer.Country = Return2290_2020.CountryType.TT; break;
                        case "TU": foreignAdd_Filer.Country = Return2290_2020.CountryType.TU; break;
                        case "TV": foreignAdd_Filer.Country = Return2290_2020.CountryType.TV; break;
                        case "TW": foreignAdd_Filer.Country = Return2290_2020.CountryType.TW; break;
                        case "TX": foreignAdd_Filer.Country = Return2290_2020.CountryType.TX; break;
                        case "TZ": foreignAdd_Filer.Country = Return2290_2020.CountryType.TZ; break;
                        case "UC": foreignAdd_Filer.Country = Return2290_2020.CountryType.UC; break;
                        case "UG": foreignAdd_Filer.Country = Return2290_2020.CountryType.UG; break;
                        case "UK": foreignAdd_Filer.Country = Return2290_2020.CountryType.UK; break;
                        case "UP": foreignAdd_Filer.Country = Return2290_2020.CountryType.UP; break;
                        case "UV": foreignAdd_Filer.Country = Return2290_2020.CountryType.UV; break;
                        case "UY": foreignAdd_Filer.Country = Return2290_2020.CountryType.UY; break;
                        case "UZ": foreignAdd_Filer.Country = Return2290_2020.CountryType.UZ; break;
                        case "VC": foreignAdd_Filer.Country = Return2290_2020.CountryType.VC; break;
                        case "VE": foreignAdd_Filer.Country = Return2290_2020.CountryType.VE; break;
                        case "VI": foreignAdd_Filer.Country = Return2290_2020.CountryType.VI; break;
                        case "VM": foreignAdd_Filer.Country = Return2290_2020.CountryType.VM; break;
                        case "VT": foreignAdd_Filer.Country = Return2290_2020.CountryType.VT; break;
                        case "WA": foreignAdd_Filer.Country = Return2290_2020.CountryType.WA; break;
                        case "WF": foreignAdd_Filer.Country = Return2290_2020.CountryType.WF; break;
                        case "WI": foreignAdd_Filer.Country = Return2290_2020.CountryType.WI; break;
                        case "WQ": foreignAdd_Filer.Country = Return2290_2020.CountryType.WQ; break;
                        case "WS": foreignAdd_Filer.Country = Return2290_2020.CountryType.WS; break;
                        case "WZ": foreignAdd_Filer.Country = Return2290_2020.CountryType.WZ; break;
                        case "YM": foreignAdd_Filer.Country = Return2290_2020.CountryType.YM; break;
                        case "ZA": foreignAdd_Filer.Country = Return2290_2020.CountryType.ZA; break;
                        case "ZI": foreignAdd_Filer.Country = Return2290_2020.CountryType.ZI; break;
                        #endregion
                    }

                    if (Filer_ZIPCode.Length > 0)
                        foreignAdd_Filer.PostalCode = Filer_ZIPCode;

                    objFiler.Item = foreignAdd_Filer;
                    #endregion
                }

                returnHeaderType.Filer = objFiler;
                #endregion

                #region OFFICER
                if (Officer_Name != "")
                {
                    Return2290_2020.ReturnHeaderTypeBusinessOfficerGrp objOfficer = new Return2290_2020.ReturnHeaderTypeBusinessOfficerGrp();

                    objOfficer.PersonNm = Officer_Name;
                    objOfficer.PersonTitleTxt = Officer_Title;

                    if (Officer_PIN.Length > 0)
                        objOfficer.TaxpayerPIN = Officer_PIN;

                    if (Officer_Phone.Length > 0)
                    {
                        objOfficer.Item = Officer_Phone;
                        if (Filer_Foreign_Country == "US" || Filer_Foreign_Country == "")
                            objOfficer.ItemElementName = Return2290_2020.ItemChoiceType.PhoneNum;
                        else
                            objOfficer.ItemElementName = Return2290_2020.ItemChoiceType.ForeignPhoneNum;
                    }

                    if (Officer_email.Length > 0)
                        objOfficer.EmailAddressTxt = Officer_email;

                    objOfficer.SignatureDt = DateTime.Now.ToUniversalTime();

                    returnHeaderType.BusinessOfficerGrp = objOfficer;
                }
                #endregion

                #region THIRD PARTY DESIGNEE

                Return2290_2020.ReturnHeaderTypeThirdPartyDesignee objThirdPrtyDesignee = new Return2290_2020.ReturnHeaderTypeThirdPartyDesignee();

                object[] itemsField = new object[4];
                Return2290_2020.ItemsChoiceType[] itemsElementNameField = new Return2290_2020.ItemsChoiceType[4];

                itemsField[0] = Return2290_2020.CheckboxType.X;

                if (ThirdPartyDesigne_Name != "")
                {
                    itemsField[1] = ThirdPartyDesigne_Name;
                    itemsField[2] = ThirdPartyDesigne_Phone;
                    itemsField[3] = ThirdPartyDesigne_PIN;

                    itemsElementNameField[0] = Return2290_2020.ItemsChoiceType.DiscussWithThirdPartyYesInd;
                    itemsElementNameField[1] = Return2290_2020.ItemsChoiceType.ThirdPartyDesigneeNm;
                    itemsElementNameField[2] = Return2290_2020.ItemsChoiceType.ThirdPartyDesigneePhoneNum;
                    itemsElementNameField[3] = Return2290_2020.ItemsChoiceType.ThirdPartyDesigneePIN;
                }
                else
                {
                    itemsElementNameField[0] = Return2290_2020.ItemsChoiceType.DiscussWithThirdPartyNoInd;
                }
                objThirdPrtyDesignee.Items = itemsField;
                objThirdPrtyDesignee.ItemsElementName = itemsElementNameField;

                returnHeaderType.ThirdPartyDesignee = objThirdPrtyDesignee;

                #endregion

                #region PREPARER SIGNINGOFFICER PREPARERPERSONGROUP

                if (Preparer_Name != "")
                {
                    Return2290_2020.ReturnHeaderTypePreparerFirmGrp objPreparerFirm = new Return2290_2020.ReturnHeaderTypePreparerFirmGrp();
                    Return2290_2020.ReturnHeaderTypePreparerPersonGrp objPreparer = new Return2290_2020.ReturnHeaderTypePreparerPersonGrp();
                    
                    //Vishwa 2020 - Neeed to add here the SigningOffice details
                    //Return2290_2020.ReturnHeaderTypeSigningOfficerGrp objOfficer = new Return2290_2020.ReturnHeaderTypeSigningOfficerGrp();

                    objPreparer.PreparerPersonNm = Preparer_Name;

                    if (IsPreparerHasPTIN)
                        objPreparer.PTIN = Preparer_PTIN_SSN;

                    if (Preparer_Phone.Length > 0)
                    {
                        objPreparer.Item = Preparer_Phone;
                        objPreparer.ItemElementName = Return2290_2020.ItemChoiceType1.PhoneNum;
                    }
                    else if (Preparer_ForeignPhone.Length > 0)
                    {
                        objPreparer.Item = Preparer_Phone;
                        objPreparer.ItemElementName = Return2290_2020.ItemChoiceType1.ForeignPhoneNum;
                    }
                    objPreparer.EmailAddressTxt = Preparer_Email;
                    objPreparer.PreparationDt = DateTime.Now.ToUniversalTime();
                    objPreparer.PreparationDtSpecified = true;

                    if (IsPreparerSelfEmployed)
                    {
                        objPreparer.SelfEmployedInd = Return2290_2020.CheckboxType.X;
                        objPreparer.SelfEmployedIndSpecified = IsPreparerSelfEmployed;
                    }

                    returnHeaderType.PreparerPersonGrp = objPreparer;
                }
                //2020 Preparer_Address not empty check 
                if (!IsPreparerSelfEmployed && Preparer_Name != "" && Preparer_Address != "")
                {
                    Return2290_2020.ReturnHeaderTypePreparerFirmGrp objPreparerFirm = new Return2290_2020.ReturnHeaderTypePreparerFirmGrp();
                    Return2290_2020.BusinessNameType PreparerBusinessName = new Return2290_2020.BusinessNameType();
                    PreparerBusinessName.BusinessNameLine1Txt = Preparer_Firm;

                    objPreparerFirm.PreparerFirmEIN = Preparer_EIN;
                    objPreparerFirm.PreparerFirmName = PreparerBusinessName;
                    if (Preparer_Country == "" || Preparer_Country == "US")
                    {
                        #region Preparer US ADDRESS

                        Return2290_2020.USAddressType usAdd_Preparer = new Return2290_2020.USAddressType();

                        if (Preparer_Address.Length > 35)
                        {
                            string Preparer_AddressLine2 = string.Empty;
                            usAdd_Preparer.AddressLine1 = Preparer_Address.Substring(0, 35).Trim();
                            Preparer_AddressLine2 = Preparer_Address.Substring(35);
                            usAdd_Preparer.AddressLine2 = Preparer_AddressLine2.Trim();
                        }
                        else
                        {
                            usAdd_Preparer.AddressLine1 = Preparer_Address.Trim();
                        }

                        usAdd_Preparer.City = Preparer_City.Trim();

                        switch (Preparer_State)
                        {
                            #region Filer_State
                            case "AA": usAdd_Preparer.State = Return2290_2020.StateType.AA; break;
                            case "AE": usAdd_Preparer.State = Return2290_2020.StateType.AE; break;
                            case "AK": usAdd_Preparer.State = Return2290_2020.StateType.AK; break;
                            case "AL": usAdd_Preparer.State = Return2290_2020.StateType.AL; break;
                            case "AP": usAdd_Preparer.State = Return2290_2020.StateType.AP; break;
                            case "AR": usAdd_Preparer.State = Return2290_2020.StateType.AR; break;
                            case "AS": usAdd_Preparer.State = Return2290_2020.StateType.AS; break;
                            case "AZ": usAdd_Preparer.State = Return2290_2020.StateType.AZ; break;
                            case "CA": usAdd_Preparer.State = Return2290_2020.StateType.CA; break;
                            case "CO": usAdd_Preparer.State = Return2290_2020.StateType.CO; break;
                            case "CT": usAdd_Preparer.State = Return2290_2020.StateType.CT; break;
                            case "DC": usAdd_Preparer.State = Return2290_2020.StateType.DC; break;
                            case "DE": usAdd_Preparer.State = Return2290_2020.StateType.DE; break;
                            case "FL": usAdd_Preparer.State = Return2290_2020.StateType.FL; break;
                            case "FM": usAdd_Preparer.State = Return2290_2020.StateType.FM; break;
                            case "GA": usAdd_Preparer.State = Return2290_2020.StateType.GA; break;
                            case "GU": usAdd_Preparer.State = Return2290_2020.StateType.GU; break;
                            case "HI": usAdd_Preparer.State = Return2290_2020.StateType.HI; break;
                            case "IA": usAdd_Preparer.State = Return2290_2020.StateType.IA; break;
                            case "ID": usAdd_Preparer.State = Return2290_2020.StateType.ID; break;
                            case "IL": usAdd_Preparer.State = Return2290_2020.StateType.IL; break;
                            case "IN": usAdd_Preparer.State = Return2290_2020.StateType.IN; break;
                            case "KS": usAdd_Preparer.State = Return2290_2020.StateType.KS; break;
                            case "KY": usAdd_Preparer.State = Return2290_2020.StateType.KY; break;
                            case "LA": usAdd_Preparer.State = Return2290_2020.StateType.LA; break;
                            case "MA": usAdd_Preparer.State = Return2290_2020.StateType.MA; break;
                            case "MD": usAdd_Preparer.State = Return2290_2020.StateType.MD; break;
                            case "ME": usAdd_Preparer.State = Return2290_2020.StateType.ME; break;
                            case "MH": usAdd_Preparer.State = Return2290_2020.StateType.MH; break;
                            case "MI": usAdd_Preparer.State = Return2290_2020.StateType.MI; break;
                            case "MN": usAdd_Preparer.State = Return2290_2020.StateType.MN; break;
                            case "MO": usAdd_Preparer.State = Return2290_2020.StateType.MO; break;
                            case "MP": usAdd_Preparer.State = Return2290_2020.StateType.MP; break;
                            case "MS": usAdd_Preparer.State = Return2290_2020.StateType.MS; break;
                            case "MT": usAdd_Preparer.State = Return2290_2020.StateType.MT; break;
                            case "NC": usAdd_Preparer.State = Return2290_2020.StateType.NC; break;
                            case "ND": usAdd_Preparer.State = Return2290_2020.StateType.ND; break;
                            case "NE": usAdd_Preparer.State = Return2290_2020.StateType.NE; break;
                            case "NH": usAdd_Preparer.State = Return2290_2020.StateType.NH; break;
                            case "NJ": usAdd_Preparer.State = Return2290_2020.StateType.NJ; break;
                            case "NM": usAdd_Preparer.State = Return2290_2020.StateType.NM; break;
                            case "NV": usAdd_Preparer.State = Return2290_2020.StateType.NV; break;
                            case "NY": usAdd_Preparer.State = Return2290_2020.StateType.NY; break;
                            case "OH": usAdd_Preparer.State = Return2290_2020.StateType.OH; break;
                            case "OK": usAdd_Preparer.State = Return2290_2020.StateType.OK; break;
                            case "OR": usAdd_Preparer.State = Return2290_2020.StateType.OR; break;
                            case "PA": usAdd_Preparer.State = Return2290_2020.StateType.PA; break;
                            case "PR": usAdd_Preparer.State = Return2290_2020.StateType.PR; break;
                            case "PW": usAdd_Preparer.State = Return2290_2020.StateType.PW; break;
                            case "RI": usAdd_Preparer.State = Return2290_2020.StateType.RI; break;
                            case "SC": usAdd_Preparer.State = Return2290_2020.StateType.SC; break;
                            case "SD": usAdd_Preparer.State = Return2290_2020.StateType.SD; break;
                            case "TN": usAdd_Preparer.State = Return2290_2020.StateType.TN; break;
                            case "TX": usAdd_Preparer.State = Return2290_2020.StateType.TX; break;
                            case "UT": usAdd_Preparer.State = Return2290_2020.StateType.UT; break;
                            case "VA": usAdd_Preparer.State = Return2290_2020.StateType.VA; break;
                            case "VI": usAdd_Preparer.State = Return2290_2020.StateType.VI; break;
                            case "VT": usAdd_Preparer.State = Return2290_2020.StateType.VT; break;
                            case "WA": usAdd_Preparer.State = Return2290_2020.StateType.WA; break;
                            case "WI": usAdd_Preparer.State = Return2290_2020.StateType.WI; break;
                            case "WV": usAdd_Preparer.State = Return2290_2020.StateType.WV; break;
                            case "WY": usAdd_Preparer.State = Return2290_2020.StateType.WY; break;
                            #endregion
                        }

                        usAdd_Preparer.ZIPCode = Preparer_Zip;

                        objPreparerFirm.Item = usAdd_Preparer;

                        #endregion
                    }
                    else
                    {
                        #region Preparer FOREIGN ADDRESS
                        Return2290_2020.ForeignAddressType foreignAdd_Preparer = new Return2290_2020.ForeignAddressType();

                        if (Preparer_Address.Length > 35)
                        {
                            string Preparer_AddressLine2 = string.Empty;
                            foreignAdd_Preparer.AddressLine1 = Preparer_Address.Substring(0, 35).Trim();
                            Preparer_AddressLine2 = Preparer_Address.Substring(35);
                            foreignAdd_Preparer.AddressLine2 = Preparer_AddressLine2.Trim();

                        }
                        else
                        {
                            foreignAdd_Preparer.AddressLine1 = Preparer_Address.Trim();
                        }
                        foreignAdd_Preparer.City = Preparer_City.Trim();

                        if (Preparer_State.Length > 0)
                            foreignAdd_Preparer.ProvinceOrState = Preparer_State;

                        switch (Preparer_Country)
                        {
                            #region Preparer_Foreign_Country
                            case "AA": foreignAdd_Preparer.Country = Return2290_2020.CountryType.AA; break;
                            case "AC": foreignAdd_Preparer.Country = Return2290_2020.CountryType.AC; break;
                            case "AE": foreignAdd_Preparer.Country = Return2290_2020.CountryType.AE; break;
                            case "AF": foreignAdd_Preparer.Country = Return2290_2020.CountryType.AF; break;
                            case "AG": foreignAdd_Preparer.Country = Return2290_2020.CountryType.AG; break;
                            case "AJ": foreignAdd_Preparer.Country = Return2290_2020.CountryType.AJ; break;
                            case "AL": foreignAdd_Preparer.Country = Return2290_2020.CountryType.AL; break;
                            case "AM": foreignAdd_Preparer.Country = Return2290_2020.CountryType.AM; break;
                            case "AN": foreignAdd_Preparer.Country = Return2290_2020.CountryType.AN; break;
                            case "AO": foreignAdd_Preparer.Country = Return2290_2020.CountryType.AO; break;
                            case "AR": foreignAdd_Preparer.Country = Return2290_2020.CountryType.AR; break;
                            case "AS": foreignAdd_Preparer.Country = Return2290_2020.CountryType.AS; break;
                            case "AT": foreignAdd_Preparer.Country = Return2290_2020.CountryType.AT; break;
                            case "AU": foreignAdd_Preparer.Country = Return2290_2020.CountryType.AU; break;
                            case "AV": foreignAdd_Preparer.Country = Return2290_2020.CountryType.AV; break;
                            case "AX": foreignAdd_Preparer.Country = Return2290_2020.CountryType.AX; break;
                            case "AY": foreignAdd_Preparer.Country = Return2290_2020.CountryType.AY; break;
                            case "BA": foreignAdd_Preparer.Country = Return2290_2020.CountryType.BA; break;
                            case "BB": foreignAdd_Preparer.Country = Return2290_2020.CountryType.BB; break;
                            case "BC": foreignAdd_Preparer.Country = Return2290_2020.CountryType.BC; break;
                            case "BD": foreignAdd_Preparer.Country = Return2290_2020.CountryType.BD; break;
                            case "BE": foreignAdd_Preparer.Country = Return2290_2020.CountryType.BE; break;
                            case "BF": foreignAdd_Preparer.Country = Return2290_2020.CountryType.BF; break;
                            case "BG": foreignAdd_Preparer.Country = Return2290_2020.CountryType.BG; break;
                            case "BH": foreignAdd_Preparer.Country = Return2290_2020.CountryType.BH; break;
                            case "BK": foreignAdd_Preparer.Country = Return2290_2020.CountryType.BK; break;
                            case "BL": foreignAdd_Preparer.Country = Return2290_2020.CountryType.BL; break;
                            case "BM": foreignAdd_Preparer.Country = Return2290_2020.CountryType.BM; break;
                            case "BN": foreignAdd_Preparer.Country = Return2290_2020.CountryType.BN; break;
                            case "BO": foreignAdd_Preparer.Country = Return2290_2020.CountryType.BO; break;
                            case "BP": foreignAdd_Preparer.Country = Return2290_2020.CountryType.BP; break;
                            case "BQ": foreignAdd_Preparer.Country = Return2290_2020.CountryType.BQ; break;
                            case "BR": foreignAdd_Preparer.Country = Return2290_2020.CountryType.BR; break;
                            case "BT": foreignAdd_Preparer.Country = Return2290_2020.CountryType.BT; break;
                            case "BU": foreignAdd_Preparer.Country = Return2290_2020.CountryType.BU; break;
                            case "BV": foreignAdd_Preparer.Country = Return2290_2020.CountryType.BV; break;
                            case "BX": foreignAdd_Preparer.Country = Return2290_2020.CountryType.BX; break;
                            case "BY": foreignAdd_Preparer.Country = Return2290_2020.CountryType.BY; break;
                            case "CA": foreignAdd_Preparer.Country = Return2290_2020.CountryType.CA; break;
                            case "CB": foreignAdd_Preparer.Country = Return2290_2020.CountryType.CB; break;
                            case "CD": foreignAdd_Preparer.Country = Return2290_2020.CountryType.CD; break;
                            case "CE": foreignAdd_Preparer.Country = Return2290_2020.CountryType.CE; break;
                            case "CF": foreignAdd_Preparer.Country = Return2290_2020.CountryType.CF; break;
                            case "CG": foreignAdd_Preparer.Country = Return2290_2020.CountryType.CG; break;
                            case "CH": foreignAdd_Preparer.Country = Return2290_2020.CountryType.CH; break;
                            case "CI": foreignAdd_Preparer.Country = Return2290_2020.CountryType.CI; break;
                            case "CJ": foreignAdd_Preparer.Country = Return2290_2020.CountryType.CJ; break;
                            case "CK": foreignAdd_Preparer.Country = Return2290_2020.CountryType.CK; break;
                            case "CM": foreignAdd_Preparer.Country = Return2290_2020.CountryType.CM; break;
                            case "CN": foreignAdd_Preparer.Country = Return2290_2020.CountryType.CN; break;
                            case "CO": foreignAdd_Preparer.Country = Return2290_2020.CountryType.CO; break;
                            case "CR": foreignAdd_Preparer.Country = Return2290_2020.CountryType.CR; break;
                            case "CS": foreignAdd_Preparer.Country = Return2290_2020.CountryType.CS; break;
                            case "CT": foreignAdd_Preparer.Country = Return2290_2020.CountryType.CT; break;
                            case "CU": foreignAdd_Preparer.Country = Return2290_2020.CountryType.CU; break;
                            case "CV": foreignAdd_Preparer.Country = Return2290_2020.CountryType.CV; break;
                            case "CW": foreignAdd_Preparer.Country = Return2290_2020.CountryType.CW; break;
                            case "CY": foreignAdd_Preparer.Country = Return2290_2020.CountryType.CY; break;
                            case "DA": foreignAdd_Preparer.Country = Return2290_2020.CountryType.DA; break;
                            case "DJ": foreignAdd_Preparer.Country = Return2290_2020.CountryType.DJ; break;
                            case "DO": foreignAdd_Preparer.Country = Return2290_2020.CountryType.DO; break;
                            case "DR": foreignAdd_Preparer.Country = Return2290_2020.CountryType.DR; break;
                            case "DX": foreignAdd_Preparer.Country = Return2290_2020.CountryType.DX; break;
                            case "EC": foreignAdd_Preparer.Country = Return2290_2020.CountryType.EC; break;
                            case "EG": foreignAdd_Preparer.Country = Return2290_2020.CountryType.EG; break;
                            case "EI": foreignAdd_Preparer.Country = Return2290_2020.CountryType.EI; break;
                            case "EK": foreignAdd_Preparer.Country = Return2290_2020.CountryType.EK; break;
                            case "EN": foreignAdd_Preparer.Country = Return2290_2020.CountryType.EN; break;
                            case "ER": foreignAdd_Preparer.Country = Return2290_2020.CountryType.ER; break;
                            case "ES": foreignAdd_Preparer.Country = Return2290_2020.CountryType.ES; break;
                            case "ET": foreignAdd_Preparer.Country = Return2290_2020.CountryType.ET; break;
                            case "EZ": foreignAdd_Preparer.Country = Return2290_2020.CountryType.EZ; break;
                            case "FI": foreignAdd_Preparer.Country = Return2290_2020.CountryType.FI; break;
                            case "FJ": foreignAdd_Preparer.Country = Return2290_2020.CountryType.FJ; break;
                            case "FK": foreignAdd_Preparer.Country = Return2290_2020.CountryType.FK; break;
                            case "FO": foreignAdd_Preparer.Country = Return2290_2020.CountryType.FO; break;
                            case "FP": foreignAdd_Preparer.Country = Return2290_2020.CountryType.FP; break;
                            case "FQ": foreignAdd_Preparer.Country = Return2290_2020.CountryType.FQ; break;
                            case "FR": foreignAdd_Preparer.Country = Return2290_2020.CountryType.FR; break;
                            case "FS": foreignAdd_Preparer.Country = Return2290_2020.CountryType.FS; break;
                            case "GA": foreignAdd_Preparer.Country = Return2290_2020.CountryType.GA; break;
                            case "GB": foreignAdd_Preparer.Country = Return2290_2020.CountryType.GB; break;
                            case "GG": foreignAdd_Preparer.Country = Return2290_2020.CountryType.GG; break;
                            case "GH": foreignAdd_Preparer.Country = Return2290_2020.CountryType.GH; break;
                            case "GI": foreignAdd_Preparer.Country = Return2290_2020.CountryType.GI; break;
                            case "GJ": foreignAdd_Preparer.Country = Return2290_2020.CountryType.GJ; break;
                            case "GK": foreignAdd_Preparer.Country = Return2290_2020.CountryType.GK; break;
                            case "GL": foreignAdd_Preparer.Country = Return2290_2020.CountryType.GL; break;
                            case "GM": foreignAdd_Preparer.Country = Return2290_2020.CountryType.GM; break;
                            case "GR": foreignAdd_Preparer.Country = Return2290_2020.CountryType.GR; break;
                            case "GT": foreignAdd_Preparer.Country = Return2290_2020.CountryType.GT; break;
                            case "GV": foreignAdd_Preparer.Country = Return2290_2020.CountryType.GV; break;
                            case "GY": foreignAdd_Preparer.Country = Return2290_2020.CountryType.GY; break;
                            case "HA": foreignAdd_Preparer.Country = Return2290_2020.CountryType.HA; break;
                            case "HK": foreignAdd_Preparer.Country = Return2290_2020.CountryType.HK; break;
                            case "HM": foreignAdd_Preparer.Country = Return2290_2020.CountryType.HM; break;
                            case "HO": foreignAdd_Preparer.Country = Return2290_2020.CountryType.HO; break;
                            case "HQ": foreignAdd_Preparer.Country = Return2290_2020.CountryType.HQ; break;
                            case "HR": foreignAdd_Preparer.Country = Return2290_2020.CountryType.HR; break;
                            case "HU": foreignAdd_Preparer.Country = Return2290_2020.CountryType.HU; break;
                            case "IC": foreignAdd_Preparer.Country = Return2290_2020.CountryType.IC; break;
                            case "ID": foreignAdd_Preparer.Country = Return2290_2020.CountryType.ID; break;
                            case "IM": foreignAdd_Preparer.Country = Return2290_2020.CountryType.IM; break;
                            case "IO": foreignAdd_Preparer.Country = Return2290_2020.CountryType.IO; break;
                            case "IP": foreignAdd_Preparer.Country = Return2290_2020.CountryType.IP; break;
                            case "IR": foreignAdd_Preparer.Country = Return2290_2020.CountryType.IR; break;
                            case "IS": foreignAdd_Preparer.Country = Return2290_2020.CountryType.IS; break;
                            case "IT": foreignAdd_Preparer.Country = Return2290_2020.CountryType.IT; break;
                            case "IV": foreignAdd_Preparer.Country = Return2290_2020.CountryType.IV; break;
                            case "IZ": foreignAdd_Preparer.Country = Return2290_2020.CountryType.IZ; break;
                            case "JA": foreignAdd_Preparer.Country = Return2290_2020.CountryType.JA; break;
                            case "JE": foreignAdd_Preparer.Country = Return2290_2020.CountryType.JE; break;
                            case "JM": foreignAdd_Preparer.Country = Return2290_2020.CountryType.JM; break;
                            case "JN": foreignAdd_Preparer.Country = Return2290_2020.CountryType.JN; break;
                            case "JO": foreignAdd_Preparer.Country = Return2290_2020.CountryType.JO; break;
                            case "JQ": foreignAdd_Preparer.Country = Return2290_2020.CountryType.JQ; break;
                            case "KE": foreignAdd_Preparer.Country = Return2290_2020.CountryType.KE; break;
                            case "KG": foreignAdd_Preparer.Country = Return2290_2020.CountryType.KG; break;
                            case "KN": foreignAdd_Preparer.Country = Return2290_2020.CountryType.KN; break;
                            case "KQ": foreignAdd_Preparer.Country = Return2290_2020.CountryType.KQ; break;
                            case "KR": foreignAdd_Preparer.Country = Return2290_2020.CountryType.KR; break;
                            case "KS": foreignAdd_Preparer.Country = Return2290_2020.CountryType.KS; break;
                            case "KT": foreignAdd_Preparer.Country = Return2290_2020.CountryType.KT; break;
                            case "KU": foreignAdd_Preparer.Country = Return2290_2020.CountryType.KU; break;
                            case "KZ": foreignAdd_Preparer.Country = Return2290_2020.CountryType.KZ; break;
                            case "LA": foreignAdd_Preparer.Country = Return2290_2020.CountryType.LA; break;
                            case "LE": foreignAdd_Preparer.Country = Return2290_2020.CountryType.LE; break;
                            case "LG": foreignAdd_Preparer.Country = Return2290_2020.CountryType.LG; break;
                            case "LH": foreignAdd_Preparer.Country = Return2290_2020.CountryType.LH; break;
                            case "LI": foreignAdd_Preparer.Country = Return2290_2020.CountryType.LI; break;
                            case "LO": foreignAdd_Preparer.Country = Return2290_2020.CountryType.LO; break;
                            case "LQ": foreignAdd_Preparer.Country = Return2290_2020.CountryType.LQ; break;
                            case "LS": foreignAdd_Preparer.Country = Return2290_2020.CountryType.LS; break;
                            case "LT": foreignAdd_Preparer.Country = Return2290_2020.CountryType.LT; break;
                            case "LU": foreignAdd_Preparer.Country = Return2290_2020.CountryType.LU; break;
                            case "LY": foreignAdd_Preparer.Country = Return2290_2020.CountryType.LY; break;
                            case "MA": foreignAdd_Preparer.Country = Return2290_2020.CountryType.MA; break;
                            case "MC": foreignAdd_Preparer.Country = Return2290_2020.CountryType.MC; break;
                            case "MD": foreignAdd_Preparer.Country = Return2290_2020.CountryType.MD; break;
                            case "MG": foreignAdd_Preparer.Country = Return2290_2020.CountryType.MG; break;
                            case "MH": foreignAdd_Preparer.Country = Return2290_2020.CountryType.MH; break;
                            case "MI": foreignAdd_Preparer.Country = Return2290_2020.CountryType.MI; break;
                            case "MJ": foreignAdd_Preparer.Country = Return2290_2020.CountryType.MJ; break;
                            case "MK": foreignAdd_Preparer.Country = Return2290_2020.CountryType.MK; break;
                            case "ML": foreignAdd_Preparer.Country = Return2290_2020.CountryType.ML; break;
                            case "MN": foreignAdd_Preparer.Country = Return2290_2020.CountryType.MN; break;
                            case "MO": foreignAdd_Preparer.Country = Return2290_2020.CountryType.MO; break;
                            case "MP": foreignAdd_Preparer.Country = Return2290_2020.CountryType.MP; break;
                            case "MQ": foreignAdd_Preparer.Country = Return2290_2020.CountryType.MQ; break;
                            case "MR": foreignAdd_Preparer.Country = Return2290_2020.CountryType.MR; break;
                            case "MT": foreignAdd_Preparer.Country = Return2290_2020.CountryType.MT; break;
                            case "MU": foreignAdd_Preparer.Country = Return2290_2020.CountryType.MU; break;
                            case "MV": foreignAdd_Preparer.Country = Return2290_2020.CountryType.MV; break;
                            case "MX": foreignAdd_Preparer.Country = Return2290_2020.CountryType.MX; break;
                            case "MY": foreignAdd_Preparer.Country = Return2290_2020.CountryType.MY; break;
                            case "MZ": foreignAdd_Preparer.Country = Return2290_2020.CountryType.MZ; break;
                            case "NC": foreignAdd_Preparer.Country = Return2290_2020.CountryType.NC; break;
                            case "NE": foreignAdd_Preparer.Country = Return2290_2020.CountryType.NE; break;
                            case "NF": foreignAdd_Preparer.Country = Return2290_2020.CountryType.NF; break;
                            case "NG": foreignAdd_Preparer.Country = Return2290_2020.CountryType.NG; break;
                            case "NH": foreignAdd_Preparer.Country = Return2290_2020.CountryType.NH; break;
                            case "NI": foreignAdd_Preparer.Country = Return2290_2020.CountryType.NI; break;
                            case "NL": foreignAdd_Preparer.Country = Return2290_2020.CountryType.NL; break;
                            case "NO": foreignAdd_Preparer.Country = Return2290_2020.CountryType.NO; break;
                            case "NP": foreignAdd_Preparer.Country = Return2290_2020.CountryType.NP; break;
                            case "NR": foreignAdd_Preparer.Country = Return2290_2020.CountryType.NR; break;
                            case "NS": foreignAdd_Preparer.Country = Return2290_2020.CountryType.NS; break;
                            case "NU": foreignAdd_Preparer.Country = Return2290_2020.CountryType.NU; break;
                            case "NZ": foreignAdd_Preparer.Country = Return2290_2020.CountryType.NZ; break;
                            case "PA": foreignAdd_Preparer.Country = Return2290_2020.CountryType.PA; break;
                            case "PC": foreignAdd_Preparer.Country = Return2290_2020.CountryType.PC; break;
                            case "PE": foreignAdd_Preparer.Country = Return2290_2020.CountryType.PE; break;
                            case "PF": foreignAdd_Preparer.Country = Return2290_2020.CountryType.PF; break;
                            case "PG": foreignAdd_Preparer.Country = Return2290_2020.CountryType.PG; break;
                            case "PK": foreignAdd_Preparer.Country = Return2290_2020.CountryType.PK; break;
                            case "PL": foreignAdd_Preparer.Country = Return2290_2020.CountryType.PL; break;
                            case "PM": foreignAdd_Preparer.Country = Return2290_2020.CountryType.PM; break;
                            case "PO": foreignAdd_Preparer.Country = Return2290_2020.CountryType.PO; break;
                            case "PP": foreignAdd_Preparer.Country = Return2290_2020.CountryType.PP; break;
                            case "PU": foreignAdd_Preparer.Country = Return2290_2020.CountryType.PU; break;
                            case "QA": foreignAdd_Preparer.Country = Return2290_2020.CountryType.QA; break;
                            case "RO": foreignAdd_Preparer.Country = Return2290_2020.CountryType.RO; break;
                            case "RP": foreignAdd_Preparer.Country = Return2290_2020.CountryType.RP; break;
                            case "RS": foreignAdd_Preparer.Country = Return2290_2020.CountryType.RS; break;
                            case "RW": foreignAdd_Preparer.Country = Return2290_2020.CountryType.RW; break;
                            case "SA": foreignAdd_Preparer.Country = Return2290_2020.CountryType.SA; break;
                            case "SB": foreignAdd_Preparer.Country = Return2290_2020.CountryType.SB; break;
                            case "SC": foreignAdd_Preparer.Country = Return2290_2020.CountryType.SC; break;
                            case "SE": foreignAdd_Preparer.Country = Return2290_2020.CountryType.SE; break;
                            case "SF": foreignAdd_Preparer.Country = Return2290_2020.CountryType.SF; break;
                            case "SG": foreignAdd_Preparer.Country = Return2290_2020.CountryType.SG; break;
                            case "SH": foreignAdd_Preparer.Country = Return2290_2020.CountryType.SH; break;
                            case "SI": foreignAdd_Preparer.Country = Return2290_2020.CountryType.SI; break;
                            case "SL": foreignAdd_Preparer.Country = Return2290_2020.CountryType.SL; break;
                            case "SM": foreignAdd_Preparer.Country = Return2290_2020.CountryType.SM; break;
                            case "SN": foreignAdd_Preparer.Country = Return2290_2020.CountryType.SN; break;
                            case "SO": foreignAdd_Preparer.Country = Return2290_2020.CountryType.SO; break;
                            case "SP": foreignAdd_Preparer.Country = Return2290_2020.CountryType.SP; break;
                            case "ST": foreignAdd_Preparer.Country = Return2290_2020.CountryType.ST; break;
                            case "SU": foreignAdd_Preparer.Country = Return2290_2020.CountryType.SU; break;
                            case "SV": foreignAdd_Preparer.Country = Return2290_2020.CountryType.SV; break;
                            case "SW": foreignAdd_Preparer.Country = Return2290_2020.CountryType.SW; break;
                            case "SX": foreignAdd_Preparer.Country = Return2290_2020.CountryType.SX; break;
                            case "SY": foreignAdd_Preparer.Country = Return2290_2020.CountryType.SY; break;
                            case "SZ": foreignAdd_Preparer.Country = Return2290_2020.CountryType.SZ; break;
                            case "TD": foreignAdd_Preparer.Country = Return2290_2020.CountryType.TD; break;
                            case "TH": foreignAdd_Preparer.Country = Return2290_2020.CountryType.TH; break;
                            case "TI": foreignAdd_Preparer.Country = Return2290_2020.CountryType.TI; break;
                            case "TK": foreignAdd_Preparer.Country = Return2290_2020.CountryType.TK; break;
                            case "TL": foreignAdd_Preparer.Country = Return2290_2020.CountryType.TL; break;
                            case "TN": foreignAdd_Preparer.Country = Return2290_2020.CountryType.TN; break;
                            case "TO": foreignAdd_Preparer.Country = Return2290_2020.CountryType.TO; break;
                            case "TP": foreignAdd_Preparer.Country = Return2290_2020.CountryType.TP; break;
                            case "TS": foreignAdd_Preparer.Country = Return2290_2020.CountryType.TS; break;
                            case "TT": foreignAdd_Preparer.Country = Return2290_2020.CountryType.TT; break;
                            case "TU": foreignAdd_Preparer.Country = Return2290_2020.CountryType.TU; break;
                            case "TV": foreignAdd_Preparer.Country = Return2290_2020.CountryType.TV; break;
                            case "TW": foreignAdd_Preparer.Country = Return2290_2020.CountryType.TW; break;
                            case "TX": foreignAdd_Preparer.Country = Return2290_2020.CountryType.TX; break;
                            case "TZ": foreignAdd_Preparer.Country = Return2290_2020.CountryType.TZ; break;
                            case "UG": foreignAdd_Preparer.Country = Return2290_2020.CountryType.UG; break;
                            case "UK": foreignAdd_Preparer.Country = Return2290_2020.CountryType.UK; break;
                            case "UP": foreignAdd_Preparer.Country = Return2290_2020.CountryType.UP; break;
                            case "UV": foreignAdd_Preparer.Country = Return2290_2020.CountryType.UV; break;
                            case "UY": foreignAdd_Preparer.Country = Return2290_2020.CountryType.UY; break;
                            case "UZ": foreignAdd_Preparer.Country = Return2290_2020.CountryType.UZ; break;
                            case "VC": foreignAdd_Preparer.Country = Return2290_2020.CountryType.VC; break;
                            case "VE": foreignAdd_Preparer.Country = Return2290_2020.CountryType.VE; break;
                            case "VI": foreignAdd_Preparer.Country = Return2290_2020.CountryType.VI; break;
                            case "VM": foreignAdd_Preparer.Country = Return2290_2020.CountryType.VM; break;
                            case "VT": foreignAdd_Preparer.Country = Return2290_2020.CountryType.VT; break;
                            case "WA": foreignAdd_Preparer.Country = Return2290_2020.CountryType.WA; break;
                            case "WF": foreignAdd_Preparer.Country = Return2290_2020.CountryType.WF; break;
                            case "WI": foreignAdd_Preparer.Country = Return2290_2020.CountryType.WI; break;
                            case "WQ": foreignAdd_Preparer.Country = Return2290_2020.CountryType.WQ; break;
                            case "WS": foreignAdd_Preparer.Country = Return2290_2020.CountryType.WS; break;
                            case "WZ": foreignAdd_Preparer.Country = Return2290_2020.CountryType.WZ; break;
                            case "YM": foreignAdd_Preparer.Country = Return2290_2020.CountryType.YM; break;
                            case "ZA": foreignAdd_Preparer.Country = Return2290_2020.CountryType.ZA; break;
                            case "ZI": foreignAdd_Preparer.Country = Return2290_2020.CountryType.ZI; break;
                            #endregion
                        }

                        if (Filer_ZIPCode.Length > 0)
                            foreignAdd_Preparer.PostalCode = Filer_ZIPCode;

                        objFiler.Item = foreignAdd_Preparer;
                        #endregion
                    }

                    returnHeaderType.PreparerFirmGrp = objPreparerFirm;
                }
                #endregion

                #region CONSENT_TO_VIN_DATA_DISCLOSURE

                object[] itemConsent = new object[4];
                Return2290_2020.ItemsChoiceType1[] itemElements = new Return2290_2020.ItemsChoiceType1[4];

                itemConsent[0] = Return2290_2020.CheckboxType.X;

                if (IsConsentToDiscloseYes)
                {
                    itemElements[0] = Return2290_2020.ItemsChoiceType1.ConsentToDiscloseYesInd;

                    Return2290_2020.ReturnHeaderTypeConsentToVINDataDisclosureGrpDisclosureFormSignatureInfo objDisclosureFormSignInfo =
                        new Return2290_2020.ReturnHeaderTypeConsentToVINDataDisclosureGrpDisclosureFormSignatureInfo();
                    objDisclosureFormSignInfo.EIN = Filer_EIN;
                    Return2290_2020.BusinessNameType disclosureFormSignInfo_business = new Return2290_2020.BusinessNameType();
                    disclosureFormSignInfo_business.BusinessNameLine1Txt = Filer_Name;
                    objDisclosureFormSignInfo.BusinessName = disclosureFormSignInfo_business;
                    objDisclosureFormSignInfo.SignatureDt = DateTime.Now.ToUniversalTime();

                    itemConsent[1] = (objDisclosureFormSignInfo);
                    itemElements[1] = (Return2290_2020.ItemsChoiceType1.DisclosureFormSignatureInfo);

                    itemConsent[2] = (Return2290_2020.ReturnHeaderTypeConsentToVINDataDisclosureGrpSignatureOptionCd.PINNumber);
                    itemElements[2] = (Return2290_2020.ItemsChoiceType1.SignatureOptionCd);

                    itemConsent[3] = (Officer_PIN);
                    itemElements[3] = (Return2290_2020.ItemsChoiceType1.PIN);
                }
                else
                {
                    itemElements[0] = (Return2290_2020.ItemsChoiceType1.ConsentToDiscloseNoInd);
                }

                Return2290_2020.ReturnHeaderTypeConsentToVINDataDisclosureGrp objConsent2DataDisclosure =
                    new Return2290_2020.ReturnHeaderTypeConsentToVINDataDisclosureGrp();
                objConsent2DataDisclosure.Items = itemConsent;
                objConsent2DataDisclosure.ItemsElementName = itemElements;

                returnHeaderType.ConsentToVINDataDisclosureGrp = objConsent2DataDisclosure;

                #endregion

                returnHeaderType.TaxYr = TaxYear;

                returnHeaderType.binaryAttachmentCnt = "0";

                returnHeaderType.SoftwareId = Variables.SoftwareID;
                returnHeaderType.SoftwareVersionNum = Variables.SoftwareVersion;
                #endregion

                #region RETURN DATA

                Return2290_2020.ReturnData returnData = new Return2290_2020.ReturnData();

                #region IRS2290

                Return2290_2020.IRS2290 objIRS2290 = new Return2290_2020.IRS2290();

                #region TAX COMPUTATION
                // NEW Changes in 2020 were proposed here
                if (dsTaxComputation.Tables.Count > 0)
                {
                    if (dsTaxComputation.Tables[0].Rows.Count > 0)
                    {
                        List<Return2290_2020.IRS2290TypeHighwayMtrVehTxComputationGrp> objTaxComputationList = new
                            List<Return2290_2020.IRS2290TypeHighwayMtrVehTxComputationGrp>();

                        foreach (DataRow ldr in dsTaxComputation.Tables[0].Rows)
                        {
                            Return2290_2020.IRS2290TypeHighwayMtrVehTxComputationGrp objTaxComputation = new
                                Return2290_2020.IRS2290TypeHighwayMtrVehTxComputationGrp();
                            objTaxComputation.VehicleCategoryCd = Convert.ToString(ldr["Category"]);

                            Return2290_2020.HighwayMtrVehTxCmptColumnsGrpType objVeh = new Return2290_2020.HighwayMtrVehTxCmptColumnsGrpType();

                            objVeh.TaxAmt = Convert.ToDecimal(ldr["Tax_Amount"]);
                            objVeh.TaxAmtSpecified = true;



                            objVeh.LoggingVehPartialTaxAmtSpecified = false;
                            objVeh.NonLoggingVehPartialTaxAmtSpecified = false;

                            if (Convert.ToString(ldr["Logging_Vehicle_Count"]) != "0")
                            {
                                if (Convert.ToString(ldr["Logging_Vehicle_Count"]) != "")
                                {
                                    numLoggingVehicleCnt = numLoggingVehicleCnt + Convert.ToInt16(ldr["Logging_Vehicle_Count"]);
                                    objVeh.LoggingVehicleCnt = Convert.ToString(ldr["Logging_Vehicle_Count"]);
                                }
                                if (Convert.ToString(ldr["Logging_Vehicle_Count"]) != "")
                                    numTotalTaxableVeh += Convert.ToInt16(ldr["Logging_Vehicle_Count"]);
                            }
                            if (Convert.ToString(ldr["Non_Logging_Vehicle_Count"]) != "0")
                            {
                                if (Convert.ToString(ldr["Non_Logging_Vehicle_Count"]) != "")
                                {
                                    numNonLoggingVehicleCnt = numNonLoggingVehicleCnt + Convert.ToInt16(ldr["Non_Logging_Vehicle_Count"]);
                                    objVeh.NonLoggingVehicleCnt = Convert.ToString(ldr["Non_Logging_Vehicle_Count"]);
                                }
                                if (Convert.ToString(ldr["Non_Logging_Vehicle_Count"]) != "")
                                    numTotalTaxableVeh += Convert.ToInt16(ldr["Non_Logging_Vehicle_Count"]);
                            }
                            objTaxComputation.HighwayMtrVehTxCmptColumnsGrp = objVeh;
                            objTaxComputationList.Add(objTaxComputation);
                        }
                        objIRS2290.HighwayMtrVehTxComputationGrp = objTaxComputationList.ToArray();

                        objIRS2290.TotalTaxAmt = Convert.ToDecimal(TaxFromTaxComputation.ToString("N2"));
                        objIRS2290.TotalTaxAmtSpecified = true;

                        objIRS2290.TotalVehicleCnt = numTotalTaxableVeh.ToString();

                    }
                }

                #endregion

                #region IS APPLICATION FOR....

                if (IsAddressChange)
                    objIRS2290.AddressChangeInd = Return2290_2020.CheckboxType.X;

                if (IsAmendedReturn)
                {
                    objIRS2290.AmendedReturnInd = Return2290_2020.CheckboxType.X;
                    objIRS2290.AmendedReturnIndSpecified = true;
                    objIRS2290.AmendedMonthNum = AmendedMonth; //format is MM
                }

                if (IsVINCorrection)
                {
                    // 2020 VINCorrection VIN Correction
                    // Vishwa Add the new TAG About the VINCorrectionExplanationStmt -> ExplanationTtx

                    //objIRS2290.VINCorrectionInd = Return2290_2020.CheckboxType.X;
                    //objIRS2290.VINCorrectionIndSpecified = true;


                    Return2290_2020.IRS2290TypeVINCorrInd objVINCor = new Return2290_2020.IRS2290TypeVINCorrInd();
                    objVINCor.Value = "X";
                    List<string> docId_VINCorrDetails = new List<string>();
                    docId_VINCorrDetails.Add("VINCorrectionExplan");
                    objVINCor.referenceDocumentId = docId_VINCorrDetails.ToArray();
                    objVINCor.referenceDocumentName = "VINCorrectionExplanationStatement";

                    objIRS2290.VINCorrectionInd = objVINCor;


                    //Return2290_2020.VINCorrectionExplanationStmtType VinCorExp = new Return2290_2020.VINCorrectionExplanationStmtType();

                    //VinCorExp.ExplanationTxt = "VIN Correction update request";
                    //objIRS2290.VINCorrectionExplanationStmt = VinCorExp;

                }
                //2020 VIN Correction
                if (IsFinalReturn && IsVINCorrection == false)
                {
                    objIRS2290.FinalReturnInd = Return2290_2020.CheckboxType.X;
                    objIRS2290.FinalReturnIndSpecified = true;
                }

                #endregion

                #region ADDITIONAL TAX
                if (AdditionalTaxAmount > 0)
                {
                    Return2290_2020.IRS2290TypeAdditionalTaxAmt objAdditionalTax = new Return2290_2020.IRS2290TypeAdditionalTaxAmt();
                    objAdditionalTax.Value = Convert.ToDecimal(AdditionalTaxAmount.ToString("N2"));
                    List<string> docId_TGWIncreaseWorksheet = new List<string>();
                    docId_TGWIncreaseWorksheet.Add("TGWIncreaseWorksheet");
                    objAdditionalTax.referenceDocumentId = docId_TGWIncreaseWorksheet.ToArray();
                    objAdditionalTax.referenceDocumentName = "TGWIncreaseWorksheet";

                    objIRS2290.AdditionalTaxAmt = objAdditionalTax;
                }
                #endregion

                objIRS2290.TotalTaxAmt = Convert.ToDecimal(TaxFromTaxComputation.ToString("N2")) + Convert.ToDecimal(AdditionalTaxAmount.ToString("N2")); ;
                objIRS2290.TotalTaxAmtSpecified = true;

                #region CREDIT AMOUNT
                if (CreditAmount > 0)
                {
                    Return2290_2020.IRS2290TypeTaxCreditsAmt objTaxCreditsAmt = new Return2290_2020.IRS2290TypeTaxCreditsAmt();
                    List<string> docId_CreditsAmountStatement = new List<string>();
                    docId_CreditsAmountStatement.Add("CreditsAmountStatement");
                    objTaxCreditsAmt.referenceDocumentId = docId_CreditsAmountStatement.ToArray();
                    objTaxCreditsAmt.referenceDocumentName = "CreditsAmountStatement";
                    objTaxCreditsAmt.Value = Convert.ToDecimal(CreditAmount.ToString("N2"));

                    objIRS2290.TaxCreditsAmt = objTaxCreditsAmt;
                }
                #endregion

                objIRS2290.BalanceDueAmt = (Convert.ToDecimal(TaxFromTaxComputation.ToString("N2")) + Convert.ToDecimal(AdditionalTaxAmount.ToString("N2"))) - Convert.ToDecimal(CreditAmount.ToString("N2"));

                if (objIRS2290.BalanceDueAmt < 0)
                    objIRS2290.BalanceDueAmt = 0.00M;

                #region IRS PAYMENT2

                Decimal BalanceDue = objIRS2290.BalanceDueAmt;

                if (BalanceDue > 0)
                {
                    if (paymentType != null)
                    {
                        /* Now Both Money Order or EFTPS will be same */
                        if (paymentType == "CMO" || paymentType == "EFTPS")
                        {
                            objIRS2290.EFTPSPaymentInd = Return2290_2020.CheckboxType.X;
                            objIRS2290.EFTPSPaymentIndSpecified = true;
                        }
                        /* Credit / Debit card payments */
                        else if (paymentType == "CDC")
                        {
                            objIRS2290.CreditDebitCardPaymentInd = Return2290_2020.CheckboxType.X;
                            objIRS2290.CreditDebitCardPaymentIndSpecified = true;

                        }
                        else if (paymentType == "DirectDebit" || paymentType == "EFW")
                        {
                            List<Return2290_2020.IRSPayment2> irsPmt2Collection = new List<Return2290_2020.IRSPayment2>();

                            Return2290_2020.IRSPayment2 objPayType = new Return2290_2020.IRSPayment2();

                            objPayType.PaymentAmt = Convert.ToDecimal(BalanceDue.ToString("N2"));
                            objPayType.RoutingTransitNum = Payment_RoutingTransitNo;
                            objPayType.BankAccountNum = Payment_Acc_No;

                            if (Payment_Acc_Type == "Checking" || Payment_Acc_Type == "0")
                                objPayType.BankAccountTypeCd = Return2290_2020.BankAccountType.Item1;
                            else if (Payment_Acc_Type == "Saving" || Payment_Acc_Type == "1")
                                objPayType.BankAccountTypeCd = Return2290_2020.BankAccountType.Item2;

                            objPayType.RequestedPaymentDt = Convert.ToDateTime(Payment_ReqPayDate);

                            objPayType.TaxpayerDaytimePhoneNum = Payment_Txpyer_Ph;

                            objPayType.documentId = "IRSPayment2";
                            //objPayType.documentName = "IRSPayment2";
                            objPayType.softwareId = Variables.SoftwareID;
                            objPayType.softwareVersion = Variables.SoftwareVersion;

                            irsPmt2Collection.Add(objPayType);

                            returnData.IRSPayment2 = irsPmt2Collection.ToArray();
                            documentCount += 1;
                        }
                    }
                }
                #endregion

                #region SUSPENDED VIN

                if (dsSuspendedVehicles.Tables[0].Rows.Count > 0)
                {
                    List<Return2290_2020.SuspendedVINInfoTypeVINDetail> objSusVehList = new List<Return2290_2020.SuspendedVINInfoTypeVINDetail>();

                    foreach (DataRow ldr_suspended in dsSuspendedVehicles.Tables[0].Rows)
                    {
                        Return2290_2020.SuspendedVINInfoTypeVINDetail objSusVeh = new Return2290_2020.SuspendedVINInfoTypeVINDetail();
                        objSusVeh.VIN = Convert.ToString(ldr_suspended["VIN"]);
                        objSusVehList.Add(objSusVeh);

                        #region ....
                        if (!Convert.ToBoolean(ldr_suspended["IsAgricultural"]))
                        {
                            objIRS2290.AgricMileageUsed7500OrLessInd = Return2290_2020.CheckboxType.X;
                            objIRS2290.AgricMileageUsed7500OrLessIndSpecified = true;
                        }
                        else if (Convert.ToBoolean(ldr_suspended["IsAgricultural"]))
                        {
                            objIRS2290.MileageUsed5000OrLessInd = Return2290_2020.CheckboxType.X;
                            objIRS2290.MileageUsed5000OrLessIndSpecified = true;
                        }

                        if (Convert.ToString(ldr_suspended["IsLogging"]) != "")
                        {
                            if (Convert.ToBoolean(ldr_suspended["IsLogging"]))
                                numTaxSuspended_LoggingVeh += 1;
                            else if (!Convert.ToBoolean(ldr_suspended["IsLogging"]))
                                numTaxSuspended_NonLoggingVeh += 1;
                        }
                        else
                        {
                            numTaxSuspended_NonLoggingVeh += 1;
                        }

                        #endregion
                    }

                    Return2290_2020.SuspendedVINStatementType objSusVehStmtType = new Return2290_2020.SuspendedVINStatementType();
                    objSusVehStmtType.SuspendedVINInfo = objSusVehList.ToArray();

                    if (numTaxSuspended_LoggingVeh != 0)
                        objIRS2290.TaxSuspendedLoggingVehCnt = numTaxSuspended_LoggingVeh.ToString();

                    if (numTaxSuspended_NonLoggingVeh != 0 && numTaxSuspended_LoggingVeh != 0)
                        objIRS2290.TaxSuspendedLoggingVehCnt = (numTaxSuspended_LoggingVeh + numTaxSuspended_NonLoggingVeh).ToString();
                    else
                    {
                        if (numTaxSuspended_NonLoggingVeh != 0)
                            objIRS2290.TaxSuspendedNonLoggingVehCnt = numTaxSuspended_NonLoggingVeh.ToString();
                    }

                    Return2290_2020.IRS2290TypeSuspendedVINReferenceTyp objSuspendedVINRef =
                        new Return2290_2020.IRS2290TypeSuspendedVINReferenceTyp();
                    List<string> docID_SuspendedVINStmt = new List<string>();
                    docID_SuspendedVINStmt.Add("SuspendedVINStatement");
                    objSuspendedVINRef.referenceDocumentId = docID_SuspendedVINStmt.ToArray();
                    objSuspendedVINRef.referenceDocumentName = "SuspendedVINStatement";


                    objIRS2290.SuspendedVINReferenceTyp = objSuspendedVINRef;
                }

                #endregion

                if (IsNotSubjectToTaxChecked)
                    objIRS2290.NotSubjectToTaxInd = Return2290_2020.CheckboxType.X;


                if (dsTGWIncreaseWorksheet.Tables.Count > 0 && IsAmendedReturn)
                {
                    /*
                    if (Convert.ToDecimal(AdditionalTaxAmount.ToString("N2")) > 0)
                    {
                        objIRS2290.TotalTaxComputationAmt = Convert.ToDecimal(AdditionalTaxAmount.ToString("N2"));
                        objIRS2290.TotalTaxComputationAmtSpecified = true;
                    }*/
                    if (Convert.ToDecimal(AdditionalTaxAmount.ToString("N2")) > 0)
                    {
                        objIRS2290.TotalTaxComputationAmt = 0.01m;//Convert.ToDecimal(TaxFromTaxComputation.ToString("N2"));
                        objIRS2290.TotalTaxComputationAmtSpecified = true;
                    }
                    else
                    {
                        objIRS2290.TotalTaxComputationAmtSpecified = false;
                    }
                }
                else
                {
                    if (Convert.ToDecimal(TaxFromTaxComputation.ToString("N2")) > 0)
                    {
                        objIRS2290.TotalTaxComputationAmt = Convert.ToDecimal(TaxFromTaxComputation.ToString("N2"));
                        objIRS2290.TotalTaxComputationAmtSpecified = true;
                    }
                    else
                    {
                        objIRS2290.TotalTaxComputationAmtSpecified = false;
                    }
                }
                objIRS2290.softwareId = Variables.SoftwareID;
                objIRS2290.softwareVersion = Variables.SoftwareVersion;
                objIRS2290.documentId = "IRS2290";
                objIRS2290.documentName = "IRS2290";

                returnData.IRS2290 = objIRS2290;
                documentCount += 1;

                #endregion

                #region IRS 2290 SCHEDULE1

                Return2290_2020.IRS2290Schedule1 irs2290Schedule1 = new Return2290_2020.IRS2290Schedule1();

                if (Convert.ToInt16(numTotalTaxableVeh) > 0)
                    irs2290Schedule1.TaxableVehicleCnt = numTotalTaxableVeh.ToString();

                int Count_taxSuspendedLogVeh = Convert.ToInt16(numTaxSuspended_LoggingVeh);
                int Count_taxSuspendedNonLogVeh = Convert.ToInt16(numTaxSuspended_NonLoggingVeh);
                if (Count_taxSuspendedLogVeh + Count_taxSuspendedNonLogVeh != 0)
                    irs2290Schedule1.TotalSuspendedVehicleCnt = Convert.ToString(Count_taxSuspendedLogVeh + Count_taxSuspendedNonLogVeh);


                if ((numTotalTaxableVeh + Count_taxSuspendedLogVeh + Count_taxSuspendedNonLogVeh) > 0)
                    irs2290Schedule1.VehicleCnt = Convert.ToString(numTotalTaxableVeh + Count_taxSuspendedLogVeh + Count_taxSuspendedNonLogVeh);
                else
                {
                    if (IsAmendedReturn)
                    {
                        if ((numTotalTaxableVeh + Count_taxSuspendedLogVeh + Count_taxSuspendedNonLogVeh) > 0)
                        {
                            irs2290Schedule1.VehicleCnt = Convert.ToString(numTotalTaxableVeh + Count_taxSuspendedLogVeh + Count_taxSuspendedNonLogVeh);
                        }
                        else
                            irs2290Schedule1.VehicleCnt = "1";
                    }
                }

                List<Return2290_2020.IRS2290Schedule1TypeVehicleReportTaxItem> objVehRptTaxItem =
                    new List<Return2290_2020.IRS2290Schedule1TypeVehicleReportTaxItem>();

                if (dsTaxableVehicles.Tables.Count > 0)
                    foreach (DataRow ldr in dsTaxableVehicles.Tables[0].Rows)
                    {
                        Return2290_2020.IRS2290Schedule1TypeVehicleReportTaxItem vrti =
                            new Return2290_2020.IRS2290Schedule1TypeVehicleReportTaxItem();
                        vrti.VehicleCategoryCd = Convert.ToString(ldr["WeightCategory"]);
                        vrti.VIN = Convert.ToString(ldr["VIN"]);
                        objVehRptTaxItem.Add(vrti);
                    }

                if (dsTGWIncreaseWorksheet.Tables.Count > 0)
                    foreach (DataRow ldr in dsTGWIncreaseWorksheet.Tables[0].Rows)
                    {
                        Return2290_2020.IRS2290Schedule1TypeVehicleReportTaxItem vrti =
                            new Return2290_2020.IRS2290Schedule1TypeVehicleReportTaxItem();
                        vrti.VehicleCategoryCd = Convert.ToString(ldr["WeightCategoryNew"]);
                        vrti.VIN = Convert.ToString(ldr["VIN"]);
                        objVehRptTaxItem.Add(vrti);
                    }

                if (dsSuspendedVehicles.Tables.Count > 0)
                {
                    foreach (DataRow ldr in dsSuspendedVehicles.Tables[0].Rows)
                    {
                        Return2290_2020.IRS2290Schedule1TypeVehicleReportTaxItem vrti = new
                            Return2290_2020.IRS2290Schedule1TypeVehicleReportTaxItem();
                        vrti.VehicleCategoryCd = "W";
                        vrti.VIN = Convert.ToString(ldr["VIN"]);
                        objVehRptTaxItem.Add(vrti);
                    }
                }

                irs2290Schedule1.VehicleReportTaxItem = objVehRptTaxItem.ToArray();

                irs2290Schedule1.documentId = "IRS2290Schedule1";
                irs2290Schedule1.documentName = "IRS2290Schedule1";
                irs2290Schedule1.softwareId = Variables.SoftwareID;
                irs2290Schedule1.softwareVersion = Variables.SoftwareVersion;

                returnData.IRS2290Schedule1 = irs2290Schedule1;
                documentCount += 1;

                #endregion

                //2020 VIN Correction 
                #region VIN CORRECTION  Explanation Statement
                if (IsVINCorrection)
                {
                    //Return2290_2020.IRS2290Schedule1 irs2290Schedule1 = new Return2290_2020.IRS2290Schedule1();

                    Return2290_2020.VINCorrectionExplanationStmtType VinCorExp = new Return2290_2020.VINCorrectionExplanationStmtType();

                    VinCorExp.ExplanationTxt = "VINCorrection update request. Update the previosuly accepted Submission to the NEW VIN attached with the IRS2290Schedule1";
                    //objIRS2290.VINCorrectionExplanationStmt = VinCorExp;

                    VinCorExp.documentId = "VINCorrectionExplan";
                    //VinCorExp.documentName = "VINCorrectionExplanationStmtType";
                    VinCorExp.softwareId = Variables.SoftwareID;
                    VinCorExp.softwareVersion = Variables.SoftwareVersion;

                    returnData.VINCorrectionExplanationStmt = VinCorExp;
                    documentCount += 1;
                }
                #endregion   // VIN CORRECTION  Explanation Statement

                #region CREDITS AMOUNT STATEMENT

                if (dsCreditVehicles.Tables[0].Rows.Count > 0)
                {
                    int rowCount = dsCreditVehicles.Tables[0].Rows.Count;
                    List<Return2290_2020.CreditsAmountInfoTypeDisposalReportingItem> dric =
                        new List<Return2290_2020.CreditsAmountInfoTypeDisposalReportingItem>();

                    foreach (DataRow ldr in dsCreditVehicles.Tables[0].Rows)
                    {
                        Return2290_2020.CreditsAmountInfoTypeDisposalReportingItem dri =
                            new Return2290_2020.CreditsAmountInfoTypeDisposalReportingItem();
                        dri.CreditsAmountExplanationTxt = Convert.ToString(ldr["Reason"]);
                        dri.DisposalReportingAmt = Convert.ToString(ldr["TaxAmount"]);
                        dri.DisposalReportingDt = Convert.ToDateTime(ldr["EffectiveDate"]);
                        dri.DisposalReportingVIN = Convert.ToString(ldr["VIN"]);

                        dric.Add(dri);
                    }

                    Return2290_2020.CreditsAmountStatement cas = new Return2290_2020.CreditsAmountStatement();
                    cas.CreditsAmountInfo = dric.ToArray();
                    cas.documentId = "CreditsAmountStatement";
                    cas.documentName = "CreditsAmountStatement";
                    cas.softwareId = Variables.SoftwareID;
                    cas.softwareVersion = Variables.SoftwareVersion;

                    Return2290_2020.CreditsAmountStatement[] casc = new Return2290_2020.CreditsAmountStatement[1];
                    casc[0] = cas;

                    returnData.CreditsAmountStatement = casc;
                    documentCount += 1;
                }
                #endregion

                #region STATEMENT IN SUPPORT OF SUSPENSION //Sold Suspended

                if (dsSoldSuspendedVehicles.Tables[0].Rows.Count > 0)
                {
                    List<Return2290_2020.StmtInSupportOfSuspensionInfoTypeVehicleSuspensionDetail> ic =
                        new List<Return2290_2020.StmtInSupportOfSuspensionInfoTypeVehicleSuspensionDetail>();

                    foreach (DataRow ldr in dsSoldSuspendedVehicles.Tables[0].Rows)
                    {
                        Return2290_2020.StmtInSupportOfSuspensionInfoTypeVehicleSuspensionDetail i =
                            new Return2290_2020.StmtInSupportOfSuspensionInfoTypeVehicleSuspensionDetail();
                        i.Dt = Convert.ToDateTime(ldr["DateSold"]);
                        Return2290_2020.BusinessNameType bnt = new Return2290_2020.BusinessNameType();
                        bnt.BusinessNameLine1Txt = Convert.ToString(ldr["Buyer"]);
                        i.BusinessName = bnt;
                        i.VIN = Convert.ToString(ldr["VIN"]);
                        ic.Add(i);
                    }

                    Return2290_2020.StmtInSupportOfSuspension sisosit = new Return2290_2020.StmtInSupportOfSuspension();
                    sisosit.StmtInSupportOfSuspensionInfo = ic.ToArray();

                    List<Return2290_2020.StmtInSupportOfSuspension> sisostc = new List<Return2290_2020.StmtInSupportOfSuspension>();
                    sisostc.Add(sisosit);

                    sisosit.documentId = "StatementInSupportOfSuspension";
                    sisosit.documentName = "StatementInSupportOfSuspension";
                    sisosit.softwareId = Variables.SoftwareID;
                    sisosit.softwareVersion = Variables.SoftwareVersion;

                    returnData.StmtInSupportOfSuspension = sisostc.ToArray();
                    documentCount += 1;
                }
                #endregion

                #region SUSPENDED VIN STATEMENT

                if (dsPriorYearMileageExceededVehicles.Tables[0].Rows.Count > 0)
                {
                    List<Return2290_2020.SuspendedVINInfoTypeVINDetail> objSVITVDList =
                        new List<Return2290_2020.SuspendedVINInfoTypeVINDetail>();
                    foreach (DataRow ldr in dsPriorYearMileageExceededVehicles.Tables[0].Rows)
                    {
                        Return2290_2020.SuspendedVINInfoTypeVINDetail objSVITVD = new Return2290_2020.SuspendedVINInfoTypeVINDetail();
                        objSVITVD.VIN = Convert.ToString(ldr["VIN"]);
                        objSVITVDList.Add(objSVITVD);
                    }

                    Return2290_2020.SuspendedVINStatement objSVS = new Return2290_2020.SuspendedVINStatement();
                    objSVS.SuspendedVINInfo = objSVITVDList.ToArray();
                    objSVS.documentId = "SuspendedVINStatement";
                    objSVS.documentName = "SuspendedVINStatement";
                    objSVS.softwareId = Variables.SoftwareID;
                    objSVS.softwareVersion = Variables.SoftwareVersion;

                    Return2290_2020.SuspendedVINStatement[] suspendedVINStmt = new Return2290_2020.SuspendedVINStatement[1];
                    suspendedVINStmt[0] = objSVS;

                    returnData.SuspendedVINStatement = suspendedVINStmt;
                    documentCount += 1;
                }
                #endregion

                #region TGW INCREASE WORKSHEET

                if (dsTGWIncreaseWorksheet.Tables[0].Rows.Count > 0)
                {
                    List<Return2290_2020.TGWIncreaseInfoType> tgwiitc = new List<Return2290_2020.TGWIncreaseInfoType>();
                    foreach (DataRow ldr in dsTGWIncreaseWorksheet.Tables[0].Rows)
                    {
                        Return2290_2020.TGWIncreaseInfoType tgwiit = new Return2290_2020.TGWIncreaseInfoType();

                        tgwiit.AdditionalTaxAmt = Convert.ToDecimal(ldr["TaxAmount"]);
                        tgwiit.AdditionalTaxAmtSpecified = true;
                        tgwiit.TGWIncreaseMonthNum = Convert.ToString(ldr["IncreasedMonthValue"]);
                        tgwiit.NewTaxAmt = Convert.ToDecimal(ldr["NewTaxAmount"]);
                        tgwiit.NewTaxAmtSpecified = true;
                        tgwiit.PreviousTaxAmt = Convert.ToDecimal(ldr["PreviousTaxAmount"]);
                        tgwiit.PreviousTaxAmtSpecified = true;
                        tgwiit.TGWCategoryCd = Convert.ToString(ldr["WeightCategoryNew"]);
                        tgwiitc.Add(tgwiit);
                    }

                    Return2290_2020.TGWIncreaseWorksheet tgwIncreaseWorksheet = new Return2290_2020.TGWIncreaseWorksheet();
                    tgwIncreaseWorksheet.TGWIncreaseInfo = tgwiitc.ToArray();
                    tgwIncreaseWorksheet.documentId = "TGWIncreaseWorksheet";
                    //tgwIncreaseWorksheet.__documentName = "TGWIncreaseWorksheet";
                    tgwIncreaseWorksheet.softwareId = Variables.SoftwareID;
                    tgwIncreaseWorksheet.softwareVersion = Variables.SoftwareVersion;

                    List<Return2290_2020.TGWIncreaseWorksheet> tgwiwc = new List<Return2290_2020.TGWIncreaseWorksheet>();
                    tgwiwc.Add(tgwIncreaseWorksheet);

                    returnData.TGWIncreaseWorksheet = tgwiwc.ToArray();
                    documentCount += 1;
                }
                #endregion

                returnData.__documentCount = documentCount.ToString();

                #endregion

                #region RETURN

                Return2290_2020.Return return2290 = new Return2290_2020.Return();
                return2290.ReturnData = returnData;
                return2290.ReturnHeader = returnHeaderType;
                return2290.returnVersion = Variables.SoftwareVersion;

                #endregion

                #region Create XML file

                IFormatter ft = new BinaryFormatter();

                ft.Serialize(mStream, return2290);

                mStream.Position = 0;

                Return2290_2020.Return irsForm2 = (Return2290_2020.Return)ft.Deserialize(mStream);

                s.Serialize(fStream2, irsForm2);

                if (mStream != null) mStream.Close();

                if (fStream2 != null) fStream2.Close();

                if (fStream != null) fStream.Close();

                try
                {
                    XmlDocument xdoc = new XmlDocument();
                    xdoc.PreserveWhitespace = false;
                    xdoc.Load("C:\\xml\\submission.xml");
                    xdoc.DocumentElement.SetAttribute("xmlns:efile", "http://www.irs.gov/efile");
                    XmlTextWriter xmlWriter = new XmlTextWriter("C:\\" + @"\xml\submission.xml", new UTF8Encoding(false));
                    xdoc.Save(xmlWriter);
                    xmlWriter.Flush();
                    xmlWriter.Close();
                }
                catch { }

                #endregion

            }
            catch (Exception ex)
            {
                if (fStream != null) fStream.Close();
                if (fStream2 != null) fStream2.Close();
                if (mStream != null) mStream.Close();
                throw ex;
            }
        }
        #endregion


        #region createReturnXml_2290_2019

        /* Vishwa New 2019 Changes, for the XML building for 2019 */
        public void createReturnXml_2290_2019
        (
        #region Parameters
                bool IsAddressChange, bool IsAmendedReturn, bool IsVINCorrection, bool IsFinalReturn,
                bool IsNotSubjectToTaxChecked, bool IsConsentToDiscloseYes, string AmendedMonth, string TaxYear,
                string First_Used_Date, string Filer_AddressLine1, string Filer_City, string Filer_EIN,
                string Filer_Foreign_Country, string Filer_Name, string Filer_NameControl, string Filer_State,
                string Filer_ZIPCode, string Officer_Name, string Officer_Phone, string Officer_PIN,
                string Officer_Title, string Officer_email, string ThirdPartyDesigne_Name, string ThirdPartyDesigne_Phone,
                string ThirdPartyDesigne_PIN, string Preparer_Firm, string Preparer_EIN, string Preparer_City,
                string Preparer_State, string Preparer_Zip, string Preparer_Country, string Preparer_Address,
                string Preparer_ForeignPhone, string Preparer_Phone, string Preparer_Email, string Preparer_Name,
                string Preparer_PTIN_SSN, bool IsPreparerHasPTIN, bool IsPreparerSelfEmployed, string paymentType,
                string Payment_Acc_No, string Payment_Acc_Type, string Payment_ReqPayDate, string Payment_RoutingTransitNo,
                string Payment_Txpyer_Ph, DataSet dsTaxableVehicles, DataSet dsCreditVehicles, DataSet dsSuspendedVehicles,
                DataSet dsSoldSuspendedVehicles, DataSet dsPriorYearMileageExceededVehicles, DataSet dsTGWIncreaseWorksheet, DataSet dsTaxComputation,
                string soldSuspendedVehicles, decimal TaxFromTaxComputation, decimal AdditionalTaxAmount, decimal CreditAmount,
                string TotalVehicles
        #endregion
)
        {
            int documentCount = 0;
            FileStream fStream = null;
            FileStream fStream2 = null;
            MemoryStream mStream = null;

            int numTotalTaxableVeh = 0;
            int numTaxSuspended_LoggingVeh = 0;
            int numTaxSuspended_NonLoggingVeh = 0;

            int numNonLoggingVehicleCnt = 0;
            int numLoggingVehicleCnt = 0;

            try
            {
                if (!Directory.Exists(path2CreateZipFile + @"\xml"))
                    Directory.CreateDirectory(path2CreateZipFile + @"\xml");

                fStream2 = new FileStream(path2CreateZipFile + @"\xml\submission.xml", FileMode.Create);

                byte[] byteArray = new byte[90000];
                mStream = new MemoryStream(byteArray);
                XmlSerializer s = new XmlSerializer(typeof(Return2290_2019.Return));

                #region RETRUN_HEADER

                Return2290_2019.ReturnHeaderType returnHeaderType = new Return2290_2019.ReturnHeaderType();

                returnHeaderType.ReturnTs = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz");

                returnHeaderType.FirstUsedDt = First_Used_Date;

                returnHeaderType.MultSoftwarePackagesUsedInd = false;
                returnHeaderType.PINEnteredByCd = Return2290_2019.ReturnHeaderTypePINEnteredByCd.Taxpayer;
                returnHeaderType.PINEnteredByCdSpecified = true;
                returnHeaderType.ReturnTypeCd = Return2290_2019.ReturnHeaderTypeReturnTypeCd.Item2290;
                returnHeaderType.SignatureOptionCd = Return2290_2019.ReturnHeaderTypeSignatureOptionCd.PINNumber;
                returnHeaderType.SignatureOptionCdSpecified = true;

                #region ORIGINATOR

                #region PRACTIONER PIN

                Return2290_2019.ReturnHeaderTypeOriginatorGrpPractitionerPINGrp objPractitionerPIN =
                    new Return2290_2019.ReturnHeaderTypeOriginatorGrpPractitionerPINGrp();
                objPractitionerPIN.EFIN = Variables.EFIN;
                objPractitionerPIN.PIN = Variables.PIN;

                #endregion

                Return2290_2019.ReturnHeaderTypeOriginatorGrp originator = new Return2290_2019.ReturnHeaderTypeOriginatorGrp();
                originator.EFIN = Variables.EFIN;
                originator.OriginatorTypeCd = Return2290_2019.OriginatorType.ERO;
                originator.PractitionerPINGrp = objPractitionerPIN;
                returnHeaderType.OriginatorGrp = originator;

                #endregion

                #region FILER
                Return2290_2019.ReturnHeaderTypeFiler objFiler = new Return2290_2019.ReturnHeaderTypeFiler();

                objFiler.EIN = Filer_EIN;
                objFiler.BusinessNameLine1Txt = Filer_Name; //max_length=60
                objFiler.BusinessNameControlTxt = Filer_NameControl;

                if (Filer_Foreign_Country == "" || Filer_Foreign_Country == "US")
                {
                    #region FILER US ADDRESS

                    Return2290_2019.USAddressType usAdd_Filer = new Return2290_2019.USAddressType();

                    if (Filer_AddressLine1.Length > 35)
                    {
                        string Filer_AddressLine2 = string.Empty;
                        usAdd_Filer.AddressLine1 = Filer_AddressLine1.Substring(0, 35).Trim();
                        Filer_AddressLine2 = Filer_AddressLine1.Substring(35);
                        usAdd_Filer.AddressLine2 = Filer_AddressLine2.Trim();

                    }
                    else
                    {
                        usAdd_Filer.AddressLine1 = Filer_AddressLine1.Trim();
                    }
                    usAdd_Filer.City = Filer_City.Trim();

                    switch (Filer_State)
                    {
                        #region Filer_State
                        case "AA": usAdd_Filer.State = Return2290_2019.StateType.AA; break;
                        case "AE": usAdd_Filer.State = Return2290_2019.StateType.AE; break;
                        case "AK": usAdd_Filer.State = Return2290_2019.StateType.AK; break;
                        case "AL": usAdd_Filer.State = Return2290_2019.StateType.AL; break;
                        case "AP": usAdd_Filer.State = Return2290_2019.StateType.AP; break;
                        case "AR": usAdd_Filer.State = Return2290_2019.StateType.AR; break;
                        case "AS": usAdd_Filer.State = Return2290_2019.StateType.AS; break;
                        case "AZ": usAdd_Filer.State = Return2290_2019.StateType.AZ; break;
                        case "CA": usAdd_Filer.State = Return2290_2019.StateType.CA; break;
                        case "CO": usAdd_Filer.State = Return2290_2019.StateType.CO; break;
                        case "CT": usAdd_Filer.State = Return2290_2019.StateType.CT; break;
                        case "DC": usAdd_Filer.State = Return2290_2019.StateType.DC; break;
                        case "DE": usAdd_Filer.State = Return2290_2019.StateType.DE; break;
                        case "FL": usAdd_Filer.State = Return2290_2019.StateType.FL; break;
                        case "FM": usAdd_Filer.State = Return2290_2019.StateType.FM; break;
                        case "GA": usAdd_Filer.State = Return2290_2019.StateType.GA; break;
                        case "GU": usAdd_Filer.State = Return2290_2019.StateType.GU; break;
                        case "HI": usAdd_Filer.State = Return2290_2019.StateType.HI; break;
                        case "IA": usAdd_Filer.State = Return2290_2019.StateType.IA; break;
                        case "ID": usAdd_Filer.State = Return2290_2019.StateType.ID; break;
                        case "IL": usAdd_Filer.State = Return2290_2019.StateType.IL; break;
                        case "IN": usAdd_Filer.State = Return2290_2019.StateType.IN; break;
                        case "KS": usAdd_Filer.State = Return2290_2019.StateType.KS; break;
                        case "KY": usAdd_Filer.State = Return2290_2019.StateType.KY; break;
                        case "LA": usAdd_Filer.State = Return2290_2019.StateType.LA; break;
                        case "MA": usAdd_Filer.State = Return2290_2019.StateType.MA; break;
                        case "MD": usAdd_Filer.State = Return2290_2019.StateType.MD; break;
                        case "ME": usAdd_Filer.State = Return2290_2019.StateType.ME; break;
                        case "MH": usAdd_Filer.State = Return2290_2019.StateType.MH; break;
                        case "MI": usAdd_Filer.State = Return2290_2019.StateType.MI; break;
                        case "MN": usAdd_Filer.State = Return2290_2019.StateType.MN; break;
                        case "MO": usAdd_Filer.State = Return2290_2019.StateType.MO; break;
                        case "MP": usAdd_Filer.State = Return2290_2019.StateType.MP; break;
                        case "MS": usAdd_Filer.State = Return2290_2019.StateType.MS; break;
                        case "MT": usAdd_Filer.State = Return2290_2019.StateType.MT; break;
                        case "NC": usAdd_Filer.State = Return2290_2019.StateType.NC; break;
                        case "ND": usAdd_Filer.State = Return2290_2019.StateType.ND; break;
                        case "NE": usAdd_Filer.State = Return2290_2019.StateType.NE; break;
                        case "NH": usAdd_Filer.State = Return2290_2019.StateType.NH; break;
                        case "NJ": usAdd_Filer.State = Return2290_2019.StateType.NJ; break;
                        case "NM": usAdd_Filer.State = Return2290_2019.StateType.NM; break;
                        case "NV": usAdd_Filer.State = Return2290_2019.StateType.NV; break;
                        case "NY": usAdd_Filer.State = Return2290_2019.StateType.NY; break;
                        case "OH": usAdd_Filer.State = Return2290_2019.StateType.OH; break;
                        case "OK": usAdd_Filer.State = Return2290_2019.StateType.OK; break;
                        case "OR": usAdd_Filer.State = Return2290_2019.StateType.OR; break;
                        case "PA": usAdd_Filer.State = Return2290_2019.StateType.PA; break;
                        case "PR": usAdd_Filer.State = Return2290_2019.StateType.PR; break;
                        case "PW": usAdd_Filer.State = Return2290_2019.StateType.PW; break;
                        case "RI": usAdd_Filer.State = Return2290_2019.StateType.RI; break;
                        case "SC": usAdd_Filer.State = Return2290_2019.StateType.SC; break;
                        case "SD": usAdd_Filer.State = Return2290_2019.StateType.SD; break;
                        case "TN": usAdd_Filer.State = Return2290_2019.StateType.TN; break;
                        case "TX": usAdd_Filer.State = Return2290_2019.StateType.TX; break;
                        case "UT": usAdd_Filer.State = Return2290_2019.StateType.UT; break;
                        case "VA": usAdd_Filer.State = Return2290_2019.StateType.VA; break;
                        case "VI": usAdd_Filer.State = Return2290_2019.StateType.VI; break;
                        case "VT": usAdd_Filer.State = Return2290_2019.StateType.VT; break;
                        case "WA": usAdd_Filer.State = Return2290_2019.StateType.WA; break;
                        case "WI": usAdd_Filer.State = Return2290_2019.StateType.WI; break;
                        case "WV": usAdd_Filer.State = Return2290_2019.StateType.WV; break;
                        case "WY": usAdd_Filer.State = Return2290_2019.StateType.WY; break;
                        #endregion
                    }

                    usAdd_Filer.ZIPCode = Filer_ZIPCode;

                    objFiler.Item = usAdd_Filer;
                    #endregion
                }
                else
                {
                    #region FILER FOREIGN ADDRESS
                    Return2290_2019.ForeignAddressType foreignAdd_Filer = new Return2290_2019.ForeignAddressType();

                    if (Filer_AddressLine1.Length > 35)
                    {
                        string Filer_AddressLine2 = string.Empty;
                        foreignAdd_Filer.AddressLine1 = Filer_AddressLine1.Substring(0, 35).Trim();
                        Filer_AddressLine2 = Filer_AddressLine1.Substring(35);
                        foreignAdd_Filer.AddressLine2 = Filer_AddressLine2.Trim();
                    }
                    else
                    {
                        foreignAdd_Filer.AddressLine1 = Filer_AddressLine1.Trim();
                    }

                    foreignAdd_Filer.City = Filer_City.Trim();

                    if (Filer_State.Length > 0)
                        foreignAdd_Filer.ProvinceOrState = Filer_State;

                    switch (Filer_Foreign_Country)
                    {
                        #region Filer_Foreign_Country
                        case "AA": foreignAdd_Filer.Country = Return2290_2019.CountryType.AA; break;
                        case "AC": foreignAdd_Filer.Country = Return2290_2019.CountryType.AC; break;
                        case "AE": foreignAdd_Filer.Country = Return2290_2019.CountryType.AE; break;
                        case "AF": foreignAdd_Filer.Country = Return2290_2019.CountryType.AF; break;
                        case "AG": foreignAdd_Filer.Country = Return2290_2019.CountryType.AG; break;
                        case "AJ": foreignAdd_Filer.Country = Return2290_2019.CountryType.AJ; break;
                        case "AL": foreignAdd_Filer.Country = Return2290_2019.CountryType.AL; break;
                        case "AM": foreignAdd_Filer.Country = Return2290_2019.CountryType.AM; break;
                        case "AN": foreignAdd_Filer.Country = Return2290_2019.CountryType.AN; break;
                        case "AO": foreignAdd_Filer.Country = Return2290_2019.CountryType.AO; break;
                        case "AR": foreignAdd_Filer.Country = Return2290_2019.CountryType.AR; break;
                        case "AS": foreignAdd_Filer.Country = Return2290_2019.CountryType.AS; break;
                        case "AT": foreignAdd_Filer.Country = Return2290_2019.CountryType.AT; break;
                        case "AU": foreignAdd_Filer.Country = Return2290_2019.CountryType.AU; break;
                        case "AV": foreignAdd_Filer.Country = Return2290_2019.CountryType.AV; break;
                        case "AX": foreignAdd_Filer.Country = Return2290_2019.CountryType.AX; break;
                        case "AY": foreignAdd_Filer.Country = Return2290_2019.CountryType.AY; break;
                        case "BA": foreignAdd_Filer.Country = Return2290_2019.CountryType.BA; break;
                        case "BB": foreignAdd_Filer.Country = Return2290_2019.CountryType.BB; break;
                        case "BC": foreignAdd_Filer.Country = Return2290_2019.CountryType.BC; break;
                        case "BD": foreignAdd_Filer.Country = Return2290_2019.CountryType.BD; break;
                        case "BE": foreignAdd_Filer.Country = Return2290_2019.CountryType.BE; break;
                        case "BF": foreignAdd_Filer.Country = Return2290_2019.CountryType.BF; break;
                        case "BG": foreignAdd_Filer.Country = Return2290_2019.CountryType.BG; break;
                        case "BH": foreignAdd_Filer.Country = Return2290_2019.CountryType.BH; break;
                        case "BK": foreignAdd_Filer.Country = Return2290_2019.CountryType.BK; break;
                        case "BL": foreignAdd_Filer.Country = Return2290_2019.CountryType.BL; break;
                        case "BM": foreignAdd_Filer.Country = Return2290_2019.CountryType.BM; break;
                        case "BN": foreignAdd_Filer.Country = Return2290_2019.CountryType.BN; break;
                        case "BO": foreignAdd_Filer.Country = Return2290_2019.CountryType.BO; break;
                        case "BP": foreignAdd_Filer.Country = Return2290_2019.CountryType.BP; break;
                        case "BQ": foreignAdd_Filer.Country = Return2290_2019.CountryType.BQ; break;
                        case "BR": foreignAdd_Filer.Country = Return2290_2019.CountryType.BR; break;
                        case "BT": foreignAdd_Filer.Country = Return2290_2019.CountryType.BT; break;
                        case "BU": foreignAdd_Filer.Country = Return2290_2019.CountryType.BU; break;
                        case "BV": foreignAdd_Filer.Country = Return2290_2019.CountryType.BV; break;
                        case "BX": foreignAdd_Filer.Country = Return2290_2019.CountryType.BX; break;
                        case "BY": foreignAdd_Filer.Country = Return2290_2019.CountryType.BY; break;
                        case "CA": foreignAdd_Filer.Country = Return2290_2019.CountryType.CA; break;
                        case "CB": foreignAdd_Filer.Country = Return2290_2019.CountryType.CB; break;
                        case "CD": foreignAdd_Filer.Country = Return2290_2019.CountryType.CD; break;
                        case "CE": foreignAdd_Filer.Country = Return2290_2019.CountryType.CE; break;
                        case "CF": foreignAdd_Filer.Country = Return2290_2019.CountryType.CF; break;
                        case "CG": foreignAdd_Filer.Country = Return2290_2019.CountryType.CG; break;
                        case "CH": foreignAdd_Filer.Country = Return2290_2019.CountryType.CH; break;
                        case "CI": foreignAdd_Filer.Country = Return2290_2019.CountryType.CI; break;
                        case "CJ": foreignAdd_Filer.Country = Return2290_2019.CountryType.CJ; break;
                        case "CK": foreignAdd_Filer.Country = Return2290_2019.CountryType.CK; break;
                        case "CM": foreignAdd_Filer.Country = Return2290_2019.CountryType.CM; break;
                        case "CN": foreignAdd_Filer.Country = Return2290_2019.CountryType.CN; break;
                        case "CO": foreignAdd_Filer.Country = Return2290_2019.CountryType.CO; break;
                        case "CR": foreignAdd_Filer.Country = Return2290_2019.CountryType.CR; break;
                        case "CS": foreignAdd_Filer.Country = Return2290_2019.CountryType.CS; break;
                        case "CT": foreignAdd_Filer.Country = Return2290_2019.CountryType.CT; break;
                        case "CU": foreignAdd_Filer.Country = Return2290_2019.CountryType.CU; break;
                        case "CV": foreignAdd_Filer.Country = Return2290_2019.CountryType.CV; break;
                        case "CW": foreignAdd_Filer.Country = Return2290_2019.CountryType.CW; break;
                        case "CY": foreignAdd_Filer.Country = Return2290_2019.CountryType.CY; break;
                        case "DA": foreignAdd_Filer.Country = Return2290_2019.CountryType.DA; break;
                        case "DJ": foreignAdd_Filer.Country = Return2290_2019.CountryType.DJ; break;
                        case "DO": foreignAdd_Filer.Country = Return2290_2019.CountryType.DO; break;
                        case "DR": foreignAdd_Filer.Country = Return2290_2019.CountryType.DR; break;
                        case "DX": foreignAdd_Filer.Country = Return2290_2019.CountryType.DX; break;
                        case "EC": foreignAdd_Filer.Country = Return2290_2019.CountryType.EC; break;
                        case "EG": foreignAdd_Filer.Country = Return2290_2019.CountryType.EG; break;
                        case "EI": foreignAdd_Filer.Country = Return2290_2019.CountryType.EI; break;
                        case "EK": foreignAdd_Filer.Country = Return2290_2019.CountryType.EK; break;
                        case "EN": foreignAdd_Filer.Country = Return2290_2019.CountryType.EN; break;
                        case "ER": foreignAdd_Filer.Country = Return2290_2019.CountryType.ER; break;
                        case "ES": foreignAdd_Filer.Country = Return2290_2019.CountryType.ES; break;
                        case "ET": foreignAdd_Filer.Country = Return2290_2019.CountryType.ET; break;
                        case "EZ": foreignAdd_Filer.Country = Return2290_2019.CountryType.EZ; break;
                        case "FI": foreignAdd_Filer.Country = Return2290_2019.CountryType.FI; break;
                        case "FJ": foreignAdd_Filer.Country = Return2290_2019.CountryType.FJ; break;
                        case "FK": foreignAdd_Filer.Country = Return2290_2019.CountryType.FK; break;
                        case "FO": foreignAdd_Filer.Country = Return2290_2019.CountryType.FO; break;
                        case "FP": foreignAdd_Filer.Country = Return2290_2019.CountryType.FP; break;
                        case "FQ": foreignAdd_Filer.Country = Return2290_2019.CountryType.FQ; break;
                        case "FR": foreignAdd_Filer.Country = Return2290_2019.CountryType.FR; break;
                        case "FS": foreignAdd_Filer.Country = Return2290_2019.CountryType.FS; break;
                        case "GA": foreignAdd_Filer.Country = Return2290_2019.CountryType.GA; break;
                        case "GB": foreignAdd_Filer.Country = Return2290_2019.CountryType.GB; break;
                        case "GG": foreignAdd_Filer.Country = Return2290_2019.CountryType.GG; break;
                        case "GH": foreignAdd_Filer.Country = Return2290_2019.CountryType.GH; break;
                        case "GI": foreignAdd_Filer.Country = Return2290_2019.CountryType.GI; break;
                        case "GJ": foreignAdd_Filer.Country = Return2290_2019.CountryType.GJ; break;
                        case "GK": foreignAdd_Filer.Country = Return2290_2019.CountryType.GK; break;
                        case "GL": foreignAdd_Filer.Country = Return2290_2019.CountryType.GL; break;
                        case "GM": foreignAdd_Filer.Country = Return2290_2019.CountryType.GM; break;
                        case "GR": foreignAdd_Filer.Country = Return2290_2019.CountryType.GR; break;
                        case "GT": foreignAdd_Filer.Country = Return2290_2019.CountryType.GT; break;
                        case "GV": foreignAdd_Filer.Country = Return2290_2019.CountryType.GV; break;
                        case "GY": foreignAdd_Filer.Country = Return2290_2019.CountryType.GY; break;
                        case "HA": foreignAdd_Filer.Country = Return2290_2019.CountryType.HA; break;
                        case "HK": foreignAdd_Filer.Country = Return2290_2019.CountryType.HK; break;
                        case "HM": foreignAdd_Filer.Country = Return2290_2019.CountryType.HM; break;
                        case "HO": foreignAdd_Filer.Country = Return2290_2019.CountryType.HO; break;
                        case "HQ": foreignAdd_Filer.Country = Return2290_2019.CountryType.HQ; break;
                        case "HR": foreignAdd_Filer.Country = Return2290_2019.CountryType.HR; break;
                        case "HU": foreignAdd_Filer.Country = Return2290_2019.CountryType.HU; break;
                        case "IC": foreignAdd_Filer.Country = Return2290_2019.CountryType.IC; break;
                        case "ID": foreignAdd_Filer.Country = Return2290_2019.CountryType.ID; break;
                        case "IM": foreignAdd_Filer.Country = Return2290_2019.CountryType.IM; break;
                        case "IO": foreignAdd_Filer.Country = Return2290_2019.CountryType.IO; break;
                        case "IP": foreignAdd_Filer.Country = Return2290_2019.CountryType.IP; break;
                        case "IR": foreignAdd_Filer.Country = Return2290_2019.CountryType.IR; break;
                        case "IS": foreignAdd_Filer.Country = Return2290_2019.CountryType.IS; break;
                        case "IT": foreignAdd_Filer.Country = Return2290_2019.CountryType.IT; break;
                        case "IV": foreignAdd_Filer.Country = Return2290_2019.CountryType.IV; break;
                        case "IZ": foreignAdd_Filer.Country = Return2290_2019.CountryType.IZ; break;
                        case "JA": foreignAdd_Filer.Country = Return2290_2019.CountryType.JA; break;
                        case "JE": foreignAdd_Filer.Country = Return2290_2019.CountryType.JE; break;
                        case "JM": foreignAdd_Filer.Country = Return2290_2019.CountryType.JM; break;
                        case "JN": foreignAdd_Filer.Country = Return2290_2019.CountryType.JN; break;
                        case "JO": foreignAdd_Filer.Country = Return2290_2019.CountryType.JO; break;
                        case "JQ": foreignAdd_Filer.Country = Return2290_2019.CountryType.JQ; break;
                        case "KE": foreignAdd_Filer.Country = Return2290_2019.CountryType.KE; break;
                        case "KG": foreignAdd_Filer.Country = Return2290_2019.CountryType.KG; break;
                        case "KN": foreignAdd_Filer.Country = Return2290_2019.CountryType.KN; break;
                        case "KQ": foreignAdd_Filer.Country = Return2290_2019.CountryType.KQ; break;
                        case "KR": foreignAdd_Filer.Country = Return2290_2019.CountryType.KR; break;
                        case "KS": foreignAdd_Filer.Country = Return2290_2019.CountryType.KS; break;
                        case "KT": foreignAdd_Filer.Country = Return2290_2019.CountryType.KT; break;
                        case "KU": foreignAdd_Filer.Country = Return2290_2019.CountryType.KU; break;
                        case "KV": foreignAdd_Filer.Country = Return2290_2019.CountryType.KV; break;
                        case "KZ": foreignAdd_Filer.Country = Return2290_2019.CountryType.KZ; break;
                        case "LA": foreignAdd_Filer.Country = Return2290_2019.CountryType.LA; break;
                        case "LE": foreignAdd_Filer.Country = Return2290_2019.CountryType.LE; break;
                        case "LG": foreignAdd_Filer.Country = Return2290_2019.CountryType.LG; break;
                        case "LH": foreignAdd_Filer.Country = Return2290_2019.CountryType.LH; break;
                        case "LI": foreignAdd_Filer.Country = Return2290_2019.CountryType.LI; break;
                        case "LO": foreignAdd_Filer.Country = Return2290_2019.CountryType.LO; break;
                        case "LQ": foreignAdd_Filer.Country = Return2290_2019.CountryType.LQ; break;
                        case "LS": foreignAdd_Filer.Country = Return2290_2019.CountryType.LS; break;
                        case "LT": foreignAdd_Filer.Country = Return2290_2019.CountryType.LT; break;
                        case "LU": foreignAdd_Filer.Country = Return2290_2019.CountryType.LU; break;
                        case "LY": foreignAdd_Filer.Country = Return2290_2019.CountryType.LY; break;
                        case "MA": foreignAdd_Filer.Country = Return2290_2019.CountryType.MA; break;
                        case "MC": foreignAdd_Filer.Country = Return2290_2019.CountryType.MC; break;
                        case "MD": foreignAdd_Filer.Country = Return2290_2019.CountryType.MD; break;
                        case "MG": foreignAdd_Filer.Country = Return2290_2019.CountryType.MG; break;
                        case "MH": foreignAdd_Filer.Country = Return2290_2019.CountryType.MH; break;
                        case "MI": foreignAdd_Filer.Country = Return2290_2019.CountryType.MI; break;
                        case "MJ": foreignAdd_Filer.Country = Return2290_2019.CountryType.MJ; break;
                        case "MK": foreignAdd_Filer.Country = Return2290_2019.CountryType.MK; break;
                        case "ML": foreignAdd_Filer.Country = Return2290_2019.CountryType.ML; break;
                        case "MN": foreignAdd_Filer.Country = Return2290_2019.CountryType.MN; break;
                        case "MO": foreignAdd_Filer.Country = Return2290_2019.CountryType.MO; break;
                        case "MP": foreignAdd_Filer.Country = Return2290_2019.CountryType.MP; break;
                        case "MQ": foreignAdd_Filer.Country = Return2290_2019.CountryType.MQ; break;
                        case "MR": foreignAdd_Filer.Country = Return2290_2019.CountryType.MR; break;
                        case "MT": foreignAdd_Filer.Country = Return2290_2019.CountryType.MT; break;
                        case "MU": foreignAdd_Filer.Country = Return2290_2019.CountryType.MU; break;
                        case "MV": foreignAdd_Filer.Country = Return2290_2019.CountryType.MV; break;
                        case "MX": foreignAdd_Filer.Country = Return2290_2019.CountryType.MX; break;
                        case "MY": foreignAdd_Filer.Country = Return2290_2019.CountryType.MY; break;
                        case "MZ": foreignAdd_Filer.Country = Return2290_2019.CountryType.MZ; break;
                        case "NC": foreignAdd_Filer.Country = Return2290_2019.CountryType.NC; break;
                        case "NE": foreignAdd_Filer.Country = Return2290_2019.CountryType.NE; break;
                        case "NF": foreignAdd_Filer.Country = Return2290_2019.CountryType.NF; break;
                        case "NG": foreignAdd_Filer.Country = Return2290_2019.CountryType.NG; break;
                        case "NH": foreignAdd_Filer.Country = Return2290_2019.CountryType.NH; break;
                        case "NI": foreignAdd_Filer.Country = Return2290_2019.CountryType.NI; break;
                        case "NL": foreignAdd_Filer.Country = Return2290_2019.CountryType.NL; break;
                        case "NN": foreignAdd_Filer.Country = Return2290_2019.CountryType.NN; break;
                        case "NO": foreignAdd_Filer.Country = Return2290_2019.CountryType.NO; break;
                        case "NP": foreignAdd_Filer.Country = Return2290_2019.CountryType.NP; break;
                        case "NR": foreignAdd_Filer.Country = Return2290_2019.CountryType.NR; break;
                        case "NS": foreignAdd_Filer.Country = Return2290_2019.CountryType.NS; break;
                        case "NU": foreignAdd_Filer.Country = Return2290_2019.CountryType.NU; break;
                        case "NZ": foreignAdd_Filer.Country = Return2290_2019.CountryType.NZ; break;
                        case "PA": foreignAdd_Filer.Country = Return2290_2019.CountryType.PA; break;
                        case "PC": foreignAdd_Filer.Country = Return2290_2019.CountryType.PC; break;
                        case "PE": foreignAdd_Filer.Country = Return2290_2019.CountryType.PE; break;
                        case "PF": foreignAdd_Filer.Country = Return2290_2019.CountryType.PF; break;
                        case "PG": foreignAdd_Filer.Country = Return2290_2019.CountryType.PG; break;
                        case "PK": foreignAdd_Filer.Country = Return2290_2019.CountryType.PK; break;
                        case "PL": foreignAdd_Filer.Country = Return2290_2019.CountryType.PL; break;
                        case "PM": foreignAdd_Filer.Country = Return2290_2019.CountryType.PM; break;
                        case "PO": foreignAdd_Filer.Country = Return2290_2019.CountryType.PO; break;
                        case "PP": foreignAdd_Filer.Country = Return2290_2019.CountryType.PP; break;
                        case "PU": foreignAdd_Filer.Country = Return2290_2019.CountryType.PU; break;
                        case "QA": foreignAdd_Filer.Country = Return2290_2019.CountryType.QA; break;
                        case "RI": foreignAdd_Filer.Country = Return2290_2019.CountryType.RI; break;
                        case "RN": foreignAdd_Filer.Country = Return2290_2019.CountryType.RN; break;
                        case "RO": foreignAdd_Filer.Country = Return2290_2019.CountryType.RO; break;
                        case "RP": foreignAdd_Filer.Country = Return2290_2019.CountryType.RP; break;
                        case "RS": foreignAdd_Filer.Country = Return2290_2019.CountryType.RS; break;
                        case "RW": foreignAdd_Filer.Country = Return2290_2019.CountryType.RW; break;
                        case "SA": foreignAdd_Filer.Country = Return2290_2019.CountryType.SA; break;
                        case "SB": foreignAdd_Filer.Country = Return2290_2019.CountryType.SB; break;
                        case "SC": foreignAdd_Filer.Country = Return2290_2019.CountryType.SC; break;
                        case "SE": foreignAdd_Filer.Country = Return2290_2019.CountryType.SE; break;
                        case "SF": foreignAdd_Filer.Country = Return2290_2019.CountryType.SF; break;
                        case "SG": foreignAdd_Filer.Country = Return2290_2019.CountryType.SG; break;
                        case "SH": foreignAdd_Filer.Country = Return2290_2019.CountryType.SH; break;
                        case "SI": foreignAdd_Filer.Country = Return2290_2019.CountryType.SI; break;
                        case "SL": foreignAdd_Filer.Country = Return2290_2019.CountryType.SL; break;
                        case "SM": foreignAdd_Filer.Country = Return2290_2019.CountryType.SM; break;
                        case "SN": foreignAdd_Filer.Country = Return2290_2019.CountryType.SN; break;
                        case "SO": foreignAdd_Filer.Country = Return2290_2019.CountryType.SO; break;
                        case "SP": foreignAdd_Filer.Country = Return2290_2019.CountryType.SP; break;
                        case "ST": foreignAdd_Filer.Country = Return2290_2019.CountryType.ST; break;
                        case "SU": foreignAdd_Filer.Country = Return2290_2019.CountryType.SU; break;
                        case "SV": foreignAdd_Filer.Country = Return2290_2019.CountryType.SV; break;
                        case "SW": foreignAdd_Filer.Country = Return2290_2019.CountryType.SW; break;
                        case "SX": foreignAdd_Filer.Country = Return2290_2019.CountryType.SX; break;
                        case "SY": foreignAdd_Filer.Country = Return2290_2019.CountryType.SY; break;
                        case "SZ": foreignAdd_Filer.Country = Return2290_2019.CountryType.SZ; break;
                        case "TB": foreignAdd_Filer.Country = Return2290_2019.CountryType.TB; break;
                        case "TD": foreignAdd_Filer.Country = Return2290_2019.CountryType.TD; break;
                        case "TH": foreignAdd_Filer.Country = Return2290_2019.CountryType.TH; break;
                        case "TI": foreignAdd_Filer.Country = Return2290_2019.CountryType.TI; break;
                        case "TK": foreignAdd_Filer.Country = Return2290_2019.CountryType.TK; break;
                        case "TL": foreignAdd_Filer.Country = Return2290_2019.CountryType.TL; break;
                        case "TN": foreignAdd_Filer.Country = Return2290_2019.CountryType.TN; break;
                        case "TO": foreignAdd_Filer.Country = Return2290_2019.CountryType.TO; break;
                        case "TP": foreignAdd_Filer.Country = Return2290_2019.CountryType.TP; break;
                        case "TS": foreignAdd_Filer.Country = Return2290_2019.CountryType.TS; break;
                        case "TT": foreignAdd_Filer.Country = Return2290_2019.CountryType.TT; break;
                        case "TU": foreignAdd_Filer.Country = Return2290_2019.CountryType.TU; break;
                        case "TV": foreignAdd_Filer.Country = Return2290_2019.CountryType.TV; break;
                        case "TW": foreignAdd_Filer.Country = Return2290_2019.CountryType.TW; break;
                        case "TX": foreignAdd_Filer.Country = Return2290_2019.CountryType.TX; break;
                        case "TZ": foreignAdd_Filer.Country = Return2290_2019.CountryType.TZ; break;
                        case "UC": foreignAdd_Filer.Country = Return2290_2019.CountryType.UC; break;
                        case "UG": foreignAdd_Filer.Country = Return2290_2019.CountryType.UG; break;
                        case "UK": foreignAdd_Filer.Country = Return2290_2019.CountryType.UK; break;
                        case "UP": foreignAdd_Filer.Country = Return2290_2019.CountryType.UP; break;
                        case "UV": foreignAdd_Filer.Country = Return2290_2019.CountryType.UV; break;
                        case "UY": foreignAdd_Filer.Country = Return2290_2019.CountryType.UY; break;
                        case "UZ": foreignAdd_Filer.Country = Return2290_2019.CountryType.UZ; break;
                        case "VC": foreignAdd_Filer.Country = Return2290_2019.CountryType.VC; break;
                        case "VE": foreignAdd_Filer.Country = Return2290_2019.CountryType.VE; break;
                        case "VI": foreignAdd_Filer.Country = Return2290_2019.CountryType.VI; break;
                        case "VM": foreignAdd_Filer.Country = Return2290_2019.CountryType.VM; break;
                        case "VT": foreignAdd_Filer.Country = Return2290_2019.CountryType.VT; break;
                        case "WA": foreignAdd_Filer.Country = Return2290_2019.CountryType.WA; break;
                        case "WF": foreignAdd_Filer.Country = Return2290_2019.CountryType.WF; break;
                        case "WI": foreignAdd_Filer.Country = Return2290_2019.CountryType.WI; break;
                        case "WQ": foreignAdd_Filer.Country = Return2290_2019.CountryType.WQ; break;
                        case "WS": foreignAdd_Filer.Country = Return2290_2019.CountryType.WS; break;
                        case "WZ": foreignAdd_Filer.Country = Return2290_2019.CountryType.WZ; break;
                        case "YM": foreignAdd_Filer.Country = Return2290_2019.CountryType.YM; break;
                        case "ZA": foreignAdd_Filer.Country = Return2290_2019.CountryType.ZA; break;
                        case "ZI": foreignAdd_Filer.Country = Return2290_2019.CountryType.ZI; break;
                        #endregion
                    }

                    if (Filer_ZIPCode.Length > 0)
                        foreignAdd_Filer.PostalCode = Filer_ZIPCode;

                    objFiler.Item = foreignAdd_Filer;
                    #endregion
                }

                returnHeaderType.Filer = objFiler;
                #endregion

                #region OFFICER
                if (Officer_Name != "")
                {
                    Return2290_2019.ReturnHeaderTypeBusinessOfficerGrp objOfficer = new Return2290_2019.ReturnHeaderTypeBusinessOfficerGrp();

                    objOfficer.PersonNm = Officer_Name;
                    objOfficer.PersonTitleTxt = Officer_Title;

                    if (Officer_PIN.Length > 0)
                        objOfficer.TaxpayerPIN = Officer_PIN;

                    if (Officer_Phone.Length > 0)
                    {
                        objOfficer.Item = Officer_Phone;
                        if (Filer_Foreign_Country == "US" || Filer_Foreign_Country == "")
                            objOfficer.ItemElementName = Return2290_2019.ItemChoiceType.PhoneNum;
                        else
                            objOfficer.ItemElementName = Return2290_2019.ItemChoiceType.ForeignPhoneNum;
                    }

                    if (Officer_email.Length > 0)
                        objOfficer.EmailAddressTxt = Officer_email;

                    objOfficer.SignatureDt = DateTime.Now.ToUniversalTime();

                    returnHeaderType.BusinessOfficerGrp = objOfficer;
                }
                #endregion

                #region THIRD PARTY DESIGNEE

                Return2290_2019.ReturnHeaderTypeThirdPartyDesignee objThirdPrtyDesignee = new Return2290_2019.ReturnHeaderTypeThirdPartyDesignee();

                object[] itemsField = new object[4];
                Return2290_2019.ItemsChoiceType[] itemsElementNameField = new Return2290_2019.ItemsChoiceType[4];

                itemsField[0] = Return2290_2019.CheckboxType.X;

                if (ThirdPartyDesigne_Name != "")
                {
                    itemsField[1] = ThirdPartyDesigne_Name;
                    itemsField[2] = ThirdPartyDesigne_Phone;
                    itemsField[3] = ThirdPartyDesigne_PIN;

                    itemsElementNameField[0] = Return2290_2019.ItemsChoiceType.DiscussWithThirdPartyYesInd;
                    itemsElementNameField[1] = Return2290_2019.ItemsChoiceType.ThirdPartyDesigneeNm;
                    itemsElementNameField[2] = Return2290_2019.ItemsChoiceType.ThirdPartyDesigneePhoneNum;
                    itemsElementNameField[3] = Return2290_2019.ItemsChoiceType.ThirdPartyDesigneePIN;
                }
                else
                {
                    itemsElementNameField[0] = Return2290_2019.ItemsChoiceType.DiscussWithThirdPartyNoInd;
                }
                objThirdPrtyDesignee.Items = itemsField;
                objThirdPrtyDesignee.ItemsElementName = itemsElementNameField;

                returnHeaderType.ThirdPartyDesignee = objThirdPrtyDesignee;

                #endregion

                #region PREPARER

                if (Preparer_Name != "")
                {
                    Return2290_2019.ReturnHeaderTypePreparerFirmGrp objPreparerFirm = new Return2290_2019.ReturnHeaderTypePreparerFirmGrp();
                    Return2290_2019.ReturnHeaderTypePreparerPersonGrp objPreparer = new Return2290_2019.ReturnHeaderTypePreparerPersonGrp();

                    objPreparer.PreparerPersonNm = Preparer_Name;

                    if (IsPreparerHasPTIN)
                        objPreparer.PTIN = Preparer_PTIN_SSN;

                    if (Preparer_Phone.Length > 0)
                    {
                        objPreparer.Item = Preparer_Phone;
                        objPreparer.ItemElementName = Return2290_2019.ItemChoiceType1.PhoneNum;
                    }
                    else if (Preparer_ForeignPhone.Length > 0)
                    {
                        objPreparer.Item = Preparer_Phone;
                        objPreparer.ItemElementName = Return2290_2019.ItemChoiceType1.ForeignPhoneNum;
                    }
                    objPreparer.EmailAddressTxt = Preparer_Email;
                    objPreparer.PreparationDt = DateTime.Now.ToUniversalTime();
                    objPreparer.PreparationDtSpecified = true;

                    if (IsPreparerSelfEmployed)
                    {
                        objPreparer.SelfEmployedInd = Return2290_2019.CheckboxType.X;
                        objPreparer.SelfEmployedIndSpecified = IsPreparerSelfEmployed;
                    }

                    returnHeaderType.PreparerPersonGrp = objPreparer;
                }
                //2019 Preparer_Address not empty check 
                if (!IsPreparerSelfEmployed && Preparer_Name != "" && Preparer_Address != "")
                {
                    Return2290_2019.ReturnHeaderTypePreparerFirmGrp objPreparerFirm = new Return2290_2019.ReturnHeaderTypePreparerFirmGrp();
                    Return2290_2019.BusinessNameType PreparerBusinessName = new Return2290_2019.BusinessNameType();
                    PreparerBusinessName.BusinessNameLine1Txt = Preparer_Firm;

                    objPreparerFirm.PreparerFirmEIN = Preparer_EIN;
                    objPreparerFirm.PreparerFirmName = PreparerBusinessName;
                    if (Preparer_Country == "" || Preparer_Country == "US")
                    {
                        #region Preparer US ADDRESS

                        Return2290_2019.USAddressType usAdd_Preparer = new Return2290_2019.USAddressType();

                        if (Preparer_Address.Length > 35)
                        {
                            string Preparer_AddressLine2 = string.Empty;
                            usAdd_Preparer.AddressLine1 = Preparer_Address.Substring(0, 35).Trim();
                            Preparer_AddressLine2 = Preparer_Address.Substring(35);
                            usAdd_Preparer.AddressLine2 = Preparer_AddressLine2.Trim();
                        }
                        else
                        {
                            usAdd_Preparer.AddressLine1 = Preparer_Address.Trim();
                        }

                        usAdd_Preparer.City = Preparer_City.Trim();

                        switch (Preparer_State)
                        {
                            #region Filer_State
                            case "AA": usAdd_Preparer.State = Return2290_2019.StateType.AA; break;
                            case "AE": usAdd_Preparer.State = Return2290_2019.StateType.AE; break;
                            case "AK": usAdd_Preparer.State = Return2290_2019.StateType.AK; break;
                            case "AL": usAdd_Preparer.State = Return2290_2019.StateType.AL; break;
                            case "AP": usAdd_Preparer.State = Return2290_2019.StateType.AP; break;
                            case "AR": usAdd_Preparer.State = Return2290_2019.StateType.AR; break;
                            case "AS": usAdd_Preparer.State = Return2290_2019.StateType.AS; break;
                            case "AZ": usAdd_Preparer.State = Return2290_2019.StateType.AZ; break;
                            case "CA": usAdd_Preparer.State = Return2290_2019.StateType.CA; break;
                            case "CO": usAdd_Preparer.State = Return2290_2019.StateType.CO; break;
                            case "CT": usAdd_Preparer.State = Return2290_2019.StateType.CT; break;
                            case "DC": usAdd_Preparer.State = Return2290_2019.StateType.DC; break;
                            case "DE": usAdd_Preparer.State = Return2290_2019.StateType.DE; break;
                            case "FL": usAdd_Preparer.State = Return2290_2019.StateType.FL; break;
                            case "FM": usAdd_Preparer.State = Return2290_2019.StateType.FM; break;
                            case "GA": usAdd_Preparer.State = Return2290_2019.StateType.GA; break;
                            case "GU": usAdd_Preparer.State = Return2290_2019.StateType.GU; break;
                            case "HI": usAdd_Preparer.State = Return2290_2019.StateType.HI; break;
                            case "IA": usAdd_Preparer.State = Return2290_2019.StateType.IA; break;
                            case "ID": usAdd_Preparer.State = Return2290_2019.StateType.ID; break;
                            case "IL": usAdd_Preparer.State = Return2290_2019.StateType.IL; break;
                            case "IN": usAdd_Preparer.State = Return2290_2019.StateType.IN; break;
                            case "KS": usAdd_Preparer.State = Return2290_2019.StateType.KS; break;
                            case "KY": usAdd_Preparer.State = Return2290_2019.StateType.KY; break;
                            case "LA": usAdd_Preparer.State = Return2290_2019.StateType.LA; break;
                            case "MA": usAdd_Preparer.State = Return2290_2019.StateType.MA; break;
                            case "MD": usAdd_Preparer.State = Return2290_2019.StateType.MD; break;
                            case "ME": usAdd_Preparer.State = Return2290_2019.StateType.ME; break;
                            case "MH": usAdd_Preparer.State = Return2290_2019.StateType.MH; break;
                            case "MI": usAdd_Preparer.State = Return2290_2019.StateType.MI; break;
                            case "MN": usAdd_Preparer.State = Return2290_2019.StateType.MN; break;
                            case "MO": usAdd_Preparer.State = Return2290_2019.StateType.MO; break;
                            case "MP": usAdd_Preparer.State = Return2290_2019.StateType.MP; break;
                            case "MS": usAdd_Preparer.State = Return2290_2019.StateType.MS; break;
                            case "MT": usAdd_Preparer.State = Return2290_2019.StateType.MT; break;
                            case "NC": usAdd_Preparer.State = Return2290_2019.StateType.NC; break;
                            case "ND": usAdd_Preparer.State = Return2290_2019.StateType.ND; break;
                            case "NE": usAdd_Preparer.State = Return2290_2019.StateType.NE; break;
                            case "NH": usAdd_Preparer.State = Return2290_2019.StateType.NH; break;
                            case "NJ": usAdd_Preparer.State = Return2290_2019.StateType.NJ; break;
                            case "NM": usAdd_Preparer.State = Return2290_2019.StateType.NM; break;
                            case "NV": usAdd_Preparer.State = Return2290_2019.StateType.NV; break;
                            case "NY": usAdd_Preparer.State = Return2290_2019.StateType.NY; break;
                            case "OH": usAdd_Preparer.State = Return2290_2019.StateType.OH; break;
                            case "OK": usAdd_Preparer.State = Return2290_2019.StateType.OK; break;
                            case "OR": usAdd_Preparer.State = Return2290_2019.StateType.OR; break;
                            case "PA": usAdd_Preparer.State = Return2290_2019.StateType.PA; break;
                            case "PR": usAdd_Preparer.State = Return2290_2019.StateType.PR; break;
                            case "PW": usAdd_Preparer.State = Return2290_2019.StateType.PW; break;
                            case "RI": usAdd_Preparer.State = Return2290_2019.StateType.RI; break;
                            case "SC": usAdd_Preparer.State = Return2290_2019.StateType.SC; break;
                            case "SD": usAdd_Preparer.State = Return2290_2019.StateType.SD; break;
                            case "TN": usAdd_Preparer.State = Return2290_2019.StateType.TN; break;
                            case "TX": usAdd_Preparer.State = Return2290_2019.StateType.TX; break;
                            case "UT": usAdd_Preparer.State = Return2290_2019.StateType.UT; break;
                            case "VA": usAdd_Preparer.State = Return2290_2019.StateType.VA; break;
                            case "VI": usAdd_Preparer.State = Return2290_2019.StateType.VI; break;
                            case "VT": usAdd_Preparer.State = Return2290_2019.StateType.VT; break;
                            case "WA": usAdd_Preparer.State = Return2290_2019.StateType.WA; break;
                            case "WI": usAdd_Preparer.State = Return2290_2019.StateType.WI; break;
                            case "WV": usAdd_Preparer.State = Return2290_2019.StateType.WV; break;
                            case "WY": usAdd_Preparer.State = Return2290_2019.StateType.WY; break;
                            #endregion
                        }

                        usAdd_Preparer.ZIPCode = Preparer_Zip;

                        objPreparerFirm.Item = usAdd_Preparer;

                        #endregion
                    }
                    else
                    {
                        #region Preparer FOREIGN ADDRESS
                        Return2290_2019.ForeignAddressType foreignAdd_Preparer = new Return2290_2019.ForeignAddressType();

                        if (Preparer_Address.Length > 35)
                        {
                            string Preparer_AddressLine2 = string.Empty;
                            foreignAdd_Preparer.AddressLine1 = Preparer_Address.Substring(0, 35).Trim();
                            Preparer_AddressLine2 = Preparer_Address.Substring(35);
                            foreignAdd_Preparer.AddressLine2 = Preparer_AddressLine2.Trim();

                        }
                        else
                        {
                            foreignAdd_Preparer.AddressLine1 = Preparer_Address.Trim();
                        }
                        foreignAdd_Preparer.City = Preparer_City.Trim();

                        if (Preparer_State.Length > 0)
                            foreignAdd_Preparer.ProvinceOrState = Preparer_State;

                        switch (Preparer_Country)
                        {
                            #region Preparer_Foreign_Country
                            case "AA": foreignAdd_Preparer.Country = Return2290_2019.CountryType.AA; break;
                            case "AC": foreignAdd_Preparer.Country = Return2290_2019.CountryType.AC; break;
                            case "AE": foreignAdd_Preparer.Country = Return2290_2019.CountryType.AE; break;
                            case "AF": foreignAdd_Preparer.Country = Return2290_2019.CountryType.AF; break;
                            case "AG": foreignAdd_Preparer.Country = Return2290_2019.CountryType.AG; break;
                            case "AJ": foreignAdd_Preparer.Country = Return2290_2019.CountryType.AJ; break;
                            case "AL": foreignAdd_Preparer.Country = Return2290_2019.CountryType.AL; break;
                            case "AM": foreignAdd_Preparer.Country = Return2290_2019.CountryType.AM; break;
                            case "AN": foreignAdd_Preparer.Country = Return2290_2019.CountryType.AN; break;
                            case "AO": foreignAdd_Preparer.Country = Return2290_2019.CountryType.AO; break;
                            case "AR": foreignAdd_Preparer.Country = Return2290_2019.CountryType.AR; break;
                            case "AS": foreignAdd_Preparer.Country = Return2290_2019.CountryType.AS; break;
                            case "AT": foreignAdd_Preparer.Country = Return2290_2019.CountryType.AT; break;
                            case "AU": foreignAdd_Preparer.Country = Return2290_2019.CountryType.AU; break;
                            case "AV": foreignAdd_Preparer.Country = Return2290_2019.CountryType.AV; break;
                            case "AX": foreignAdd_Preparer.Country = Return2290_2019.CountryType.AX; break;
                            case "AY": foreignAdd_Preparer.Country = Return2290_2019.CountryType.AY; break;
                            case "BA": foreignAdd_Preparer.Country = Return2290_2019.CountryType.BA; break;
                            case "BB": foreignAdd_Preparer.Country = Return2290_2019.CountryType.BB; break;
                            case "BC": foreignAdd_Preparer.Country = Return2290_2019.CountryType.BC; break;
                            case "BD": foreignAdd_Preparer.Country = Return2290_2019.CountryType.BD; break;
                            case "BE": foreignAdd_Preparer.Country = Return2290_2019.CountryType.BE; break;
                            case "BF": foreignAdd_Preparer.Country = Return2290_2019.CountryType.BF; break;
                            case "BG": foreignAdd_Preparer.Country = Return2290_2019.CountryType.BG; break;
                            case "BH": foreignAdd_Preparer.Country = Return2290_2019.CountryType.BH; break;
                            case "BK": foreignAdd_Preparer.Country = Return2290_2019.CountryType.BK; break;
                            case "BL": foreignAdd_Preparer.Country = Return2290_2019.CountryType.BL; break;
                            case "BM": foreignAdd_Preparer.Country = Return2290_2019.CountryType.BM; break;
                            case "BN": foreignAdd_Preparer.Country = Return2290_2019.CountryType.BN; break;
                            case "BO": foreignAdd_Preparer.Country = Return2290_2019.CountryType.BO; break;
                            case "BP": foreignAdd_Preparer.Country = Return2290_2019.CountryType.BP; break;
                            case "BQ": foreignAdd_Preparer.Country = Return2290_2019.CountryType.BQ; break;
                            case "BR": foreignAdd_Preparer.Country = Return2290_2019.CountryType.BR; break;
                            case "BT": foreignAdd_Preparer.Country = Return2290_2019.CountryType.BT; break;
                            case "BU": foreignAdd_Preparer.Country = Return2290_2019.CountryType.BU; break;
                            case "BV": foreignAdd_Preparer.Country = Return2290_2019.CountryType.BV; break;
                            case "BX": foreignAdd_Preparer.Country = Return2290_2019.CountryType.BX; break;
                            case "BY": foreignAdd_Preparer.Country = Return2290_2019.CountryType.BY; break;
                            case "CA": foreignAdd_Preparer.Country = Return2290_2019.CountryType.CA; break;
                            case "CB": foreignAdd_Preparer.Country = Return2290_2019.CountryType.CB; break;
                            case "CD": foreignAdd_Preparer.Country = Return2290_2019.CountryType.CD; break;
                            case "CE": foreignAdd_Preparer.Country = Return2290_2019.CountryType.CE; break;
                            case "CF": foreignAdd_Preparer.Country = Return2290_2019.CountryType.CF; break;
                            case "CG": foreignAdd_Preparer.Country = Return2290_2019.CountryType.CG; break;
                            case "CH": foreignAdd_Preparer.Country = Return2290_2019.CountryType.CH; break;
                            case "CI": foreignAdd_Preparer.Country = Return2290_2019.CountryType.CI; break;
                            case "CJ": foreignAdd_Preparer.Country = Return2290_2019.CountryType.CJ; break;
                            case "CK": foreignAdd_Preparer.Country = Return2290_2019.CountryType.CK; break;
                            case "CM": foreignAdd_Preparer.Country = Return2290_2019.CountryType.CM; break;
                            case "CN": foreignAdd_Preparer.Country = Return2290_2019.CountryType.CN; break;
                            case "CO": foreignAdd_Preparer.Country = Return2290_2019.CountryType.CO; break;
                            case "CR": foreignAdd_Preparer.Country = Return2290_2019.CountryType.CR; break;
                            case "CS": foreignAdd_Preparer.Country = Return2290_2019.CountryType.CS; break;
                            case "CT": foreignAdd_Preparer.Country = Return2290_2019.CountryType.CT; break;
                            case "CU": foreignAdd_Preparer.Country = Return2290_2019.CountryType.CU; break;
                            case "CV": foreignAdd_Preparer.Country = Return2290_2019.CountryType.CV; break;
                            case "CW": foreignAdd_Preparer.Country = Return2290_2019.CountryType.CW; break;
                            case "CY": foreignAdd_Preparer.Country = Return2290_2019.CountryType.CY; break;
                            case "DA": foreignAdd_Preparer.Country = Return2290_2019.CountryType.DA; break;
                            case "DJ": foreignAdd_Preparer.Country = Return2290_2019.CountryType.DJ; break;
                            case "DO": foreignAdd_Preparer.Country = Return2290_2019.CountryType.DO; break;
                            case "DR": foreignAdd_Preparer.Country = Return2290_2019.CountryType.DR; break;
                            case "DX": foreignAdd_Preparer.Country = Return2290_2019.CountryType.DX; break;
                            case "EC": foreignAdd_Preparer.Country = Return2290_2019.CountryType.EC; break;
                            case "EG": foreignAdd_Preparer.Country = Return2290_2019.CountryType.EG; break;
                            case "EI": foreignAdd_Preparer.Country = Return2290_2019.CountryType.EI; break;
                            case "EK": foreignAdd_Preparer.Country = Return2290_2019.CountryType.EK; break;
                            case "EN": foreignAdd_Preparer.Country = Return2290_2019.CountryType.EN; break;
                            case "ER": foreignAdd_Preparer.Country = Return2290_2019.CountryType.ER; break;
                            case "ES": foreignAdd_Preparer.Country = Return2290_2019.CountryType.ES; break;
                            case "ET": foreignAdd_Preparer.Country = Return2290_2019.CountryType.ET; break;
                            case "EZ": foreignAdd_Preparer.Country = Return2290_2019.CountryType.EZ; break;
                            case "FI": foreignAdd_Preparer.Country = Return2290_2019.CountryType.FI; break;
                            case "FJ": foreignAdd_Preparer.Country = Return2290_2019.CountryType.FJ; break;
                            case "FK": foreignAdd_Preparer.Country = Return2290_2019.CountryType.FK; break;
                            case "FO": foreignAdd_Preparer.Country = Return2290_2019.CountryType.FO; break;
                            case "FP": foreignAdd_Preparer.Country = Return2290_2019.CountryType.FP; break;
                            case "FQ": foreignAdd_Preparer.Country = Return2290_2019.CountryType.FQ; break;
                            case "FR": foreignAdd_Preparer.Country = Return2290_2019.CountryType.FR; break;
                            case "FS": foreignAdd_Preparer.Country = Return2290_2019.CountryType.FS; break;
                            case "GA": foreignAdd_Preparer.Country = Return2290_2019.CountryType.GA; break;
                            case "GB": foreignAdd_Preparer.Country = Return2290_2019.CountryType.GB; break;
                            case "GG": foreignAdd_Preparer.Country = Return2290_2019.CountryType.GG; break;
                            case "GH": foreignAdd_Preparer.Country = Return2290_2019.CountryType.GH; break;
                            case "GI": foreignAdd_Preparer.Country = Return2290_2019.CountryType.GI; break;
                            case "GJ": foreignAdd_Preparer.Country = Return2290_2019.CountryType.GJ; break;
                            case "GK": foreignAdd_Preparer.Country = Return2290_2019.CountryType.GK; break;
                            case "GL": foreignAdd_Preparer.Country = Return2290_2019.CountryType.GL; break;
                            case "GM": foreignAdd_Preparer.Country = Return2290_2019.CountryType.GM; break;
                            case "GR": foreignAdd_Preparer.Country = Return2290_2019.CountryType.GR; break;
                            case "GT": foreignAdd_Preparer.Country = Return2290_2019.CountryType.GT; break;
                            case "GV": foreignAdd_Preparer.Country = Return2290_2019.CountryType.GV; break;
                            case "GY": foreignAdd_Preparer.Country = Return2290_2019.CountryType.GY; break;
                            case "HA": foreignAdd_Preparer.Country = Return2290_2019.CountryType.HA; break;
                            case "HK": foreignAdd_Preparer.Country = Return2290_2019.CountryType.HK; break;
                            case "HM": foreignAdd_Preparer.Country = Return2290_2019.CountryType.HM; break;
                            case "HO": foreignAdd_Preparer.Country = Return2290_2019.CountryType.HO; break;
                            case "HQ": foreignAdd_Preparer.Country = Return2290_2019.CountryType.HQ; break;
                            case "HR": foreignAdd_Preparer.Country = Return2290_2019.CountryType.HR; break;
                            case "HU": foreignAdd_Preparer.Country = Return2290_2019.CountryType.HU; break;
                            case "IC": foreignAdd_Preparer.Country = Return2290_2019.CountryType.IC; break;
                            case "ID": foreignAdd_Preparer.Country = Return2290_2019.CountryType.ID; break;
                            case "IM": foreignAdd_Preparer.Country = Return2290_2019.CountryType.IM; break;
                            case "IO": foreignAdd_Preparer.Country = Return2290_2019.CountryType.IO; break;
                            case "IP": foreignAdd_Preparer.Country = Return2290_2019.CountryType.IP; break;
                            case "IR": foreignAdd_Preparer.Country = Return2290_2019.CountryType.IR; break;
                            case "IS": foreignAdd_Preparer.Country = Return2290_2019.CountryType.IS; break;
                            case "IT": foreignAdd_Preparer.Country = Return2290_2019.CountryType.IT; break;
                            case "IV": foreignAdd_Preparer.Country = Return2290_2019.CountryType.IV; break;
                            case "IZ": foreignAdd_Preparer.Country = Return2290_2019.CountryType.IZ; break;
                            case "JA": foreignAdd_Preparer.Country = Return2290_2019.CountryType.JA; break;
                            case "JE": foreignAdd_Preparer.Country = Return2290_2019.CountryType.JE; break;
                            case "JM": foreignAdd_Preparer.Country = Return2290_2019.CountryType.JM; break;
                            case "JN": foreignAdd_Preparer.Country = Return2290_2019.CountryType.JN; break;
                            case "JO": foreignAdd_Preparer.Country = Return2290_2019.CountryType.JO; break;
                            case "JQ": foreignAdd_Preparer.Country = Return2290_2019.CountryType.JQ; break;
                            case "KE": foreignAdd_Preparer.Country = Return2290_2019.CountryType.KE; break;
                            case "KG": foreignAdd_Preparer.Country = Return2290_2019.CountryType.KG; break;
                            case "KN": foreignAdd_Preparer.Country = Return2290_2019.CountryType.KN; break;
                            case "KQ": foreignAdd_Preparer.Country = Return2290_2019.CountryType.KQ; break;
                            case "KR": foreignAdd_Preparer.Country = Return2290_2019.CountryType.KR; break;
                            case "KS": foreignAdd_Preparer.Country = Return2290_2019.CountryType.KS; break;
                            case "KT": foreignAdd_Preparer.Country = Return2290_2019.CountryType.KT; break;
                            case "KU": foreignAdd_Preparer.Country = Return2290_2019.CountryType.KU; break;
                            case "KZ": foreignAdd_Preparer.Country = Return2290_2019.CountryType.KZ; break;
                            case "LA": foreignAdd_Preparer.Country = Return2290_2019.CountryType.LA; break;
                            case "LE": foreignAdd_Preparer.Country = Return2290_2019.CountryType.LE; break;
                            case "LG": foreignAdd_Preparer.Country = Return2290_2019.CountryType.LG; break;
                            case "LH": foreignAdd_Preparer.Country = Return2290_2019.CountryType.LH; break;
                            case "LI": foreignAdd_Preparer.Country = Return2290_2019.CountryType.LI; break;
                            case "LO": foreignAdd_Preparer.Country = Return2290_2019.CountryType.LO; break;
                            case "LQ": foreignAdd_Preparer.Country = Return2290_2019.CountryType.LQ; break;
                            case "LS": foreignAdd_Preparer.Country = Return2290_2019.CountryType.LS; break;
                            case "LT": foreignAdd_Preparer.Country = Return2290_2019.CountryType.LT; break;
                            case "LU": foreignAdd_Preparer.Country = Return2290_2019.CountryType.LU; break;
                            case "LY": foreignAdd_Preparer.Country = Return2290_2019.CountryType.LY; break;
                            case "MA": foreignAdd_Preparer.Country = Return2290_2019.CountryType.MA; break;
                            case "MC": foreignAdd_Preparer.Country = Return2290_2019.CountryType.MC; break;
                            case "MD": foreignAdd_Preparer.Country = Return2290_2019.CountryType.MD; break;
                            case "MG": foreignAdd_Preparer.Country = Return2290_2019.CountryType.MG; break;
                            case "MH": foreignAdd_Preparer.Country = Return2290_2019.CountryType.MH; break;
                            case "MI": foreignAdd_Preparer.Country = Return2290_2019.CountryType.MI; break;
                            case "MJ": foreignAdd_Preparer.Country = Return2290_2019.CountryType.MJ; break;
                            case "MK": foreignAdd_Preparer.Country = Return2290_2019.CountryType.MK; break;
                            case "ML": foreignAdd_Preparer.Country = Return2290_2019.CountryType.ML; break;
                            case "MN": foreignAdd_Preparer.Country = Return2290_2019.CountryType.MN; break;
                            case "MO": foreignAdd_Preparer.Country = Return2290_2019.CountryType.MO; break;
                            case "MP": foreignAdd_Preparer.Country = Return2290_2019.CountryType.MP; break;
                            case "MQ": foreignAdd_Preparer.Country = Return2290_2019.CountryType.MQ; break;
                            case "MR": foreignAdd_Preparer.Country = Return2290_2019.CountryType.MR; break;
                            case "MT": foreignAdd_Preparer.Country = Return2290_2019.CountryType.MT; break;
                            case "MU": foreignAdd_Preparer.Country = Return2290_2019.CountryType.MU; break;
                            case "MV": foreignAdd_Preparer.Country = Return2290_2019.CountryType.MV; break;
                            case "MX": foreignAdd_Preparer.Country = Return2290_2019.CountryType.MX; break;
                            case "MY": foreignAdd_Preparer.Country = Return2290_2019.CountryType.MY; break;
                            case "MZ": foreignAdd_Preparer.Country = Return2290_2019.CountryType.MZ; break;
                            case "NC": foreignAdd_Preparer.Country = Return2290_2019.CountryType.NC; break;
                            case "NE": foreignAdd_Preparer.Country = Return2290_2019.CountryType.NE; break;
                            case "NF": foreignAdd_Preparer.Country = Return2290_2019.CountryType.NF; break;
                            case "NG": foreignAdd_Preparer.Country = Return2290_2019.CountryType.NG; break;
                            case "NH": foreignAdd_Preparer.Country = Return2290_2019.CountryType.NH; break;
                            case "NI": foreignAdd_Preparer.Country = Return2290_2019.CountryType.NI; break;
                            case "NL": foreignAdd_Preparer.Country = Return2290_2019.CountryType.NL; break;
                            case "NO": foreignAdd_Preparer.Country = Return2290_2019.CountryType.NO; break;
                            case "NP": foreignAdd_Preparer.Country = Return2290_2019.CountryType.NP; break;
                            case "NR": foreignAdd_Preparer.Country = Return2290_2019.CountryType.NR; break;
                            case "NS": foreignAdd_Preparer.Country = Return2290_2019.CountryType.NS; break;
                            case "NU": foreignAdd_Preparer.Country = Return2290_2019.CountryType.NU; break;
                            case "NZ": foreignAdd_Preparer.Country = Return2290_2019.CountryType.NZ; break;
                            case "PA": foreignAdd_Preparer.Country = Return2290_2019.CountryType.PA; break;
                            case "PC": foreignAdd_Preparer.Country = Return2290_2019.CountryType.PC; break;
                            case "PE": foreignAdd_Preparer.Country = Return2290_2019.CountryType.PE; break;
                            case "PF": foreignAdd_Preparer.Country = Return2290_2019.CountryType.PF; break;
                            case "PG": foreignAdd_Preparer.Country = Return2290_2019.CountryType.PG; break;
                            case "PK": foreignAdd_Preparer.Country = Return2290_2019.CountryType.PK; break;
                            case "PL": foreignAdd_Preparer.Country = Return2290_2019.CountryType.PL; break;
                            case "PM": foreignAdd_Preparer.Country = Return2290_2019.CountryType.PM; break;
                            case "PO": foreignAdd_Preparer.Country = Return2290_2019.CountryType.PO; break;
                            case "PP": foreignAdd_Preparer.Country = Return2290_2019.CountryType.PP; break;
                            case "PU": foreignAdd_Preparer.Country = Return2290_2019.CountryType.PU; break;
                            case "QA": foreignAdd_Preparer.Country = Return2290_2019.CountryType.QA; break;
                            case "RO": foreignAdd_Preparer.Country = Return2290_2019.CountryType.RO; break;
                            case "RP": foreignAdd_Preparer.Country = Return2290_2019.CountryType.RP; break;
                            case "RS": foreignAdd_Preparer.Country = Return2290_2019.CountryType.RS; break;
                            case "RW": foreignAdd_Preparer.Country = Return2290_2019.CountryType.RW; break;
                            case "SA": foreignAdd_Preparer.Country = Return2290_2019.CountryType.SA; break;
                            case "SB": foreignAdd_Preparer.Country = Return2290_2019.CountryType.SB; break;
                            case "SC": foreignAdd_Preparer.Country = Return2290_2019.CountryType.SC; break;
                            case "SE": foreignAdd_Preparer.Country = Return2290_2019.CountryType.SE; break;
                            case "SF": foreignAdd_Preparer.Country = Return2290_2019.CountryType.SF; break;
                            case "SG": foreignAdd_Preparer.Country = Return2290_2019.CountryType.SG; break;
                            case "SH": foreignAdd_Preparer.Country = Return2290_2019.CountryType.SH; break;
                            case "SI": foreignAdd_Preparer.Country = Return2290_2019.CountryType.SI; break;
                            case "SL": foreignAdd_Preparer.Country = Return2290_2019.CountryType.SL; break;
                            case "SM": foreignAdd_Preparer.Country = Return2290_2019.CountryType.SM; break;
                            case "SN": foreignAdd_Preparer.Country = Return2290_2019.CountryType.SN; break;
                            case "SO": foreignAdd_Preparer.Country = Return2290_2019.CountryType.SO; break;
                            case "SP": foreignAdd_Preparer.Country = Return2290_2019.CountryType.SP; break;
                            case "ST": foreignAdd_Preparer.Country = Return2290_2019.CountryType.ST; break;
                            case "SU": foreignAdd_Preparer.Country = Return2290_2019.CountryType.SU; break;
                            case "SV": foreignAdd_Preparer.Country = Return2290_2019.CountryType.SV; break;
                            case "SW": foreignAdd_Preparer.Country = Return2290_2019.CountryType.SW; break;
                            case "SX": foreignAdd_Preparer.Country = Return2290_2019.CountryType.SX; break;
                            case "SY": foreignAdd_Preparer.Country = Return2290_2019.CountryType.SY; break;
                            case "SZ": foreignAdd_Preparer.Country = Return2290_2019.CountryType.SZ; break;
                            case "TD": foreignAdd_Preparer.Country = Return2290_2019.CountryType.TD; break;
                            case "TH": foreignAdd_Preparer.Country = Return2290_2019.CountryType.TH; break;
                            case "TI": foreignAdd_Preparer.Country = Return2290_2019.CountryType.TI; break;
                            case "TK": foreignAdd_Preparer.Country = Return2290_2019.CountryType.TK; break;
                            case "TL": foreignAdd_Preparer.Country = Return2290_2019.CountryType.TL; break;
                            case "TN": foreignAdd_Preparer.Country = Return2290_2019.CountryType.TN; break;
                            case "TO": foreignAdd_Preparer.Country = Return2290_2019.CountryType.TO; break;
                            case "TP": foreignAdd_Preparer.Country = Return2290_2019.CountryType.TP; break;
                            case "TS": foreignAdd_Preparer.Country = Return2290_2019.CountryType.TS; break;
                            case "TT": foreignAdd_Preparer.Country = Return2290_2019.CountryType.TT; break;
                            case "TU": foreignAdd_Preparer.Country = Return2290_2019.CountryType.TU; break;
                            case "TV": foreignAdd_Preparer.Country = Return2290_2019.CountryType.TV; break;
                            case "TW": foreignAdd_Preparer.Country = Return2290_2019.CountryType.TW; break;
                            case "TX": foreignAdd_Preparer.Country = Return2290_2019.CountryType.TX; break;
                            case "TZ": foreignAdd_Preparer.Country = Return2290_2019.CountryType.TZ; break;
                            case "UG": foreignAdd_Preparer.Country = Return2290_2019.CountryType.UG; break;
                            case "UK": foreignAdd_Preparer.Country = Return2290_2019.CountryType.UK; break;
                            case "UP": foreignAdd_Preparer.Country = Return2290_2019.CountryType.UP; break;
                            case "UV": foreignAdd_Preparer.Country = Return2290_2019.CountryType.UV; break;
                            case "UY": foreignAdd_Preparer.Country = Return2290_2019.CountryType.UY; break;
                            case "UZ": foreignAdd_Preparer.Country = Return2290_2019.CountryType.UZ; break;
                            case "VC": foreignAdd_Preparer.Country = Return2290_2019.CountryType.VC; break;
                            case "VE": foreignAdd_Preparer.Country = Return2290_2019.CountryType.VE; break;
                            case "VI": foreignAdd_Preparer.Country = Return2290_2019.CountryType.VI; break;
                            case "VM": foreignAdd_Preparer.Country = Return2290_2019.CountryType.VM; break;
                            case "VT": foreignAdd_Preparer.Country = Return2290_2019.CountryType.VT; break;
                            case "WA": foreignAdd_Preparer.Country = Return2290_2019.CountryType.WA; break;
                            case "WF": foreignAdd_Preparer.Country = Return2290_2019.CountryType.WF; break;
                            case "WI": foreignAdd_Preparer.Country = Return2290_2019.CountryType.WI; break;
                            case "WQ": foreignAdd_Preparer.Country = Return2290_2019.CountryType.WQ; break;
                            case "WS": foreignAdd_Preparer.Country = Return2290_2019.CountryType.WS; break;
                            case "WZ": foreignAdd_Preparer.Country = Return2290_2019.CountryType.WZ; break;
                            case "YM": foreignAdd_Preparer.Country = Return2290_2019.CountryType.YM; break;
                            case "ZA": foreignAdd_Preparer.Country = Return2290_2019.CountryType.ZA; break;
                            case "ZI": foreignAdd_Preparer.Country = Return2290_2019.CountryType.ZI; break;
                            #endregion
                        }

                        if (Filer_ZIPCode.Length > 0)
                            foreignAdd_Preparer.PostalCode = Filer_ZIPCode;

                        objFiler.Item = foreignAdd_Preparer;
                        #endregion
                    }

                    returnHeaderType.PreparerFirmGrp = objPreparerFirm;
                }
                #endregion

                #region CONSENT_TO_VIN_DATA_DISCLOSURE

                object[] itemConsent = new object[4];
                Return2290_2019.ItemsChoiceType1[] itemElements = new Return2290_2019.ItemsChoiceType1[4];

                itemConsent[0] = Return2290_2019.CheckboxType.X;

                if (IsConsentToDiscloseYes)
                {
                    itemElements[0] = Return2290_2019.ItemsChoiceType1.ConsentToDiscloseYesInd;

                    Return2290_2019.ReturnHeaderTypeConsentToVINDataDisclosureGrpDisclosureFormSignatureInfo objDisclosureFormSignInfo =
                        new Return2290_2019.ReturnHeaderTypeConsentToVINDataDisclosureGrpDisclosureFormSignatureInfo();
                    objDisclosureFormSignInfo.EIN = Filer_EIN;
                    Return2290_2019.BusinessNameType disclosureFormSignInfo_business = new Return2290_2019.BusinessNameType();
                    disclosureFormSignInfo_business.BusinessNameLine1Txt = Filer_Name;
                    objDisclosureFormSignInfo.BusinessName = disclosureFormSignInfo_business;
                    objDisclosureFormSignInfo.SignatureDt = DateTime.Now.ToUniversalTime();

                    itemConsent[1] = (objDisclosureFormSignInfo);
                    itemElements[1] = (Return2290_2019.ItemsChoiceType1.DisclosureFormSignatureInfo);

                    itemConsent[2] = (Return2290_2019.ReturnHeaderTypeConsentToVINDataDisclosureGrpSignatureOptionCd.PINNumber);
                    itemElements[2] = (Return2290_2019.ItemsChoiceType1.SignatureOptionCd);

                    itemConsent[3] = (Officer_PIN);
                    itemElements[3] = (Return2290_2019.ItemsChoiceType1.PIN);
                }
                else
                {
                    itemElements[0] = (Return2290_2019.ItemsChoiceType1.ConsentToDiscloseNoInd);
                }

                Return2290_2019.ReturnHeaderTypeConsentToVINDataDisclosureGrp objConsent2DataDisclosure =
                    new Return2290_2019.ReturnHeaderTypeConsentToVINDataDisclosureGrp();
                objConsent2DataDisclosure.Items = itemConsent;
                objConsent2DataDisclosure.ItemsElementName = itemElements;

                returnHeaderType.ConsentToVINDataDisclosureGrp = objConsent2DataDisclosure;

                #endregion

                returnHeaderType.TaxYr = TaxYear;

                returnHeaderType.binaryAttachmentCnt = "0";

                returnHeaderType.SoftwareId = Variables.SoftwareID;
                returnHeaderType.SoftwareVersionNum = Variables.SoftwareVersion;
                #endregion

                #region RETURN DATA

                Return2290_2019.ReturnData returnData = new Return2290_2019.ReturnData();

                #region IRS2290

                Return2290_2019.IRS2290 objIRS2290 = new Return2290_2019.IRS2290();

                #region TAX COMPUTATION
                // NEW Changes in 2019 were proposed here
                if (dsTaxComputation.Tables.Count > 0)
                {
                    if (dsTaxComputation.Tables[0].Rows.Count > 0)
                    {
                        List<Return2290_2019.IRS2290TypeHighwayMtrVehTxComputationGrp> objTaxComputationList = new
                            List<Return2290_2019.IRS2290TypeHighwayMtrVehTxComputationGrp>();

                        foreach (DataRow ldr in dsTaxComputation.Tables[0].Rows)
                        {
                            Return2290_2019.IRS2290TypeHighwayMtrVehTxComputationGrp objTaxComputation = new
                                Return2290_2019.IRS2290TypeHighwayMtrVehTxComputationGrp();
                            objTaxComputation.VehicleCategoryCd = Convert.ToString(ldr["Category"]);

                            Return2290_2019.HighwayMtrVehTxCmptColumnsGrpType objVeh = new Return2290_2019.HighwayMtrVehTxCmptColumnsGrpType();
                            
                            objVeh.TaxAmt = Convert.ToDecimal(ldr["Tax_Amount"]);
                            objVeh.TaxAmtSpecified = true;

                            

                            objVeh.LoggingVehPartialTaxAmtSpecified = false;
                            objVeh.NonLoggingVehPartialTaxAmtSpecified = false;

                            if (Convert.ToString(ldr["Logging_Vehicle_Count"]) != "0")
                            {
                                if (Convert.ToString(ldr["Logging_Vehicle_Count"]) != "")
                                {
                                    numLoggingVehicleCnt = numLoggingVehicleCnt + Convert.ToInt16(ldr["Logging_Vehicle_Count"]);
                                    objVeh.LoggingVehicleCnt = Convert.ToString(ldr["Logging_Vehicle_Count"]);
                                }
                                if (Convert.ToString(ldr["Logging_Vehicle_Count"]) != "")
                                    numTotalTaxableVeh += Convert.ToInt16(ldr["Logging_Vehicle_Count"]);
                            }
                            if (Convert.ToString(ldr["Non_Logging_Vehicle_Count"]) != "0")
                            {
                                if (Convert.ToString(ldr["Non_Logging_Vehicle_Count"]) != "")
                                {
                                    numNonLoggingVehicleCnt = numNonLoggingVehicleCnt + Convert.ToInt16(ldr["Non_Logging_Vehicle_Count"]);
                                    objVeh.NonLoggingVehicleCnt = Convert.ToString(ldr["Non_Logging_Vehicle_Count"]);
                                }
                                if (Convert.ToString(ldr["Non_Logging_Vehicle_Count"]) != "")
                                    numTotalTaxableVeh += Convert.ToInt16(ldr["Non_Logging_Vehicle_Count"]);
                            }
                            objTaxComputation.HighwayMtrVehTxCmptColumnsGrp = objVeh;
                            objTaxComputationList.Add(objTaxComputation);
                        }
                        objIRS2290.HighwayMtrVehTxComputationGrp = objTaxComputationList.ToArray();

                        objIRS2290.TotalTaxAmt = Convert.ToDecimal(TaxFromTaxComputation.ToString("N2"));
                        objIRS2290.TotalTaxAmtSpecified = true;

                        objIRS2290.TotalVehicleCnt = numTotalTaxableVeh.ToString();

                    }
                }

                #endregion

                #region IS APPLICATION FOR....

                if (IsAddressChange)
                    objIRS2290.AddressChangeInd = Return2290_2019.CheckboxType.X;

                if (IsAmendedReturn)
                {
                    objIRS2290.AmendedReturnInd = Return2290_2019.CheckboxType.X;
                    objIRS2290.AmendedReturnIndSpecified = true;
                    objIRS2290.AmendedMonthNum = AmendedMonth; //format is MM
                }

                if (IsVINCorrection)
                {
                    // 2019 VINCorrection VIN Correction
                    // Vishwa Add the new TAG About the VINCorrectionExplanationStmt -> ExplanationTtx

                    //objIRS2290.VINCorrectionInd = Return2290_2019.CheckboxType.X;
                    //objIRS2290.VINCorrectionIndSpecified = true;


                    Return2290_2019.IRS2290TypeVINCorrInd objVINCor = new Return2290_2019.IRS2290TypeVINCorrInd();
                    objVINCor.Value = "X";
                    List<string> docId_VINCorrDetails = new List<string>();
                    docId_VINCorrDetails.Add("VINCorrectionExplan");
                    objVINCor.referenceDocumentId = docId_VINCorrDetails.ToArray();
                    objVINCor.referenceDocumentName = "VINCorrectionExplanationStatement";

                    objIRS2290.VINCorrectionInd = objVINCor;


                    //Return2290_2019.VINCorrectionExplanationStmtType VinCorExp = new Return2290_2019.VINCorrectionExplanationStmtType();

                    //VinCorExp.ExplanationTxt = "VIN Correction update request";
                    //objIRS2290.VINCorrectionExplanationStmt = VinCorExp;

                }
                //2019 VIN Correction
                if (IsFinalReturn && IsVINCorrection == false)
                {
                    objIRS2290.FinalReturnInd = Return2290_2019.CheckboxType.X;
                    objIRS2290.FinalReturnIndSpecified = true;
                }

                #endregion

                #region ADDITIONAL TAX

                Return2290_2019.IRS2290TypeAdditionalTaxAmt objAdditionalTax = new Return2290_2019.IRS2290TypeAdditionalTaxAmt();
                objAdditionalTax.Value = Convert.ToDecimal(AdditionalTaxAmount.ToString("N2"));
                List<string> docId_TGWIncreaseWorksheet = new List<string>();
                docId_TGWIncreaseWorksheet.Add("TGWIncreaseWorksheet");
                objAdditionalTax.referenceDocumentId = docId_TGWIncreaseWorksheet.ToArray();
                objAdditionalTax.referenceDocumentName = "TGWIncreaseWorksheet";

                objIRS2290.AdditionalTaxAmt = objAdditionalTax;

                #endregion

                objIRS2290.TotalTaxAmt = Convert.ToDecimal(TaxFromTaxComputation.ToString("N2")) + Convert.ToDecimal(AdditionalTaxAmount.ToString("N2")); ;
                objIRS2290.TotalTaxAmtSpecified = true;

                #region CREDIT AMOUNT

                Return2290_2019.IRS2290TypeTaxCreditsAmt objTaxCreditsAmt = new Return2290_2019.IRS2290TypeTaxCreditsAmt();
                List<string> docId_CreditsAmountStatement = new List<string>();
                docId_CreditsAmountStatement.Add("CreditsAmountStatement");
                objTaxCreditsAmt.referenceDocumentId = docId_CreditsAmountStatement.ToArray();
                objTaxCreditsAmt.referenceDocumentName = "CreditsAmountStatement";
                objTaxCreditsAmt.Value = Convert.ToDecimal(CreditAmount.ToString("N2"));

                objIRS2290.TaxCreditsAmt = objTaxCreditsAmt;

                #endregion

                objIRS2290.BalanceDueAmt = (Convert.ToDecimal(TaxFromTaxComputation.ToString("N2")) + Convert.ToDecimal(AdditionalTaxAmount.ToString("N2"))) - Convert.ToDecimal(CreditAmount.ToString("N2"));

                if (objIRS2290.BalanceDueAmt < 0)
                    objIRS2290.BalanceDueAmt = 0.00M;

                #region IRS PAYMENT2

                Decimal BalanceDue = objIRS2290.BalanceDueAmt;

                if (BalanceDue > 0)
                {
                    if (paymentType != null)
                    {
                        /* Now Both Money Order or EFTPS will be same */
                        if (paymentType == "CMO" || paymentType == "EFTPS")
                        {
                            objIRS2290.EFTPSPaymentInd = Return2290_2019.CheckboxType.X;
                            objIRS2290.EFTPSPaymentIndSpecified = true;
                        }
                        /* Credit / Debit card payments */
                        else if (paymentType == "CDC")
                        {
                            objIRS2290.CreditDebitCardPaymentInd = Return2290_2019.CheckboxType.X;
                            objIRS2290.CreditDebitCardPaymentIndSpecified = true;

                        }
                        else if (paymentType == "DirectDebit" || paymentType == "EFW")
                        {
                            List<Return2290_2019.IRSPayment2> irsPmt2Collection = new List<Return2290_2019.IRSPayment2>();

                            Return2290_2019.IRSPayment2 objPayType = new Return2290_2019.IRSPayment2();

                            objPayType.PaymentAmt = Convert.ToDecimal(BalanceDue.ToString("N2"));
                            objPayType.RoutingTransitNum = Payment_RoutingTransitNo;
                            objPayType.BankAccountNum = Payment_Acc_No;

                            if (Payment_Acc_Type == "Checking" || Payment_Acc_Type == "0")
                                objPayType.BankAccountTypeCd = Return2290_2019.BankAccountType.Item1;
                            else if (Payment_Acc_Type == "Saving" || Payment_Acc_Type == "1")
                                objPayType.BankAccountTypeCd = Return2290_2019.BankAccountType.Item2;

                            objPayType.RequestedPaymentDt = Convert.ToDateTime(Payment_ReqPayDate);

                            objPayType.TaxpayerDaytimePhoneNum = Payment_Txpyer_Ph;

                            objPayType.documentId = "IRSPayment2";
                            //objPayType.documentName = "IRSPayment2";
                            objPayType.softwareId = Variables.SoftwareID;
                            objPayType.softwareVersion = Variables.SoftwareVersion;

                            irsPmt2Collection.Add(objPayType);

                            returnData.IRSPayment2 = irsPmt2Collection.ToArray();
                            documentCount += 1;
                        }
                    }
                }
                #endregion

                #region SUSPENDED VIN

                if (dsSuspendedVehicles.Tables[0].Rows.Count > 0)
                {
                    List<Return2290_2019.SuspendedVINInfoTypeVINDetail> objSusVehList = new List<Return2290_2019.SuspendedVINInfoTypeVINDetail>();

                    foreach (DataRow ldr_suspended in dsSuspendedVehicles.Tables[0].Rows)
                    {
                        Return2290_2019.SuspendedVINInfoTypeVINDetail objSusVeh = new Return2290_2019.SuspendedVINInfoTypeVINDetail();
                        objSusVeh.VIN = Convert.ToString(ldr_suspended["VIN"]);
                        objSusVehList.Add(objSusVeh);

                        #region ....
                        if (!Convert.ToBoolean(ldr_suspended["IsAgricultural"]))
                        {
                            objIRS2290.AgricMileageUsed7500OrLessInd = Return2290_2019.CheckboxType.X;
                            objIRS2290.AgricMileageUsed7500OrLessIndSpecified = true;
                        }
                        else if (Convert.ToBoolean(ldr_suspended["IsAgricultural"]))
                        {
                            objIRS2290.MileageUsed5000OrLessInd = Return2290_2019.CheckboxType.X;
                            objIRS2290.MileageUsed5000OrLessIndSpecified = true;
                        }

                        if (Convert.ToString(ldr_suspended["IsLogging"]) != "")
                        {
                            if (Convert.ToBoolean(ldr_suspended["IsLogging"]))
                                numTaxSuspended_LoggingVeh += 1;
                            else if (!Convert.ToBoolean(ldr_suspended["IsLogging"]))
                                numTaxSuspended_NonLoggingVeh += 1;
                        }
                        else
                        {
                            numTaxSuspended_NonLoggingVeh += 1;
                        }

                        #endregion
                    }

                    Return2290_2019.SuspendedVINStatementType objSusVehStmtType = new Return2290_2019.SuspendedVINStatementType();
                    objSusVehStmtType.SuspendedVINInfo = objSusVehList.ToArray();

                    if (numTaxSuspended_LoggingVeh != 0)
                        objIRS2290.TaxSuspendedLoggingVehCnt = numTaxSuspended_LoggingVeh.ToString();

                    if (numTaxSuspended_NonLoggingVeh != 0 && numTaxSuspended_LoggingVeh != 0)
                        objIRS2290.TaxSuspendedLoggingVehCnt = (numTaxSuspended_LoggingVeh + numTaxSuspended_NonLoggingVeh).ToString();
                    else
                    {
                        if (numTaxSuspended_NonLoggingVeh != 0)
                            objIRS2290.TaxSuspendedNonLoggingVehCnt = numTaxSuspended_NonLoggingVeh.ToString();
                    }

                    Return2290_2019.IRS2290TypeSuspendedVINReferenceTyp objSuspendedVINRef =
                        new Return2290_2019.IRS2290TypeSuspendedVINReferenceTyp();
                    List<string> docID_SuspendedVINStmt = new List<string>();
                    docID_SuspendedVINStmt.Add("SuspendedVINStatement");
                    objSuspendedVINRef.referenceDocumentId = docID_SuspendedVINStmt.ToArray();
                    objSuspendedVINRef.referenceDocumentName = "SuspendedVINStatement";


                    objIRS2290.SuspendedVINReferenceTyp = objSuspendedVINRef;
                }

                #endregion

                if (IsNotSubjectToTaxChecked)
                    objIRS2290.NotSubjectToTaxInd = Return2290_2019.CheckboxType.X;

                
                if (dsTGWIncreaseWorksheet.Tables.Count > 0 && IsAmendedReturn)
                {
                    /*
                    if (Convert.ToDecimal(AdditionalTaxAmount.ToString("N2")) > 0)
                    {
                        objIRS2290.TotalTaxComputationAmt = Convert.ToDecimal(AdditionalTaxAmount.ToString("N2"));
                        objIRS2290.TotalTaxComputationAmtSpecified = true;
                    }*/
                    if (Convert.ToDecimal(AdditionalTaxAmount.ToString("N2")) > 0)
                    {
                        objIRS2290.TotalTaxComputationAmt = 0.01m;//Convert.ToDecimal(TaxFromTaxComputation.ToString("N2"));
                        objIRS2290.TotalTaxComputationAmtSpecified = true;
                    }
                    else
                    {
                        objIRS2290.TotalTaxComputationAmtSpecified = false;
                    }
                }
                else
                {
                    if (Convert.ToDecimal(TaxFromTaxComputation.ToString("N2")) > 0)
                    {
                        objIRS2290.TotalTaxComputationAmt = Convert.ToDecimal(TaxFromTaxComputation.ToString("N2"));
                        objIRS2290.TotalTaxComputationAmtSpecified = true;
                    }
                    else
                    {
                        objIRS2290.TotalTaxComputationAmtSpecified = false;
                    }
                }
                objIRS2290.softwareId = Variables.SoftwareID;
                objIRS2290.softwareVersion = Variables.SoftwareVersion;
                objIRS2290.documentId = "IRS2290";
                objIRS2290.documentName = "IRS2290";

                returnData.IRS2290 = objIRS2290;
                documentCount += 1;

                #endregion

                #region IRS 2290 SCHEDULE1

                Return2290_2019.IRS2290Schedule1 irs2290Schedule1 = new Return2290_2019.IRS2290Schedule1();

                if (Convert.ToInt16(numTotalTaxableVeh) > 0)
                    irs2290Schedule1.TaxableVehicleCnt = numTotalTaxableVeh.ToString();

                int Count_taxSuspendedLogVeh = Convert.ToInt16(numTaxSuspended_LoggingVeh);
                int Count_taxSuspendedNonLogVeh = Convert.ToInt16(numTaxSuspended_NonLoggingVeh);
                if (Count_taxSuspendedLogVeh + Count_taxSuspendedNonLogVeh != 0)
                    irs2290Schedule1.TotalSuspendedVehicleCnt = Convert.ToString(Count_taxSuspendedLogVeh + Count_taxSuspendedNonLogVeh);

                
                if ((numTotalTaxableVeh + Count_taxSuspendedLogVeh + Count_taxSuspendedNonLogVeh) > 0)
                    irs2290Schedule1.VehicleCnt = Convert.ToString(numTotalTaxableVeh + Count_taxSuspendedLogVeh + Count_taxSuspendedNonLogVeh);
                else
                {
                    if (IsAmendedReturn)
                    {
                        if ((numTotalTaxableVeh + Count_taxSuspendedLogVeh + Count_taxSuspendedNonLogVeh) > 0)
                        {
                            irs2290Schedule1.VehicleCnt = Convert.ToString(numTotalTaxableVeh + Count_taxSuspendedLogVeh + Count_taxSuspendedNonLogVeh);
                        }
                        else
                            irs2290Schedule1.VehicleCnt = "1";
                    }
                }

                List<Return2290_2019.IRS2290Schedule1TypeVehicleReportTaxItem> objVehRptTaxItem =
                    new List<Return2290_2019.IRS2290Schedule1TypeVehicleReportTaxItem>();

                if (dsTaxableVehicles.Tables.Count > 0)
                    foreach (DataRow ldr in dsTaxableVehicles.Tables[0].Rows)
                    {
                        Return2290_2019.IRS2290Schedule1TypeVehicleReportTaxItem vrti =
                            new Return2290_2019.IRS2290Schedule1TypeVehicleReportTaxItem();
                        vrti.VehicleCategoryCd = Convert.ToString(ldr["WeightCategory"]);
                        vrti.VIN = Convert.ToString(ldr["VIN"]);
                        objVehRptTaxItem.Add(vrti);
                    }

                if (dsTGWIncreaseWorksheet.Tables.Count > 0)
                    foreach (DataRow ldr in dsTGWIncreaseWorksheet.Tables[0].Rows)
                    {
                        Return2290_2019.IRS2290Schedule1TypeVehicleReportTaxItem vrti =
                            new Return2290_2019.IRS2290Schedule1TypeVehicleReportTaxItem();
                        vrti.VehicleCategoryCd = Convert.ToString(ldr["WeightCategoryNew"]);
                        vrti.VIN = Convert.ToString(ldr["VIN"]);
                        objVehRptTaxItem.Add(vrti);
                    }

                if (dsSuspendedVehicles.Tables.Count > 0)
                {
                    foreach (DataRow ldr in dsSuspendedVehicles.Tables[0].Rows)
                    {
                        Return2290_2019.IRS2290Schedule1TypeVehicleReportTaxItem vrti = new
                            Return2290_2019.IRS2290Schedule1TypeVehicleReportTaxItem();
                        vrti.VehicleCategoryCd = "W";
                        vrti.VIN = Convert.ToString(ldr["VIN"]);
                        objVehRptTaxItem.Add(vrti);
                    }
                }

                irs2290Schedule1.VehicleReportTaxItem = objVehRptTaxItem.ToArray();

                irs2290Schedule1.documentId = "IRS2290Schedule1";
                irs2290Schedule1.documentName = "IRS2290Schedule1";
                irs2290Schedule1.softwareId = Variables.SoftwareID;
                irs2290Schedule1.softwareVersion = Variables.SoftwareVersion;

                returnData.IRS2290Schedule1 = irs2290Schedule1;
                documentCount += 1;

                #endregion

                //2019 VIN Correction 
                #region VIN CORRECTION  Explanation Statement
                if (IsVINCorrection)
                {
                    //Return2290_2019.IRS2290Schedule1 irs2290Schedule1 = new Return2290_2019.IRS2290Schedule1();

                    Return2290_2019.VINCorrectionExplanationStmtType VinCorExp = new Return2290_2019.VINCorrectionExplanationStmtType();

                    VinCorExp.ExplanationTxt = "VINCorrection update request. Update the previosuly accepted Submission to the NEW VIN attached with the IRS2290Schedule1";
                    //objIRS2290.VINCorrectionExplanationStmt = VinCorExp;

                    VinCorExp.documentId = "VINCorrectionExplan";
                    //VinCorExp.documentName = "VINCorrectionExplanationStmtType";
                    VinCorExp.softwareId = Variables.SoftwareID;
                    VinCorExp.softwareVersion = Variables.SoftwareVersion;

                    returnData.VINCorrectionExplanationStmt = VinCorExp;
                    documentCount += 1;
                }
                #endregion   // VIN CORRECTION  Explanation Statement

                #region CREDITS AMOUNT STATEMENT

                if (dsCreditVehicles.Tables[0].Rows.Count > 0)
                {
                    int rowCount = dsCreditVehicles.Tables[0].Rows.Count;
                    List<Return2290_2019.CreditsAmountInfoTypeDisposalReportingItem> dric =
                        new List<Return2290_2019.CreditsAmountInfoTypeDisposalReportingItem>();

                    foreach (DataRow ldr in dsCreditVehicles.Tables[0].Rows)
                    {
                        Return2290_2019.CreditsAmountInfoTypeDisposalReportingItem dri =
                            new Return2290_2019.CreditsAmountInfoTypeDisposalReportingItem();
                        dri.CreditsAmountExplanationTxt = Convert.ToString(ldr["Reason"]);
                        dri.DisposalReportingAmt = Convert.ToString(ldr["TaxAmount"]);
                        dri.DisposalReportingDt = Convert.ToDateTime(ldr["EffectiveDate"]);
                        dri.DisposalReportingVIN = Convert.ToString(ldr["VIN"]);

                        dric.Add(dri);
                    }

                    Return2290_2019.CreditsAmountStatement cas = new Return2290_2019.CreditsAmountStatement();
                    cas.CreditsAmountInfo = dric.ToArray();
                    cas.documentId = "CreditsAmountStatement";
                    cas.documentName = "CreditsAmountStatement";
                    cas.softwareId = Variables.SoftwareID;
                    cas.softwareVersion = Variables.SoftwareVersion;

                    Return2290_2019.CreditsAmountStatement[] casc = new Return2290_2019.CreditsAmountStatement[1];
                    casc[0] = cas;

                    returnData.CreditsAmountStatement = casc;
                    documentCount += 1;
                }
                #endregion

                #region STATEMENT IN SUPPORT OF SUSPENSION //Sold Suspended

                if (dsSoldSuspendedVehicles.Tables[0].Rows.Count > 0)
                {
                    List<Return2290_2019.StmtInSupportOfSuspensionInfoTypeVehicleSuspensionDetail> ic =
                        new List<Return2290_2019.StmtInSupportOfSuspensionInfoTypeVehicleSuspensionDetail>();

                    foreach (DataRow ldr in dsSoldSuspendedVehicles.Tables[0].Rows)
                    {
                        Return2290_2019.StmtInSupportOfSuspensionInfoTypeVehicleSuspensionDetail i =
                            new Return2290_2019.StmtInSupportOfSuspensionInfoTypeVehicleSuspensionDetail();
                        i.Dt = Convert.ToDateTime(ldr["DateSold"]);
                        Return2290_2019.BusinessNameType bnt = new Return2290_2019.BusinessNameType();
                        bnt.BusinessNameLine1Txt = Convert.ToString(ldr["Buyer"]);
                        i.BusinessName = bnt;
                        i.VIN = Convert.ToString(ldr["VIN"]);
                        ic.Add(i);
                    }

                    Return2290_2019.StmtInSupportOfSuspension sisosit = new Return2290_2019.StmtInSupportOfSuspension();
                    sisosit.StmtInSupportOfSuspensionInfo = ic.ToArray();

                    List<Return2290_2019.StmtInSupportOfSuspension> sisostc = new List<Return2290_2019.StmtInSupportOfSuspension>();
                    sisostc.Add(sisosit);

                    sisosit.documentId = "StatementInSupportOfSuspension";
                    sisosit.documentName = "StatementInSupportOfSuspension";
                    sisosit.softwareId = Variables.SoftwareID;
                    sisosit.softwareVersion = Variables.SoftwareVersion;

                    returnData.StmtInSupportOfSuspension = sisostc.ToArray();
                    documentCount += 1;
                }
                #endregion

                #region SUSPENDED VIN STATEMENT

                if (dsPriorYearMileageExceededVehicles.Tables[0].Rows.Count > 0)
                {
                    List<Return2290_2019.SuspendedVINInfoTypeVINDetail> objSVITVDList =
                        new List<Return2290_2019.SuspendedVINInfoTypeVINDetail>();
                    foreach (DataRow ldr in dsPriorYearMileageExceededVehicles.Tables[0].Rows)
                    {
                        Return2290_2019.SuspendedVINInfoTypeVINDetail objSVITVD = new Return2290_2019.SuspendedVINInfoTypeVINDetail();
                        objSVITVD.VIN = Convert.ToString(ldr["VIN"]);
                        objSVITVDList.Add(objSVITVD);
                    }

                    Return2290_2019.SuspendedVINStatement objSVS = new Return2290_2019.SuspendedVINStatement();
                    objSVS.SuspendedVINInfo = objSVITVDList.ToArray();
                    objSVS.documentId = "SuspendedVINStatement";
                    objSVS.documentName = "SuspendedVINStatement";
                    objSVS.softwareId = Variables.SoftwareID;
                    objSVS.softwareVersion = Variables.SoftwareVersion;

                    Return2290_2019.SuspendedVINStatement[] suspendedVINStmt = new Return2290_2019.SuspendedVINStatement[1];
                    suspendedVINStmt[0] = objSVS;

                    returnData.SuspendedVINStatement = suspendedVINStmt;
                    documentCount += 1;
                }
                #endregion

                #region TGW INCREASE WORKSHEET

                if (dsTGWIncreaseWorksheet.Tables[0].Rows.Count > 0)
                {
                    List<Return2290_2019.TGWIncreaseInfoType> tgwiitc = new List<Return2290_2019.TGWIncreaseInfoType>();
                    foreach (DataRow ldr in dsTGWIncreaseWorksheet.Tables[0].Rows)
                    {
                        Return2290_2019.TGWIncreaseInfoType tgwiit = new Return2290_2019.TGWIncreaseInfoType();

                        tgwiit.AdditionalTaxAmt = Convert.ToDecimal(ldr["TaxAmount"]);
                        tgwiit.AdditionalTaxAmtSpecified = true;
                        tgwiit.TGWIncreaseMonthNum = Convert.ToString(ldr["IncreasedMonthValue"]);
                        tgwiit.NewTaxAmt = Convert.ToDecimal(ldr["NewTaxAmount"]);
                        tgwiit.NewTaxAmtSpecified = true;
                        tgwiit.PreviousTaxAmt = Convert.ToDecimal(ldr["PreviousTaxAmount"]);
                        tgwiit.PreviousTaxAmtSpecified = true;
                        tgwiit.TGWCategoryCd = Convert.ToString(ldr["WeightCategoryNew"]);
                        tgwiitc.Add(tgwiit);
                    }

                    Return2290_2019.TGWIncreaseWorksheet tgwIncreaseWorksheet = new Return2290_2019.TGWIncreaseWorksheet();
                    tgwIncreaseWorksheet.TGWIncreaseInfo = tgwiitc.ToArray();
                    tgwIncreaseWorksheet.documentId = "TGWIncreaseWorksheet";
                    //tgwIncreaseWorksheet.__documentName = "TGWIncreaseWorksheet";
                    tgwIncreaseWorksheet.softwareId = Variables.SoftwareID;
                    tgwIncreaseWorksheet.softwareVersion = Variables.SoftwareVersion;

                    List<Return2290_2019.TGWIncreaseWorksheet> tgwiwc = new List<Return2290_2019.TGWIncreaseWorksheet>();
                    tgwiwc.Add(tgwIncreaseWorksheet);

                    returnData.TGWIncreaseWorksheet = tgwiwc.ToArray();
                    documentCount += 1;
                }
                #endregion

                returnData.__documentCount = documentCount.ToString();

                #endregion

                #region RETURN

                Return2290_2019.Return return2290 = new Return2290_2019.Return();
                return2290.ReturnData = returnData;
                return2290.ReturnHeader = returnHeaderType;
                return2290.returnVersion = Variables.SoftwareVersion;

                #endregion

                #region Create XML file

                IFormatter ft = new BinaryFormatter();

                ft.Serialize(mStream, return2290);

                mStream.Position = 0;

                Return2290_2019.Return irsForm2 = (Return2290_2019.Return)ft.Deserialize(mStream);

                s.Serialize(fStream2, irsForm2);

                if (mStream != null) mStream.Close();

                if (fStream2 != null) fStream2.Close();

                if (fStream != null) fStream.Close();

                try
                {
                    XmlDocument xdoc = new XmlDocument();
                    xdoc.PreserveWhitespace = false;
                    xdoc.Load("C:\\xml\\submission.xml");
                    xdoc.DocumentElement.SetAttribute("xmlns:efile", "http://www.irs.gov/efile");
                    XmlTextWriter xmlWriter = new XmlTextWriter("C:\\" + @"\xml\submission.xml", new UTF8Encoding(false));
                    xdoc.Save(xmlWriter);
                    xmlWriter.Flush();
                    xmlWriter.Close();
                }
                catch { }

                #endregion

            }
            catch (Exception ex)
            {
                if (fStream != null) fStream.Close();
                if (fStream2 != null) fStream2.Close();
                if (mStream != null) mStream.Close();
                throw ex;
            }
        }
        #endregion

        #region createReturnXml_2290_2018

        /* Vishwa New 2018 Changes, for the XML building for 2018 */
        public void createReturnXml_2290_2018
        (
        #region Parameters
                bool IsAddressChange, bool IsAmendedReturn, bool IsVINCorrection, bool IsFinalReturn,
                bool IsNotSubjectToTaxChecked, bool IsConsentToDiscloseYes, string AmendedMonth, string TaxYear,
                string First_Used_Date, string Filer_AddressLine1, string Filer_City, string Filer_EIN,
                string Filer_Foreign_Country, string Filer_Name, string Filer_NameControl, string Filer_State,
                string Filer_ZIPCode, string Officer_Name, string Officer_Phone, string Officer_PIN,
                string Officer_Title, string Officer_email, string ThirdPartyDesigne_Name, string ThirdPartyDesigne_Phone,
                string ThirdPartyDesigne_PIN, string Preparer_Firm, string Preparer_EIN, string Preparer_City,
                string Preparer_State, string Preparer_Zip, string Preparer_Country, string Preparer_Address,
                string Preparer_ForeignPhone, string Preparer_Phone, string Preparer_Email, string Preparer_Name,
                string Preparer_PTIN_SSN, bool IsPreparerHasPTIN, bool IsPreparerSelfEmployed, string paymentType,
                string Payment_Acc_No, string Payment_Acc_Type, string Payment_ReqPayDate, string Payment_RoutingTransitNo,
                string Payment_Txpyer_Ph, DataSet dsTaxableVehicles, DataSet dsCreditVehicles, DataSet dsSuspendedVehicles,
                DataSet dsSoldSuspendedVehicles, DataSet dsPriorYearMileageExceededVehicles, DataSet dsTGWIncreaseWorksheet, DataSet dsTaxComputation,
                string soldSuspendedVehicles, decimal TaxFromTaxComputation, decimal AdditionalTaxAmount, decimal CreditAmount,
                string TotalVehicles
        #endregion
        )
        {
            int documentCount = 0;
            FileStream fStream = null;
            FileStream fStream2 = null;
            MemoryStream mStream = null;

            int numTotalTaxableVeh = 0;
            int numTaxSuspended_LoggingVeh = 0;
            int numTaxSuspended_NonLoggingVeh = 0;

            int numNonLoggingVehicleCnt = 0;
            int numLoggingVehicleCnt = 0;

            try
            {
                if (!Directory.Exists(path2CreateZipFile + @"\xml"))
                    Directory.CreateDirectory(path2CreateZipFile + @"\xml");

                fStream2 = new FileStream(path2CreateZipFile + @"\xml\submission.xml", FileMode.Create);

                byte[] byteArray = new byte[90000];
                mStream = new MemoryStream(byteArray);
                XmlSerializer s = new XmlSerializer(typeof(Return2290_2018.Return));

                #region RETRUN_HEADER

                Return2290_2018.ReturnHeaderType returnHeaderType = new Return2290_2018.ReturnHeaderType();

                returnHeaderType.ReturnTs = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz");

                returnHeaderType.FirstUsedDt = First_Used_Date;

                returnHeaderType.MultSoftwarePackagesUsedInd = false;
                returnHeaderType.PINEnteredByCd = Return2290_2018.ReturnHeaderTypePINEnteredByCd.Taxpayer;
                returnHeaderType.PINEnteredByCdSpecified = true;
                returnHeaderType.ReturnTypeCd = Return2290_2018.ReturnHeaderTypeReturnTypeCd.Item2290;
                returnHeaderType.SignatureOptionCd = Return2290_2018.ReturnHeaderTypeSignatureOptionCd.PINNumber;
                returnHeaderType.SignatureOptionCdSpecified = true;

                #region ORIGINATOR

                #region PRACTIONER PIN

                Return2290_2018.ReturnHeaderTypeOriginatorGrpPractitionerPINGrp objPractitionerPIN =
                    new Return2290_2018.ReturnHeaderTypeOriginatorGrpPractitionerPINGrp();
                objPractitionerPIN.EFIN = Variables.EFIN;
                objPractitionerPIN.PIN = Variables.PIN;

                #endregion

                Return2290_2018.ReturnHeaderTypeOriginatorGrp originator = new Return2290_2018.ReturnHeaderTypeOriginatorGrp();
                originator.EFIN = Variables.EFIN;
                originator.OriginatorTypeCd = Return2290_2018.OriginatorType.ERO;
                originator.PractitionerPINGrp = objPractitionerPIN;
                returnHeaderType.OriginatorGrp = originator;

                #endregion

                #region FILER
                Return2290_2018.ReturnHeaderTypeFiler objFiler = new Return2290_2018.ReturnHeaderTypeFiler();

                objFiler.EIN = Filer_EIN;
                objFiler.BusinessNameLine1Txt = Filer_Name; //max_length=60
                objFiler.BusinessNameControlTxt = Filer_NameControl;

                if (Filer_Foreign_Country == "" || Filer_Foreign_Country == "US")
                {
                    #region FILER US ADDRESS

                    Return2290_2018.USAddressType usAdd_Filer = new Return2290_2018.USAddressType();

                    if (Filer_AddressLine1.Length > 35)
                    {
                        string Filer_AddressLine2 = string.Empty;
                        usAdd_Filer.AddressLine1 = Filer_AddressLine1.Substring(0, 35).Trim();
                        Filer_AddressLine2 = Filer_AddressLine1.Substring(35);
                        usAdd_Filer.AddressLine2 = Filer_AddressLine2.Trim();

                    }
                    else
                    {
                        usAdd_Filer.AddressLine1 = Filer_AddressLine1.Trim();
                    }
                    usAdd_Filer.City = Filer_City.Trim();

                    switch (Filer_State)
                    {
                        #region Filer_State
                        case "AA": usAdd_Filer.State = Return2290_2018.StateType.AA; break;
                        case "AE": usAdd_Filer.State = Return2290_2018.StateType.AE; break;
                        case "AK": usAdd_Filer.State = Return2290_2018.StateType.AK; break;
                        case "AL": usAdd_Filer.State = Return2290_2018.StateType.AL; break;
                        case "AP": usAdd_Filer.State = Return2290_2018.StateType.AP; break;
                        case "AR": usAdd_Filer.State = Return2290_2018.StateType.AR; break;
                        case "AS": usAdd_Filer.State = Return2290_2018.StateType.AS; break;
                        case "AZ": usAdd_Filer.State = Return2290_2018.StateType.AZ; break;
                        case "CA": usAdd_Filer.State = Return2290_2018.StateType.CA; break;
                        case "CO": usAdd_Filer.State = Return2290_2018.StateType.CO; break;
                        case "CT": usAdd_Filer.State = Return2290_2018.StateType.CT; break;
                        case "DC": usAdd_Filer.State = Return2290_2018.StateType.DC; break;
                        case "DE": usAdd_Filer.State = Return2290_2018.StateType.DE; break;
                        case "FL": usAdd_Filer.State = Return2290_2018.StateType.FL; break;
                        case "FM": usAdd_Filer.State = Return2290_2018.StateType.FM; break;
                        case "GA": usAdd_Filer.State = Return2290_2018.StateType.GA; break;
                        case "GU": usAdd_Filer.State = Return2290_2018.StateType.GU; break;
                        case "HI": usAdd_Filer.State = Return2290_2018.StateType.HI; break;
                        case "IA": usAdd_Filer.State = Return2290_2018.StateType.IA; break;
                        case "ID": usAdd_Filer.State = Return2290_2018.StateType.ID; break;
                        case "IL": usAdd_Filer.State = Return2290_2018.StateType.IL; break;
                        case "IN": usAdd_Filer.State = Return2290_2018.StateType.IN; break;
                        case "KS": usAdd_Filer.State = Return2290_2018.StateType.KS; break;
                        case "KY": usAdd_Filer.State = Return2290_2018.StateType.KY; break;
                        case "LA": usAdd_Filer.State = Return2290_2018.StateType.LA; break;
                        case "MA": usAdd_Filer.State = Return2290_2018.StateType.MA; break;
                        case "MD": usAdd_Filer.State = Return2290_2018.StateType.MD; break;
                        case "ME": usAdd_Filer.State = Return2290_2018.StateType.ME; break;
                        case "MH": usAdd_Filer.State = Return2290_2018.StateType.MH; break;
                        case "MI": usAdd_Filer.State = Return2290_2018.StateType.MI; break;
                        case "MN": usAdd_Filer.State = Return2290_2018.StateType.MN; break;
                        case "MO": usAdd_Filer.State = Return2290_2018.StateType.MO; break;
                        case "MP": usAdd_Filer.State = Return2290_2018.StateType.MP; break;
                        case "MS": usAdd_Filer.State = Return2290_2018.StateType.MS; break;
                        case "MT": usAdd_Filer.State = Return2290_2018.StateType.MT; break;
                        case "NC": usAdd_Filer.State = Return2290_2018.StateType.NC; break;
                        case "ND": usAdd_Filer.State = Return2290_2018.StateType.ND; break;
                        case "NE": usAdd_Filer.State = Return2290_2018.StateType.NE; break;
                        case "NH": usAdd_Filer.State = Return2290_2018.StateType.NH; break;
                        case "NJ": usAdd_Filer.State = Return2290_2018.StateType.NJ; break;
                        case "NM": usAdd_Filer.State = Return2290_2018.StateType.NM; break;
                        case "NV": usAdd_Filer.State = Return2290_2018.StateType.NV; break;
                        case "NY": usAdd_Filer.State = Return2290_2018.StateType.NY; break;
                        case "OH": usAdd_Filer.State = Return2290_2018.StateType.OH; break;
                        case "OK": usAdd_Filer.State = Return2290_2018.StateType.OK; break;
                        case "OR": usAdd_Filer.State = Return2290_2018.StateType.OR; break;
                        case "PA": usAdd_Filer.State = Return2290_2018.StateType.PA; break;
                        case "PR": usAdd_Filer.State = Return2290_2018.StateType.PR; break;
                        case "PW": usAdd_Filer.State = Return2290_2018.StateType.PW; break;
                        case "RI": usAdd_Filer.State = Return2290_2018.StateType.RI; break;
                        case "SC": usAdd_Filer.State = Return2290_2018.StateType.SC; break;
                        case "SD": usAdd_Filer.State = Return2290_2018.StateType.SD; break;
                        case "TN": usAdd_Filer.State = Return2290_2018.StateType.TN; break;
                        case "TX": usAdd_Filer.State = Return2290_2018.StateType.TX; break;
                        case "UT": usAdd_Filer.State = Return2290_2018.StateType.UT; break;
                        case "VA": usAdd_Filer.State = Return2290_2018.StateType.VA; break;
                        case "VI": usAdd_Filer.State = Return2290_2018.StateType.VI; break;
                        case "VT": usAdd_Filer.State = Return2290_2018.StateType.VT; break;
                        case "WA": usAdd_Filer.State = Return2290_2018.StateType.WA; break;
                        case "WI": usAdd_Filer.State = Return2290_2018.StateType.WI; break;
                        case "WV": usAdd_Filer.State = Return2290_2018.StateType.WV; break;
                        case "WY": usAdd_Filer.State = Return2290_2018.StateType.WY; break;
                        #endregion
                    }

                    usAdd_Filer.ZIPCode = Filer_ZIPCode;

                    objFiler.Item = usAdd_Filer;
                    #endregion
                }
                else
                {
                    #region FILER FOREIGN ADDRESS
                    Return2290_2018.ForeignAddressType foreignAdd_Filer = new Return2290_2018.ForeignAddressType();

                    if (Filer_AddressLine1.Length > 35)
                    {
                        string Filer_AddressLine2 = string.Empty;
                        foreignAdd_Filer.AddressLine1 = Filer_AddressLine1.Substring(0, 35).Trim();
                        Filer_AddressLine2 = Filer_AddressLine1.Substring(35);
                        foreignAdd_Filer.AddressLine2 = Filer_AddressLine2.Trim();
                    }
                    else
                    {
                        foreignAdd_Filer.AddressLine1 = Filer_AddressLine1.Trim();
                    }

                    foreignAdd_Filer.City = Filer_City.Trim();

                    if (Filer_State.Length > 0)
                        foreignAdd_Filer.ProvinceOrState = Filer_State;

                    switch (Filer_Foreign_Country)
                    {
                        #region Filer_Foreign_Country
                        case "AA": foreignAdd_Filer.Country = Return2290_2018.CountryType.AA; break;
                        case "AC": foreignAdd_Filer.Country = Return2290_2018.CountryType.AC; break;
                        case "AE": foreignAdd_Filer.Country = Return2290_2018.CountryType.AE; break;
                        case "AF": foreignAdd_Filer.Country = Return2290_2018.CountryType.AF; break;
                        case "AG": foreignAdd_Filer.Country = Return2290_2018.CountryType.AG; break;
                        case "AJ": foreignAdd_Filer.Country = Return2290_2018.CountryType.AJ; break;
                        case "AL": foreignAdd_Filer.Country = Return2290_2018.CountryType.AL; break;
                        case "AM": foreignAdd_Filer.Country = Return2290_2018.CountryType.AM; break;
                        case "AN": foreignAdd_Filer.Country = Return2290_2018.CountryType.AN; break;
                        case "AO": foreignAdd_Filer.Country = Return2290_2018.CountryType.AO; break;
                        case "AR": foreignAdd_Filer.Country = Return2290_2018.CountryType.AR; break;
                        case "AS": foreignAdd_Filer.Country = Return2290_2018.CountryType.AS; break;
                        case "AT": foreignAdd_Filer.Country = Return2290_2018.CountryType.AT; break;
                        case "AU": foreignAdd_Filer.Country = Return2290_2018.CountryType.AU; break;
                        case "AV": foreignAdd_Filer.Country = Return2290_2018.CountryType.AV; break;
                        case "AX": foreignAdd_Filer.Country = Return2290_2018.CountryType.AX; break;
                        case "AY": foreignAdd_Filer.Country = Return2290_2018.CountryType.AY; break;
                        case "BA": foreignAdd_Filer.Country = Return2290_2018.CountryType.BA; break;
                        case "BB": foreignAdd_Filer.Country = Return2290_2018.CountryType.BB; break;
                        case "BC": foreignAdd_Filer.Country = Return2290_2018.CountryType.BC; break;
                        case "BD": foreignAdd_Filer.Country = Return2290_2018.CountryType.BD; break;
                        case "BE": foreignAdd_Filer.Country = Return2290_2018.CountryType.BE; break;
                        case "BF": foreignAdd_Filer.Country = Return2290_2018.CountryType.BF; break;
                        case "BG": foreignAdd_Filer.Country = Return2290_2018.CountryType.BG; break;
                        case "BH": foreignAdd_Filer.Country = Return2290_2018.CountryType.BH; break;
                        case "BK": foreignAdd_Filer.Country = Return2290_2018.CountryType.BK; break;
                        case "BL": foreignAdd_Filer.Country = Return2290_2018.CountryType.BL; break;
                        case "BM": foreignAdd_Filer.Country = Return2290_2018.CountryType.BM; break;
                        case "BN": foreignAdd_Filer.Country = Return2290_2018.CountryType.BN; break;
                        case "BO": foreignAdd_Filer.Country = Return2290_2018.CountryType.BO; break;
                        case "BP": foreignAdd_Filer.Country = Return2290_2018.CountryType.BP; break;
                        case "BQ": foreignAdd_Filer.Country = Return2290_2018.CountryType.BQ; break;
                        case "BR": foreignAdd_Filer.Country = Return2290_2018.CountryType.BR; break;
                        case "BT": foreignAdd_Filer.Country = Return2290_2018.CountryType.BT; break;
                        case "BU": foreignAdd_Filer.Country = Return2290_2018.CountryType.BU; break;
                        case "BV": foreignAdd_Filer.Country = Return2290_2018.CountryType.BV; break;
                        case "BX": foreignAdd_Filer.Country = Return2290_2018.CountryType.BX; break;
                        case "BY": foreignAdd_Filer.Country = Return2290_2018.CountryType.BY; break;
                        case "CA": foreignAdd_Filer.Country = Return2290_2018.CountryType.CA; break;
                        case "CB": foreignAdd_Filer.Country = Return2290_2018.CountryType.CB; break;
                        case "CD": foreignAdd_Filer.Country = Return2290_2018.CountryType.CD; break;
                        case "CE": foreignAdd_Filer.Country = Return2290_2018.CountryType.CE; break;
                        case "CF": foreignAdd_Filer.Country = Return2290_2018.CountryType.CF; break;
                        case "CG": foreignAdd_Filer.Country = Return2290_2018.CountryType.CG; break;
                        case "CH": foreignAdd_Filer.Country = Return2290_2018.CountryType.CH; break;
                        case "CI": foreignAdd_Filer.Country = Return2290_2018.CountryType.CI; break;
                        case "CJ": foreignAdd_Filer.Country = Return2290_2018.CountryType.CJ; break;
                        case "CK": foreignAdd_Filer.Country = Return2290_2018.CountryType.CK; break;
                        case "CM": foreignAdd_Filer.Country = Return2290_2018.CountryType.CM; break;
                        case "CN": foreignAdd_Filer.Country = Return2290_2018.CountryType.CN; break;
                        case "CO": foreignAdd_Filer.Country = Return2290_2018.CountryType.CO; break;
                        case "CR": foreignAdd_Filer.Country = Return2290_2018.CountryType.CR; break;
                        case "CS": foreignAdd_Filer.Country = Return2290_2018.CountryType.CS; break;
                        case "CT": foreignAdd_Filer.Country = Return2290_2018.CountryType.CT; break;
                        case "CU": foreignAdd_Filer.Country = Return2290_2018.CountryType.CU; break;
                        case "CV": foreignAdd_Filer.Country = Return2290_2018.CountryType.CV; break;
                        case "CW": foreignAdd_Filer.Country = Return2290_2018.CountryType.CW; break;
                        case "CY": foreignAdd_Filer.Country = Return2290_2018.CountryType.CY; break;
                        case "DA": foreignAdd_Filer.Country = Return2290_2018.CountryType.DA; break;
                        case "DJ": foreignAdd_Filer.Country = Return2290_2018.CountryType.DJ; break;
                        case "DO": foreignAdd_Filer.Country = Return2290_2018.CountryType.DO; break;
                        case "DR": foreignAdd_Filer.Country = Return2290_2018.CountryType.DR; break;
                        case "DX": foreignAdd_Filer.Country = Return2290_2018.CountryType.DX; break;
                        case "EC": foreignAdd_Filer.Country = Return2290_2018.CountryType.EC; break;
                        case "EG": foreignAdd_Filer.Country = Return2290_2018.CountryType.EG; break;
                        case "EI": foreignAdd_Filer.Country = Return2290_2018.CountryType.EI; break;
                        case "EK": foreignAdd_Filer.Country = Return2290_2018.CountryType.EK; break;
                        case "EN": foreignAdd_Filer.Country = Return2290_2018.CountryType.EN; break;
                        case "ER": foreignAdd_Filer.Country = Return2290_2018.CountryType.ER; break;
                        case "ES": foreignAdd_Filer.Country = Return2290_2018.CountryType.ES; break;
                        case "ET": foreignAdd_Filer.Country = Return2290_2018.CountryType.ET; break;
                        case "EZ": foreignAdd_Filer.Country = Return2290_2018.CountryType.EZ; break;
                        case "FI": foreignAdd_Filer.Country = Return2290_2018.CountryType.FI; break;
                        case "FJ": foreignAdd_Filer.Country = Return2290_2018.CountryType.FJ; break;
                        case "FK": foreignAdd_Filer.Country = Return2290_2018.CountryType.FK; break;
                        case "FO": foreignAdd_Filer.Country = Return2290_2018.CountryType.FO; break;
                        case "FP": foreignAdd_Filer.Country = Return2290_2018.CountryType.FP; break;
                        case "FQ": foreignAdd_Filer.Country = Return2290_2018.CountryType.FQ; break;
                        case "FR": foreignAdd_Filer.Country = Return2290_2018.CountryType.FR; break;
                        case "FS": foreignAdd_Filer.Country = Return2290_2018.CountryType.FS; break;
                        case "GA": foreignAdd_Filer.Country = Return2290_2018.CountryType.GA; break;
                        case "GB": foreignAdd_Filer.Country = Return2290_2018.CountryType.GB; break;
                        case "GG": foreignAdd_Filer.Country = Return2290_2018.CountryType.GG; break;
                        case "GH": foreignAdd_Filer.Country = Return2290_2018.CountryType.GH; break;
                        case "GI": foreignAdd_Filer.Country = Return2290_2018.CountryType.GI; break;
                        case "GJ": foreignAdd_Filer.Country = Return2290_2018.CountryType.GJ; break;
                        case "GK": foreignAdd_Filer.Country = Return2290_2018.CountryType.GK; break;
                        case "GL": foreignAdd_Filer.Country = Return2290_2018.CountryType.GL; break;
                        case "GM": foreignAdd_Filer.Country = Return2290_2018.CountryType.GM; break;
                        case "GR": foreignAdd_Filer.Country = Return2290_2018.CountryType.GR; break;
                        case "GT": foreignAdd_Filer.Country = Return2290_2018.CountryType.GT; break;
                        case "GV": foreignAdd_Filer.Country = Return2290_2018.CountryType.GV; break;
                        case "GY": foreignAdd_Filer.Country = Return2290_2018.CountryType.GY; break;
                        case "HA": foreignAdd_Filer.Country = Return2290_2018.CountryType.HA; break;
                        case "HK": foreignAdd_Filer.Country = Return2290_2018.CountryType.HK; break;
                        case "HM": foreignAdd_Filer.Country = Return2290_2018.CountryType.HM; break;
                        case "HO": foreignAdd_Filer.Country = Return2290_2018.CountryType.HO; break;
                        case "HQ": foreignAdd_Filer.Country = Return2290_2018.CountryType.HQ; break;
                        case "HR": foreignAdd_Filer.Country = Return2290_2018.CountryType.HR; break;
                        case "HU": foreignAdd_Filer.Country = Return2290_2018.CountryType.HU; break;
                        case "IC": foreignAdd_Filer.Country = Return2290_2018.CountryType.IC; break;
                        case "ID": foreignAdd_Filer.Country = Return2290_2018.CountryType.ID; break;
                        case "IM": foreignAdd_Filer.Country = Return2290_2018.CountryType.IM; break;
                        case "IO": foreignAdd_Filer.Country = Return2290_2018.CountryType.IO; break;
                        case "IP": foreignAdd_Filer.Country = Return2290_2018.CountryType.IP; break;
                        case "IR": foreignAdd_Filer.Country = Return2290_2018.CountryType.IR; break;
                        case "IS": foreignAdd_Filer.Country = Return2290_2018.CountryType.IS; break;
                        case "IT": foreignAdd_Filer.Country = Return2290_2018.CountryType.IT; break;
                        case "IV": foreignAdd_Filer.Country = Return2290_2018.CountryType.IV; break;
                        case "IZ": foreignAdd_Filer.Country = Return2290_2018.CountryType.IZ; break;
                        case "JA": foreignAdd_Filer.Country = Return2290_2018.CountryType.JA; break;
                        case "JE": foreignAdd_Filer.Country = Return2290_2018.CountryType.JE; break;
                        case "JM": foreignAdd_Filer.Country = Return2290_2018.CountryType.JM; break;
                        case "JN": foreignAdd_Filer.Country = Return2290_2018.CountryType.JN; break;
                        case "JO": foreignAdd_Filer.Country = Return2290_2018.CountryType.JO; break;
                        case "JQ": foreignAdd_Filer.Country = Return2290_2018.CountryType.JQ; break;
                        case "KE": foreignAdd_Filer.Country = Return2290_2018.CountryType.KE; break;
                        case "KG": foreignAdd_Filer.Country = Return2290_2018.CountryType.KG; break;
                        case "KN": foreignAdd_Filer.Country = Return2290_2018.CountryType.KN; break;
                        case "KQ": foreignAdd_Filer.Country = Return2290_2018.CountryType.KQ; break;
                        case "KR": foreignAdd_Filer.Country = Return2290_2018.CountryType.KR; break;
                        case "KS": foreignAdd_Filer.Country = Return2290_2018.CountryType.KS; break;
                        case "KT": foreignAdd_Filer.Country = Return2290_2018.CountryType.KT; break;
                        case "KU": foreignAdd_Filer.Country = Return2290_2018.CountryType.KU; break;
                        case "KV": foreignAdd_Filer.Country = Return2290_2018.CountryType.KV; break;
                        case "KZ": foreignAdd_Filer.Country = Return2290_2018.CountryType.KZ; break;
                        case "LA": foreignAdd_Filer.Country = Return2290_2018.CountryType.LA; break;
                        case "LE": foreignAdd_Filer.Country = Return2290_2018.CountryType.LE; break;
                        case "LG": foreignAdd_Filer.Country = Return2290_2018.CountryType.LG; break;
                        case "LH": foreignAdd_Filer.Country = Return2290_2018.CountryType.LH; break;
                        case "LI": foreignAdd_Filer.Country = Return2290_2018.CountryType.LI; break;
                        case "LO": foreignAdd_Filer.Country = Return2290_2018.CountryType.LO; break;
                        case "LQ": foreignAdd_Filer.Country = Return2290_2018.CountryType.LQ; break;
                        case "LS": foreignAdd_Filer.Country = Return2290_2018.CountryType.LS; break;
                        case "LT": foreignAdd_Filer.Country = Return2290_2018.CountryType.LT; break;
                        case "LU": foreignAdd_Filer.Country = Return2290_2018.CountryType.LU; break;
                        case "LY": foreignAdd_Filer.Country = Return2290_2018.CountryType.LY; break;
                        case "MA": foreignAdd_Filer.Country = Return2290_2018.CountryType.MA; break;
                        case "MC": foreignAdd_Filer.Country = Return2290_2018.CountryType.MC; break;
                        case "MD": foreignAdd_Filer.Country = Return2290_2018.CountryType.MD; break;
                        case "MG": foreignAdd_Filer.Country = Return2290_2018.CountryType.MG; break;
                        case "MH": foreignAdd_Filer.Country = Return2290_2018.CountryType.MH; break;
                        case "MI": foreignAdd_Filer.Country = Return2290_2018.CountryType.MI; break;
                        case "MJ": foreignAdd_Filer.Country = Return2290_2018.CountryType.MJ; break;
                        case "MK": foreignAdd_Filer.Country = Return2290_2018.CountryType.MK; break;
                        case "ML": foreignAdd_Filer.Country = Return2290_2018.CountryType.ML; break;
                        case "MN": foreignAdd_Filer.Country = Return2290_2018.CountryType.MN; break;
                        case "MO": foreignAdd_Filer.Country = Return2290_2018.CountryType.MO; break;
                        case "MP": foreignAdd_Filer.Country = Return2290_2018.CountryType.MP; break;
                        case "MQ": foreignAdd_Filer.Country = Return2290_2018.CountryType.MQ; break;
                        case "MR": foreignAdd_Filer.Country = Return2290_2018.CountryType.MR; break;
                        case "MT": foreignAdd_Filer.Country = Return2290_2018.CountryType.MT; break;
                        case "MU": foreignAdd_Filer.Country = Return2290_2018.CountryType.MU; break;
                        case "MV": foreignAdd_Filer.Country = Return2290_2018.CountryType.MV; break;
                        case "MX": foreignAdd_Filer.Country = Return2290_2018.CountryType.MX; break;
                        case "MY": foreignAdd_Filer.Country = Return2290_2018.CountryType.MY; break;
                        case "MZ": foreignAdd_Filer.Country = Return2290_2018.CountryType.MZ; break;
                        case "NC": foreignAdd_Filer.Country = Return2290_2018.CountryType.NC; break;
                        case "NE": foreignAdd_Filer.Country = Return2290_2018.CountryType.NE; break;
                        case "NF": foreignAdd_Filer.Country = Return2290_2018.CountryType.NF; break;
                        case "NG": foreignAdd_Filer.Country = Return2290_2018.CountryType.NG; break;
                        case "NH": foreignAdd_Filer.Country = Return2290_2018.CountryType.NH; break;
                        case "NI": foreignAdd_Filer.Country = Return2290_2018.CountryType.NI; break;
                        case "NL": foreignAdd_Filer.Country = Return2290_2018.CountryType.NL; break;
                        case "NN": foreignAdd_Filer.Country = Return2290_2018.CountryType.NN; break;
                        case "NO": foreignAdd_Filer.Country = Return2290_2018.CountryType.NO; break;
                        case "NP": foreignAdd_Filer.Country = Return2290_2018.CountryType.NP; break;
                        case "NR": foreignAdd_Filer.Country = Return2290_2018.CountryType.NR; break;
                        case "NS": foreignAdd_Filer.Country = Return2290_2018.CountryType.NS; break;
                        case "NU": foreignAdd_Filer.Country = Return2290_2018.CountryType.NU; break;
                        case "NZ": foreignAdd_Filer.Country = Return2290_2018.CountryType.NZ; break;
                        case "PA": foreignAdd_Filer.Country = Return2290_2018.CountryType.PA; break;
                        case "PC": foreignAdd_Filer.Country = Return2290_2018.CountryType.PC; break;
                        case "PE": foreignAdd_Filer.Country = Return2290_2018.CountryType.PE; break;
                        case "PF": foreignAdd_Filer.Country = Return2290_2018.CountryType.PF; break;
                        case "PG": foreignAdd_Filer.Country = Return2290_2018.CountryType.PG; break;
                        case "PK": foreignAdd_Filer.Country = Return2290_2018.CountryType.PK; break;
                        case "PL": foreignAdd_Filer.Country = Return2290_2018.CountryType.PL; break;
                        case "PM": foreignAdd_Filer.Country = Return2290_2018.CountryType.PM; break;
                        case "PO": foreignAdd_Filer.Country = Return2290_2018.CountryType.PO; break;
                        case "PP": foreignAdd_Filer.Country = Return2290_2018.CountryType.PP; break;
                        case "PU": foreignAdd_Filer.Country = Return2290_2018.CountryType.PU; break;
                        case "QA": foreignAdd_Filer.Country = Return2290_2018.CountryType.QA; break;
                        case "RI": foreignAdd_Filer.Country = Return2290_2018.CountryType.RI; break;
                        case "RN": foreignAdd_Filer.Country = Return2290_2018.CountryType.RN; break;
                        case "RO": foreignAdd_Filer.Country = Return2290_2018.CountryType.RO; break;
                        case "RP": foreignAdd_Filer.Country = Return2290_2018.CountryType.RP; break;
                        case "RS": foreignAdd_Filer.Country = Return2290_2018.CountryType.RS; break;
                        case "RW": foreignAdd_Filer.Country = Return2290_2018.CountryType.RW; break;
                        case "SA": foreignAdd_Filer.Country = Return2290_2018.CountryType.SA; break;
                        case "SB": foreignAdd_Filer.Country = Return2290_2018.CountryType.SB; break;
                        case "SC": foreignAdd_Filer.Country = Return2290_2018.CountryType.SC; break;
                        case "SE": foreignAdd_Filer.Country = Return2290_2018.CountryType.SE; break;
                        case "SF": foreignAdd_Filer.Country = Return2290_2018.CountryType.SF; break;
                        case "SG": foreignAdd_Filer.Country = Return2290_2018.CountryType.SG; break;
                        case "SH": foreignAdd_Filer.Country = Return2290_2018.CountryType.SH; break;
                        case "SI": foreignAdd_Filer.Country = Return2290_2018.CountryType.SI; break;
                        case "SL": foreignAdd_Filer.Country = Return2290_2018.CountryType.SL; break;
                        case "SM": foreignAdd_Filer.Country = Return2290_2018.CountryType.SM; break;
                        case "SN": foreignAdd_Filer.Country = Return2290_2018.CountryType.SN; break;
                        case "SO": foreignAdd_Filer.Country = Return2290_2018.CountryType.SO; break;
                        case "SP": foreignAdd_Filer.Country = Return2290_2018.CountryType.SP; break;
                        case "ST": foreignAdd_Filer.Country = Return2290_2018.CountryType.ST; break;
                        case "SU": foreignAdd_Filer.Country = Return2290_2018.CountryType.SU; break;
                        case "SV": foreignAdd_Filer.Country = Return2290_2018.CountryType.SV; break;
                        case "SW": foreignAdd_Filer.Country = Return2290_2018.CountryType.SW; break;
                        case "SX": foreignAdd_Filer.Country = Return2290_2018.CountryType.SX; break;
                        case "SY": foreignAdd_Filer.Country = Return2290_2018.CountryType.SY; break;
                        case "SZ": foreignAdd_Filer.Country = Return2290_2018.CountryType.SZ; break;
                        case "TB": foreignAdd_Filer.Country = Return2290_2018.CountryType.TB; break;
                        case "TD": foreignAdd_Filer.Country = Return2290_2018.CountryType.TD; break;
                        case "TH": foreignAdd_Filer.Country = Return2290_2018.CountryType.TH; break;
                        case "TI": foreignAdd_Filer.Country = Return2290_2018.CountryType.TI; break;
                        case "TK": foreignAdd_Filer.Country = Return2290_2018.CountryType.TK; break;
                        case "TL": foreignAdd_Filer.Country = Return2290_2018.CountryType.TL; break;
                        case "TN": foreignAdd_Filer.Country = Return2290_2018.CountryType.TN; break;
                        case "TO": foreignAdd_Filer.Country = Return2290_2018.CountryType.TO; break;
                        case "TP": foreignAdd_Filer.Country = Return2290_2018.CountryType.TP; break;
                        case "TS": foreignAdd_Filer.Country = Return2290_2018.CountryType.TS; break;
                        case "TT": foreignAdd_Filer.Country = Return2290_2018.CountryType.TT; break;
                        case "TU": foreignAdd_Filer.Country = Return2290_2018.CountryType.TU; break;
                        case "TV": foreignAdd_Filer.Country = Return2290_2018.CountryType.TV; break;
                        case "TW": foreignAdd_Filer.Country = Return2290_2018.CountryType.TW; break;
                        case "TX": foreignAdd_Filer.Country = Return2290_2018.CountryType.TX; break;
                        case "TZ": foreignAdd_Filer.Country = Return2290_2018.CountryType.TZ; break;
                        case "UC": foreignAdd_Filer.Country = Return2290_2018.CountryType.UC; break;
                        case "UG": foreignAdd_Filer.Country = Return2290_2018.CountryType.UG; break;
                        case "UK": foreignAdd_Filer.Country = Return2290_2018.CountryType.UK; break;
                        case "UP": foreignAdd_Filer.Country = Return2290_2018.CountryType.UP; break;
                        case "UV": foreignAdd_Filer.Country = Return2290_2018.CountryType.UV; break;
                        case "UY": foreignAdd_Filer.Country = Return2290_2018.CountryType.UY; break;
                        case "UZ": foreignAdd_Filer.Country = Return2290_2018.CountryType.UZ; break;
                        case "VC": foreignAdd_Filer.Country = Return2290_2018.CountryType.VC; break;
                        case "VE": foreignAdd_Filer.Country = Return2290_2018.CountryType.VE; break;
                        case "VI": foreignAdd_Filer.Country = Return2290_2018.CountryType.VI; break;
                        case "VM": foreignAdd_Filer.Country = Return2290_2018.CountryType.VM; break;
                        case "VT": foreignAdd_Filer.Country = Return2290_2018.CountryType.VT; break;
                        case "WA": foreignAdd_Filer.Country = Return2290_2018.CountryType.WA; break;
                        case "WF": foreignAdd_Filer.Country = Return2290_2018.CountryType.WF; break;
                        case "WI": foreignAdd_Filer.Country = Return2290_2018.CountryType.WI; break;
                        case "WQ": foreignAdd_Filer.Country = Return2290_2018.CountryType.WQ; break;
                        case "WS": foreignAdd_Filer.Country = Return2290_2018.CountryType.WS; break;
                        case "WZ": foreignAdd_Filer.Country = Return2290_2018.CountryType.WZ; break;
                        case "YM": foreignAdd_Filer.Country = Return2290_2018.CountryType.YM; break;
                        case "ZA": foreignAdd_Filer.Country = Return2290_2018.CountryType.ZA; break;
                        case "ZI": foreignAdd_Filer.Country = Return2290_2018.CountryType.ZI; break;
                        #endregion
                    }

                    if (Filer_ZIPCode.Length > 0)
                        foreignAdd_Filer.PostalCode = Filer_ZIPCode;

                    objFiler.Item = foreignAdd_Filer;
                    #endregion
                }

                returnHeaderType.Filer = objFiler;
                #endregion

                #region OFFICER
                if (Officer_Name != "")
                {
                    Return2290_2018.ReturnHeaderTypeBusinessOfficerGrp objOfficer = new Return2290_2018.ReturnHeaderTypeBusinessOfficerGrp();

                    objOfficer.PersonNm = Officer_Name;
                    objOfficer.PersonTitleTxt = Officer_Title;

                    if (Officer_PIN.Length > 0)
                        objOfficer.TaxpayerPIN = Officer_PIN;

                    if (Officer_Phone.Length > 0)
                    {
                        objOfficer.Item = Officer_Phone;
                        if (Filer_Foreign_Country == "US" || Filer_Foreign_Country == "")
                            objOfficer.ItemElementName = Return2290_2018.ItemChoiceType.PhoneNum;
                        else
                            objOfficer.ItemElementName = Return2290_2018.ItemChoiceType.ForeignPhoneNum;
                    }

                    if (Officer_email.Length > 0)
                        objOfficer.EmailAddressTxt = Officer_email;

                    objOfficer.SignatureDt = DateTime.Now.ToUniversalTime();

                    returnHeaderType.BusinessOfficerGrp = objOfficer;
                }
                #endregion

                #region THIRD PARTY DESIGNEE

                Return2290_2018.ReturnHeaderTypeThirdPartyDesignee objThirdPrtyDesignee = new Return2290_2018.ReturnHeaderTypeThirdPartyDesignee();

                object[] itemsField = new object[4];
                Return2290_2018.ItemsChoiceType[] itemsElementNameField = new Return2290_2018.ItemsChoiceType[4];

                itemsField[0] = Return2290_2018.CheckboxType.X;

                if (ThirdPartyDesigne_Name != "")
                {
                    itemsField[1] = ThirdPartyDesigne_Name;
                    itemsField[2] = ThirdPartyDesigne_Phone;
                    itemsField[3] = ThirdPartyDesigne_PIN;

                    itemsElementNameField[0] = Return2290_2018.ItemsChoiceType.DiscussWithThirdPartyYesInd;
                    itemsElementNameField[1] = Return2290_2018.ItemsChoiceType.ThirdPartyDesigneeNm;
                    itemsElementNameField[2] = Return2290_2018.ItemsChoiceType.ThirdPartyDesigneePhoneNum;
                    itemsElementNameField[3] = Return2290_2018.ItemsChoiceType.ThirdPartyDesigneePIN;
                }
                else
                {
                    itemsElementNameField[0] = Return2290_2018.ItemsChoiceType.DiscussWithThirdPartyNoInd;
                }
                objThirdPrtyDesignee.Items = itemsField;
                objThirdPrtyDesignee.ItemsElementName = itemsElementNameField;

                returnHeaderType.ThirdPartyDesignee = objThirdPrtyDesignee;

                #endregion

                #region PREPARER

                if (Preparer_Name != "")
                {
                    Return2290_2018.ReturnHeaderTypePreparerFirmGrp objPreparerFirm = new Return2290_2018.ReturnHeaderTypePreparerFirmGrp();
                    Return2290_2018.ReturnHeaderTypePreparerPersonGrp objPreparer = new Return2290_2018.ReturnHeaderTypePreparerPersonGrp();

                    objPreparer.PreparerPersonNm = Preparer_Name;

                    if (IsPreparerHasPTIN)
                        objPreparer.PTIN = Preparer_PTIN_SSN;

                    if (Preparer_Phone.Length > 0)
                    {
                        objPreparer.Item = Preparer_Phone;
                        objPreparer.ItemElementName = Return2290_2018.ItemChoiceType1.PhoneNum;
                    }
                    else if (Preparer_ForeignPhone.Length > 0)
                    {
                        objPreparer.Item = Preparer_Phone;
                        objPreparer.ItemElementName = Return2290_2018.ItemChoiceType1.ForeignPhoneNum;
                    }
                    objPreparer.EmailAddressTxt = Preparer_Email;
                    objPreparer.PreparationDt = DateTime.Now.ToUniversalTime();
                    objPreparer.PreparationDtSpecified = true;

                    if (IsPreparerSelfEmployed)
                    {
                        objPreparer.SelfEmployedInd = Return2290_2018.CheckboxType.X;
                        objPreparer.SelfEmployedIndSpecified = IsPreparerSelfEmployed;
                    }

                    returnHeaderType.PreparerPersonGrp = objPreparer;
                }

                if (!IsPreparerSelfEmployed && Preparer_Name != "")
                {
                    Return2290_2018.ReturnHeaderTypePreparerFirmGrp objPreparerFirm = new Return2290_2018.ReturnHeaderTypePreparerFirmGrp();
                    Return2290_2018.BusinessNameType PreparerBusinessName = new Return2290_2018.BusinessNameType();
                    PreparerBusinessName.BusinessNameLine1Txt = Preparer_Firm;

                    objPreparerFirm.PreparerFirmEIN = Preparer_EIN;
                    objPreparerFirm.PreparerFirmName = PreparerBusinessName;
                    if (Preparer_Country == "" || Preparer_Country == "US")
                    {
                        #region Preparer US ADDRESS

                        Return2290_2018.USAddressType usAdd_Preparer = new Return2290_2018.USAddressType();

                        if (Preparer_Address.Length > 35)
                        {
                            string Preparer_AddressLine2 = string.Empty;
                            usAdd_Preparer.AddressLine1 = Preparer_Address.Substring(0, 35).Trim();
                            Preparer_AddressLine2 = Preparer_Address.Substring(35);
                            usAdd_Preparer.AddressLine2 = Preparer_AddressLine2.Trim();
                        }
                        else
                        {
                            usAdd_Preparer.AddressLine1 = Preparer_Address.Trim();
                        }

                        usAdd_Preparer.City = Preparer_City.Trim();

                        switch (Preparer_State)
                        {
                            #region Filer_State
                            case "AA": usAdd_Preparer.State = Return2290_2018.StateType.AA; break;
                            case "AE": usAdd_Preparer.State = Return2290_2018.StateType.AE; break;
                            case "AK": usAdd_Preparer.State = Return2290_2018.StateType.AK; break;
                            case "AL": usAdd_Preparer.State = Return2290_2018.StateType.AL; break;
                            case "AP": usAdd_Preparer.State = Return2290_2018.StateType.AP; break;
                            case "AR": usAdd_Preparer.State = Return2290_2018.StateType.AR; break;
                            case "AS": usAdd_Preparer.State = Return2290_2018.StateType.AS; break;
                            case "AZ": usAdd_Preparer.State = Return2290_2018.StateType.AZ; break;
                            case "CA": usAdd_Preparer.State = Return2290_2018.StateType.CA; break;
                            case "CO": usAdd_Preparer.State = Return2290_2018.StateType.CO; break;
                            case "CT": usAdd_Preparer.State = Return2290_2018.StateType.CT; break;
                            case "DC": usAdd_Preparer.State = Return2290_2018.StateType.DC; break;
                            case "DE": usAdd_Preparer.State = Return2290_2018.StateType.DE; break;
                            case "FL": usAdd_Preparer.State = Return2290_2018.StateType.FL; break;
                            case "FM": usAdd_Preparer.State = Return2290_2018.StateType.FM; break;
                            case "GA": usAdd_Preparer.State = Return2290_2018.StateType.GA; break;
                            case "GU": usAdd_Preparer.State = Return2290_2018.StateType.GU; break;
                            case "HI": usAdd_Preparer.State = Return2290_2018.StateType.HI; break;
                            case "IA": usAdd_Preparer.State = Return2290_2018.StateType.IA; break;
                            case "ID": usAdd_Preparer.State = Return2290_2018.StateType.ID; break;
                            case "IL": usAdd_Preparer.State = Return2290_2018.StateType.IL; break;
                            case "IN": usAdd_Preparer.State = Return2290_2018.StateType.IN; break;
                            case "KS": usAdd_Preparer.State = Return2290_2018.StateType.KS; break;
                            case "KY": usAdd_Preparer.State = Return2290_2018.StateType.KY; break;
                            case "LA": usAdd_Preparer.State = Return2290_2018.StateType.LA; break;
                            case "MA": usAdd_Preparer.State = Return2290_2018.StateType.MA; break;
                            case "MD": usAdd_Preparer.State = Return2290_2018.StateType.MD; break;
                            case "ME": usAdd_Preparer.State = Return2290_2018.StateType.ME; break;
                            case "MH": usAdd_Preparer.State = Return2290_2018.StateType.MH; break;
                            case "MI": usAdd_Preparer.State = Return2290_2018.StateType.MI; break;
                            case "MN": usAdd_Preparer.State = Return2290_2018.StateType.MN; break;
                            case "MO": usAdd_Preparer.State = Return2290_2018.StateType.MO; break;
                            case "MP": usAdd_Preparer.State = Return2290_2018.StateType.MP; break;
                            case "MS": usAdd_Preparer.State = Return2290_2018.StateType.MS; break;
                            case "MT": usAdd_Preparer.State = Return2290_2018.StateType.MT; break;
                            case "NC": usAdd_Preparer.State = Return2290_2018.StateType.NC; break;
                            case "ND": usAdd_Preparer.State = Return2290_2018.StateType.ND; break;
                            case "NE": usAdd_Preparer.State = Return2290_2018.StateType.NE; break;
                            case "NH": usAdd_Preparer.State = Return2290_2018.StateType.NH; break;
                            case "NJ": usAdd_Preparer.State = Return2290_2018.StateType.NJ; break;
                            case "NM": usAdd_Preparer.State = Return2290_2018.StateType.NM; break;
                            case "NV": usAdd_Preparer.State = Return2290_2018.StateType.NV; break;
                            case "NY": usAdd_Preparer.State = Return2290_2018.StateType.NY; break;
                            case "OH": usAdd_Preparer.State = Return2290_2018.StateType.OH; break;
                            case "OK": usAdd_Preparer.State = Return2290_2018.StateType.OK; break;
                            case "OR": usAdd_Preparer.State = Return2290_2018.StateType.OR; break;
                            case "PA": usAdd_Preparer.State = Return2290_2018.StateType.PA; break;
                            case "PR": usAdd_Preparer.State = Return2290_2018.StateType.PR; break;
                            case "PW": usAdd_Preparer.State = Return2290_2018.StateType.PW; break;
                            case "RI": usAdd_Preparer.State = Return2290_2018.StateType.RI; break;
                            case "SC": usAdd_Preparer.State = Return2290_2018.StateType.SC; break;
                            case "SD": usAdd_Preparer.State = Return2290_2018.StateType.SD; break;
                            case "TN": usAdd_Preparer.State = Return2290_2018.StateType.TN; break;
                            case "TX": usAdd_Preparer.State = Return2290_2018.StateType.TX; break;
                            case "UT": usAdd_Preparer.State = Return2290_2018.StateType.UT; break;
                            case "VA": usAdd_Preparer.State = Return2290_2018.StateType.VA; break;
                            case "VI": usAdd_Preparer.State = Return2290_2018.StateType.VI; break;
                            case "VT": usAdd_Preparer.State = Return2290_2018.StateType.VT; break;
                            case "WA": usAdd_Preparer.State = Return2290_2018.StateType.WA; break;
                            case "WI": usAdd_Preparer.State = Return2290_2018.StateType.WI; break;
                            case "WV": usAdd_Preparer.State = Return2290_2018.StateType.WV; break;
                            case "WY": usAdd_Preparer.State = Return2290_2018.StateType.WY; break;
                            #endregion
                        }

                        usAdd_Preparer.ZIPCode = Preparer_Zip;

                        objPreparerFirm.Item = usAdd_Preparer;

                        #endregion
                    }
                    else
                    {
                        #region Preparer FOREIGN ADDRESS
                        Return2290_2018.ForeignAddressType foreignAdd_Preparer = new Return2290_2018.ForeignAddressType();

                        if (Preparer_Address.Length > 35)
                        {
                            string Preparer_AddressLine2 = string.Empty;
                            foreignAdd_Preparer.AddressLine1 = Preparer_Address.Substring(0, 35).Trim();
                            Preparer_AddressLine2 = Preparer_Address.Substring(35);
                            foreignAdd_Preparer.AddressLine2 = Preparer_AddressLine2.Trim();

                        }
                        else
                        {
                            foreignAdd_Preparer.AddressLine1 = Preparer_Address.Trim();
                        }
                        foreignAdd_Preparer.City = Preparer_City.Trim();

                        if (Preparer_State.Length > 0)
                            foreignAdd_Preparer.ProvinceOrState = Preparer_State;

                        switch (Preparer_Country)
                        {
                            #region Preparer_Foreign_Country
                            case "AA": foreignAdd_Preparer.Country = Return2290_2018.CountryType.AA; break;
                            case "AC": foreignAdd_Preparer.Country = Return2290_2018.CountryType.AC; break;
                            case "AE": foreignAdd_Preparer.Country = Return2290_2018.CountryType.AE; break;
                            case "AF": foreignAdd_Preparer.Country = Return2290_2018.CountryType.AF; break;
                            case "AG": foreignAdd_Preparer.Country = Return2290_2018.CountryType.AG; break;
                            case "AJ": foreignAdd_Preparer.Country = Return2290_2018.CountryType.AJ; break;
                            case "AL": foreignAdd_Preparer.Country = Return2290_2018.CountryType.AL; break;
                            case "AM": foreignAdd_Preparer.Country = Return2290_2018.CountryType.AM; break;
                            case "AN": foreignAdd_Preparer.Country = Return2290_2018.CountryType.AN; break;
                            case "AO": foreignAdd_Preparer.Country = Return2290_2018.CountryType.AO; break;
                            case "AR": foreignAdd_Preparer.Country = Return2290_2018.CountryType.AR; break;
                            case "AS": foreignAdd_Preparer.Country = Return2290_2018.CountryType.AS; break;
                            case "AT": foreignAdd_Preparer.Country = Return2290_2018.CountryType.AT; break;
                            case "AU": foreignAdd_Preparer.Country = Return2290_2018.CountryType.AU; break;
                            case "AV": foreignAdd_Preparer.Country = Return2290_2018.CountryType.AV; break;
                            case "AX": foreignAdd_Preparer.Country = Return2290_2018.CountryType.AX; break;
                            case "AY": foreignAdd_Preparer.Country = Return2290_2018.CountryType.AY; break;
                            case "BA": foreignAdd_Preparer.Country = Return2290_2018.CountryType.BA; break;
                            case "BB": foreignAdd_Preparer.Country = Return2290_2018.CountryType.BB; break;
                            case "BC": foreignAdd_Preparer.Country = Return2290_2018.CountryType.BC; break;
                            case "BD": foreignAdd_Preparer.Country = Return2290_2018.CountryType.BD; break;
                            case "BE": foreignAdd_Preparer.Country = Return2290_2018.CountryType.BE; break;
                            case "BF": foreignAdd_Preparer.Country = Return2290_2018.CountryType.BF; break;
                            case "BG": foreignAdd_Preparer.Country = Return2290_2018.CountryType.BG; break;
                            case "BH": foreignAdd_Preparer.Country = Return2290_2018.CountryType.BH; break;
                            case "BK": foreignAdd_Preparer.Country = Return2290_2018.CountryType.BK; break;
                            case "BL": foreignAdd_Preparer.Country = Return2290_2018.CountryType.BL; break;
                            case "BM": foreignAdd_Preparer.Country = Return2290_2018.CountryType.BM; break;
                            case "BN": foreignAdd_Preparer.Country = Return2290_2018.CountryType.BN; break;
                            case "BO": foreignAdd_Preparer.Country = Return2290_2018.CountryType.BO; break;
                            case "BP": foreignAdd_Preparer.Country = Return2290_2018.CountryType.BP; break;
                            case "BQ": foreignAdd_Preparer.Country = Return2290_2018.CountryType.BQ; break;
                            case "BR": foreignAdd_Preparer.Country = Return2290_2018.CountryType.BR; break;
                            case "BT": foreignAdd_Preparer.Country = Return2290_2018.CountryType.BT; break;
                            case "BU": foreignAdd_Preparer.Country = Return2290_2018.CountryType.BU; break;
                            case "BV": foreignAdd_Preparer.Country = Return2290_2018.CountryType.BV; break;
                            case "BX": foreignAdd_Preparer.Country = Return2290_2018.CountryType.BX; break;
                            case "BY": foreignAdd_Preparer.Country = Return2290_2018.CountryType.BY; break;
                            case "CA": foreignAdd_Preparer.Country = Return2290_2018.CountryType.CA; break;
                            case "CB": foreignAdd_Preparer.Country = Return2290_2018.CountryType.CB; break;
                            case "CD": foreignAdd_Preparer.Country = Return2290_2018.CountryType.CD; break;
                            case "CE": foreignAdd_Preparer.Country = Return2290_2018.CountryType.CE; break;
                            case "CF": foreignAdd_Preparer.Country = Return2290_2018.CountryType.CF; break;
                            case "CG": foreignAdd_Preparer.Country = Return2290_2018.CountryType.CG; break;
                            case "CH": foreignAdd_Preparer.Country = Return2290_2018.CountryType.CH; break;
                            case "CI": foreignAdd_Preparer.Country = Return2290_2018.CountryType.CI; break;
                            case "CJ": foreignAdd_Preparer.Country = Return2290_2018.CountryType.CJ; break;
                            case "CK": foreignAdd_Preparer.Country = Return2290_2018.CountryType.CK; break;
                            case "CM": foreignAdd_Preparer.Country = Return2290_2018.CountryType.CM; break;
                            case "CN": foreignAdd_Preparer.Country = Return2290_2018.CountryType.CN; break;
                            case "CO": foreignAdd_Preparer.Country = Return2290_2018.CountryType.CO; break;
                            case "CR": foreignAdd_Preparer.Country = Return2290_2018.CountryType.CR; break;
                            case "CS": foreignAdd_Preparer.Country = Return2290_2018.CountryType.CS; break;
                            case "CT": foreignAdd_Preparer.Country = Return2290_2018.CountryType.CT; break;
                            case "CU": foreignAdd_Preparer.Country = Return2290_2018.CountryType.CU; break;
                            case "CV": foreignAdd_Preparer.Country = Return2290_2018.CountryType.CV; break;
                            case "CW": foreignAdd_Preparer.Country = Return2290_2018.CountryType.CW; break;
                            case "CY": foreignAdd_Preparer.Country = Return2290_2018.CountryType.CY; break;
                            case "DA": foreignAdd_Preparer.Country = Return2290_2018.CountryType.DA; break;
                            case "DJ": foreignAdd_Preparer.Country = Return2290_2018.CountryType.DJ; break;
                            case "DO": foreignAdd_Preparer.Country = Return2290_2018.CountryType.DO; break;
                            case "DR": foreignAdd_Preparer.Country = Return2290_2018.CountryType.DR; break;
                            case "DX": foreignAdd_Preparer.Country = Return2290_2018.CountryType.DX; break;
                            case "EC": foreignAdd_Preparer.Country = Return2290_2018.CountryType.EC; break;
                            case "EG": foreignAdd_Preparer.Country = Return2290_2018.CountryType.EG; break;
                            case "EI": foreignAdd_Preparer.Country = Return2290_2018.CountryType.EI; break;
                            case "EK": foreignAdd_Preparer.Country = Return2290_2018.CountryType.EK; break;
                            case "EN": foreignAdd_Preparer.Country = Return2290_2018.CountryType.EN; break;
                            case "ER": foreignAdd_Preparer.Country = Return2290_2018.CountryType.ER; break;
                            case "ES": foreignAdd_Preparer.Country = Return2290_2018.CountryType.ES; break;
                            case "ET": foreignAdd_Preparer.Country = Return2290_2018.CountryType.ET; break;
                            case "EZ": foreignAdd_Preparer.Country = Return2290_2018.CountryType.EZ; break;
                            case "FI": foreignAdd_Preparer.Country = Return2290_2018.CountryType.FI; break;
                            case "FJ": foreignAdd_Preparer.Country = Return2290_2018.CountryType.FJ; break;
                            case "FK": foreignAdd_Preparer.Country = Return2290_2018.CountryType.FK; break;
                            case "FO": foreignAdd_Preparer.Country = Return2290_2018.CountryType.FO; break;
                            case "FP": foreignAdd_Preparer.Country = Return2290_2018.CountryType.FP; break;
                            case "FQ": foreignAdd_Preparer.Country = Return2290_2018.CountryType.FQ; break;
                            case "FR": foreignAdd_Preparer.Country = Return2290_2018.CountryType.FR; break;
                            case "FS": foreignAdd_Preparer.Country = Return2290_2018.CountryType.FS; break;
                            case "GA": foreignAdd_Preparer.Country = Return2290_2018.CountryType.GA; break;
                            case "GB": foreignAdd_Preparer.Country = Return2290_2018.CountryType.GB; break;
                            case "GG": foreignAdd_Preparer.Country = Return2290_2018.CountryType.GG; break;
                            case "GH": foreignAdd_Preparer.Country = Return2290_2018.CountryType.GH; break;
                            case "GI": foreignAdd_Preparer.Country = Return2290_2018.CountryType.GI; break;
                            case "GJ": foreignAdd_Preparer.Country = Return2290_2018.CountryType.GJ; break;
                            case "GK": foreignAdd_Preparer.Country = Return2290_2018.CountryType.GK; break;
                            case "GL": foreignAdd_Preparer.Country = Return2290_2018.CountryType.GL; break;
                            case "GM": foreignAdd_Preparer.Country = Return2290_2018.CountryType.GM; break;
                            case "GR": foreignAdd_Preparer.Country = Return2290_2018.CountryType.GR; break;
                            case "GT": foreignAdd_Preparer.Country = Return2290_2018.CountryType.GT; break;
                            case "GV": foreignAdd_Preparer.Country = Return2290_2018.CountryType.GV; break;
                            case "GY": foreignAdd_Preparer.Country = Return2290_2018.CountryType.GY; break;
                            case "HA": foreignAdd_Preparer.Country = Return2290_2018.CountryType.HA; break;
                            case "HK": foreignAdd_Preparer.Country = Return2290_2018.CountryType.HK; break;
                            case "HM": foreignAdd_Preparer.Country = Return2290_2018.CountryType.HM; break;
                            case "HO": foreignAdd_Preparer.Country = Return2290_2018.CountryType.HO; break;
                            case "HQ": foreignAdd_Preparer.Country = Return2290_2018.CountryType.HQ; break;
                            case "HR": foreignAdd_Preparer.Country = Return2290_2018.CountryType.HR; break;
                            case "HU": foreignAdd_Preparer.Country = Return2290_2018.CountryType.HU; break;
                            case "IC": foreignAdd_Preparer.Country = Return2290_2018.CountryType.IC; break;
                            case "ID": foreignAdd_Preparer.Country = Return2290_2018.CountryType.ID; break;
                            case "IM": foreignAdd_Preparer.Country = Return2290_2018.CountryType.IM; break;
                            case "IO": foreignAdd_Preparer.Country = Return2290_2018.CountryType.IO; break;
                            case "IP": foreignAdd_Preparer.Country = Return2290_2018.CountryType.IP; break;
                            case "IR": foreignAdd_Preparer.Country = Return2290_2018.CountryType.IR; break;
                            case "IS": foreignAdd_Preparer.Country = Return2290_2018.CountryType.IS; break;
                            case "IT": foreignAdd_Preparer.Country = Return2290_2018.CountryType.IT; break;
                            case "IV": foreignAdd_Preparer.Country = Return2290_2018.CountryType.IV; break;
                            case "IZ": foreignAdd_Preparer.Country = Return2290_2018.CountryType.IZ; break;
                            case "JA": foreignAdd_Preparer.Country = Return2290_2018.CountryType.JA; break;
                            case "JE": foreignAdd_Preparer.Country = Return2290_2018.CountryType.JE; break;
                            case "JM": foreignAdd_Preparer.Country = Return2290_2018.CountryType.JM; break;
                            case "JN": foreignAdd_Preparer.Country = Return2290_2018.CountryType.JN; break;
                            case "JO": foreignAdd_Preparer.Country = Return2290_2018.CountryType.JO; break;
                            case "JQ": foreignAdd_Preparer.Country = Return2290_2018.CountryType.JQ; break;
                            case "KE": foreignAdd_Preparer.Country = Return2290_2018.CountryType.KE; break;
                            case "KG": foreignAdd_Preparer.Country = Return2290_2018.CountryType.KG; break;
                            case "KN": foreignAdd_Preparer.Country = Return2290_2018.CountryType.KN; break;
                            case "KQ": foreignAdd_Preparer.Country = Return2290_2018.CountryType.KQ; break;
                            case "KR": foreignAdd_Preparer.Country = Return2290_2018.CountryType.KR; break;
                            case "KS": foreignAdd_Preparer.Country = Return2290_2018.CountryType.KS; break;
                            case "KT": foreignAdd_Preparer.Country = Return2290_2018.CountryType.KT; break;
                            case "KU": foreignAdd_Preparer.Country = Return2290_2018.CountryType.KU; break;
                            case "KZ": foreignAdd_Preparer.Country = Return2290_2018.CountryType.KZ; break;
                            case "LA": foreignAdd_Preparer.Country = Return2290_2018.CountryType.LA; break;
                            case "LE": foreignAdd_Preparer.Country = Return2290_2018.CountryType.LE; break;
                            case "LG": foreignAdd_Preparer.Country = Return2290_2018.CountryType.LG; break;
                            case "LH": foreignAdd_Preparer.Country = Return2290_2018.CountryType.LH; break;
                            case "LI": foreignAdd_Preparer.Country = Return2290_2018.CountryType.LI; break;
                            case "LO": foreignAdd_Preparer.Country = Return2290_2018.CountryType.LO; break;
                            case "LQ": foreignAdd_Preparer.Country = Return2290_2018.CountryType.LQ; break;
                            case "LS": foreignAdd_Preparer.Country = Return2290_2018.CountryType.LS; break;
                            case "LT": foreignAdd_Preparer.Country = Return2290_2018.CountryType.LT; break;
                            case "LU": foreignAdd_Preparer.Country = Return2290_2018.CountryType.LU; break;
                            case "LY": foreignAdd_Preparer.Country = Return2290_2018.CountryType.LY; break;
                            case "MA": foreignAdd_Preparer.Country = Return2290_2018.CountryType.MA; break;
                            case "MC": foreignAdd_Preparer.Country = Return2290_2018.CountryType.MC; break;
                            case "MD": foreignAdd_Preparer.Country = Return2290_2018.CountryType.MD; break;
                            case "MG": foreignAdd_Preparer.Country = Return2290_2018.CountryType.MG; break;
                            case "MH": foreignAdd_Preparer.Country = Return2290_2018.CountryType.MH; break;
                            case "MI": foreignAdd_Preparer.Country = Return2290_2018.CountryType.MI; break;
                            case "MJ": foreignAdd_Preparer.Country = Return2290_2018.CountryType.MJ; break;
                            case "MK": foreignAdd_Preparer.Country = Return2290_2018.CountryType.MK; break;
                            case "ML": foreignAdd_Preparer.Country = Return2290_2018.CountryType.ML; break;
                            case "MN": foreignAdd_Preparer.Country = Return2290_2018.CountryType.MN; break;
                            case "MO": foreignAdd_Preparer.Country = Return2290_2018.CountryType.MO; break;
                            case "MP": foreignAdd_Preparer.Country = Return2290_2018.CountryType.MP; break;
                            case "MQ": foreignAdd_Preparer.Country = Return2290_2018.CountryType.MQ; break;
                            case "MR": foreignAdd_Preparer.Country = Return2290_2018.CountryType.MR; break;
                            case "MT": foreignAdd_Preparer.Country = Return2290_2018.CountryType.MT; break;
                            case "MU": foreignAdd_Preparer.Country = Return2290_2018.CountryType.MU; break;
                            case "MV": foreignAdd_Preparer.Country = Return2290_2018.CountryType.MV; break;
                            case "MX": foreignAdd_Preparer.Country = Return2290_2018.CountryType.MX; break;
                            case "MY": foreignAdd_Preparer.Country = Return2290_2018.CountryType.MY; break;
                            case "MZ": foreignAdd_Preparer.Country = Return2290_2018.CountryType.MZ; break;
                            case "NC": foreignAdd_Preparer.Country = Return2290_2018.CountryType.NC; break;
                            case "NE": foreignAdd_Preparer.Country = Return2290_2018.CountryType.NE; break;
                            case "NF": foreignAdd_Preparer.Country = Return2290_2018.CountryType.NF; break;
                            case "NG": foreignAdd_Preparer.Country = Return2290_2018.CountryType.NG; break;
                            case "NH": foreignAdd_Preparer.Country = Return2290_2018.CountryType.NH; break;
                            case "NI": foreignAdd_Preparer.Country = Return2290_2018.CountryType.NI; break;
                            case "NL": foreignAdd_Preparer.Country = Return2290_2018.CountryType.NL; break;
                            case "NO": foreignAdd_Preparer.Country = Return2290_2018.CountryType.NO; break;
                            case "NP": foreignAdd_Preparer.Country = Return2290_2018.CountryType.NP; break;
                            case "NR": foreignAdd_Preparer.Country = Return2290_2018.CountryType.NR; break;
                            case "NS": foreignAdd_Preparer.Country = Return2290_2018.CountryType.NS; break;
                            case "NU": foreignAdd_Preparer.Country = Return2290_2018.CountryType.NU; break;
                            case "NZ": foreignAdd_Preparer.Country = Return2290_2018.CountryType.NZ; break;
                            case "PA": foreignAdd_Preparer.Country = Return2290_2018.CountryType.PA; break;
                            case "PC": foreignAdd_Preparer.Country = Return2290_2018.CountryType.PC; break;
                            case "PE": foreignAdd_Preparer.Country = Return2290_2018.CountryType.PE; break;
                            case "PF": foreignAdd_Preparer.Country = Return2290_2018.CountryType.PF; break;
                            case "PG": foreignAdd_Preparer.Country = Return2290_2018.CountryType.PG; break;
                            case "PK": foreignAdd_Preparer.Country = Return2290_2018.CountryType.PK; break;
                            case "PL": foreignAdd_Preparer.Country = Return2290_2018.CountryType.PL; break;
                            case "PM": foreignAdd_Preparer.Country = Return2290_2018.CountryType.PM; break;
                            case "PO": foreignAdd_Preparer.Country = Return2290_2018.CountryType.PO; break;
                            case "PP": foreignAdd_Preparer.Country = Return2290_2018.CountryType.PP; break;
                            case "PU": foreignAdd_Preparer.Country = Return2290_2018.CountryType.PU; break;
                            case "QA": foreignAdd_Preparer.Country = Return2290_2018.CountryType.QA; break;
                            case "RO": foreignAdd_Preparer.Country = Return2290_2018.CountryType.RO; break;
                            case "RP": foreignAdd_Preparer.Country = Return2290_2018.CountryType.RP; break;
                            case "RS": foreignAdd_Preparer.Country = Return2290_2018.CountryType.RS; break;
                            case "RW": foreignAdd_Preparer.Country = Return2290_2018.CountryType.RW; break;
                            case "SA": foreignAdd_Preparer.Country = Return2290_2018.CountryType.SA; break;
                            case "SB": foreignAdd_Preparer.Country = Return2290_2018.CountryType.SB; break;
                            case "SC": foreignAdd_Preparer.Country = Return2290_2018.CountryType.SC; break;
                            case "SE": foreignAdd_Preparer.Country = Return2290_2018.CountryType.SE; break;
                            case "SF": foreignAdd_Preparer.Country = Return2290_2018.CountryType.SF; break;
                            case "SG": foreignAdd_Preparer.Country = Return2290_2018.CountryType.SG; break;
                            case "SH": foreignAdd_Preparer.Country = Return2290_2018.CountryType.SH; break;
                            case "SI": foreignAdd_Preparer.Country = Return2290_2018.CountryType.SI; break;
                            case "SL": foreignAdd_Preparer.Country = Return2290_2018.CountryType.SL; break;
                            case "SM": foreignAdd_Preparer.Country = Return2290_2018.CountryType.SM; break;
                            case "SN": foreignAdd_Preparer.Country = Return2290_2018.CountryType.SN; break;
                            case "SO": foreignAdd_Preparer.Country = Return2290_2018.CountryType.SO; break;
                            case "SP": foreignAdd_Preparer.Country = Return2290_2018.CountryType.SP; break;
                            case "ST": foreignAdd_Preparer.Country = Return2290_2018.CountryType.ST; break;
                            case "SU": foreignAdd_Preparer.Country = Return2290_2018.CountryType.SU; break;
                            case "SV": foreignAdd_Preparer.Country = Return2290_2018.CountryType.SV; break;
                            case "SW": foreignAdd_Preparer.Country = Return2290_2018.CountryType.SW; break;
                            case "SX": foreignAdd_Preparer.Country = Return2290_2018.CountryType.SX; break;
                            case "SY": foreignAdd_Preparer.Country = Return2290_2018.CountryType.SY; break;
                            case "SZ": foreignAdd_Preparer.Country = Return2290_2018.CountryType.SZ; break;
                            case "TD": foreignAdd_Preparer.Country = Return2290_2018.CountryType.TD; break;
                            case "TH": foreignAdd_Preparer.Country = Return2290_2018.CountryType.TH; break;
                            case "TI": foreignAdd_Preparer.Country = Return2290_2018.CountryType.TI; break;
                            case "TK": foreignAdd_Preparer.Country = Return2290_2018.CountryType.TK; break;
                            case "TL": foreignAdd_Preparer.Country = Return2290_2018.CountryType.TL; break;
                            case "TN": foreignAdd_Preparer.Country = Return2290_2018.CountryType.TN; break;
                            case "TO": foreignAdd_Preparer.Country = Return2290_2018.CountryType.TO; break;
                            case "TP": foreignAdd_Preparer.Country = Return2290_2018.CountryType.TP; break;
                            case "TS": foreignAdd_Preparer.Country = Return2290_2018.CountryType.TS; break;
                            case "TT": foreignAdd_Preparer.Country = Return2290_2018.CountryType.TT; break;
                            case "TU": foreignAdd_Preparer.Country = Return2290_2018.CountryType.TU; break;
                            case "TV": foreignAdd_Preparer.Country = Return2290_2018.CountryType.TV; break;
                            case "TW": foreignAdd_Preparer.Country = Return2290_2018.CountryType.TW; break;
                            case "TX": foreignAdd_Preparer.Country = Return2290_2018.CountryType.TX; break;
                            case "TZ": foreignAdd_Preparer.Country = Return2290_2018.CountryType.TZ; break;
                            case "UG": foreignAdd_Preparer.Country = Return2290_2018.CountryType.UG; break;
                            case "UK": foreignAdd_Preparer.Country = Return2290_2018.CountryType.UK; break;
                            case "UP": foreignAdd_Preparer.Country = Return2290_2018.CountryType.UP; break;
                            case "UV": foreignAdd_Preparer.Country = Return2290_2018.CountryType.UV; break;
                            case "UY": foreignAdd_Preparer.Country = Return2290_2018.CountryType.UY; break;
                            case "UZ": foreignAdd_Preparer.Country = Return2290_2018.CountryType.UZ; break;
                            case "VC": foreignAdd_Preparer.Country = Return2290_2018.CountryType.VC; break;
                            case "VE": foreignAdd_Preparer.Country = Return2290_2018.CountryType.VE; break;
                            case "VI": foreignAdd_Preparer.Country = Return2290_2018.CountryType.VI; break;
                            case "VM": foreignAdd_Preparer.Country = Return2290_2018.CountryType.VM; break;
                            case "VT": foreignAdd_Preparer.Country = Return2290_2018.CountryType.VT; break;
                            case "WA": foreignAdd_Preparer.Country = Return2290_2018.CountryType.WA; break;
                            case "WF": foreignAdd_Preparer.Country = Return2290_2018.CountryType.WF; break;
                            case "WI": foreignAdd_Preparer.Country = Return2290_2018.CountryType.WI; break;
                            case "WQ": foreignAdd_Preparer.Country = Return2290_2018.CountryType.WQ; break;
                            case "WS": foreignAdd_Preparer.Country = Return2290_2018.CountryType.WS; break;
                            case "WZ": foreignAdd_Preparer.Country = Return2290_2018.CountryType.WZ; break;
                            case "YM": foreignAdd_Preparer.Country = Return2290_2018.CountryType.YM; break;
                            case "ZA": foreignAdd_Preparer.Country = Return2290_2018.CountryType.ZA; break;
                            case "ZI": foreignAdd_Preparer.Country = Return2290_2018.CountryType.ZI; break;
                            #endregion
                        }

                        if (Filer_ZIPCode.Length > 0)
                            foreignAdd_Preparer.PostalCode = Filer_ZIPCode;

                        objFiler.Item = foreignAdd_Preparer;
                        #endregion
                    }

                    returnHeaderType.PreparerFirmGrp = objPreparerFirm;
                }
                #endregion

                #region CONSENT_TO_VIN_DATA_DISCLOSURE

                object[] itemConsent = new object[4];
                Return2290_2018.ItemsChoiceType1[] itemElements = new Return2290_2018.ItemsChoiceType1[4];

                itemConsent[0] = Return2290_2018.CheckboxType.X;

                if (IsConsentToDiscloseYes)
                {
                    itemElements[0] = Return2290_2018.ItemsChoiceType1.ConsentToDiscloseYesInd;

                    Return2290_2018.ReturnHeaderTypeConsentToVINDataDisclosureGrpDisclosureFormSignatureInfo objDisclosureFormSignInfo =
                        new Return2290_2018.ReturnHeaderTypeConsentToVINDataDisclosureGrpDisclosureFormSignatureInfo();
                    objDisclosureFormSignInfo.EIN = Filer_EIN;
                    Return2290_2018.BusinessNameType disclosureFormSignInfo_business = new Return2290_2018.BusinessNameType();
                    disclosureFormSignInfo_business.BusinessNameLine1Txt = Filer_Name;
                    objDisclosureFormSignInfo.BusinessName = disclosureFormSignInfo_business;
                    objDisclosureFormSignInfo.SignatureDt = DateTime.Now.ToUniversalTime();

                    itemConsent[1] = (objDisclosureFormSignInfo);
                    itemElements[1] = (Return2290_2018.ItemsChoiceType1.DisclosureFormSignatureInfo);

                    itemConsent[2] = (Return2290_2018.ReturnHeaderTypeConsentToVINDataDisclosureGrpSignatureOptionCd.PINNumber);
                    itemElements[2] = (Return2290_2018.ItemsChoiceType1.SignatureOptionCd);

                    itemConsent[3] = (Officer_PIN);
                    itemElements[3] = (Return2290_2018.ItemsChoiceType1.PIN);
                }
                else
                {
                    itemElements[0] = (Return2290_2018.ItemsChoiceType1.ConsentToDiscloseNoInd);
                }

                Return2290_2018.ReturnHeaderTypeConsentToVINDataDisclosureGrp objConsent2DataDisclosure =
                    new Return2290_2018.ReturnHeaderTypeConsentToVINDataDisclosureGrp();
                objConsent2DataDisclosure.Items = itemConsent;
                objConsent2DataDisclosure.ItemsElementName = itemElements;

                returnHeaderType.ConsentToVINDataDisclosureGrp = objConsent2DataDisclosure;

                #endregion

                returnHeaderType.TaxYr = TaxYear;

                returnHeaderType.binaryAttachmentCnt = "0";

                returnHeaderType.SoftwareId = Variables.SoftwareID;
                returnHeaderType.SoftwareVersionNum = Variables.SoftwareVersion;
                #endregion

                #region RETURN DATA

                Return2290_2018.ReturnData returnData = new Return2290_2018.ReturnData();

                #region IRS2290

                Return2290_2018.IRS2290 objIRS2290 = new Return2290_2018.IRS2290();

                #region TAX COMPUTATION
                // NEW Changes in 2018 were proposed here
                if (dsTaxComputation.Tables.Count > 0)
                {
                    if (dsTaxComputation.Tables[0].Rows.Count > 0)
                    {
                        List<Return2290_2018.IRS2290TypeHighwayMtrVehTxComputationGrp> objTaxComputationList = new
                            List<Return2290_2018.IRS2290TypeHighwayMtrVehTxComputationGrp>();

                        foreach (DataRow ldr in dsTaxComputation.Tables[0].Rows)
                        {
                            Return2290_2018.IRS2290TypeHighwayMtrVehTxComputationGrp objTaxComputation = new
                                Return2290_2018.IRS2290TypeHighwayMtrVehTxComputationGrp();
                            objTaxComputation.VehicleCategoryCd = Convert.ToString(ldr["Category"]);

                            Return2290_2018.HighwayMtrVehTxCmptColumnsGrpType objVeh = new Return2290_2018.HighwayMtrVehTxCmptColumnsGrpType();
                            
                            objVeh.TaxAmt = Convert.ToDecimal(ldr["Tax_Amount"]);
                            objVeh.TaxAmtSpecified = true;

                           
                            objVeh.LoggingVehPartialTaxAmtSpecified = false;
                            objVeh.NonLoggingVehPartialTaxAmtSpecified = false;

                            if (Convert.ToString(ldr["Logging_Vehicle_Count"]) != "0")
                            {
                                if (Convert.ToString(ldr["Logging_Vehicle_Count"]) != "")
                                {
                                    numLoggingVehicleCnt = numLoggingVehicleCnt + Convert.ToInt16(ldr["Logging_Vehicle_Count"]);
                                    objVeh.LoggingVehicleCnt = Convert.ToString(ldr["Logging_Vehicle_Count"]);
                                }
                                if (Convert.ToString(ldr["Logging_Vehicle_Count"]) != "")
                                    numTotalTaxableVeh += Convert.ToInt16(ldr["Logging_Vehicle_Count"]);
                            }
                            if (Convert.ToString(ldr["Non_Logging_Vehicle_Count"]) != "0")
                            {
                                if (Convert.ToString(ldr["Non_Logging_Vehicle_Count"]) != "")
                                {
                                    numNonLoggingVehicleCnt = numNonLoggingVehicleCnt + Convert.ToInt16(ldr["Non_Logging_Vehicle_Count"]);
                                    objVeh.NonLoggingVehicleCnt = Convert.ToString(ldr["Non_Logging_Vehicle_Count"]);
                                }
                                if (Convert.ToString(ldr["Non_Logging_Vehicle_Count"]) != "")
                                    numTotalTaxableVeh += Convert.ToInt16(ldr["Non_Logging_Vehicle_Count"]);
                            }
                            objTaxComputation.HighwayMtrVehTxCmptColumnsGrp = objVeh;
                            objTaxComputationList.Add(objTaxComputation);
                        }
                        objIRS2290.HighwayMtrVehTxComputationGrp = objTaxComputationList.ToArray();

                        objIRS2290.TotalTaxAmt = Convert.ToDecimal(TaxFromTaxComputation.ToString("N2"));
                        objIRS2290.TotalTaxAmtSpecified = true;

                        objIRS2290.TotalVehicleCnt = numTotalTaxableVeh.ToString();

                    }
                }

                #endregion

                #region IS APPLICATION FOR....

                if (IsAddressChange)
                    objIRS2290.AddressChangeInd = Return2290_2018.CheckboxType.X;

                if (IsAmendedReturn)
                {
                    objIRS2290.AmendedReturnInd = Return2290_2018.CheckboxType.X;
                    objIRS2290.AmendedReturnIndSpecified = true;
                    objIRS2290.AmendedMonthNum = AmendedMonth; //format is MM
                }

                if (IsVINCorrection)
                {
                    // 2018 VINCorrection VIN Correction
                    // Vishwa Add the new TAG About the VINCorrectionExplanationStmt -> ExplanationTtx

                    //objIRS2290.VINCorrectionInd = Return2290_2018.CheckboxType.X;
                    //objIRS2290.VINCorrectionIndSpecified = true;


                    Return2290_2018.IRS2290TypeVINCorrInd objVINCor = new Return2290_2018.IRS2290TypeVINCorrInd();
                    objVINCor.Value = "X";
                    List<string> docId_VINCorrDetails = new List<string>();
                    docId_VINCorrDetails.Add("VINCorrectionExplan");
                    objVINCor.referenceDocumentId = docId_VINCorrDetails.ToArray();
                    objVINCor.referenceDocumentName = "VINCorrectionExplanationStatement";

                    objIRS2290.VINCorrectionInd = objVINCor;


                    //Return2290_2018.VINCorrectionExplanationStmtType VinCorExp = new Return2290_2018.VINCorrectionExplanationStmtType();

                    //VinCorExp.ExplanationTxt = "VIN Correction update request";
                    //objIRS2290.VINCorrectionExplanationStmt = VinCorExp;

                }
                //2018 VIN Correction
                if (IsFinalReturn && IsVINCorrection == false)
                {
                    objIRS2290.FinalReturnInd = Return2290_2018.CheckboxType.X;
                    objIRS2290.FinalReturnIndSpecified = true;
                }

                #endregion

                #region ADDITIONAL TAX

                Return2290_2018.IRS2290TypeAdditionalTaxAmt objAdditionalTax = new Return2290_2018.IRS2290TypeAdditionalTaxAmt();
                objAdditionalTax.Value = Convert.ToDecimal(AdditionalTaxAmount.ToString("N2"));
                List<string> docId_TGWIncreaseWorksheet = new List<string>();
                docId_TGWIncreaseWorksheet.Add("TGWIncreaseWorksheet");
                objAdditionalTax.referenceDocumentId = docId_TGWIncreaseWorksheet.ToArray();
                objAdditionalTax.referenceDocumentName = "TGWIncreaseWorksheet";

                objIRS2290.AdditionalTaxAmt = objAdditionalTax;

                #endregion

                objIRS2290.TotalTaxAmt = Convert.ToDecimal(TaxFromTaxComputation.ToString("N2")) + Convert.ToDecimal(AdditionalTaxAmount.ToString("N2")); ;
                objIRS2290.TotalTaxAmtSpecified = true;

                #region CREDIT AMOUNT

                Return2290_2018.IRS2290TypeTaxCreditsAmt objTaxCreditsAmt = new Return2290_2018.IRS2290TypeTaxCreditsAmt();
                List<string> docId_CreditsAmountStatement = new List<string>();
                docId_CreditsAmountStatement.Add("CreditsAmountStatement");
                objTaxCreditsAmt.referenceDocumentId = docId_CreditsAmountStatement.ToArray();
                objTaxCreditsAmt.referenceDocumentName = "CreditsAmountStatement";
                objTaxCreditsAmt.Value = Convert.ToDecimal(CreditAmount.ToString("N2"));

                objIRS2290.TaxCreditsAmt = objTaxCreditsAmt;

                #endregion

                objIRS2290.BalanceDueAmt = (Convert.ToDecimal(TaxFromTaxComputation.ToString("N2")) + Convert.ToDecimal(AdditionalTaxAmount.ToString("N2"))) - Convert.ToDecimal(CreditAmount.ToString("N2"));

                if (objIRS2290.BalanceDueAmt < 0)
                    objIRS2290.BalanceDueAmt = 0.00M;

                #region IRS PAYMENT2

                Decimal BalanceDue = objIRS2290.BalanceDueAmt;

                if (BalanceDue > 0)
                {
                    if (paymentType != null)
                    {
                        /* Now Both Money Order or EFTPS will be same */
                        if (paymentType == "CMO" || paymentType == "EFTPS")
                        {
                            objIRS2290.EFTPSPaymentInd = Return2290_2018.CheckboxType.X;
                            objIRS2290.EFTPSPaymentIndSpecified = true;
                        }
                        /* Credit / Debit card payments */
                        else if (paymentType == "CDC")
                        {
                            objIRS2290.CreditDebitCardPaymentInd = Return2290_2018.CheckboxType.X;
                            objIRS2290.CreditDebitCardPaymentIndSpecified = true;

                        }
                        else if (paymentType == "DirectDebit" || paymentType == "EFW")
                        {
                            List<Return2290_2018.IRSPayment2> irsPmt2Collection = new List<Return2290_2018.IRSPayment2>();

                            Return2290_2018.IRSPayment2 objPayType = new Return2290_2018.IRSPayment2();

                            objPayType.PaymentAmt = Convert.ToDecimal(BalanceDue.ToString("N2"));
                            objPayType.RoutingTransitNum = Payment_RoutingTransitNo;
                            objPayType.BankAccountNum = Payment_Acc_No;

                            if (Payment_Acc_Type == "Checking" || Payment_Acc_Type == "0")
                                objPayType.BankAccountTypeCd = Return2290_2018.BankAccountType.Item1;
                            else if (Payment_Acc_Type == "Saving" || Payment_Acc_Type == "1")
                                objPayType.BankAccountTypeCd = Return2290_2018.BankAccountType.Item2;

                            objPayType.RequestedPaymentDt = Convert.ToDateTime(Payment_ReqPayDate);

                            objPayType.TaxpayerDaytimePhoneNum = Payment_Txpyer_Ph;

                            objPayType.documentId = "IRSPayment2";
                            //objPayType.documentName = "IRSPayment2";
                            objPayType.softwareId = Variables.SoftwareID;
                            objPayType.softwareVersion = Variables.SoftwareVersion;

                            irsPmt2Collection.Add(objPayType);

                            returnData.IRSPayment2 = irsPmt2Collection.ToArray();
                            documentCount += 1;
                        }
                    }
                }
                #endregion

                #region SUSPENDED VIN

                if (dsSuspendedVehicles.Tables[0].Rows.Count > 0)
                {
                    List<Return2290_2018.SuspendedVINInfoTypeVINDetail> objSusVehList = new List<Return2290_2018.SuspendedVINInfoTypeVINDetail>();

                    foreach (DataRow ldr_suspended in dsSuspendedVehicles.Tables[0].Rows)
                    {
                        Return2290_2018.SuspendedVINInfoTypeVINDetail objSusVeh = new Return2290_2018.SuspendedVINInfoTypeVINDetail();
                        objSusVeh.VIN = Convert.ToString(ldr_suspended["VIN"]);
                        objSusVehList.Add(objSusVeh);

                        #region ....
                        if (!Convert.ToBoolean(ldr_suspended["IsAgricultural"]))
                        {
                            objIRS2290.AgricMileageUsed7500OrLessInd = Return2290_2018.CheckboxType.X;
                            objIRS2290.AgricMileageUsed7500OrLessIndSpecified = true;
                        }
                        else if (Convert.ToBoolean(ldr_suspended["IsAgricultural"]))
                        {
                            objIRS2290.MileageUsed5000OrLessInd = Return2290_2018.CheckboxType.X;
                            objIRS2290.MileageUsed5000OrLessIndSpecified = true;
                        }

                        if (Convert.ToString(ldr_suspended["IsLogging"]) != "")
                        {
                            if (Convert.ToBoolean(ldr_suspended["IsLogging"]))
                                numTaxSuspended_LoggingVeh += 1;
                            else if (!Convert.ToBoolean(ldr_suspended["IsLogging"]))
                                numTaxSuspended_NonLoggingVeh += 1;
                        }
                        else
                        {
                            numTaxSuspended_NonLoggingVeh += 1;
                        }

                        #endregion
                    }

                    Return2290_2018.SuspendedVINStatementType objSusVehStmtType = new Return2290_2018.SuspendedVINStatementType();
                    objSusVehStmtType.SuspendedVINInfo = objSusVehList.ToArray();

                    if (numTaxSuspended_LoggingVeh != 0)
                        objIRS2290.TaxSuspendedLoggingVehCnt = numTaxSuspended_LoggingVeh.ToString();

                    if (numTaxSuspended_NonLoggingVeh != 0 && numTaxSuspended_LoggingVeh != 0)
                        objIRS2290.TaxSuspendedLoggingVehCnt = (numTaxSuspended_LoggingVeh + numTaxSuspended_NonLoggingVeh).ToString();
                    else
                    {
                        if (numTaxSuspended_NonLoggingVeh != 0)
                            objIRS2290.TaxSuspendedNonLoggingVehCnt = numTaxSuspended_NonLoggingVeh.ToString();
                    }


                    Return2290_2018.IRS2290TypeSuspendedVINReferenceTyp objSuspendedVINRef =
                        new Return2290_2018.IRS2290TypeSuspendedVINReferenceTyp();
                    List<string> docID_SuspendedVINStmt = new List<string>();
                    docID_SuspendedVINStmt.Add("SuspendedVINStatement");
                    objSuspendedVINRef.referenceDocumentId = docID_SuspendedVINStmt.ToArray();
                    objSuspendedVINRef.referenceDocumentName = "SuspendedVINStatement";


                    objIRS2290.SuspendedVINReferenceTyp = objSuspendedVINRef;
                }

                #endregion

                if (IsNotSubjectToTaxChecked)
                    objIRS2290.NotSubjectToTaxInd = Return2290_2018.CheckboxType.X;

                
                if (dsTGWIncreaseWorksheet.Tables.Count > 0 && IsAmendedReturn)
                {
                   
                    if (Convert.ToDecimal(AdditionalTaxAmount.ToString("N2")) > 0)
                    {
                        objIRS2290.TotalTaxComputationAmt = 0.01m;//Convert.ToDecimal(TaxFromTaxComputation.ToString("N2"));
                        objIRS2290.TotalTaxComputationAmtSpecified = true;
                    }
                    else
                    {
                        objIRS2290.TotalTaxComputationAmtSpecified = false;
                    }
                }
                else
                {
                    if (Convert.ToDecimal(TaxFromTaxComputation.ToString("N2")) > 0)
                    {
                        objIRS2290.TotalTaxComputationAmt = Convert.ToDecimal(TaxFromTaxComputation.ToString("N2"));
                        objIRS2290.TotalTaxComputationAmtSpecified = true;
                    }
                    else
                    {
                        objIRS2290.TotalTaxComputationAmtSpecified = false;
                    }
                }
                objIRS2290.softwareId = Variables.SoftwareID;
                objIRS2290.softwareVersion = Variables.SoftwareVersion;
                objIRS2290.documentId = "IRS2290";
                objIRS2290.documentName = "IRS2290";

                returnData.IRS2290 = objIRS2290;
                documentCount += 1;

                #endregion

                #region IRS 2290 SCHEDULE1

                Return2290_2018.IRS2290Schedule1 irs2290Schedule1 = new Return2290_2018.IRS2290Schedule1();

                if (Convert.ToInt16(numTotalTaxableVeh) > 0)
                    irs2290Schedule1.TaxableVehicleCnt = numTotalTaxableVeh.ToString();

                int Count_taxSuspendedLogVeh = Convert.ToInt16(numTaxSuspended_LoggingVeh);
                int Count_taxSuspendedNonLogVeh = Convert.ToInt16(numTaxSuspended_NonLoggingVeh);
                if (Count_taxSuspendedLogVeh + Count_taxSuspendedNonLogVeh != 0)
                    irs2290Schedule1.TotalSuspendedVehicleCnt = Convert.ToString(Count_taxSuspendedLogVeh + Count_taxSuspendedNonLogVeh);

                
                if ((numTotalTaxableVeh + Count_taxSuspendedLogVeh + Count_taxSuspendedNonLogVeh) > 0)
                    irs2290Schedule1.VehicleCnt = Convert.ToString(numTotalTaxableVeh + Count_taxSuspendedLogVeh + Count_taxSuspendedNonLogVeh);
                else
                {
                    if (IsAmendedReturn)
                    {
                        if ((numTotalTaxableVeh + Count_taxSuspendedLogVeh + Count_taxSuspendedNonLogVeh) > 0)
                        {
                            irs2290Schedule1.VehicleCnt = Convert.ToString(numTotalTaxableVeh + Count_taxSuspendedLogVeh + Count_taxSuspendedNonLogVeh);
                        }
                        else
                            irs2290Schedule1.VehicleCnt = "1";
                    }
                }

                List<Return2290_2018.IRS2290Schedule1TypeVehicleReportTaxItem> objVehRptTaxItem =
                    new List<Return2290_2018.IRS2290Schedule1TypeVehicleReportTaxItem>();

                if (dsTaxableVehicles.Tables.Count > 0)
                    foreach (DataRow ldr in dsTaxableVehicles.Tables[0].Rows)
                    {
                        Return2290_2018.IRS2290Schedule1TypeVehicleReportTaxItem vrti =
                            new Return2290_2018.IRS2290Schedule1TypeVehicleReportTaxItem();
                        vrti.VehicleCategoryCd = Convert.ToString(ldr["WeightCategory"]);
                        vrti.VIN = Convert.ToString(ldr["VIN"]);
                        objVehRptTaxItem.Add(vrti);
                    }

                if (dsTGWIncreaseWorksheet.Tables.Count > 0)
                    foreach (DataRow ldr in dsTGWIncreaseWorksheet.Tables[0].Rows)
                    {
                        Return2290_2018.IRS2290Schedule1TypeVehicleReportTaxItem vrti =
                            new Return2290_2018.IRS2290Schedule1TypeVehicleReportTaxItem();
                        vrti.VehicleCategoryCd = Convert.ToString(ldr["WeightCategoryNew"]);
                        vrti.VIN = Convert.ToString(ldr["VIN"]);
                        objVehRptTaxItem.Add(vrti);
                    }

                if (dsSuspendedVehicles.Tables.Count > 0)
                {
                    foreach (DataRow ldr in dsSuspendedVehicles.Tables[0].Rows)
                    {
                        Return2290_2018.IRS2290Schedule1TypeVehicleReportTaxItem vrti = new
                            Return2290_2018.IRS2290Schedule1TypeVehicleReportTaxItem();
                        vrti.VehicleCategoryCd = "W";
                        vrti.VIN = Convert.ToString(ldr["VIN"]);
                        objVehRptTaxItem.Add(vrti);
                    }
                }

                irs2290Schedule1.VehicleReportTaxItem = objVehRptTaxItem.ToArray();

                irs2290Schedule1.documentId = "IRS2290Schedule1";
                irs2290Schedule1.documentName = "IRS2290Schedule1";
                irs2290Schedule1.softwareId = Variables.SoftwareID;
                irs2290Schedule1.softwareVersion = Variables.SoftwareVersion;

                returnData.IRS2290Schedule1 = irs2290Schedule1;
                documentCount += 1;

                #endregion

                //2018 VIN Correction 
                #region VIN CORRECTION  Explanation Statement
                if (IsVINCorrection)
                {
                    //Return2290_2018.IRS2290Schedule1 irs2290Schedule1 = new Return2290_2018.IRS2290Schedule1();

                    Return2290_2018.VINCorrectionExplanationStmtType VinCorExp = new Return2290_2018.VINCorrectionExplanationStmtType();

                    VinCorExp.ExplanationTxt = "VINCorrection update request. Update the previosuly accepted Submission to the NEW VIN attached with the IRS2290Schedule1";
                    //objIRS2290.VINCorrectionExplanationStmt = VinCorExp;

                    VinCorExp.documentId = "VINCorrectionExplan";
                    //VinCorExp.documentName = "VINCorrectionExplanationStmtType";
                    VinCorExp.softwareId = Variables.SoftwareID;
                    VinCorExp.softwareVersion = Variables.SoftwareVersion;

                    returnData.VINCorrectionExplanationStmt = VinCorExp;
                    documentCount += 1;
                }
                #endregion   // VIN CORRECTION  Explanation Statement

                #region CREDITS AMOUNT STATEMENT

                if (dsCreditVehicles.Tables[0].Rows.Count > 0)
                {
                    int rowCount = dsCreditVehicles.Tables[0].Rows.Count;
                    List<Return2290_2018.CreditsAmountInfoTypeDisposalReportingItem> dric =
                        new List<Return2290_2018.CreditsAmountInfoTypeDisposalReportingItem>();

                    foreach (DataRow ldr in dsCreditVehicles.Tables[0].Rows)
                    {
                        Return2290_2018.CreditsAmountInfoTypeDisposalReportingItem dri =
                            new Return2290_2018.CreditsAmountInfoTypeDisposalReportingItem();
                        dri.CreditsAmountExplanationTxt = Convert.ToString(ldr["Reason"]);
                        dri.DisposalReportingAmt = Convert.ToString(ldr["TaxAmount"]);
                        dri.DisposalReportingDt = Convert.ToDateTime(ldr["EffectiveDate"]);
                        dri.DisposalReportingVIN = Convert.ToString(ldr["VIN"]);

                        dric.Add(dri);
                    }

                    Return2290_2018.CreditsAmountStatement cas = new Return2290_2018.CreditsAmountStatement();
                    cas.CreditsAmountInfo = dric.ToArray();
                    cas.documentId = "CreditsAmountStatement";
                    cas.documentName = "CreditsAmountStatement";
                    cas.softwareId = Variables.SoftwareID;
                    cas.softwareVersion = Variables.SoftwareVersion;

                    Return2290_2018.CreditsAmountStatement[] casc = new Return2290_2018.CreditsAmountStatement[1];
                    casc[0] = cas;

                    returnData.CreditsAmountStatement = casc;
                    documentCount += 1;
                }
                #endregion

                #region STATEMENT IN SUPPORT OF SUSPENSION //Sold Suspended

                if (dsSoldSuspendedVehicles.Tables[0].Rows.Count > 0)
                {
                    List<Return2290_2018.StmtInSupportOfSuspensionInfoTypeVehicleSuspensionDetail> ic =
                        new List<Return2290_2018.StmtInSupportOfSuspensionInfoTypeVehicleSuspensionDetail>();

                    foreach (DataRow ldr in dsSoldSuspendedVehicles.Tables[0].Rows)
                    {
                        Return2290_2018.StmtInSupportOfSuspensionInfoTypeVehicleSuspensionDetail i =
                            new Return2290_2018.StmtInSupportOfSuspensionInfoTypeVehicleSuspensionDetail();
                        i.Dt = Convert.ToDateTime(ldr["DateSold"]);
                        Return2290_2018.BusinessNameType bnt = new Return2290_2018.BusinessNameType();
                        bnt.BusinessNameLine1Txt = Convert.ToString(ldr["Buyer"]);
                        i.BusinessName = bnt;
                        i.VIN = Convert.ToString(ldr["VIN"]);
                        ic.Add(i);
                    }

                    Return2290_2018.StmtInSupportOfSuspension sisosit = new Return2290_2018.StmtInSupportOfSuspension();
                    sisosit.StmtInSupportOfSuspensionInfo = ic.ToArray();

                    List<Return2290_2018.StmtInSupportOfSuspension> sisostc = new List<Return2290_2018.StmtInSupportOfSuspension>();
                    sisostc.Add(sisosit);

                    sisosit.documentId = "StatementInSupportOfSuspension";
                    sisosit.documentName = "StatementInSupportOfSuspension";
                    sisosit.softwareId = Variables.SoftwareID;
                    sisosit.softwareVersion = Variables.SoftwareVersion;

                    returnData.StmtInSupportOfSuspension = sisostc.ToArray();
                    documentCount += 1;
                }
                #endregion

                #region SUSPENDED VIN STATEMENT

                if (dsPriorYearMileageExceededVehicles.Tables[0].Rows.Count > 0)
                {
                    List<Return2290_2018.SuspendedVINInfoTypeVINDetail> objSVITVDList =
                        new List<Return2290_2018.SuspendedVINInfoTypeVINDetail>();
                    foreach (DataRow ldr in dsPriorYearMileageExceededVehicles.Tables[0].Rows)
                    {
                        Return2290_2018.SuspendedVINInfoTypeVINDetail objSVITVD = new Return2290_2018.SuspendedVINInfoTypeVINDetail();
                        objSVITVD.VIN = Convert.ToString(ldr["VIN"]);
                        objSVITVDList.Add(objSVITVD);
                    }

                    Return2290_2018.SuspendedVINStatement objSVS = new Return2290_2018.SuspendedVINStatement();
                    objSVS.SuspendedVINInfo = objSVITVDList.ToArray();
                    objSVS.documentId = "SuspendedVINStatement";
                    objSVS.documentName = "SuspendedVINStatement";
                    objSVS.softwareId = Variables.SoftwareID;
                    objSVS.softwareVersion = Variables.SoftwareVersion;

                    Return2290_2018.SuspendedVINStatement[] suspendedVINStmt = new Return2290_2018.SuspendedVINStatement[1];
                    suspendedVINStmt[0] = objSVS;

                    returnData.SuspendedVINStatement = suspendedVINStmt;
                    documentCount += 1;
                }
                #endregion

                #region TGW INCREASE WORKSHEET

                if (dsTGWIncreaseWorksheet.Tables[0].Rows.Count > 0)
                {
                    List<Return2290_2018.TGWIncreaseInfoType> tgwiitc = new List<Return2290_2018.TGWIncreaseInfoType>();
                    foreach (DataRow ldr in dsTGWIncreaseWorksheet.Tables[0].Rows)
                    {
                        Return2290_2018.TGWIncreaseInfoType tgwiit = new Return2290_2018.TGWIncreaseInfoType();

                        tgwiit.AdditionalTaxAmt = Convert.ToDecimal(ldr["TaxAmount"]);
                        tgwiit.AdditionalTaxAmtSpecified = true;
                        tgwiit.TGWIncreaseMonthNum = Convert.ToString(ldr["IncreasedMonthValue"]);
                        tgwiit.NewTaxAmt = Convert.ToDecimal(ldr["NewTaxAmount"]);
                        tgwiit.NewTaxAmtSpecified = true;
                        tgwiit.PreviousTaxAmt = Convert.ToDecimal(ldr["PreviousTaxAmount"]);
                        tgwiit.PreviousTaxAmtSpecified = true;
                        tgwiit.TGWCategoryCd = Convert.ToString(ldr["WeightCategoryNew"]);
                        tgwiitc.Add(tgwiit);
                    }

                    Return2290_2018.TGWIncreaseWorksheet tgwIncreaseWorksheet = new Return2290_2018.TGWIncreaseWorksheet();
                    tgwIncreaseWorksheet.TGWIncreaseInfo = tgwiitc.ToArray();
                    tgwIncreaseWorksheet.documentId = "TGWIncreaseWorksheet";
                    //tgwIncreaseWorksheet.__documentName = "TGWIncreaseWorksheet";
                    tgwIncreaseWorksheet.softwareId = Variables.SoftwareID;
                    tgwIncreaseWorksheet.softwareVersion = Variables.SoftwareVersion;

                    List<Return2290_2018.TGWIncreaseWorksheet> tgwiwc = new List<Return2290_2018.TGWIncreaseWorksheet>();
                    tgwiwc.Add(tgwIncreaseWorksheet);

                    returnData.TGWIncreaseWorksheet = tgwiwc.ToArray();
                    documentCount += 1;
                }
                #endregion

                returnData.__documentCount = documentCount.ToString();

                #endregion

                #region RETURN

                Return2290_2018.Return return2290 = new Return2290_2018.Return();
                return2290.ReturnData = returnData;
                return2290.ReturnHeader = returnHeaderType;
                return2290.returnVersion = Variables.SoftwareVersion;

                #endregion

                #region Create XML file

                IFormatter ft = new BinaryFormatter();

                ft.Serialize(mStream, return2290);

                mStream.Position = 0;

                Return2290_2018.Return irsForm2 = (Return2290_2018.Return)ft.Deserialize(mStream);

                s.Serialize(fStream2, irsForm2);

                if (mStream != null) mStream.Close();

                if (fStream2 != null) fStream2.Close();

                if (fStream != null) fStream.Close();

                try
                {
                    XmlDocument xdoc = new XmlDocument();
                    xdoc.PreserveWhitespace = false;
                    xdoc.Load("C:\\xml\\submission.xml");
                    xdoc.DocumentElement.SetAttribute("xmlns:efile", "http://www.irs.gov/efile");
                    XmlTextWriter xmlWriter = new XmlTextWriter("C:\\" + @"\xml\submission.xml", new UTF8Encoding(false));
                    xdoc.Save(xmlWriter);
                    xmlWriter.Flush();
                    xmlWriter.Close();
                }
                catch { }

                #endregion

            }
            catch (Exception ex)
            {
                if (fStream != null) fStream.Close();
                if (fStream2 != null) fStream2.Close();
                if (mStream != null) mStream.Close();
                throw ex;
            }
        }
        #endregion

        #region createReturnXml_2290_2017

        /* Vishwa New 2017 Changes, for the XML building for 2017 */
        public void createReturnXml_2290_2017
        (
            #region Parameters
                bool IsAddressChange, bool IsAmendedReturn, bool IsVINCorrection, bool IsFinalReturn,
                bool IsNotSubjectToTaxChecked, bool IsConsentToDiscloseYes, string AmendedMonth, string TaxYear,
                string First_Used_Date, string Filer_AddressLine1, string Filer_City, string Filer_EIN,
                string Filer_Foreign_Country, string Filer_Name, string Filer_NameControl, string Filer_State,
                string Filer_ZIPCode, string Officer_Name, string Officer_Phone, string Officer_PIN,
                string Officer_Title, string Officer_email, string ThirdPartyDesigne_Name, string ThirdPartyDesigne_Phone,
                string ThirdPartyDesigne_PIN, string Preparer_Firm, string Preparer_EIN, string Preparer_City,
                string Preparer_State, string Preparer_Zip, string Preparer_Country, string Preparer_Address,
                string Preparer_ForeignPhone, string Preparer_Phone, string Preparer_Email, string Preparer_Name,
                string Preparer_PTIN_SSN, bool IsPreparerHasPTIN, bool IsPreparerSelfEmployed, string paymentType,
                string Payment_Acc_No, string Payment_Acc_Type, string Payment_ReqPayDate, string Payment_RoutingTransitNo,
                string Payment_Txpyer_Ph, DataSet dsTaxableVehicles, DataSet dsCreditVehicles, DataSet dsSuspendedVehicles,
                DataSet dsSoldSuspendedVehicles, DataSet dsPriorYearMileageExceededVehicles, DataSet dsTGWIncreaseWorksheet, DataSet dsTaxComputation,
                string soldSuspendedVehicles, decimal TaxFromTaxComputation, decimal AdditionalTaxAmount, decimal CreditAmount,
                string TotalVehicles
            #endregion
        )
        {
            int documentCount = 0;
            FileStream fStream = null;
            FileStream fStream2 = null;
            MemoryStream mStream = null;

            int numTotalTaxableVeh = 0;
            int numTaxSuspended_LoggingVeh = 0;
            int numTaxSuspended_NonLoggingVeh = 0;

            int numNonLoggingVehicleCnt = 0;
            int numLoggingVehicleCnt = 0;

            try
            {
                if (!Directory.Exists(path2CreateZipFile + @"\xml"))
                    Directory.CreateDirectory(path2CreateZipFile + @"\xml");

                fStream2 = new FileStream(path2CreateZipFile + @"\xml\submission.xml", FileMode.Create);

                byte[] byteArray = new byte[90000];
                mStream = new MemoryStream(byteArray);
                XmlSerializer s = new XmlSerializer(typeof(Return2290_2017.Return));

                #region RETRUN_HEADER

                Return2290_2017.ReturnHeaderType returnHeaderType = new Return2290_2017.ReturnHeaderType();

                returnHeaderType.ReturnTs = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz");

                returnHeaderType.FirstUsedDt = First_Used_Date;

                returnHeaderType.MultSoftwarePackagesUsedInd = false;
                returnHeaderType.PINEnteredByCd = Return2290_2017.ReturnHeaderTypePINEnteredByCd.Taxpayer;
                returnHeaderType.PINEnteredByCdSpecified = true;
                returnHeaderType.ReturnTypeCd = Return2290_2017.ReturnHeaderTypeReturnTypeCd.Item2290;
                returnHeaderType.SignatureOptionCd = Return2290_2017.ReturnHeaderTypeSignatureOptionCd.PINNumber;
                returnHeaderType.SignatureOptionCdSpecified = true;

                #region ORIGINATOR

                #region PRACTIONER PIN

                Return2290_2017.ReturnHeaderTypeOriginatorGrpPractitionerPINGrp objPractitionerPIN =
                    new Return2290_2017.ReturnHeaderTypeOriginatorGrpPractitionerPINGrp();
                objPractitionerPIN.EFIN = Variables.EFIN;
                objPractitionerPIN.PIN = Variables.PIN;

                #endregion

                Return2290_2017.ReturnHeaderTypeOriginatorGrp originator = new Return2290_2017.ReturnHeaderTypeOriginatorGrp();
                originator.EFIN = Variables.EFIN;
                originator.OriginatorTypeCd = Return2290_2017.OriginatorType.ERO;
                originator.PractitionerPINGrp = objPractitionerPIN;
                returnHeaderType.OriginatorGrp = originator;

                #endregion

                #region FILER
                Return2290_2017.ReturnHeaderTypeFiler objFiler = new Return2290_2017.ReturnHeaderTypeFiler();

                objFiler.EIN = Filer_EIN;
                objFiler.BusinessNameLine1Txt = Filer_Name; //max_length=60
                objFiler.BusinessNameControlTxt = Filer_NameControl;

                if (Filer_Foreign_Country == "" || Filer_Foreign_Country == "US")
                {
                    #region FILER US ADDRESS

                    Return2290_2017.USAddressType usAdd_Filer = new Return2290_2017.USAddressType();

                    if (Filer_AddressLine1.Length > 35)
                    {
                        string Filer_AddressLine2 = string.Empty;
                        usAdd_Filer.AddressLine1 = Filer_AddressLine1.Substring(0, 35).Trim();
                        Filer_AddressLine2 = Filer_AddressLine1.Substring(35);
                        usAdd_Filer.AddressLine2 = Filer_AddressLine2.Trim();

                    }
                    else
                    {
                        usAdd_Filer.AddressLine1 = Filer_AddressLine1.Trim();
                    }
                    usAdd_Filer.City = Filer_City.Trim();

                    switch (Filer_State)
                    {
                        #region Filer_State
                        case "AA": usAdd_Filer.State = Return2290_2017.StateType.AA; break;
                        case "AE": usAdd_Filer.State = Return2290_2017.StateType.AE; break;
                        case "AK": usAdd_Filer.State = Return2290_2017.StateType.AK; break;
                        case "AL": usAdd_Filer.State = Return2290_2017.StateType.AL; break;
                        case "AP": usAdd_Filer.State = Return2290_2017.StateType.AP; break;
                        case "AR": usAdd_Filer.State = Return2290_2017.StateType.AR; break;
                        case "AS": usAdd_Filer.State = Return2290_2017.StateType.AS; break;
                        case "AZ": usAdd_Filer.State = Return2290_2017.StateType.AZ; break;
                        case "CA": usAdd_Filer.State = Return2290_2017.StateType.CA; break;
                        case "CO": usAdd_Filer.State = Return2290_2017.StateType.CO; break;
                        case "CT": usAdd_Filer.State = Return2290_2017.StateType.CT; break;
                        case "DC": usAdd_Filer.State = Return2290_2017.StateType.DC; break;
                        case "DE": usAdd_Filer.State = Return2290_2017.StateType.DE; break;
                        case "FL": usAdd_Filer.State = Return2290_2017.StateType.FL; break;
                        case "FM": usAdd_Filer.State = Return2290_2017.StateType.FM; break;
                        case "GA": usAdd_Filer.State = Return2290_2017.StateType.GA; break;
                        case "GU": usAdd_Filer.State = Return2290_2017.StateType.GU; break;
                        case "HI": usAdd_Filer.State = Return2290_2017.StateType.HI; break;
                        case "IA": usAdd_Filer.State = Return2290_2017.StateType.IA; break;
                        case "ID": usAdd_Filer.State = Return2290_2017.StateType.ID; break;
                        case "IL": usAdd_Filer.State = Return2290_2017.StateType.IL; break;
                        case "IN": usAdd_Filer.State = Return2290_2017.StateType.IN; break;
                        case "KS": usAdd_Filer.State = Return2290_2017.StateType.KS; break;
                        case "KY": usAdd_Filer.State = Return2290_2017.StateType.KY; break;
                        case "LA": usAdd_Filer.State = Return2290_2017.StateType.LA; break;
                        case "MA": usAdd_Filer.State = Return2290_2017.StateType.MA; break;
                        case "MD": usAdd_Filer.State = Return2290_2017.StateType.MD; break;
                        case "ME": usAdd_Filer.State = Return2290_2017.StateType.ME; break;
                        case "MH": usAdd_Filer.State = Return2290_2017.StateType.MH; break;
                        case "MI": usAdd_Filer.State = Return2290_2017.StateType.MI; break;
                        case "MN": usAdd_Filer.State = Return2290_2017.StateType.MN; break;
                        case "MO": usAdd_Filer.State = Return2290_2017.StateType.MO; break;
                        case "MP": usAdd_Filer.State = Return2290_2017.StateType.MP; break;
                        case "MS": usAdd_Filer.State = Return2290_2017.StateType.MS; break;
                        case "MT": usAdd_Filer.State = Return2290_2017.StateType.MT; break;
                        case "NC": usAdd_Filer.State = Return2290_2017.StateType.NC; break;
                        case "ND": usAdd_Filer.State = Return2290_2017.StateType.ND; break;
                        case "NE": usAdd_Filer.State = Return2290_2017.StateType.NE; break;
                        case "NH": usAdd_Filer.State = Return2290_2017.StateType.NH; break;
                        case "NJ": usAdd_Filer.State = Return2290_2017.StateType.NJ; break;
                        case "NM": usAdd_Filer.State = Return2290_2017.StateType.NM; break;
                        case "NV": usAdd_Filer.State = Return2290_2017.StateType.NV; break;
                        case "NY": usAdd_Filer.State = Return2290_2017.StateType.NY; break;
                        case "OH": usAdd_Filer.State = Return2290_2017.StateType.OH; break;
                        case "OK": usAdd_Filer.State = Return2290_2017.StateType.OK; break;
                        case "OR": usAdd_Filer.State = Return2290_2017.StateType.OR; break;
                        case "PA": usAdd_Filer.State = Return2290_2017.StateType.PA; break;
                        case "PR": usAdd_Filer.State = Return2290_2017.StateType.PR; break;
                        case "PW": usAdd_Filer.State = Return2290_2017.StateType.PW; break;
                        case "RI": usAdd_Filer.State = Return2290_2017.StateType.RI; break;
                        case "SC": usAdd_Filer.State = Return2290_2017.StateType.SC; break;
                        case "SD": usAdd_Filer.State = Return2290_2017.StateType.SD; break;
                        case "TN": usAdd_Filer.State = Return2290_2017.StateType.TN; break;
                        case "TX": usAdd_Filer.State = Return2290_2017.StateType.TX; break;
                        case "UT": usAdd_Filer.State = Return2290_2017.StateType.UT; break;
                        case "VA": usAdd_Filer.State = Return2290_2017.StateType.VA; break;
                        case "VI": usAdd_Filer.State = Return2290_2017.StateType.VI; break;
                        case "VT": usAdd_Filer.State = Return2290_2017.StateType.VT; break;
                        case "WA": usAdd_Filer.State = Return2290_2017.StateType.WA; break;
                        case "WI": usAdd_Filer.State = Return2290_2017.StateType.WI; break;
                        case "WV": usAdd_Filer.State = Return2290_2017.StateType.WV; break;
                        case "WY": usAdd_Filer.State = Return2290_2017.StateType.WY; break;
                        #endregion
                    }

                    usAdd_Filer.ZIPCode = Filer_ZIPCode;

                    objFiler.Item = usAdd_Filer;
                    #endregion
                }
                else
                {
                    #region FILER FOREIGN ADDRESS
                    Return2290_2017.ForeignAddressType foreignAdd_Filer = new Return2290_2017.ForeignAddressType();

                    if (Filer_AddressLine1.Length > 35)
                    {
                        string Filer_AddressLine2 = string.Empty;
                        foreignAdd_Filer.AddressLine1 = Filer_AddressLine1.Substring(0, 35).Trim();
                        Filer_AddressLine2 = Filer_AddressLine1.Substring(35);
                        foreignAdd_Filer.AddressLine2 = Filer_AddressLine2.Trim();
                    }
                    else
                    {
                        foreignAdd_Filer.AddressLine1 = Filer_AddressLine1.Trim();
                    }

                    foreignAdd_Filer.City = Filer_City.Trim();

                    if (Filer_State.Length > 0)
                        foreignAdd_Filer.ProvinceOrState = Filer_State;

                    switch (Filer_Foreign_Country)
                    {
                        #region Filer_Foreign_Country
                        case "AA": foreignAdd_Filer.Country = Return2290_2017.CountryType.AA; break;
                        case "AC": foreignAdd_Filer.Country = Return2290_2017.CountryType.AC; break;
                        case "AE": foreignAdd_Filer.Country = Return2290_2017.CountryType.AE; break;
                        case "AF": foreignAdd_Filer.Country = Return2290_2017.CountryType.AF; break;
                        case "AG": foreignAdd_Filer.Country = Return2290_2017.CountryType.AG; break;
                        case "AJ": foreignAdd_Filer.Country = Return2290_2017.CountryType.AJ; break;
                        case "AL": foreignAdd_Filer.Country = Return2290_2017.CountryType.AL; break;
                        case "AM": foreignAdd_Filer.Country = Return2290_2017.CountryType.AM; break;
                        case "AN": foreignAdd_Filer.Country = Return2290_2017.CountryType.AN; break;
                        case "AO": foreignAdd_Filer.Country = Return2290_2017.CountryType.AO; break;
                        case "AR": foreignAdd_Filer.Country = Return2290_2017.CountryType.AR; break;
                        case "AS": foreignAdd_Filer.Country = Return2290_2017.CountryType.AS; break;
                        case "AT": foreignAdd_Filer.Country = Return2290_2017.CountryType.AT; break;
                        case "AU": foreignAdd_Filer.Country = Return2290_2017.CountryType.AU; break;
                        case "AV": foreignAdd_Filer.Country = Return2290_2017.CountryType.AV; break;
                        case "AX": foreignAdd_Filer.Country = Return2290_2017.CountryType.AX; break;
                        case "AY": foreignAdd_Filer.Country = Return2290_2017.CountryType.AY; break;
                        case "BA": foreignAdd_Filer.Country = Return2290_2017.CountryType.BA; break;
                        case "BB": foreignAdd_Filer.Country = Return2290_2017.CountryType.BB; break;
                        case "BC": foreignAdd_Filer.Country = Return2290_2017.CountryType.BC; break;
                        case "BD": foreignAdd_Filer.Country = Return2290_2017.CountryType.BD; break;
                        case "BE": foreignAdd_Filer.Country = Return2290_2017.CountryType.BE; break;
                        case "BF": foreignAdd_Filer.Country = Return2290_2017.CountryType.BF; break;
                        case "BG": foreignAdd_Filer.Country = Return2290_2017.CountryType.BG; break;
                        case "BH": foreignAdd_Filer.Country = Return2290_2017.CountryType.BH; break;
                        case "BK": foreignAdd_Filer.Country = Return2290_2017.CountryType.BK; break;
                        case "BL": foreignAdd_Filer.Country = Return2290_2017.CountryType.BL; break;
                        case "BM": foreignAdd_Filer.Country = Return2290_2017.CountryType.BM; break;
                        case "BN": foreignAdd_Filer.Country = Return2290_2017.CountryType.BN; break;
                        case "BO": foreignAdd_Filer.Country = Return2290_2017.CountryType.BO; break;
                        case "BP": foreignAdd_Filer.Country = Return2290_2017.CountryType.BP; break;
                        case "BQ": foreignAdd_Filer.Country = Return2290_2017.CountryType.BQ; break;
                        case "BR": foreignAdd_Filer.Country = Return2290_2017.CountryType.BR; break;
                        case "BT": foreignAdd_Filer.Country = Return2290_2017.CountryType.BT; break;
                        case "BU": foreignAdd_Filer.Country = Return2290_2017.CountryType.BU; break;
                        case "BV": foreignAdd_Filer.Country = Return2290_2017.CountryType.BV; break;
                        case "BX": foreignAdd_Filer.Country = Return2290_2017.CountryType.BX; break;
                        case "BY": foreignAdd_Filer.Country = Return2290_2017.CountryType.BY; break;
                        case "CA": foreignAdd_Filer.Country = Return2290_2017.CountryType.CA; break;
                        case "CB": foreignAdd_Filer.Country = Return2290_2017.CountryType.CB; break;
                        case "CD": foreignAdd_Filer.Country = Return2290_2017.CountryType.CD; break;
                        case "CE": foreignAdd_Filer.Country = Return2290_2017.CountryType.CE; break;
                        case "CF": foreignAdd_Filer.Country = Return2290_2017.CountryType.CF; break;
                        case "CG": foreignAdd_Filer.Country = Return2290_2017.CountryType.CG; break;
                        case "CH": foreignAdd_Filer.Country = Return2290_2017.CountryType.CH; break;
                        case "CI": foreignAdd_Filer.Country = Return2290_2017.CountryType.CI; break;
                        case "CJ": foreignAdd_Filer.Country = Return2290_2017.CountryType.CJ; break;
                        case "CK": foreignAdd_Filer.Country = Return2290_2017.CountryType.CK; break;
                        case "CM": foreignAdd_Filer.Country = Return2290_2017.CountryType.CM; break;
                        case "CN": foreignAdd_Filer.Country = Return2290_2017.CountryType.CN; break;
                        case "CO": foreignAdd_Filer.Country = Return2290_2017.CountryType.CO; break;
                        case "CR": foreignAdd_Filer.Country = Return2290_2017.CountryType.CR; break;
                        case "CS": foreignAdd_Filer.Country = Return2290_2017.CountryType.CS; break;
                        case "CT": foreignAdd_Filer.Country = Return2290_2017.CountryType.CT; break;
                        case "CU": foreignAdd_Filer.Country = Return2290_2017.CountryType.CU; break;
                        case "CV": foreignAdd_Filer.Country = Return2290_2017.CountryType.CV; break;
                        case "CW": foreignAdd_Filer.Country = Return2290_2017.CountryType.CW; break;
                        case "CY": foreignAdd_Filer.Country = Return2290_2017.CountryType.CY; break;
                        case "DA": foreignAdd_Filer.Country = Return2290_2017.CountryType.DA; break;
                        case "DJ": foreignAdd_Filer.Country = Return2290_2017.CountryType.DJ; break;
                        case "DO": foreignAdd_Filer.Country = Return2290_2017.CountryType.DO; break;
                        case "DR": foreignAdd_Filer.Country = Return2290_2017.CountryType.DR; break;
                        case "DX": foreignAdd_Filer.Country = Return2290_2017.CountryType.DX; break;
                        case "EC": foreignAdd_Filer.Country = Return2290_2017.CountryType.EC; break;
                        case "EG": foreignAdd_Filer.Country = Return2290_2017.CountryType.EG; break;
                        case "EI": foreignAdd_Filer.Country = Return2290_2017.CountryType.EI; break;
                        case "EK": foreignAdd_Filer.Country = Return2290_2017.CountryType.EK; break;
                        case "EN": foreignAdd_Filer.Country = Return2290_2017.CountryType.EN; break;
                        case "ER": foreignAdd_Filer.Country = Return2290_2017.CountryType.ER; break;
                        case "ES": foreignAdd_Filer.Country = Return2290_2017.CountryType.ES; break;
                        case "ET": foreignAdd_Filer.Country = Return2290_2017.CountryType.ET; break;
                        case "EZ": foreignAdd_Filer.Country = Return2290_2017.CountryType.EZ; break;
                        case "FI": foreignAdd_Filer.Country = Return2290_2017.CountryType.FI; break;
                        case "FJ": foreignAdd_Filer.Country = Return2290_2017.CountryType.FJ; break;
                        case "FK": foreignAdd_Filer.Country = Return2290_2017.CountryType.FK; break;
                        case "FO": foreignAdd_Filer.Country = Return2290_2017.CountryType.FO; break;
                        case "FP": foreignAdd_Filer.Country = Return2290_2017.CountryType.FP; break;
                        case "FQ": foreignAdd_Filer.Country = Return2290_2017.CountryType.FQ; break;
                        case "FR": foreignAdd_Filer.Country = Return2290_2017.CountryType.FR; break;
                        case "FS": foreignAdd_Filer.Country = Return2290_2017.CountryType.FS; break;
                        case "GA": foreignAdd_Filer.Country = Return2290_2017.CountryType.GA; break;
                        case "GB": foreignAdd_Filer.Country = Return2290_2017.CountryType.GB; break;
                        case "GG": foreignAdd_Filer.Country = Return2290_2017.CountryType.GG; break;
                        case "GH": foreignAdd_Filer.Country = Return2290_2017.CountryType.GH; break;
                        case "GI": foreignAdd_Filer.Country = Return2290_2017.CountryType.GI; break;
                        case "GJ": foreignAdd_Filer.Country = Return2290_2017.CountryType.GJ; break;
                        case "GK": foreignAdd_Filer.Country = Return2290_2017.CountryType.GK; break;
                        case "GL": foreignAdd_Filer.Country = Return2290_2017.CountryType.GL; break;
                        case "GM": foreignAdd_Filer.Country = Return2290_2017.CountryType.GM; break;
                        case "GR": foreignAdd_Filer.Country = Return2290_2017.CountryType.GR; break;
                        case "GT": foreignAdd_Filer.Country = Return2290_2017.CountryType.GT; break;
                        case "GV": foreignAdd_Filer.Country = Return2290_2017.CountryType.GV; break;
                        case "GY": foreignAdd_Filer.Country = Return2290_2017.CountryType.GY; break;
                        case "HA": foreignAdd_Filer.Country = Return2290_2017.CountryType.HA; break;
                        case "HK": foreignAdd_Filer.Country = Return2290_2017.CountryType.HK; break;
                        case "HM": foreignAdd_Filer.Country = Return2290_2017.CountryType.HM; break;
                        case "HO": foreignAdd_Filer.Country = Return2290_2017.CountryType.HO; break;
                        case "HQ": foreignAdd_Filer.Country = Return2290_2017.CountryType.HQ; break;
                        case "HR": foreignAdd_Filer.Country = Return2290_2017.CountryType.HR; break;
                        case "HU": foreignAdd_Filer.Country = Return2290_2017.CountryType.HU; break;
                        case "IC": foreignAdd_Filer.Country = Return2290_2017.CountryType.IC; break;
                        case "ID": foreignAdd_Filer.Country = Return2290_2017.CountryType.ID; break;
                        case "IM": foreignAdd_Filer.Country = Return2290_2017.CountryType.IM; break;
                        case "IO": foreignAdd_Filer.Country = Return2290_2017.CountryType.IO; break;
                        case "IP": foreignAdd_Filer.Country = Return2290_2017.CountryType.IP; break;
                        case "IR": foreignAdd_Filer.Country = Return2290_2017.CountryType.IR; break;
                        case "IS": foreignAdd_Filer.Country = Return2290_2017.CountryType.IS; break;
                        case "IT": foreignAdd_Filer.Country = Return2290_2017.CountryType.IT; break;
                        case "IV": foreignAdd_Filer.Country = Return2290_2017.CountryType.IV; break;
                        case "IZ": foreignAdd_Filer.Country = Return2290_2017.CountryType.IZ; break;
                        case "JA": foreignAdd_Filer.Country = Return2290_2017.CountryType.JA; break;
                        case "JE": foreignAdd_Filer.Country = Return2290_2017.CountryType.JE; break;
                        case "JM": foreignAdd_Filer.Country = Return2290_2017.CountryType.JM; break;
                        case "JN": foreignAdd_Filer.Country = Return2290_2017.CountryType.JN; break;
                        case "JO": foreignAdd_Filer.Country = Return2290_2017.CountryType.JO; break;
                        case "JQ": foreignAdd_Filer.Country = Return2290_2017.CountryType.JQ; break;
                        case "KE": foreignAdd_Filer.Country = Return2290_2017.CountryType.KE; break;
                        case "KG": foreignAdd_Filer.Country = Return2290_2017.CountryType.KG; break;
                        case "KN": foreignAdd_Filer.Country = Return2290_2017.CountryType.KN; break;
                        case "KQ": foreignAdd_Filer.Country = Return2290_2017.CountryType.KQ; break;
                        case "KR": foreignAdd_Filer.Country = Return2290_2017.CountryType.KR; break;
                        case "KS": foreignAdd_Filer.Country = Return2290_2017.CountryType.KS; break;
                        case "KT": foreignAdd_Filer.Country = Return2290_2017.CountryType.KT; break;
                        case "KU": foreignAdd_Filer.Country = Return2290_2017.CountryType.KU; break;
                        case "KV": foreignAdd_Filer.Country = Return2290_2017.CountryType.KV; break;
                        case "KZ": foreignAdd_Filer.Country = Return2290_2017.CountryType.KZ; break;
                        case "LA": foreignAdd_Filer.Country = Return2290_2017.CountryType.LA; break;
                        case "LE": foreignAdd_Filer.Country = Return2290_2017.CountryType.LE; break;
                        case "LG": foreignAdd_Filer.Country = Return2290_2017.CountryType.LG; break;
                        case "LH": foreignAdd_Filer.Country = Return2290_2017.CountryType.LH; break;
                        case "LI": foreignAdd_Filer.Country = Return2290_2017.CountryType.LI; break;
                        case "LO": foreignAdd_Filer.Country = Return2290_2017.CountryType.LO; break;
                        case "LQ": foreignAdd_Filer.Country = Return2290_2017.CountryType.LQ; break;
                        case "LS": foreignAdd_Filer.Country = Return2290_2017.CountryType.LS; break;
                        case "LT": foreignAdd_Filer.Country = Return2290_2017.CountryType.LT; break;
                        case "LU": foreignAdd_Filer.Country = Return2290_2017.CountryType.LU; break;
                        case "LY": foreignAdd_Filer.Country = Return2290_2017.CountryType.LY; break;
                        case "MA": foreignAdd_Filer.Country = Return2290_2017.CountryType.MA; break;
                        case "MC": foreignAdd_Filer.Country = Return2290_2017.CountryType.MC; break;
                        case "MD": foreignAdd_Filer.Country = Return2290_2017.CountryType.MD; break;
                        case "MG": foreignAdd_Filer.Country = Return2290_2017.CountryType.MG; break;
                        case "MH": foreignAdd_Filer.Country = Return2290_2017.CountryType.MH; break;
                        case "MI": foreignAdd_Filer.Country = Return2290_2017.CountryType.MI; break;
                        case "MJ": foreignAdd_Filer.Country = Return2290_2017.CountryType.MJ; break;
                        case "MK": foreignAdd_Filer.Country = Return2290_2017.CountryType.MK; break;
                        case "ML": foreignAdd_Filer.Country = Return2290_2017.CountryType.ML; break;
                        case "MN": foreignAdd_Filer.Country = Return2290_2017.CountryType.MN; break;
                        case "MO": foreignAdd_Filer.Country = Return2290_2017.CountryType.MO; break;
                        case "MP": foreignAdd_Filer.Country = Return2290_2017.CountryType.MP; break;
                        case "MQ": foreignAdd_Filer.Country = Return2290_2017.CountryType.MQ; break;
                        case "MR": foreignAdd_Filer.Country = Return2290_2017.CountryType.MR; break;
                        case "MT": foreignAdd_Filer.Country = Return2290_2017.CountryType.MT; break;
                        case "MU": foreignAdd_Filer.Country = Return2290_2017.CountryType.MU; break;
                        case "MV": foreignAdd_Filer.Country = Return2290_2017.CountryType.MV; break;
                        case "MX": foreignAdd_Filer.Country = Return2290_2017.CountryType.MX; break;
                        case "MY": foreignAdd_Filer.Country = Return2290_2017.CountryType.MY; break;
                        case "MZ": foreignAdd_Filer.Country = Return2290_2017.CountryType.MZ; break;
                        case "NC": foreignAdd_Filer.Country = Return2290_2017.CountryType.NC; break;
                        case "NE": foreignAdd_Filer.Country = Return2290_2017.CountryType.NE; break;
                        case "NF": foreignAdd_Filer.Country = Return2290_2017.CountryType.NF; break;
                        case "NG": foreignAdd_Filer.Country = Return2290_2017.CountryType.NG; break;
                        case "NH": foreignAdd_Filer.Country = Return2290_2017.CountryType.NH; break;
                        case "NI": foreignAdd_Filer.Country = Return2290_2017.CountryType.NI; break;
                        case "NL": foreignAdd_Filer.Country = Return2290_2017.CountryType.NL; break;
                        case "NN": foreignAdd_Filer.Country = Return2290_2017.CountryType.NN; break;
                        case "NO": foreignAdd_Filer.Country = Return2290_2017.CountryType.NO; break;
                        case "NP": foreignAdd_Filer.Country = Return2290_2017.CountryType.NP; break;
                        case "NR": foreignAdd_Filer.Country = Return2290_2017.CountryType.NR; break;
                        case "NS": foreignAdd_Filer.Country = Return2290_2017.CountryType.NS; break;
                        case "NU": foreignAdd_Filer.Country = Return2290_2017.CountryType.NU; break;
                        case "NZ": foreignAdd_Filer.Country = Return2290_2017.CountryType.NZ; break;
                        case "PA": foreignAdd_Filer.Country = Return2290_2017.CountryType.PA; break;
                        case "PC": foreignAdd_Filer.Country = Return2290_2017.CountryType.PC; break;
                        case "PE": foreignAdd_Filer.Country = Return2290_2017.CountryType.PE; break;
                        case "PF": foreignAdd_Filer.Country = Return2290_2017.CountryType.PF; break;
                        case "PG": foreignAdd_Filer.Country = Return2290_2017.CountryType.PG; break;
                        case "PK": foreignAdd_Filer.Country = Return2290_2017.CountryType.PK; break;
                        case "PL": foreignAdd_Filer.Country = Return2290_2017.CountryType.PL; break;
                        case "PM": foreignAdd_Filer.Country = Return2290_2017.CountryType.PM; break;
                        case "PO": foreignAdd_Filer.Country = Return2290_2017.CountryType.PO; break;
                        case "PP": foreignAdd_Filer.Country = Return2290_2017.CountryType.PP; break;
                        case "PU": foreignAdd_Filer.Country = Return2290_2017.CountryType.PU; break;
                        case "QA": foreignAdd_Filer.Country = Return2290_2017.CountryType.QA; break;
                        case "RI": foreignAdd_Filer.Country = Return2290_2017.CountryType.RI; break;
                        case "RN": foreignAdd_Filer.Country = Return2290_2017.CountryType.RN; break;
                        case "RO": foreignAdd_Filer.Country = Return2290_2017.CountryType.RO; break;
                        case "RP": foreignAdd_Filer.Country = Return2290_2017.CountryType.RP; break;
                        case "RS": foreignAdd_Filer.Country = Return2290_2017.CountryType.RS; break;
                        case "RW": foreignAdd_Filer.Country = Return2290_2017.CountryType.RW; break;
                        case "SA": foreignAdd_Filer.Country = Return2290_2017.CountryType.SA; break;
                        case "SB": foreignAdd_Filer.Country = Return2290_2017.CountryType.SB; break;
                        case "SC": foreignAdd_Filer.Country = Return2290_2017.CountryType.SC; break;
                        case "SE": foreignAdd_Filer.Country = Return2290_2017.CountryType.SE; break;
                        case "SF": foreignAdd_Filer.Country = Return2290_2017.CountryType.SF; break;
                        case "SG": foreignAdd_Filer.Country = Return2290_2017.CountryType.SG; break;
                        case "SH": foreignAdd_Filer.Country = Return2290_2017.CountryType.SH; break;
                        case "SI": foreignAdd_Filer.Country = Return2290_2017.CountryType.SI; break;
                        case "SL": foreignAdd_Filer.Country = Return2290_2017.CountryType.SL; break;
                        case "SM": foreignAdd_Filer.Country = Return2290_2017.CountryType.SM; break;
                        case "SN": foreignAdd_Filer.Country = Return2290_2017.CountryType.SN; break;
                        case "SO": foreignAdd_Filer.Country = Return2290_2017.CountryType.SO; break;
                        case "SP": foreignAdd_Filer.Country = Return2290_2017.CountryType.SP; break;
                        case "ST": foreignAdd_Filer.Country = Return2290_2017.CountryType.ST; break;
                        case "SU": foreignAdd_Filer.Country = Return2290_2017.CountryType.SU; break;
                        case "SV": foreignAdd_Filer.Country = Return2290_2017.CountryType.SV; break;
                        case "SW": foreignAdd_Filer.Country = Return2290_2017.CountryType.SW; break;
                        case "SX": foreignAdd_Filer.Country = Return2290_2017.CountryType.SX; break;
                        case "SY": foreignAdd_Filer.Country = Return2290_2017.CountryType.SY; break;
                        case "SZ": foreignAdd_Filer.Country = Return2290_2017.CountryType.SZ; break;
                        case "TB": foreignAdd_Filer.Country = Return2290_2017.CountryType.TB; break;
                        case "TD": foreignAdd_Filer.Country = Return2290_2017.CountryType.TD; break;
                        case "TH": foreignAdd_Filer.Country = Return2290_2017.CountryType.TH; break;
                        case "TI": foreignAdd_Filer.Country = Return2290_2017.CountryType.TI; break;
                        case "TK": foreignAdd_Filer.Country = Return2290_2017.CountryType.TK; break;
                        case "TL": foreignAdd_Filer.Country = Return2290_2017.CountryType.TL; break;
                        case "TN": foreignAdd_Filer.Country = Return2290_2017.CountryType.TN; break;
                        case "TO": foreignAdd_Filer.Country = Return2290_2017.CountryType.TO; break;
                        case "TP": foreignAdd_Filer.Country = Return2290_2017.CountryType.TP; break;
                        case "TS": foreignAdd_Filer.Country = Return2290_2017.CountryType.TS; break;
                        case "TT": foreignAdd_Filer.Country = Return2290_2017.CountryType.TT; break;
                        case "TU": foreignAdd_Filer.Country = Return2290_2017.CountryType.TU; break;
                        case "TV": foreignAdd_Filer.Country = Return2290_2017.CountryType.TV; break;
                        case "TW": foreignAdd_Filer.Country = Return2290_2017.CountryType.TW; break;
                        case "TX": foreignAdd_Filer.Country = Return2290_2017.CountryType.TX; break;
                        case "TZ": foreignAdd_Filer.Country = Return2290_2017.CountryType.TZ; break;
                        case "UC": foreignAdd_Filer.Country = Return2290_2017.CountryType.UC; break;
                        case "UG": foreignAdd_Filer.Country = Return2290_2017.CountryType.UG; break;
                        case "UK": foreignAdd_Filer.Country = Return2290_2017.CountryType.UK; break;
                        case "UP": foreignAdd_Filer.Country = Return2290_2017.CountryType.UP; break;
                        case "UV": foreignAdd_Filer.Country = Return2290_2017.CountryType.UV; break;
                        case "UY": foreignAdd_Filer.Country = Return2290_2017.CountryType.UY; break;
                        case "UZ": foreignAdd_Filer.Country = Return2290_2017.CountryType.UZ; break;
                        case "VC": foreignAdd_Filer.Country = Return2290_2017.CountryType.VC; break;
                        case "VE": foreignAdd_Filer.Country = Return2290_2017.CountryType.VE; break;
                        case "VI": foreignAdd_Filer.Country = Return2290_2017.CountryType.VI; break;
                        case "VM": foreignAdd_Filer.Country = Return2290_2017.CountryType.VM; break;
                        case "VT": foreignAdd_Filer.Country = Return2290_2017.CountryType.VT; break;
                        case "WA": foreignAdd_Filer.Country = Return2290_2017.CountryType.WA; break;
                        case "WF": foreignAdd_Filer.Country = Return2290_2017.CountryType.WF; break;
                        case "WI": foreignAdd_Filer.Country = Return2290_2017.CountryType.WI; break;
                        case "WQ": foreignAdd_Filer.Country = Return2290_2017.CountryType.WQ; break;
                        case "WS": foreignAdd_Filer.Country = Return2290_2017.CountryType.WS; break;
                        case "WZ": foreignAdd_Filer.Country = Return2290_2017.CountryType.WZ; break;
                        case "YM": foreignAdd_Filer.Country = Return2290_2017.CountryType.YM; break;
                        case "ZA": foreignAdd_Filer.Country = Return2290_2017.CountryType.ZA; break;
                        case "ZI": foreignAdd_Filer.Country = Return2290_2017.CountryType.ZI; break;
                        #endregion
                    }

                    if (Filer_ZIPCode.Length > 0)
                        foreignAdd_Filer.PostalCode = Filer_ZIPCode;

                    objFiler.Item = foreignAdd_Filer;
                    #endregion
                }

                returnHeaderType.Filer = objFiler;
                #endregion

                #region OFFICER
                if (Officer_Name != "")
                {
                    Return2290_2017.ReturnHeaderTypeBusinessOfficerGrp objOfficer = new Return2290_2017.ReturnHeaderTypeBusinessOfficerGrp();

                    objOfficer.PersonNm = Officer_Name;
                    objOfficer.PersonTitleTxt = Officer_Title;

                    if (Officer_PIN.Length > 0)
                        objOfficer.TaxpayerPIN = Officer_PIN;

                    if (Officer_Phone.Length > 0)
                    {
                        objOfficer.Item = Officer_Phone;
                        if (Filer_Foreign_Country == "US" || Filer_Foreign_Country == "")
                            objOfficer.ItemElementName = Return2290_2017.ItemChoiceType.PhoneNum;
                        else
                            objOfficer.ItemElementName = Return2290_2017.ItemChoiceType.ForeignPhoneNum;
                    }

                    if (Officer_email.Length > 0)
                        objOfficer.EmailAddressTxt = Officer_email;

                    objOfficer.SignatureDt = DateTime.Now.ToUniversalTime();

                    returnHeaderType.BusinessOfficerGrp = objOfficer;
                }
                #endregion

                #region THIRD PARTY DESIGNEE

                Return2290_2017.ReturnHeaderTypeThirdPartyDesignee objThirdPrtyDesignee = new Return2290_2017.ReturnHeaderTypeThirdPartyDesignee();

                object[] itemsField = new object[4];
                Return2290_2017.ItemsChoiceType[] itemsElementNameField = new Return2290_2017.ItemsChoiceType[4];

                itemsField[0] = Return2290_2017.CheckboxType.X;

                if (ThirdPartyDesigne_Name != "")
                {
                    itemsField[1] = ThirdPartyDesigne_Name;
                    itemsField[2] = ThirdPartyDesigne_Phone;
                    itemsField[3] = ThirdPartyDesigne_PIN;

                    itemsElementNameField[0] = Return2290_2017.ItemsChoiceType.DiscussWithThirdPartyYesInd;
                    itemsElementNameField[1] = Return2290_2017.ItemsChoiceType.ThirdPartyDesigneeNm;
                    itemsElementNameField[2] = Return2290_2017.ItemsChoiceType.ThirdPartyDesigneePhoneNum;
                    itemsElementNameField[3] = Return2290_2017.ItemsChoiceType.ThirdPartyDesigneePIN;
                }
                else
                {
                    itemsElementNameField[0] = Return2290_2017.ItemsChoiceType.DiscussWithThirdPartyNoInd;
                }
                objThirdPrtyDesignee.Items = itemsField;
                objThirdPrtyDesignee.ItemsElementName = itemsElementNameField;

                returnHeaderType.ThirdPartyDesignee = objThirdPrtyDesignee;

                #endregion

                #region PREPARER

                if (Preparer_Name != "")
                {
                    Return2290_2017.ReturnHeaderTypePreparerFirmGrp objPreparerFirm = new Return2290_2017.ReturnHeaderTypePreparerFirmGrp();
                    Return2290_2017.ReturnHeaderTypePreparerPersonGrp objPreparer = new Return2290_2017.ReturnHeaderTypePreparerPersonGrp();

                    objPreparer.PreparerPersonNm = Preparer_Name;

                    if (IsPreparerHasPTIN)
                        objPreparer.PTIN = Preparer_PTIN_SSN;

                    if (Preparer_Phone.Length > 0)
                    {
                        objPreparer.Item = Preparer_Phone;
                        objPreparer.ItemElementName = Return2290_2017.ItemChoiceType1.PhoneNum;
                    }
                    else if (Preparer_ForeignPhone.Length > 0)
                    {
                        objPreparer.Item = Preparer_Phone;
                        objPreparer.ItemElementName = Return2290_2017.ItemChoiceType1.ForeignPhoneNum;
                    }
                    objPreparer.EmailAddressTxt = Preparer_Email;
                    objPreparer.PreparationDt = DateTime.Now.ToUniversalTime();
                    objPreparer.PreparationDtSpecified = true;

                    if (IsPreparerSelfEmployed)
                    {
                        objPreparer.SelfEmployedInd = Return2290_2017.CheckboxType.X;
                        objPreparer.SelfEmployedIndSpecified = IsPreparerSelfEmployed;
                    }

                    returnHeaderType.PreparerPersonGrp = objPreparer;
                }

                if (!IsPreparerSelfEmployed && Preparer_Name != "")
                {
                    Return2290_2017.ReturnHeaderTypePreparerFirmGrp objPreparerFirm = new Return2290_2017.ReturnHeaderTypePreparerFirmGrp();
                    Return2290_2017.BusinessNameType PreparerBusinessName = new Return2290_2017.BusinessNameType();
                    PreparerBusinessName.BusinessNameLine1Txt = Preparer_Firm;

                    objPreparerFirm.PreparerFirmEIN = Preparer_EIN;
                    objPreparerFirm.PreparerFirmName = PreparerBusinessName;
                    if (Preparer_Country == "" || Preparer_Country == "US")
                    {
                        #region Preparer US ADDRESS

                        Return2290_2017.USAddressType usAdd_Preparer = new Return2290_2017.USAddressType();

                        if (Preparer_Address.Length > 35)
                        {
                            string Preparer_AddressLine2 = string.Empty;
                            usAdd_Preparer.AddressLine1 = Preparer_Address.Substring(0, 35).Trim();
                            Preparer_AddressLine2 = Preparer_Address.Substring(35);
                            usAdd_Preparer.AddressLine2 = Preparer_AddressLine2.Trim();
                        }
                        else
                        {
                            usAdd_Preparer.AddressLine1 = Preparer_Address.Trim();
                        }

                        usAdd_Preparer.City = Preparer_City.Trim();

                        switch (Preparer_State)
                        {
                            #region Filer_State
                            case "AA": usAdd_Preparer.State = Return2290_2017.StateType.AA; break;
                            case "AE": usAdd_Preparer.State = Return2290_2017.StateType.AE; break;
                            case "AK": usAdd_Preparer.State = Return2290_2017.StateType.AK; break;
                            case "AL": usAdd_Preparer.State = Return2290_2017.StateType.AL; break;
                            case "AP": usAdd_Preparer.State = Return2290_2017.StateType.AP; break;
                            case "AR": usAdd_Preparer.State = Return2290_2017.StateType.AR; break;
                            case "AS": usAdd_Preparer.State = Return2290_2017.StateType.AS; break;
                            case "AZ": usAdd_Preparer.State = Return2290_2017.StateType.AZ; break;
                            case "CA": usAdd_Preparer.State = Return2290_2017.StateType.CA; break;
                            case "CO": usAdd_Preparer.State = Return2290_2017.StateType.CO; break;
                            case "CT": usAdd_Preparer.State = Return2290_2017.StateType.CT; break;
                            case "DC": usAdd_Preparer.State = Return2290_2017.StateType.DC; break;
                            case "DE": usAdd_Preparer.State = Return2290_2017.StateType.DE; break;
                            case "FL": usAdd_Preparer.State = Return2290_2017.StateType.FL; break;
                            case "FM": usAdd_Preparer.State = Return2290_2017.StateType.FM; break;
                            case "GA": usAdd_Preparer.State = Return2290_2017.StateType.GA; break;
                            case "GU": usAdd_Preparer.State = Return2290_2017.StateType.GU; break;
                            case "HI": usAdd_Preparer.State = Return2290_2017.StateType.HI; break;
                            case "IA": usAdd_Preparer.State = Return2290_2017.StateType.IA; break;
                            case "ID": usAdd_Preparer.State = Return2290_2017.StateType.ID; break;
                            case "IL": usAdd_Preparer.State = Return2290_2017.StateType.IL; break;
                            case "IN": usAdd_Preparer.State = Return2290_2017.StateType.IN; break;
                            case "KS": usAdd_Preparer.State = Return2290_2017.StateType.KS; break;
                            case "KY": usAdd_Preparer.State = Return2290_2017.StateType.KY; break;
                            case "LA": usAdd_Preparer.State = Return2290_2017.StateType.LA; break;
                            case "MA": usAdd_Preparer.State = Return2290_2017.StateType.MA; break;
                            case "MD": usAdd_Preparer.State = Return2290_2017.StateType.MD; break;
                            case "ME": usAdd_Preparer.State = Return2290_2017.StateType.ME; break;
                            case "MH": usAdd_Preparer.State = Return2290_2017.StateType.MH; break;
                            case "MI": usAdd_Preparer.State = Return2290_2017.StateType.MI; break;
                            case "MN": usAdd_Preparer.State = Return2290_2017.StateType.MN; break;
                            case "MO": usAdd_Preparer.State = Return2290_2017.StateType.MO; break;
                            case "MP": usAdd_Preparer.State = Return2290_2017.StateType.MP; break;
                            case "MS": usAdd_Preparer.State = Return2290_2017.StateType.MS; break;
                            case "MT": usAdd_Preparer.State = Return2290_2017.StateType.MT; break;
                            case "NC": usAdd_Preparer.State = Return2290_2017.StateType.NC; break;
                            case "ND": usAdd_Preparer.State = Return2290_2017.StateType.ND; break;
                            case "NE": usAdd_Preparer.State = Return2290_2017.StateType.NE; break;
                            case "NH": usAdd_Preparer.State = Return2290_2017.StateType.NH; break;
                            case "NJ": usAdd_Preparer.State = Return2290_2017.StateType.NJ; break;
                            case "NM": usAdd_Preparer.State = Return2290_2017.StateType.NM; break;
                            case "NV": usAdd_Preparer.State = Return2290_2017.StateType.NV; break;
                            case "NY": usAdd_Preparer.State = Return2290_2017.StateType.NY; break;
                            case "OH": usAdd_Preparer.State = Return2290_2017.StateType.OH; break;
                            case "OK": usAdd_Preparer.State = Return2290_2017.StateType.OK; break;
                            case "OR": usAdd_Preparer.State = Return2290_2017.StateType.OR; break;
                            case "PA": usAdd_Preparer.State = Return2290_2017.StateType.PA; break;
                            case "PR": usAdd_Preparer.State = Return2290_2017.StateType.PR; break;
                            case "PW": usAdd_Preparer.State = Return2290_2017.StateType.PW; break;
                            case "RI": usAdd_Preparer.State = Return2290_2017.StateType.RI; break;
                            case "SC": usAdd_Preparer.State = Return2290_2017.StateType.SC; break;
                            case "SD": usAdd_Preparer.State = Return2290_2017.StateType.SD; break;
                            case "TN": usAdd_Preparer.State = Return2290_2017.StateType.TN; break;
                            case "TX": usAdd_Preparer.State = Return2290_2017.StateType.TX; break;
                            case "UT": usAdd_Preparer.State = Return2290_2017.StateType.UT; break;
                            case "VA": usAdd_Preparer.State = Return2290_2017.StateType.VA; break;
                            case "VI": usAdd_Preparer.State = Return2290_2017.StateType.VI; break;
                            case "VT": usAdd_Preparer.State = Return2290_2017.StateType.VT; break;
                            case "WA": usAdd_Preparer.State = Return2290_2017.StateType.WA; break;
                            case "WI": usAdd_Preparer.State = Return2290_2017.StateType.WI; break;
                            case "WV": usAdd_Preparer.State = Return2290_2017.StateType.WV; break;
                            case "WY": usAdd_Preparer.State = Return2290_2017.StateType.WY; break;
                            #endregion
                        }

                        usAdd_Preparer.ZIPCode = Preparer_Zip;

                        objPreparerFirm.Item = usAdd_Preparer;

                        #endregion
                    }
                    else
                    {
                        #region Preparer FOREIGN ADDRESS
                        Return2290_2017.ForeignAddressType foreignAdd_Preparer = new Return2290_2017.ForeignAddressType();

                        if (Preparer_Address.Length > 35)
                        {
                            string Preparer_AddressLine2 = string.Empty;
                            foreignAdd_Preparer.AddressLine1 = Preparer_Address.Substring(0, 35).Trim();
                            Preparer_AddressLine2 = Preparer_Address.Substring(35);
                            foreignAdd_Preparer.AddressLine2 = Preparer_AddressLine2.Trim();

                        }
                        else
                        {
                            foreignAdd_Preparer.AddressLine1 = Preparer_Address.Trim();
                        }
                        foreignAdd_Preparer.City = Preparer_City.Trim();

                        if (Preparer_State.Length > 0)
                            foreignAdd_Preparer.ProvinceOrState = Preparer_State;

                        switch (Preparer_Country)
                        {
                            #region Preparer_Foreign_Country
                            case "AA": foreignAdd_Preparer.Country = Return2290_2017.CountryType.AA; break;
                            case "AC": foreignAdd_Preparer.Country = Return2290_2017.CountryType.AC; break;
                            case "AE": foreignAdd_Preparer.Country = Return2290_2017.CountryType.AE; break;
                            case "AF": foreignAdd_Preparer.Country = Return2290_2017.CountryType.AF; break;
                            case "AG": foreignAdd_Preparer.Country = Return2290_2017.CountryType.AG; break;
                            case "AJ": foreignAdd_Preparer.Country = Return2290_2017.CountryType.AJ; break;
                            case "AL": foreignAdd_Preparer.Country = Return2290_2017.CountryType.AL; break;
                            case "AM": foreignAdd_Preparer.Country = Return2290_2017.CountryType.AM; break;
                            case "AN": foreignAdd_Preparer.Country = Return2290_2017.CountryType.AN; break;
                            case "AO": foreignAdd_Preparer.Country = Return2290_2017.CountryType.AO; break;
                            case "AR": foreignAdd_Preparer.Country = Return2290_2017.CountryType.AR; break;
                            case "AS": foreignAdd_Preparer.Country = Return2290_2017.CountryType.AS; break;
                            case "AT": foreignAdd_Preparer.Country = Return2290_2017.CountryType.AT; break;
                            case "AU": foreignAdd_Preparer.Country = Return2290_2017.CountryType.AU; break;
                            case "AV": foreignAdd_Preparer.Country = Return2290_2017.CountryType.AV; break;
                            case "AX": foreignAdd_Preparer.Country = Return2290_2017.CountryType.AX; break;
                            case "AY": foreignAdd_Preparer.Country = Return2290_2017.CountryType.AY; break;
                            case "BA": foreignAdd_Preparer.Country = Return2290_2017.CountryType.BA; break;
                            case "BB": foreignAdd_Preparer.Country = Return2290_2017.CountryType.BB; break;
                            case "BC": foreignAdd_Preparer.Country = Return2290_2017.CountryType.BC; break;
                            case "BD": foreignAdd_Preparer.Country = Return2290_2017.CountryType.BD; break;
                            case "BE": foreignAdd_Preparer.Country = Return2290_2017.CountryType.BE; break;
                            case "BF": foreignAdd_Preparer.Country = Return2290_2017.CountryType.BF; break;
                            case "BG": foreignAdd_Preparer.Country = Return2290_2017.CountryType.BG; break;
                            case "BH": foreignAdd_Preparer.Country = Return2290_2017.CountryType.BH; break;
                            case "BK": foreignAdd_Preparer.Country = Return2290_2017.CountryType.BK; break;
                            case "BL": foreignAdd_Preparer.Country = Return2290_2017.CountryType.BL; break;
                            case "BM": foreignAdd_Preparer.Country = Return2290_2017.CountryType.BM; break;
                            case "BN": foreignAdd_Preparer.Country = Return2290_2017.CountryType.BN; break;
                            case "BO": foreignAdd_Preparer.Country = Return2290_2017.CountryType.BO; break;
                            case "BP": foreignAdd_Preparer.Country = Return2290_2017.CountryType.BP; break;
                            case "BQ": foreignAdd_Preparer.Country = Return2290_2017.CountryType.BQ; break;
                            case "BR": foreignAdd_Preparer.Country = Return2290_2017.CountryType.BR; break;
                            case "BT": foreignAdd_Preparer.Country = Return2290_2017.CountryType.BT; break;
                            case "BU": foreignAdd_Preparer.Country = Return2290_2017.CountryType.BU; break;
                            case "BV": foreignAdd_Preparer.Country = Return2290_2017.CountryType.BV; break;
                            case "BX": foreignAdd_Preparer.Country = Return2290_2017.CountryType.BX; break;
                            case "BY": foreignAdd_Preparer.Country = Return2290_2017.CountryType.BY; break;
                            case "CA": foreignAdd_Preparer.Country = Return2290_2017.CountryType.CA; break;
                            case "CB": foreignAdd_Preparer.Country = Return2290_2017.CountryType.CB; break;
                            case "CD": foreignAdd_Preparer.Country = Return2290_2017.CountryType.CD; break;
                            case "CE": foreignAdd_Preparer.Country = Return2290_2017.CountryType.CE; break;
                            case "CF": foreignAdd_Preparer.Country = Return2290_2017.CountryType.CF; break;
                            case "CG": foreignAdd_Preparer.Country = Return2290_2017.CountryType.CG; break;
                            case "CH": foreignAdd_Preparer.Country = Return2290_2017.CountryType.CH; break;
                            case "CI": foreignAdd_Preparer.Country = Return2290_2017.CountryType.CI; break;
                            case "CJ": foreignAdd_Preparer.Country = Return2290_2017.CountryType.CJ; break;
                            case "CK": foreignAdd_Preparer.Country = Return2290_2017.CountryType.CK; break;
                            case "CM": foreignAdd_Preparer.Country = Return2290_2017.CountryType.CM; break;
                            case "CN": foreignAdd_Preparer.Country = Return2290_2017.CountryType.CN; break;
                            case "CO": foreignAdd_Preparer.Country = Return2290_2017.CountryType.CO; break;
                            case "CR": foreignAdd_Preparer.Country = Return2290_2017.CountryType.CR; break;
                            case "CS": foreignAdd_Preparer.Country = Return2290_2017.CountryType.CS; break;
                            case "CT": foreignAdd_Preparer.Country = Return2290_2017.CountryType.CT; break;
                            case "CU": foreignAdd_Preparer.Country = Return2290_2017.CountryType.CU; break;
                            case "CV": foreignAdd_Preparer.Country = Return2290_2017.CountryType.CV; break;
                            case "CW": foreignAdd_Preparer.Country = Return2290_2017.CountryType.CW; break;
                            case "CY": foreignAdd_Preparer.Country = Return2290_2017.CountryType.CY; break;
                            case "DA": foreignAdd_Preparer.Country = Return2290_2017.CountryType.DA; break;
                            case "DJ": foreignAdd_Preparer.Country = Return2290_2017.CountryType.DJ; break;
                            case "DO": foreignAdd_Preparer.Country = Return2290_2017.CountryType.DO; break;
                            case "DR": foreignAdd_Preparer.Country = Return2290_2017.CountryType.DR; break;
                            case "DX": foreignAdd_Preparer.Country = Return2290_2017.CountryType.DX; break;
                            case "EC": foreignAdd_Preparer.Country = Return2290_2017.CountryType.EC; break;
                            case "EG": foreignAdd_Preparer.Country = Return2290_2017.CountryType.EG; break;
                            case "EI": foreignAdd_Preparer.Country = Return2290_2017.CountryType.EI; break;
                            case "EK": foreignAdd_Preparer.Country = Return2290_2017.CountryType.EK; break;
                            case "EN": foreignAdd_Preparer.Country = Return2290_2017.CountryType.EN; break;
                            case "ER": foreignAdd_Preparer.Country = Return2290_2017.CountryType.ER; break;
                            case "ES": foreignAdd_Preparer.Country = Return2290_2017.CountryType.ES; break;
                            case "ET": foreignAdd_Preparer.Country = Return2290_2017.CountryType.ET; break;
                            case "EZ": foreignAdd_Preparer.Country = Return2290_2017.CountryType.EZ; break;
                            case "FI": foreignAdd_Preparer.Country = Return2290_2017.CountryType.FI; break;
                            case "FJ": foreignAdd_Preparer.Country = Return2290_2017.CountryType.FJ; break;
                            case "FK": foreignAdd_Preparer.Country = Return2290_2017.CountryType.FK; break;
                            case "FO": foreignAdd_Preparer.Country = Return2290_2017.CountryType.FO; break;
                            case "FP": foreignAdd_Preparer.Country = Return2290_2017.CountryType.FP; break;
                            case "FQ": foreignAdd_Preparer.Country = Return2290_2017.CountryType.FQ; break;
                            case "FR": foreignAdd_Preparer.Country = Return2290_2017.CountryType.FR; break;
                            case "FS": foreignAdd_Preparer.Country = Return2290_2017.CountryType.FS; break;
                            case "GA": foreignAdd_Preparer.Country = Return2290_2017.CountryType.GA; break;
                            case "GB": foreignAdd_Preparer.Country = Return2290_2017.CountryType.GB; break;
                            case "GG": foreignAdd_Preparer.Country = Return2290_2017.CountryType.GG; break;
                            case "GH": foreignAdd_Preparer.Country = Return2290_2017.CountryType.GH; break;
                            case "GI": foreignAdd_Preparer.Country = Return2290_2017.CountryType.GI; break;
                            case "GJ": foreignAdd_Preparer.Country = Return2290_2017.CountryType.GJ; break;
                            case "GK": foreignAdd_Preparer.Country = Return2290_2017.CountryType.GK; break;
                            case "GL": foreignAdd_Preparer.Country = Return2290_2017.CountryType.GL; break;
                            case "GM": foreignAdd_Preparer.Country = Return2290_2017.CountryType.GM; break;
                            case "GR": foreignAdd_Preparer.Country = Return2290_2017.CountryType.GR; break;
                            case "GT": foreignAdd_Preparer.Country = Return2290_2017.CountryType.GT; break;
                            case "GV": foreignAdd_Preparer.Country = Return2290_2017.CountryType.GV; break;
                            case "GY": foreignAdd_Preparer.Country = Return2290_2017.CountryType.GY; break;
                            case "HA": foreignAdd_Preparer.Country = Return2290_2017.CountryType.HA; break;
                            case "HK": foreignAdd_Preparer.Country = Return2290_2017.CountryType.HK; break;
                            case "HM": foreignAdd_Preparer.Country = Return2290_2017.CountryType.HM; break;
                            case "HO": foreignAdd_Preparer.Country = Return2290_2017.CountryType.HO; break;
                            case "HQ": foreignAdd_Preparer.Country = Return2290_2017.CountryType.HQ; break;
                            case "HR": foreignAdd_Preparer.Country = Return2290_2017.CountryType.HR; break;
                            case "HU": foreignAdd_Preparer.Country = Return2290_2017.CountryType.HU; break;
                            case "IC": foreignAdd_Preparer.Country = Return2290_2017.CountryType.IC; break;
                            case "ID": foreignAdd_Preparer.Country = Return2290_2017.CountryType.ID; break;
                            case "IM": foreignAdd_Preparer.Country = Return2290_2017.CountryType.IM; break;
                            case "IO": foreignAdd_Preparer.Country = Return2290_2017.CountryType.IO; break;
                            case "IP": foreignAdd_Preparer.Country = Return2290_2017.CountryType.IP; break;
                            case "IR": foreignAdd_Preparer.Country = Return2290_2017.CountryType.IR; break;
                            case "IS": foreignAdd_Preparer.Country = Return2290_2017.CountryType.IS; break;
                            case "IT": foreignAdd_Preparer.Country = Return2290_2017.CountryType.IT; break;
                            case "IV": foreignAdd_Preparer.Country = Return2290_2017.CountryType.IV; break;
                            case "IZ": foreignAdd_Preparer.Country = Return2290_2017.CountryType.IZ; break;
                            case "JA": foreignAdd_Preparer.Country = Return2290_2017.CountryType.JA; break;
                            case "JE": foreignAdd_Preparer.Country = Return2290_2017.CountryType.JE; break;
                            case "JM": foreignAdd_Preparer.Country = Return2290_2017.CountryType.JM; break;
                            case "JN": foreignAdd_Preparer.Country = Return2290_2017.CountryType.JN; break;
                            case "JO": foreignAdd_Preparer.Country = Return2290_2017.CountryType.JO; break;
                            case "JQ": foreignAdd_Preparer.Country = Return2290_2017.CountryType.JQ; break;
                            case "KE": foreignAdd_Preparer.Country = Return2290_2017.CountryType.KE; break;
                            case "KG": foreignAdd_Preparer.Country = Return2290_2017.CountryType.KG; break;
                            case "KN": foreignAdd_Preparer.Country = Return2290_2017.CountryType.KN; break;
                            case "KQ": foreignAdd_Preparer.Country = Return2290_2017.CountryType.KQ; break;
                            case "KR": foreignAdd_Preparer.Country = Return2290_2017.CountryType.KR; break;
                            case "KS": foreignAdd_Preparer.Country = Return2290_2017.CountryType.KS; break;
                            case "KT": foreignAdd_Preparer.Country = Return2290_2017.CountryType.KT; break;
                            case "KU": foreignAdd_Preparer.Country = Return2290_2017.CountryType.KU; break;
                            case "KZ": foreignAdd_Preparer.Country = Return2290_2017.CountryType.KZ; break;
                            case "LA": foreignAdd_Preparer.Country = Return2290_2017.CountryType.LA; break;
                            case "LE": foreignAdd_Preparer.Country = Return2290_2017.CountryType.LE; break;
                            case "LG": foreignAdd_Preparer.Country = Return2290_2017.CountryType.LG; break;
                            case "LH": foreignAdd_Preparer.Country = Return2290_2017.CountryType.LH; break;
                            case "LI": foreignAdd_Preparer.Country = Return2290_2017.CountryType.LI; break;
                            case "LO": foreignAdd_Preparer.Country = Return2290_2017.CountryType.LO; break;
                            case "LQ": foreignAdd_Preparer.Country = Return2290_2017.CountryType.LQ; break;
                            case "LS": foreignAdd_Preparer.Country = Return2290_2017.CountryType.LS; break;
                            case "LT": foreignAdd_Preparer.Country = Return2290_2017.CountryType.LT; break;
                            case "LU": foreignAdd_Preparer.Country = Return2290_2017.CountryType.LU; break;
                            case "LY": foreignAdd_Preparer.Country = Return2290_2017.CountryType.LY; break;
                            case "MA": foreignAdd_Preparer.Country = Return2290_2017.CountryType.MA; break;
                            case "MC": foreignAdd_Preparer.Country = Return2290_2017.CountryType.MC; break;
                            case "MD": foreignAdd_Preparer.Country = Return2290_2017.CountryType.MD; break;
                            case "MG": foreignAdd_Preparer.Country = Return2290_2017.CountryType.MG; break;
                            case "MH": foreignAdd_Preparer.Country = Return2290_2017.CountryType.MH; break;
                            case "MI": foreignAdd_Preparer.Country = Return2290_2017.CountryType.MI; break;
                            case "MJ": foreignAdd_Preparer.Country = Return2290_2017.CountryType.MJ; break;
                            case "MK": foreignAdd_Preparer.Country = Return2290_2017.CountryType.MK; break;
                            case "ML": foreignAdd_Preparer.Country = Return2290_2017.CountryType.ML; break;
                            case "MN": foreignAdd_Preparer.Country = Return2290_2017.CountryType.MN; break;
                            case "MO": foreignAdd_Preparer.Country = Return2290_2017.CountryType.MO; break;
                            case "MP": foreignAdd_Preparer.Country = Return2290_2017.CountryType.MP; break;
                            case "MQ": foreignAdd_Preparer.Country = Return2290_2017.CountryType.MQ; break;
                            case "MR": foreignAdd_Preparer.Country = Return2290_2017.CountryType.MR; break;
                            case "MT": foreignAdd_Preparer.Country = Return2290_2017.CountryType.MT; break;
                            case "MU": foreignAdd_Preparer.Country = Return2290_2017.CountryType.MU; break;
                            case "MV": foreignAdd_Preparer.Country = Return2290_2017.CountryType.MV; break;
                            case "MX": foreignAdd_Preparer.Country = Return2290_2017.CountryType.MX; break;
                            case "MY": foreignAdd_Preparer.Country = Return2290_2017.CountryType.MY; break;
                            case "MZ": foreignAdd_Preparer.Country = Return2290_2017.CountryType.MZ; break;
                            case "NC": foreignAdd_Preparer.Country = Return2290_2017.CountryType.NC; break;
                            case "NE": foreignAdd_Preparer.Country = Return2290_2017.CountryType.NE; break;
                            case "NF": foreignAdd_Preparer.Country = Return2290_2017.CountryType.NF; break;
                            case "NG": foreignAdd_Preparer.Country = Return2290_2017.CountryType.NG; break;
                            case "NH": foreignAdd_Preparer.Country = Return2290_2017.CountryType.NH; break;
                            case "NI": foreignAdd_Preparer.Country = Return2290_2017.CountryType.NI; break;
                            case "NL": foreignAdd_Preparer.Country = Return2290_2017.CountryType.NL; break;
                            case "NO": foreignAdd_Preparer.Country = Return2290_2017.CountryType.NO; break;
                            case "NP": foreignAdd_Preparer.Country = Return2290_2017.CountryType.NP; break;
                            case "NR": foreignAdd_Preparer.Country = Return2290_2017.CountryType.NR; break;
                            case "NS": foreignAdd_Preparer.Country = Return2290_2017.CountryType.NS; break;
                            case "NU": foreignAdd_Preparer.Country = Return2290_2017.CountryType.NU; break;
                            case "NZ": foreignAdd_Preparer.Country = Return2290_2017.CountryType.NZ; break;
                            case "PA": foreignAdd_Preparer.Country = Return2290_2017.CountryType.PA; break;
                            case "PC": foreignAdd_Preparer.Country = Return2290_2017.CountryType.PC; break;
                            case "PE": foreignAdd_Preparer.Country = Return2290_2017.CountryType.PE; break;
                            case "PF": foreignAdd_Preparer.Country = Return2290_2017.CountryType.PF; break;
                            case "PG": foreignAdd_Preparer.Country = Return2290_2017.CountryType.PG; break;
                            case "PK": foreignAdd_Preparer.Country = Return2290_2017.CountryType.PK; break;
                            case "PL": foreignAdd_Preparer.Country = Return2290_2017.CountryType.PL; break;
                            case "PM": foreignAdd_Preparer.Country = Return2290_2017.CountryType.PM; break;
                            case "PO": foreignAdd_Preparer.Country = Return2290_2017.CountryType.PO; break;
                            case "PP": foreignAdd_Preparer.Country = Return2290_2017.CountryType.PP; break;
                            case "PU": foreignAdd_Preparer.Country = Return2290_2017.CountryType.PU; break;
                            case "QA": foreignAdd_Preparer.Country = Return2290_2017.CountryType.QA; break;
                            case "RO": foreignAdd_Preparer.Country = Return2290_2017.CountryType.RO; break;
                            case "RP": foreignAdd_Preparer.Country = Return2290_2017.CountryType.RP; break;
                            case "RS": foreignAdd_Preparer.Country = Return2290_2017.CountryType.RS; break;
                            case "RW": foreignAdd_Preparer.Country = Return2290_2017.CountryType.RW; break;
                            case "SA": foreignAdd_Preparer.Country = Return2290_2017.CountryType.SA; break;
                            case "SB": foreignAdd_Preparer.Country = Return2290_2017.CountryType.SB; break;
                            case "SC": foreignAdd_Preparer.Country = Return2290_2017.CountryType.SC; break;
                            case "SE": foreignAdd_Preparer.Country = Return2290_2017.CountryType.SE; break;
                            case "SF": foreignAdd_Preparer.Country = Return2290_2017.CountryType.SF; break;
                            case "SG": foreignAdd_Preparer.Country = Return2290_2017.CountryType.SG; break;
                            case "SH": foreignAdd_Preparer.Country = Return2290_2017.CountryType.SH; break;
                            case "SI": foreignAdd_Preparer.Country = Return2290_2017.CountryType.SI; break;
                            case "SL": foreignAdd_Preparer.Country = Return2290_2017.CountryType.SL; break;
                            case "SM": foreignAdd_Preparer.Country = Return2290_2017.CountryType.SM; break;
                            case "SN": foreignAdd_Preparer.Country = Return2290_2017.CountryType.SN; break;
                            case "SO": foreignAdd_Preparer.Country = Return2290_2017.CountryType.SO; break;
                            case "SP": foreignAdd_Preparer.Country = Return2290_2017.CountryType.SP; break;
                            case "ST": foreignAdd_Preparer.Country = Return2290_2017.CountryType.ST; break;
                            case "SU": foreignAdd_Preparer.Country = Return2290_2017.CountryType.SU; break;
                            case "SV": foreignAdd_Preparer.Country = Return2290_2017.CountryType.SV; break;
                            case "SW": foreignAdd_Preparer.Country = Return2290_2017.CountryType.SW; break;
                            case "SX": foreignAdd_Preparer.Country = Return2290_2017.CountryType.SX; break;
                            case "SY": foreignAdd_Preparer.Country = Return2290_2017.CountryType.SY; break;
                            case "SZ": foreignAdd_Preparer.Country = Return2290_2017.CountryType.SZ; break;
                            case "TD": foreignAdd_Preparer.Country = Return2290_2017.CountryType.TD; break;
                            case "TH": foreignAdd_Preparer.Country = Return2290_2017.CountryType.TH; break;
                            case "TI": foreignAdd_Preparer.Country = Return2290_2017.CountryType.TI; break;
                            case "TK": foreignAdd_Preparer.Country = Return2290_2017.CountryType.TK; break;
                            case "TL": foreignAdd_Preparer.Country = Return2290_2017.CountryType.TL; break;
                            case "TN": foreignAdd_Preparer.Country = Return2290_2017.CountryType.TN; break;
                            case "TO": foreignAdd_Preparer.Country = Return2290_2017.CountryType.TO; break;
                            case "TP": foreignAdd_Preparer.Country = Return2290_2017.CountryType.TP; break;
                            case "TS": foreignAdd_Preparer.Country = Return2290_2017.CountryType.TS; break;
                            case "TT": foreignAdd_Preparer.Country = Return2290_2017.CountryType.TT; break;
                            case "TU": foreignAdd_Preparer.Country = Return2290_2017.CountryType.TU; break;
                            case "TV": foreignAdd_Preparer.Country = Return2290_2017.CountryType.TV; break;
                            case "TW": foreignAdd_Preparer.Country = Return2290_2017.CountryType.TW; break;
                            case "TX": foreignAdd_Preparer.Country = Return2290_2017.CountryType.TX; break;
                            case "TZ": foreignAdd_Preparer.Country = Return2290_2017.CountryType.TZ; break;
                            case "UG": foreignAdd_Preparer.Country = Return2290_2017.CountryType.UG; break;
                            case "UK": foreignAdd_Preparer.Country = Return2290_2017.CountryType.UK; break;
                            case "UP": foreignAdd_Preparer.Country = Return2290_2017.CountryType.UP; break;
                            case "UV": foreignAdd_Preparer.Country = Return2290_2017.CountryType.UV; break;
                            case "UY": foreignAdd_Preparer.Country = Return2290_2017.CountryType.UY; break;
                            case "UZ": foreignAdd_Preparer.Country = Return2290_2017.CountryType.UZ; break;
                            case "VC": foreignAdd_Preparer.Country = Return2290_2017.CountryType.VC; break;
                            case "VE": foreignAdd_Preparer.Country = Return2290_2017.CountryType.VE; break;
                            case "VI": foreignAdd_Preparer.Country = Return2290_2017.CountryType.VI; break;
                            case "VM": foreignAdd_Preparer.Country = Return2290_2017.CountryType.VM; break;
                            case "VT": foreignAdd_Preparer.Country = Return2290_2017.CountryType.VT; break;
                            case "WA": foreignAdd_Preparer.Country = Return2290_2017.CountryType.WA; break;
                            case "WF": foreignAdd_Preparer.Country = Return2290_2017.CountryType.WF; break;
                            case "WI": foreignAdd_Preparer.Country = Return2290_2017.CountryType.WI; break;
                            case "WQ": foreignAdd_Preparer.Country = Return2290_2017.CountryType.WQ; break;
                            case "WS": foreignAdd_Preparer.Country = Return2290_2017.CountryType.WS; break;
                            case "WZ": foreignAdd_Preparer.Country = Return2290_2017.CountryType.WZ; break;
                            case "YM": foreignAdd_Preparer.Country = Return2290_2017.CountryType.YM; break;
                            case "ZA": foreignAdd_Preparer.Country = Return2290_2017.CountryType.ZA; break;
                            case "ZI": foreignAdd_Preparer.Country = Return2290_2017.CountryType.ZI; break;
                            #endregion
                        }

                        if (Filer_ZIPCode.Length > 0)
                            foreignAdd_Preparer.PostalCode = Filer_ZIPCode;

                        objFiler.Item = foreignAdd_Preparer;
                        #endregion
                    }

                    returnHeaderType.PreparerFirmGrp = objPreparerFirm;
                }
                #endregion

                #region CONSENT_TO_VIN_DATA_DISCLOSURE

                object[] itemConsent = new object[4];
                Return2290_2017.ItemsChoiceType1[] itemElements = new Return2290_2017.ItemsChoiceType1[4];

                itemConsent[0] = Return2290_2017.CheckboxType.X;

                if (IsConsentToDiscloseYes)
                {
                    itemElements[0] = Return2290_2017.ItemsChoiceType1.ConsentToDiscloseYesInd;

                    Return2290_2017.ReturnHeaderTypeConsentToVINDataDisclosureGrpDisclosureFormSignatureInfo objDisclosureFormSignInfo =
                        new Return2290_2017.ReturnHeaderTypeConsentToVINDataDisclosureGrpDisclosureFormSignatureInfo();
                    objDisclosureFormSignInfo.EIN = Filer_EIN;
                    Return2290_2017.BusinessNameType disclosureFormSignInfo_business = new Return2290_2017.BusinessNameType();
                    disclosureFormSignInfo_business.BusinessNameLine1Txt = Filer_Name;
                    objDisclosureFormSignInfo.BusinessName = disclosureFormSignInfo_business;
                    objDisclosureFormSignInfo.SignatureDt = DateTime.Now.ToUniversalTime();

                    itemConsent[1] = (objDisclosureFormSignInfo);
                    itemElements[1] = (Return2290_2017.ItemsChoiceType1.DisclosureFormSignatureInfo);

                    itemConsent[2] = (Return2290_2017.ReturnHeaderTypeConsentToVINDataDisclosureGrpSignatureOptionCd.PINNumber);
                    itemElements[2] = (Return2290_2017.ItemsChoiceType1.SignatureOptionCd);

                    itemConsent[3] = (Officer_PIN);
                    itemElements[3] = (Return2290_2017.ItemsChoiceType1.PIN);
                }
                else
                {
                    itemElements[0] = (Return2290_2017.ItemsChoiceType1.ConsentToDiscloseNoInd);
                }

                Return2290_2017.ReturnHeaderTypeConsentToVINDataDisclosureGrp objConsent2DataDisclosure =
                    new Return2290_2017.ReturnHeaderTypeConsentToVINDataDisclosureGrp();
                objConsent2DataDisclosure.Items = itemConsent;
                objConsent2DataDisclosure.ItemsElementName = itemElements;

                returnHeaderType.ConsentToVINDataDisclosureGrp = objConsent2DataDisclosure;

                #endregion

                returnHeaderType.TaxYr = TaxYear;

                returnHeaderType.binaryAttachmentCnt = "0";

                returnHeaderType.SoftwareId = Variables.SoftwareID;
                returnHeaderType.SoftwareVersionNum = Variables.SoftwareVersion;
                #endregion

                #region RETURN DATA

                Return2290_2017.ReturnData returnData = new Return2290_2017.ReturnData();

                #region IRS2290

                Return2290_2017.IRS2290 objIRS2290 = new Return2290_2017.IRS2290();

                #region TAX COMPUTATION
               
                if (dsTaxComputation.Tables.Count > 0)
                {
                    if (dsTaxComputation.Tables[0].Rows.Count > 0)
                    {
                        List<Return2290_2017.IRS2290TypeHighwayMtrVehTxComputationGrp> objTaxComputationList = new
                            List<Return2290_2017.IRS2290TypeHighwayMtrVehTxComputationGrp>();

                        foreach (DataRow ldr in dsTaxComputation.Tables[0].Rows)
                        {
                            Return2290_2017.IRS2290TypeHighwayMtrVehTxComputationGrp objTaxComputation = new
                                Return2290_2017.IRS2290TypeHighwayMtrVehTxComputationGrp();
                            objTaxComputation.VehicleCategoryCd = Convert.ToString(ldr["Category"]);

                            Return2290_2017.HighwayMtrVehTxCmptColumnsGrpType objVeh = new Return2290_2017.HighwayMtrVehTxCmptColumnsGrpType();
                            
                            objVeh.TaxAmt = Convert.ToDecimal(ldr["Tax_Amount"]);
                            objVeh.TaxAmtSpecified = true;

                            
                            objVeh.LoggingVehPartialTaxAmtSpecified = false;
                            objVeh.NonLoggingVehPartialTaxAmtSpecified = false;

                            if (Convert.ToString(ldr["Logging_Vehicle_Count"]) != "0")
                            {
                                if (Convert.ToString(ldr["Logging_Vehicle_Count"]) != "")
                                {
                                    numLoggingVehicleCnt = numLoggingVehicleCnt + Convert.ToInt16(ldr["Logging_Vehicle_Count"]);
                                    objVeh.LoggingVehicleCnt = Convert.ToString(ldr["Logging_Vehicle_Count"]);
                                }
                                if (Convert.ToString(ldr["Logging_Vehicle_Count"]) != "")
                                    numTotalTaxableVeh += Convert.ToInt16(ldr["Logging_Vehicle_Count"]);
                            }
                            if (Convert.ToString(ldr["Non_Logging_Vehicle_Count"]) != "0")
                            {
                                if (Convert.ToString(ldr["Non_Logging_Vehicle_Count"]) != "")
                                {
                                    numNonLoggingVehicleCnt = numNonLoggingVehicleCnt + Convert.ToInt16(ldr["Non_Logging_Vehicle_Count"]);
                                    objVeh.NonLoggingVehicleCnt = Convert.ToString(ldr["Non_Logging_Vehicle_Count"]);
                                }
                                if (Convert.ToString(ldr["Non_Logging_Vehicle_Count"]) != "")
                                    numTotalTaxableVeh += Convert.ToInt16(ldr["Non_Logging_Vehicle_Count"]);
                            }
                            objTaxComputation.HighwayMtrVehTxCmptColumnsGrp = objVeh;
                            objTaxComputationList.Add(objTaxComputation);
                        }
                        objIRS2290.HighwayMtrVehTxComputationGrp = objTaxComputationList.ToArray();

                        objIRS2290.TotalTaxAmt = Convert.ToDecimal(TaxFromTaxComputation.ToString("N2"));
                        objIRS2290.TotalTaxAmtSpecified = true;

                        objIRS2290.TotalVehicleCnt = numTotalTaxableVeh.ToString();

                    }
                }

                #endregion

                #region IS APPLICATION FOR....

                if (IsAddressChange)
                    objIRS2290.AddressChangeInd = Return2290_2017.CheckboxType.X;

                if (IsAmendedReturn)
                {
                    objIRS2290.AmendedReturnInd = Return2290_2017.CheckboxType.X;
                    objIRS2290.AmendedReturnIndSpecified = true;
                    objIRS2290.AmendedMonthNum = AmendedMonth; //format is MM
                }

                if (IsVINCorrection)
                {
                    // 2017 VINCorrection VIN Correction
                    // Vishwa Add the new TAG About the VINCorrectionExplanationStmt -> ExplanationTtx

                    //objIRS2290.VINCorrectionInd = Return2290_2017.CheckboxType.X;
                    //objIRS2290.VINCorrectionIndSpecified = true;

                    
                    Return2290_2017.IRS2290TypeVINCorrInd objVINCor = new Return2290_2017.IRS2290TypeVINCorrInd();
                    objVINCor.Value = "X";
                    List<string> docId_VINCorrDetails = new List<string>();
                    docId_VINCorrDetails.Add("VINCorrectionExplan");
                    objVINCor.referenceDocumentId = docId_VINCorrDetails.ToArray();
                    objVINCor.referenceDocumentName = "VINCorrectionExplanationStatement";

                    objIRS2290.VINCorrectionInd = objVINCor;


                    //Return2290_2017.VINCorrectionExplanationStmtType VinCorExp = new Return2290_2017.VINCorrectionExplanationStmtType();

                    //VinCorExp.ExplanationTxt = "VIN Correction update request";
                    //objIRS2290.VINCorrectionExplanationStmt = VinCorExp;

                }
                //2017 VIN Correction
                if (IsFinalReturn && IsVINCorrection == false)
                {
                    objIRS2290.FinalReturnInd = Return2290_2017.CheckboxType.X;
                    objIRS2290.FinalReturnIndSpecified = true;
                }

                #endregion

                #region ADDITIONAL TAX

                Return2290_2017.IRS2290TypeAdditionalTaxAmt objAdditionalTax = new Return2290_2017.IRS2290TypeAdditionalTaxAmt();
                objAdditionalTax.Value = Convert.ToDecimal(AdditionalTaxAmount.ToString("N2"));
                List<string> docId_TGWIncreaseWorksheet = new List<string>();
                docId_TGWIncreaseWorksheet.Add("TGWIncreaseWorksheet");
                objAdditionalTax.referenceDocumentId = docId_TGWIncreaseWorksheet.ToArray();
                objAdditionalTax.referenceDocumentName = "TGWIncreaseWorksheet";

                objIRS2290.AdditionalTaxAmt = objAdditionalTax;

                #endregion

                objIRS2290.TotalTaxAmt = Convert.ToDecimal(TaxFromTaxComputation.ToString("N2")) + Convert.ToDecimal(AdditionalTaxAmount.ToString("N2")); ;
                objIRS2290.TotalTaxAmtSpecified = true;

                #region CREDIT AMOUNT

                Return2290_2017.IRS2290TypeTaxCreditsAmt objTaxCreditsAmt = new Return2290_2017.IRS2290TypeTaxCreditsAmt();
                List<string> docId_CreditsAmountStatement = new List<string>();
                docId_CreditsAmountStatement.Add("CreditsAmountStatement");
                objTaxCreditsAmt.referenceDocumentId = docId_CreditsAmountStatement.ToArray();
                objTaxCreditsAmt.referenceDocumentName = "CreditsAmountStatement";
                objTaxCreditsAmt.Value = Convert.ToDecimal(CreditAmount.ToString("N2"));

                objIRS2290.TaxCreditsAmt = objTaxCreditsAmt;

                #endregion

                objIRS2290.BalanceDueAmt = (Convert.ToDecimal(TaxFromTaxComputation.ToString("N2")) + Convert.ToDecimal(AdditionalTaxAmount.ToString("N2"))) - Convert.ToDecimal(CreditAmount.ToString("N2"));

                if (objIRS2290.BalanceDueAmt < 0)
                    objIRS2290.BalanceDueAmt = 0.00M;

                #region IRS PAYMENT2

                Decimal BalanceDue = objIRS2290.BalanceDueAmt;

                if (BalanceDue > 0)
                {
                    if (paymentType != null)
                    {
                        /* Now Both Money Order or EFTPS will be same */
                        if (paymentType == "CMO" || paymentType == "EFTPS")
                        {
                            objIRS2290.EFTPSPaymentInd = Return2290_2017.CheckboxType.X;
                            objIRS2290.EFTPSPaymentIndSpecified = true;
                        }
                        /* Credit / Debit card payments */
                        else if (paymentType == "CDC")
                        {
                            objIRS2290.CreditDebitCardPaymentInd = Return2290_2017.CheckboxType.X;
                            objIRS2290.CreditDebitCardPaymentIndSpecified = true;

                        }
                        else if (paymentType == "DirectDebit" || paymentType == "EFW")
                        {
                            List<Return2290_2017.IRSPayment2> irsPmt2Collection = new List<Return2290_2017.IRSPayment2>();

                            Return2290_2017.IRSPayment2 objPayType = new Return2290_2017.IRSPayment2();

                            objPayType.PaymentAmt = Convert.ToDecimal(BalanceDue.ToString("N2"));
                            objPayType.RoutingTransitNum = Payment_RoutingTransitNo;
                            objPayType.BankAccountNum = Payment_Acc_No;

                            if (Payment_Acc_Type == "Checking" || Payment_Acc_Type == "0")
                                objPayType.BankAccountTypeCd = Return2290_2017.BankAccountType.Item1;
                            else if (Payment_Acc_Type == "Saving" || Payment_Acc_Type == "1")
                                objPayType.BankAccountTypeCd = Return2290_2017.BankAccountType.Item2;

                            objPayType.RequestedPaymentDt = Convert.ToDateTime(Payment_ReqPayDate);

                            objPayType.TaxpayerDaytimePhoneNum = Payment_Txpyer_Ph;

                            objPayType.documentId = "IRSPayment2";
                            //objPayType.documentName = "IRSPayment2";
                            objPayType.softwareId = Variables.SoftwareID;
                            objPayType.softwareVersion = Variables.SoftwareVersion;

                            irsPmt2Collection.Add(objPayType);

                            returnData.IRSPayment2 = irsPmt2Collection.ToArray();
                            documentCount += 1;
                        }
                    }
                }
                #endregion

                #region SUSPENDED VIN

                if (dsSuspendedVehicles.Tables[0].Rows.Count > 0)
                {
                    List<Return2290_2017.SuspendedVINInfoTypeVINDetail> objSusVehList = new List<Return2290_2017.SuspendedVINInfoTypeVINDetail>();

                    foreach (DataRow ldr_suspended in dsSuspendedVehicles.Tables[0].Rows)
                    {
                        Return2290_2017.SuspendedVINInfoTypeVINDetail objSusVeh = new Return2290_2017.SuspendedVINInfoTypeVINDetail();
                        objSusVeh.VIN = Convert.ToString(ldr_suspended["VIN"]);
                        objSusVehList.Add(objSusVeh);

                        #region ....
                        if (!Convert.ToBoolean(ldr_suspended["IsAgricultural"]))
                        {
                            objIRS2290.AgricMileageUsed7500OrLessInd = Return2290_2017.CheckboxType.X;
                            objIRS2290.AgricMileageUsed7500OrLessIndSpecified = true;
                        }
                        else if (Convert.ToBoolean(ldr_suspended["IsAgricultural"]))
                        {
                            objIRS2290.MileageUsed5000OrLessInd = Return2290_2017.CheckboxType.X;
                            objIRS2290.MileageUsed5000OrLessIndSpecified = true;
                        }

                        if (Convert.ToString(ldr_suspended["IsLogging"]) != "")
                        {
                            if (Convert.ToBoolean(ldr_suspended["IsLogging"]))
                                numTaxSuspended_LoggingVeh += 1;
                            else if (!Convert.ToBoolean(ldr_suspended["IsLogging"]))
                                numTaxSuspended_NonLoggingVeh += 1;
                        }
                        else
                        {
                            numTaxSuspended_NonLoggingVeh += 1;
                        }

                        #endregion
                    }

                    Return2290_2017.SuspendedVINStatementType objSusVehStmtType = new Return2290_2017.SuspendedVINStatementType();
                    objSusVehStmtType.SuspendedVINInfo = objSusVehList.ToArray();

                    if (numTaxSuspended_LoggingVeh != 0)
                        objIRS2290.TaxSuspendedLoggingVehCnt = numTaxSuspended_LoggingVeh.ToString();
                    if (numTaxSuspended_NonLoggingVeh != 0)
                        objIRS2290.TaxSuspendedNonLoggingVehCnt = numTaxSuspended_NonLoggingVeh.ToString();

                    Return2290_2017.IRS2290TypeSuspendedVINReferenceTyp objSuspendedVINRef =
                        new Return2290_2017.IRS2290TypeSuspendedVINReferenceTyp();
                    List<string> docID_SuspendedVINStmt = new List<string>();
                    docID_SuspendedVINStmt.Add("SuspendedVINStatement");
                    objSuspendedVINRef.referenceDocumentId = docID_SuspendedVINStmt.ToArray();
                    objSuspendedVINRef.referenceDocumentName = "SuspendedVINStatement";


                    objIRS2290.SuspendedVINReferenceTyp = objSuspendedVINRef;
                }

                #endregion

                if (IsNotSubjectToTaxChecked)
                    objIRS2290.NotSubjectToTaxInd = Return2290_2017.CheckboxType.X;

                
                if (dsTGWIncreaseWorksheet.Tables.Count > 0 && IsAmendedReturn)
                {
                    if (Convert.ToDecimal(AdditionalTaxAmount.ToString("N2")) > 0)
                    {
                        objIRS2290.TotalTaxComputationAmt = 0.01m;//Convert.ToDecimal(TaxFromTaxComputation.ToString("N2"));
                        objIRS2290.TotalTaxComputationAmtSpecified = true;
                    }
                    else
                    {
                        objIRS2290.TotalTaxComputationAmtSpecified = false;
                    }
                }
                else
                {
                    if (Convert.ToDecimal(TaxFromTaxComputation.ToString("N2")) > 0)
                    {
                        objIRS2290.TotalTaxComputationAmt = Convert.ToDecimal(TaxFromTaxComputation.ToString("N2"));
                        objIRS2290.TotalTaxComputationAmtSpecified = true;
                    }
                    else
                    {
                        objIRS2290.TotalTaxComputationAmtSpecified = false;
                    }
                }
                objIRS2290.softwareId = Variables.SoftwareID;
                objIRS2290.softwareVersion = Variables.SoftwareVersion;
                objIRS2290.documentId = "IRS2290";
                objIRS2290.documentName = "IRS2290";

                returnData.IRS2290 = objIRS2290;
                documentCount += 1;

                #endregion

                #region IRS 2290 SCHEDULE1

                Return2290_2017.IRS2290Schedule1 irs2290Schedule1 = new Return2290_2017.IRS2290Schedule1();

                if (Convert.ToInt16(numTotalTaxableVeh) > 0)
                    irs2290Schedule1.TaxableVehicleCnt = numTotalTaxableVeh.ToString();

                int Count_taxSuspendedLogVeh = Convert.ToInt16(numTaxSuspended_LoggingVeh);
                int Count_taxSuspendedNonLogVeh = Convert.ToInt16(numTaxSuspended_NonLoggingVeh);
                if (Count_taxSuspendedLogVeh + Count_taxSuspendedNonLogVeh != 0)
                    irs2290Schedule1.TotalSuspendedVehicleCnt = Convert.ToString(Count_taxSuspendedLogVeh + Count_taxSuspendedNonLogVeh);

                
                if ((numTotalTaxableVeh + Count_taxSuspendedLogVeh + Count_taxSuspendedNonLogVeh) > 0)
                    irs2290Schedule1.VehicleCnt = Convert.ToString(numTotalTaxableVeh + Count_taxSuspendedLogVeh + Count_taxSuspendedNonLogVeh);
                else
                {
                    if (IsAmendedReturn)
                    {
                        if ((numTotalTaxableVeh + Count_taxSuspendedLogVeh + Count_taxSuspendedNonLogVeh) > 0)
                        {
                            irs2290Schedule1.VehicleCnt = Convert.ToString(numTotalTaxableVeh + Count_taxSuspendedLogVeh + Count_taxSuspendedNonLogVeh);
                        }
                        else
                            irs2290Schedule1.VehicleCnt = "1";
                    }
                }

                List<Return2290_2017.IRS2290Schedule1TypeVehicleReportTaxItem> objVehRptTaxItem =
                    new List<Return2290_2017.IRS2290Schedule1TypeVehicleReportTaxItem>();

                if (dsTaxableVehicles.Tables.Count > 0)
                    foreach (DataRow ldr in dsTaxableVehicles.Tables[0].Rows)
                    {
                        Return2290_2017.IRS2290Schedule1TypeVehicleReportTaxItem vrti =
                            new Return2290_2017.IRS2290Schedule1TypeVehicleReportTaxItem();
                        vrti.VehicleCategoryCd = Convert.ToString(ldr["WeightCategory"]);
                        vrti.VIN = Convert.ToString(ldr["VIN"]);
                        objVehRptTaxItem.Add(vrti);
                    }

                if (dsTGWIncreaseWorksheet.Tables.Count > 0)
                    foreach (DataRow ldr in dsTGWIncreaseWorksheet.Tables[0].Rows)
                    {
                        Return2290_2017.IRS2290Schedule1TypeVehicleReportTaxItem vrti =
                            new Return2290_2017.IRS2290Schedule1TypeVehicleReportTaxItem();
                        vrti.VehicleCategoryCd = Convert.ToString(ldr["WeightCategoryNew"]);
                        vrti.VIN = Convert.ToString(ldr["VIN"]);
                        objVehRptTaxItem.Add(vrti);
                    }

                if (dsSuspendedVehicles.Tables.Count > 0)
                {
                    foreach (DataRow ldr in dsSuspendedVehicles.Tables[0].Rows)
                    {
                        Return2290_2017.IRS2290Schedule1TypeVehicleReportTaxItem vrti = new
                            Return2290_2017.IRS2290Schedule1TypeVehicleReportTaxItem();
                        vrti.VehicleCategoryCd = "W";
                        vrti.VIN = Convert.ToString(ldr["VIN"]);
                        objVehRptTaxItem.Add(vrti);
                    }
                }

                irs2290Schedule1.VehicleReportTaxItem = objVehRptTaxItem.ToArray();

                irs2290Schedule1.documentId = "IRS2290Schedule1";
                irs2290Schedule1.documentName = "IRS2290Schedule1";
                irs2290Schedule1.softwareId = Variables.SoftwareID;
                irs2290Schedule1.softwareVersion = Variables.SoftwareVersion;

                returnData.IRS2290Schedule1 = irs2290Schedule1;
                documentCount += 1;

                #endregion

                //2017 VIN Correction 
                #region VIN CORRECTION  Explanation Statement
                if (IsVINCorrection)
                {
                    //Return2290_2017.IRS2290Schedule1 irs2290Schedule1 = new Return2290_2017.IRS2290Schedule1();

                    Return2290_2017.VINCorrectionExplanationStmtType VinCorExp = new Return2290_2017.VINCorrectionExplanationStmtType();

                    VinCorExp.ExplanationTxt = "VINCorrection update request. Update the previosuly accepted Submission to the NEW VIN attached with the IRS2290Schedule1";
                    //objIRS2290.VINCorrectionExplanationStmt = VinCorExp;

                    VinCorExp.documentId = "VINCorrectionExplan";
                    //VinCorExp.documentName = "VINCorrectionExplanationStmtType";
                    VinCorExp.softwareId = Variables.SoftwareID;
                    VinCorExp.softwareVersion = Variables.SoftwareVersion;

                    returnData.VINCorrectionExplanationStmt = VinCorExp;
                    documentCount += 1;
                }
                #endregion   // VIN CORRECTION  Explanation Statement

                #region CREDITS AMOUNT STATEMENT

                if (dsCreditVehicles.Tables[0].Rows.Count > 0)
                {
                    int rowCount = dsCreditVehicles.Tables[0].Rows.Count;
                    List<Return2290_2017.CreditsAmountInfoTypeDisposalReportingItem> dric =
                        new List<Return2290_2017.CreditsAmountInfoTypeDisposalReportingItem>();

                    foreach (DataRow ldr in dsCreditVehicles.Tables[0].Rows)
                    {
                        Return2290_2017.CreditsAmountInfoTypeDisposalReportingItem dri =
                            new Return2290_2017.CreditsAmountInfoTypeDisposalReportingItem();
                        dri.CreditsAmountExplanationTxt = Convert.ToString(ldr["Reason"]);
                        dri.DisposalReportingAmt = Convert.ToString(ldr["TaxAmount"]);
                        dri.DisposalReportingDt = Convert.ToDateTime(ldr["EffectiveDate"]);
                        dri.DisposalReportingVIN = Convert.ToString(ldr["VIN"]);

                        dric.Add(dri);
                    }

                    Return2290_2017.CreditsAmountStatement cas = new Return2290_2017.CreditsAmountStatement();
                    cas.CreditsAmountInfo = dric.ToArray();
                    cas.documentId = "CreditsAmountStatement";
                    cas.documentName = "CreditsAmountStatement";
                    cas.softwareId = Variables.SoftwareID;
                    cas.softwareVersion = Variables.SoftwareVersion;

                    Return2290_2017.CreditsAmountStatement[] casc = new Return2290_2017.CreditsAmountStatement[1];
                    casc[0] = cas;

                    returnData.CreditsAmountStatement = casc;
                    documentCount += 1;
                }
                #endregion

                #region STATEMENT IN SUPPORT OF SUSPENSION //Sold Suspended

                if (dsSoldSuspendedVehicles.Tables[0].Rows.Count > 0)
                {
                    List<Return2290_2017.StmtInSupportOfSuspensionInfoTypeVehicleSuspensionDetail> ic =
                        new List<Return2290_2017.StmtInSupportOfSuspensionInfoTypeVehicleSuspensionDetail>();

                    foreach (DataRow ldr in dsSoldSuspendedVehicles.Tables[0].Rows)
                    {
                        Return2290_2017.StmtInSupportOfSuspensionInfoTypeVehicleSuspensionDetail i =
                            new Return2290_2017.StmtInSupportOfSuspensionInfoTypeVehicleSuspensionDetail();
                        i.Dt = Convert.ToDateTime(ldr["DateSold"]);
                        Return2290_2017.BusinessNameType bnt = new Return2290_2017.BusinessNameType();
                        bnt.BusinessNameLine1Txt = Convert.ToString(ldr["Buyer"]);
                        i.BusinessName = bnt;
                        i.VIN = Convert.ToString(ldr["VIN"]);
                        ic.Add(i);
                    }

                    Return2290_2017.StmtInSupportOfSuspension sisosit = new Return2290_2017.StmtInSupportOfSuspension();
                    sisosit.StmtInSupportOfSuspensionInfo = ic.ToArray();

                    List<Return2290_2017.StmtInSupportOfSuspension> sisostc = new List<Return2290_2017.StmtInSupportOfSuspension>();
                    sisostc.Add(sisosit);

                    sisosit.documentId = "StatementInSupportOfSuspension";
                    sisosit.documentName = "StatementInSupportOfSuspension";
                    sisosit.softwareId = Variables.SoftwareID;
                    sisosit.softwareVersion = Variables.SoftwareVersion;

                    returnData.StmtInSupportOfSuspension = sisostc.ToArray();
                    documentCount += 1;
                }
                #endregion

                #region SUSPENDED VIN STATEMENT

                if (dsPriorYearMileageExceededVehicles.Tables[0].Rows.Count > 0)
                {
                    List<Return2290_2017.SuspendedVINInfoTypeVINDetail> objSVITVDList =
                        new List<Return2290_2017.SuspendedVINInfoTypeVINDetail>();
                    foreach (DataRow ldr in dsPriorYearMileageExceededVehicles.Tables[0].Rows)
                    {
                        Return2290_2017.SuspendedVINInfoTypeVINDetail objSVITVD = new Return2290_2017.SuspendedVINInfoTypeVINDetail();
                        objSVITVD.VIN = Convert.ToString(ldr["VIN"]);
                        objSVITVDList.Add(objSVITVD);
                    }

                    Return2290_2017.SuspendedVINStatement objSVS = new Return2290_2017.SuspendedVINStatement();
                    objSVS.SuspendedVINInfo = objSVITVDList.ToArray();
                    objSVS.documentId = "SuspendedVINStatement";
                    objSVS.documentName = "SuspendedVINStatement";
                    objSVS.softwareId = Variables.SoftwareID;
                    objSVS.softwareVersion = Variables.SoftwareVersion;

                    Return2290_2017.SuspendedVINStatement[] suspendedVINStmt = new Return2290_2017.SuspendedVINStatement[1];
                    suspendedVINStmt[0] = objSVS;

                    returnData.SuspendedVINStatement = suspendedVINStmt;
                    documentCount += 1;
                }
                #endregion

                #region TGW INCREASE WORKSHEET

                if (dsTGWIncreaseWorksheet.Tables[0].Rows.Count > 0)
                {
                    List<Return2290_2017.TGWIncreaseInfoType> tgwiitc = new List<Return2290_2017.TGWIncreaseInfoType>();
                    foreach (DataRow ldr in dsTGWIncreaseWorksheet.Tables[0].Rows)
                    {
                        Return2290_2017.TGWIncreaseInfoType tgwiit = new Return2290_2017.TGWIncreaseInfoType();

                        tgwiit.AdditionalTaxAmt = Convert.ToDecimal(ldr["TaxAmount"]);
                        tgwiit.AdditionalTaxAmtSpecified = true;
                        tgwiit.TGWIncreaseMonthNum = Convert.ToString(ldr["IncreasedMonthValue"]);
                        tgwiit.NewTaxAmt = Convert.ToDecimal(ldr["NewTaxAmount"]);
                        tgwiit.NewTaxAmtSpecified = true;
                        tgwiit.PreviousTaxAmt = Convert.ToDecimal(ldr["PreviousTaxAmount"]);
                        tgwiit.PreviousTaxAmtSpecified = true;
                        tgwiit.TGWCategoryCd = Convert.ToString(ldr["WeightCategoryNew"]);
                        tgwiitc.Add(tgwiit);
                    }

                    Return2290_2017.TGWIncreaseWorksheet tgwIncreaseWorksheet = new Return2290_2017.TGWIncreaseWorksheet();
                    tgwIncreaseWorksheet.TGWIncreaseInfo = tgwiitc.ToArray();
                    tgwIncreaseWorksheet.documentId = "TGWIncreaseWorksheet";
                    //tgwIncreaseWorksheet.__documentName = "TGWIncreaseWorksheet";
                    tgwIncreaseWorksheet.softwareId = Variables.SoftwareID;
                    tgwIncreaseWorksheet.softwareVersion = Variables.SoftwareVersion;

                    List<Return2290_2017.TGWIncreaseWorksheet> tgwiwc = new List<Return2290_2017.TGWIncreaseWorksheet>();
                    tgwiwc.Add(tgwIncreaseWorksheet);

                    returnData.TGWIncreaseWorksheet = tgwiwc.ToArray();
                    documentCount += 1;
                }
                #endregion

                returnData.__documentCount = documentCount.ToString();

                #endregion

                #region RETURN

                Return2290_2017.Return return2290 = new Return2290_2017.Return();
                return2290.ReturnData = returnData;
                return2290.ReturnHeader = returnHeaderType;
                return2290.returnVersion = Variables.SoftwareVersion;

                #endregion

                #region Create XML file

                IFormatter ft = new BinaryFormatter();

                ft.Serialize(mStream, return2290);

                mStream.Position = 0;

                Return2290_2017.Return irsForm2 = (Return2290_2017.Return)ft.Deserialize(mStream);

                s.Serialize(fStream2, irsForm2);

                if (mStream != null) mStream.Close();

                if (fStream2 != null) fStream2.Close();

                if (fStream != null) fStream.Close();

                try
                {
                    XmlDocument xdoc = new XmlDocument();
                    xdoc.PreserveWhitespace = false;
                    xdoc.Load("C:\\xml\\submission.xml");
                    xdoc.DocumentElement.SetAttribute("xmlns:efile", "http://www.irs.gov/efile");
                    XmlTextWriter xmlWriter = new XmlTextWriter("C:\\" + @"\xml\submission.xml", new UTF8Encoding(false));
                    xdoc.Save(xmlWriter);
                    xmlWriter.Flush();
                    xmlWriter.Close();
                }
                catch { }

                #endregion

            }
            catch (Exception ex)
            {
                if (fStream != null) fStream.Close();
                if (fStream2 != null) fStream2.Close();
                if (mStream != null) mStream.Close();
                throw ex;
            }
        }
        #endregion


      

        #region createReturnXml_2290
        static void createReturnXml_2290
           (
        #region Parameters

bool IsAddressChange,
           bool IsAmendedReturn,
           bool IsVINCorrection,
           bool IsFinalReturn,
           bool IsNotSubjectToTaxChecked,
           bool IsConsentToDiscloseYes,
           string AmendedMonth,
           string TaxYear,
           string First_Used_Date,

           string Filer_AddressLine1,
           string Filer_City,
           string Filer_EIN,
           string Filer_Foreign_Country,
           string Filer_Name,
           string Filer_NameControl,
           string Filer_State,
           string Filer_ZIPCode,

           string Officer_Name,
           string Officer_Phone,
           string Officer_PIN,
           string Officer_Title,

           string ThirdPartyDesigne_Name,
           string ThirdPartyDesigne_Phone,
           string ThirdPartyDesigne_PIN,

           string Preparer_Firm,
           string Preparer_EIN,
           string Preparer_City,
           string Preparer_State,
           string Preparer_Zip,
           string Preparer_Country,
           string Preparer_Address,
           string Preparer_ForeignPhone,
           string Preparer_Phone,
           string Preparer_Email,
           string Preparer_Name,
           string Preparer_PTIN_SSN,
           bool IsPreparerHasPTIN,
           bool IsPreparerSelfEmployed,

           string paymentType,
           string Payment_Acc_No,
           string Payment_Acc_Type,
           string Payment_ReqPayDate,
           string Payment_RoutingTransitNo,
           string Payment_Txpyer_Ph,

           DataSet dsTaxableVehicles,
           DataSet dsCreditVehicles,
           DataSet dsSuspendedVehicles,
           DataSet dsSoldSuspendedVehicles,
           DataSet dsPriorYearMileageExceededVehicles,
           DataSet dsTGWIncreaseWorksheet,
           DataSet dsTaxComputation,
           string soldSuspendedVehicles,
           decimal TaxFromTaxComputation,
           decimal AdditionalTaxAmount,
           decimal CreditAmount,
           string TotalVehicles
        #endregion
)
        {
            int documentCount = 0;
            FileStream fStream = null;
            FileStream fStream2 = null;
            MemoryStream mStream = null;

            int numTotalTaxableVeh = 0;
            int numTaxSuspended_LoggingVeh = 0;
            int numTaxSuspended_NonLoggingVeh = 0;


            try
            {
                if (!Directory.Exists(path2CreateZipFile + @"\xml"))
                    Directory.CreateDirectory(path2CreateZipFile + @"\xml");

                fStream2 = new FileStream(path2CreateZipFile + @"\xml\submission.xml", FileMode.Create);

                byte[] byteArray = new byte[90000];
                mStream = new MemoryStream(byteArray);
                XmlSerializer s = new XmlSerializer(typeof(IRS_Return_2290.Return));

                #region RETRUN_HEADER

                IRS_Return_2290.ReturnHeaderType returnHeaderType = new IRS_Return_2290.ReturnHeaderType();

                returnHeaderType.Timestamp = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz");
                returnHeaderType.MultipleSoftwarePackagesUsed = false;
                returnHeaderType.PINEnteredBy = IRS_Return_2290.PINEnteredBy.Taxpayer;
                returnHeaderType.__PINEnteredBySpecified = true;
                returnHeaderType.ReturnType = IRS_Return_2290.ReturnType._2290;
                returnHeaderType.__ReturnTypeSpecified = true;
                returnHeaderType.SignatureOption = IRS_Return_2290.SignatureOption.PIN_Number;

                returnHeaderType.FirstUsedDate = First_Used_Date;

                #region PRACTIONER PIN
                IRS_Return_2290.PractitionerPIN PractitionerPIN = new IRS_Return_2290.PractitionerPIN();
                PractitionerPIN.EFIN = Variables.EFIN;
                PractitionerPIN.PIN = Variables.PIN;

                #endregion

                #region ORIGINATOR
                IRS_Return_2290.Originator originator = new IRS_Return_2290.Originator();
                originator.EFIN = Variables.EFIN;
                originator.Type = IRS_Return_2290.OriginatorType.ERO;
                originator.__TypeSpecified = true;
                originator.PractitionerPIN = PractitionerPIN;
                returnHeaderType.Originator = originator;
                #endregion

                #region FILER
                IRS_Return_2290.Filer filer = new IRS_Return_2290.Filer();

                filer.EIN = Filer_EIN;
                filer.Name = Filer_Name; 

                filer.NameControl = Filer_NameControl;

                if (Filer_Foreign_Country == "")
                {
                    #region FILER US ADDRESS
                    IRS_Return_2290.USAddressType usAdd_Filer = new IRS_Return_2290.USAddressType();


                    if (Filer_AddressLine1.Length > 35)
                    {
                        string Filer_AddressLine2 = string.Empty;
                        usAdd_Filer.AddressLine1 = Filer_AddressLine1.Substring(0, 35).Trim();
                        Filer_AddressLine2 = Filer_AddressLine1.Substring(35);
                        usAdd_Filer.AddressLine2 = Filer_AddressLine2.Trim();

                    }
                    else
                    {
                        usAdd_Filer.AddressLine1 = Filer_AddressLine1.Trim();
                    }
                    usAdd_Filer.City = Filer_City.Trim();

                    switch (Filer_State)
                    {
                        #region Filer_State
                        case "AA": usAdd_Filer.State = IRS_Return_2290.StateType.AA; break;
                        case "AE": usAdd_Filer.State = IRS_Return_2290.StateType.AE; break;
                        case "AK": usAdd_Filer.State = IRS_Return_2290.StateType.AK; break;
                        case "AL": usAdd_Filer.State = IRS_Return_2290.StateType.AL; break;
                        case "AP": usAdd_Filer.State = IRS_Return_2290.StateType.AP; break;
                        case "AR": usAdd_Filer.State = IRS_Return_2290.StateType.AR; break;
                        case "AS": usAdd_Filer.State = IRS_Return_2290.StateType.AS; break;
                        case "AZ": usAdd_Filer.State = IRS_Return_2290.StateType.AZ; break;
                        case "CA": usAdd_Filer.State = IRS_Return_2290.StateType.CA; break;
                        case "CO": usAdd_Filer.State = IRS_Return_2290.StateType.CO; break;
                        case "CT": usAdd_Filer.State = IRS_Return_2290.StateType.CT; break;
                        case "DC": usAdd_Filer.State = IRS_Return_2290.StateType.DC; break;
                        case "DE": usAdd_Filer.State = IRS_Return_2290.StateType.DE; break;
                        case "FL": usAdd_Filer.State = IRS_Return_2290.StateType.FL; break;
                        case "FM": usAdd_Filer.State = IRS_Return_2290.StateType.FM; break;
                        case "GA": usAdd_Filer.State = IRS_Return_2290.StateType.GA; break;
                        case "GU": usAdd_Filer.State = IRS_Return_2290.StateType.GU; break;
                        case "HI": usAdd_Filer.State = IRS_Return_2290.StateType.HI; break;
                        case "IA": usAdd_Filer.State = IRS_Return_2290.StateType.IA; break;
                        case "ID": usAdd_Filer.State = IRS_Return_2290.StateType.ID; break;
                        case "IL": usAdd_Filer.State = IRS_Return_2290.StateType.IL; break;
                        case "IN": usAdd_Filer.State = IRS_Return_2290.StateType.IN; break;
                        case "KS": usAdd_Filer.State = IRS_Return_2290.StateType.KS; break;
                        case "KY": usAdd_Filer.State = IRS_Return_2290.StateType.KY; break;
                        case "LA": usAdd_Filer.State = IRS_Return_2290.StateType.LA; break;
                        case "MA": usAdd_Filer.State = IRS_Return_2290.StateType.MA; break;
                        case "MD": usAdd_Filer.State = IRS_Return_2290.StateType.MD; break;
                        case "ME": usAdd_Filer.State = IRS_Return_2290.StateType.ME; break;
                        case "MH": usAdd_Filer.State = IRS_Return_2290.StateType.MH; break;
                        case "MI": usAdd_Filer.State = IRS_Return_2290.StateType.MI; break;
                        case "MN": usAdd_Filer.State = IRS_Return_2290.StateType.MN; break;
                        case "MO": usAdd_Filer.State = IRS_Return_2290.StateType.MO; break;
                        case "MP": usAdd_Filer.State = IRS_Return_2290.StateType.MP; break;
                        case "MS": usAdd_Filer.State = IRS_Return_2290.StateType.MS; break;
                        case "MT": usAdd_Filer.State = IRS_Return_2290.StateType.MT; break;
                        case "NC": usAdd_Filer.State = IRS_Return_2290.StateType.NC; break;
                        case "ND": usAdd_Filer.State = IRS_Return_2290.StateType.ND; break;
                        case "NE": usAdd_Filer.State = IRS_Return_2290.StateType.NE; break;
                        case "NH": usAdd_Filer.State = IRS_Return_2290.StateType.NH; break;
                        case "NJ": usAdd_Filer.State = IRS_Return_2290.StateType.NJ; break;
                        case "NM": usAdd_Filer.State = IRS_Return_2290.StateType.NM; break;
                        case "NV": usAdd_Filer.State = IRS_Return_2290.StateType.NV; break;
                        case "NY": usAdd_Filer.State = IRS_Return_2290.StateType.NY; break;
                        case "OH": usAdd_Filer.State = IRS_Return_2290.StateType.OH; break;
                        case "OK": usAdd_Filer.State = IRS_Return_2290.StateType.OK; break;
                        case "OR": usAdd_Filer.State = IRS_Return_2290.StateType.OR; break;
                        case "PA": usAdd_Filer.State = IRS_Return_2290.StateType.PA; break;
                        case "PR": usAdd_Filer.State = IRS_Return_2290.StateType.PR; break;
                        case "PW": usAdd_Filer.State = IRS_Return_2290.StateType.PW; break;
                        case "RI": usAdd_Filer.State = IRS_Return_2290.StateType.RI; break;
                        case "SC": usAdd_Filer.State = IRS_Return_2290.StateType.SC; break;
                        case "SD": usAdd_Filer.State = IRS_Return_2290.StateType.SD; break;
                        case "TN": usAdd_Filer.State = IRS_Return_2290.StateType.TN; break;
                        case "TX": usAdd_Filer.State = IRS_Return_2290.StateType.TX; break;
                        case "UT": usAdd_Filer.State = IRS_Return_2290.StateType.UT; break;
                        case "VA": usAdd_Filer.State = IRS_Return_2290.StateType.VA; break;
                        case "VI": usAdd_Filer.State = IRS_Return_2290.StateType.VI; break;
                        case "VT": usAdd_Filer.State = IRS_Return_2290.StateType.VT; break;
                        case "WA": usAdd_Filer.State = IRS_Return_2290.StateType.WA; break;
                        case "WI": usAdd_Filer.State = IRS_Return_2290.StateType.WI; break;
                        case "WV": usAdd_Filer.State = IRS_Return_2290.StateType.WV; break;
                        case "WY": usAdd_Filer.State = IRS_Return_2290.StateType.WY; break;
                        #endregion
                    }

                    usAdd_Filer.__StateSpecified = true;

                    usAdd_Filer.ZIPCode = Filer_ZIPCode;

                    filer.USAddress = usAdd_Filer;
                    #endregion
                }
                else
                {
                    #region FILER FOREIGN ADDRESS
                    IRS_Return_2290.ForeignAddressType foreignAdd_Filer = new IRS_Return_2290.ForeignAddressType();

                    if (Filer_AddressLine1.Length > 35)
                    {
                        string Filer_AddressLine2 = string.Empty;
                        foreignAdd_Filer.AddressLine1 = Filer_AddressLine1.Substring(0, 35).Trim();
                        Filer_AddressLine2 = Filer_AddressLine1.Substring(35);
                        foreignAdd_Filer.AddressLine2 = Filer_AddressLine2.Trim();
                    }
                    else
                    {
                        foreignAdd_Filer.AddressLine1 = Filer_AddressLine1.Trim();
                    }

                    foreignAdd_Filer.City = Filer_City.Trim();

                    if (Filer_State.Length > 0)
                        foreignAdd_Filer.ProvinceOrState = Filer_State;

                    switch (Filer_Foreign_Country)
                    {
                        #region Filer_Foreign_Country
                        case "AA": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.AA; break;
                        case "AC": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.AC; break;
                        case "AE": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.AE; break;
                        case "AF": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.AF; break;
                        case "AG": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.AG; break;
                        case "AJ": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.AJ; break;
                        case "AL": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.AL; break;
                        case "AM": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.AM; break;
                        case "AN": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.AN; break;
                        case "AO": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.AO; break;
                        case "AQ": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.AQ; break;
                        case "AR": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.AR; break;
                        case "AS": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.AS; break;
                        case "AT": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.AT; break;
                        case "AU": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.AU; break;
                        case "AV": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.AV; break;
                        case "AX": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.AX; break;
                        case "AY": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.AY; break;
                        case "BA": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.BA; break;
                        case "BB": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.BB; break;
                        case "BC": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.BC; break;
                        case "BD": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.BD; break;
                        case "BE": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.BE; break;
                        case "BF": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.BF; break;
                        case "BG": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.BG; break;
                        case "BH": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.BH; break;
                        case "BK": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.BK; break;
                        case "BL": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.BL; break;
                        case "BM": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.BM; break;
                        case "BN": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.BN; break;
                        case "BO": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.BO; break;
                        case "BP": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.BP; break;
                        case "BQ": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.BQ; break;
                        case "BR": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.BR; break;
                        case "BT": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.BT; break;
                        case "BU": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.BU; break;
                        case "BV": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.BV; break;
                        case "BX": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.BX; break;
                        case "BY": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.BY; break;
                        case "CA": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.CA; break;
                        case "CB": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.CB; break;
                        case "CD": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.CD; break;
                        case "CE": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.CE; break;
                        case "CF": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.CF; break;
                        case "CG": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.CG; break;
                        case "CH": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.CH; break;
                        case "CI": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.CI; break;
                        case "CJ": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.CJ; break;
                        case "CK": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.CK; break;
                        case "CM": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.CM; break;
                        case "CN": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.CN; break;
                        case "CO": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.CO; break;
                        case "CQ": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.CQ; break;
                        case "CR": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.CR; break;
                        case "CS": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.CS; break;
                        case "CT": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.CT; break;
                        case "CU": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.CU; break;
                        case "CV": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.CV; break;
                        case "CW": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.CW; break;
                        case "CY": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.CY; break;
                        case "DA": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.DA; break;
                        case "DJ": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.DJ; break;
                        case "DO": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.DO; break;
                        case "DR": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.DR; break;
                        case "DX": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.DX; break;
                        case "EC": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.EC; break;
                        case "EG": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.EG; break;
                        case "EI": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.EI; break;
                        case "EK": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.EK; break;
                        case "EN": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.EN; break;
                        case "ER": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.ER; break;
                        case "ES": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.ES; break;
                        case "ET": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.ET; break;
                        case "EZ": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.EZ; break;
                        case "FI": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.FI; break;
                        case "FJ": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.FJ; break;
                        case "FK": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.FK; break;
                        case "FM": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.FM; break;
                        case "FO": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.FO; break;
                        case "FP": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.FP; break;
                        case "FQ": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.FQ; break;
                        case "FR": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.FR; break;
                        case "FS": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.FS; break;
                        case "GA": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.GA; break;
                        case "GB": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.GB; break;
                        case "GG": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.GG; break;
                        case "GH": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.GH; break;
                        case "GI": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.GI; break;
                        case "GJ": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.GJ; break;
                        case "GK": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.GK; break;
                        case "GL": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.GL; break;
                        case "GM": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.GM; break;
                        case "GP": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.GP; break;
                        case "GQ": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.GQ; break;
                        case "GR": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.GR; break;
                        case "GT": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.GT; break;
                        case "GV": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.GV; break;
                        case "GY": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.GY; break;
                        case "HA": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.HA; break;
                        case "HK": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.HK; break;
                        case "HM": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.HM; break;
                        case "HO": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.HO; break;
                        case "HQ": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.HQ; break;
                        case "HR": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.HR; break;
                        case "HU": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.HU; break;
                        case "IC": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.IC; break;
                        case "ID": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.ID; break;
                        case "IM": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.IM; break;
                        case "IO": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.IO; break;
                        case "IP": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.IP; break;
                        case "IR": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.IR; break;
                        case "IS": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.IS; break;
                        case "IT": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.IT; break;
                        case "IV": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.IV; break;
                        case "IZ": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.IZ; break;
                        case "JA": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.JA; break;
                        case "JE": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.JE; break;
                        case "JM": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.JM; break;
                        case "JN": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.JN; break;
                        case "JO": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.JO; break;
                        case "JQ": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.JQ; break;
                        case "KE": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.KE; break;
                        case "KG": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.KG; break;
                        case "KN": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.KN; break;
                        case "KQ": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.KQ; break;
                        case "KR": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.KR; break;
                        case "KS": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.KS; break;
                        case "KT": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.KT; break;
                        case "KU": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.KU; break;
                        case "KZ": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.KZ; break;
                        case "LA": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.LA; break;
                        case "LE": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.LE; break;
                        case "LG": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.LG; break;
                        case "LH": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.LH; break;
                        case "LI": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.LI; break;
                        case "LO": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.LO; break;
                        case "LQ": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.LQ; break;
                        case "LS": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.LS; break;
                        case "LT": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.LT; break;
                        case "LU": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.LU; break;
                        case "LY": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.LY; break;
                        case "MA": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.MA; break;
                        case "MC": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.MC; break;
                        case "MD": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.MD; break;
                        case "MF": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.MF; break;
                        case "MG": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.MG; break;
                        case "MH": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.MH; break;
                        case "MI": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.MI; break;
                        case "MJ": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.MJ; break;
                        case "MK": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.MK; break;
                        case "ML": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.ML; break;
                        case "MN": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.MN; break;
                        case "MO": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.MO; break;
                        case "MP": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.MP; break;
                        case "MQ": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.MQ; break;
                        case "MR": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.MR; break;
                        case "MT": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.MT; break;
                        case "MU": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.MU; break;
                        case "MV": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.MV; break;
                        case "MX": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.MX; break;
                        case "MY": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.MY; break;
                        case "MZ": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.MZ; break;
                        case "NC": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.NC; break;
                        case "NE": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.NE; break;
                        case "NF": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.NF; break;
                        case "NG": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.NG; break;
                        case "NH": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.NH; break;
                        case "NI": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.NI; break;
                        case "NL": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.NL; break;
                        case "NO": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.NO; break;
                        case "NP": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.NP; break;
                        case "NR": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.NR; break;
                        case "NS": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.NS; break;
                        case "NT": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.NT; break;
                        case "NU": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.NU; break;
                        case "NZ": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.NZ; break;
                        case "PA": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.PA; break;
                        case "PC": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.PC; break;
                        case "PE": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.PE; break;
                        case "PF": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.PF; break;
                        case "PG": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.PG; break;
                        case "PK": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.PK; break;
                        case "PL": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.PL; break;
                        case "PM": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.PM; break;
                        case "PO": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.PO; break;
                        case "PP": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.PP; break;
                        case "PS": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.PS; break;
                        case "PU": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.PU; break;
                        case "QA": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.QA; break;
                        case "RM": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.RM; break;
                        case "RO": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.RO; break;
                        case "RP": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.RP; break;
                        case "RQ": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.RQ; break;
                        case "RS": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.RS; break;
                        case "RW": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.RW; break;
                        case "SA": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.SA; break;
                        case "SB": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.SB; break;
                        case "SC": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.SC; break;
                        case "SE": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.SE; break;
                        case "SF": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.SF; break;
                        case "SG": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.SG; break;
                        case "SH": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.SH; break;
                        case "SI": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.SI; break;
                        case "SL": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.SL; break;
                        case "SM": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.SM; break;
                        case "SN": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.SN; break;
                        case "SO": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.SO; break;
                        case "SP": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.SP; break;
                        case "ST": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.ST; break;
                        case "SU": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.SU; break;
                        case "SV": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.SV; break;
                        case "SW": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.SW; break;
                        case "SX": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.SX; break;
                        case "SY": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.SY; break;
                        case "SZ": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.SZ; break;
                        case "TD": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.TD; break;
                        case "TH": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.TH; break;
                        case "TI": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.TI; break;
                        case "TK": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.TK; break;
                        case "TL": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.TL; break;
                        case "TN": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.TN; break;
                        case "TO": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.TO; break;
                        case "TP": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.TP; break;
                        case "TS": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.TS; break;
                        case "TT": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.TT; break;
                        case "TU": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.TU; break;
                        case "TV": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.TV; break;
                        case "TW": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.TW; break;
                        case "TX": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.TX; break;
                        case "TZ": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.TZ; break;
                        case "UG": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.UG; break;
                        case "UK": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.UK; break;
                        case "UP": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.UP; break;
                        case "UV": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.UV; break;
                        case "UY": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.UY; break;
                        case "UZ": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.UZ; break;
                        case "VC": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.VC; break;
                        case "VE": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.VE; break;
                        case "VI": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.VI; break;
                        case "VM": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.VM; break;
                        case "VP": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.VP; break;
                        case "VQ": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.VQ; break;
                        case "VT": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.VT; break;
                        case "WA": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.WA; break;
                        case "WF": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.WF; break;
                        case "WI": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.WI; break;
                        case "WQ": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.WQ; break;
                        case "WS": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.WS; break;
                        case "WZ": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.WZ; break;
                        case "XA": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.XA; break;
                        case "XI": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.XI; break;
                        case "XZ": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.XZ; break;
                        case "YI": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.YI; break;
                        case "YM": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.YM; break;
                        case "ZA": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.ZA; break;
                        case "ZI": foreignAdd_Filer.Country = IRS_Return_2290.CountryType.ZI; break;
                        #endregion
                    }

                    foreignAdd_Filer.__CountrySpecified = true;

                    if (Filer_ZIPCode.Length > 0)
                        foreignAdd_Filer.PostalCode = Filer_ZIPCode;

                    filer.ForeignAddress = foreignAdd_Filer;
                    #endregion
                }

                returnHeaderType.Filer = filer;
                #endregion

                #region OFFICER
                if (Officer_Name != "")
                {
                    IRS_Return_2290.Officer officer = new IRS_Return_2290.Officer();

                    officer.Name = Officer_Name;
                    officer.Title = Officer_Title;

                    if (Officer_PIN.Length > 0)
                        officer.TaxpayerPIN = Officer_PIN;

                    if (Officer_Phone.Length > 0)
                        officer.Phone = Officer_Phone;

                    officer.DateSigned = DateTime.Now.ToUniversalTime();
                    officer.__DateSignedSpecified = true;

                    returnHeaderType.Officer = officer;
                }
                #endregion

                #region THIRD PARTY DESIGNEE
                IRS_Return_2290.ThirdPartyDesignee thirdPrtyDesignee = new IRS_Return_2290.ThirdPartyDesignee();

                if (ThirdPartyDesigne_Name != "")
                {
                    thirdPrtyDesignee.DiscussWithThirdPartyYes = IRS_Return_2290.CheckboxType.X;
                    thirdPrtyDesignee.DesigneeName = ThirdPartyDesigne_Name;
                    thirdPrtyDesignee.DesigneePhone = ThirdPartyDesigne_Phone;
                    thirdPrtyDesignee.DesigneePIN = ThirdPartyDesigne_PIN;
                }
                else
                    thirdPrtyDesignee.DiscussWithThirdPartyNo = IRS_Return_2290.CheckboxType.X;

                returnHeaderType.ThirdPartyDesignee = thirdPrtyDesignee;
                #endregion

                #region PREPARER

                if (Preparer_Name != "")
                {
                    IRS_Return_2290.Preparer preparer = new IRS_Return_2290.Preparer();

                    preparer.Name = Preparer_Name;

                    if (IsPreparerHasPTIN)
                        preparer.PTIN = Preparer_PTIN_SSN;
                    else
                        preparer.SSN = Preparer_PTIN_SSN;

                    if (Preparer_Phone.Length > 0)
                        preparer.Phone = Preparer_Phone;
                    else if (Preparer_ForeignPhone.Length > 0)
                        preparer.ForeignPhone = Preparer_Phone;

                    preparer.EmailAddress = Preparer_Email;
                    preparer.DatePrepared = DateTime.Now.ToUniversalTime();

                    if (IsPreparerSelfEmployed)
                        preparer.SelfEmployed = IRS_Return_2290.CheckboxType.X;

                    returnHeaderType.Preparer = preparer;
                }

                if (!IsPreparerSelfEmployed && Preparer_Name != "")
                {
                    IRS_Return_2290.PreparerFirm PreparerFirm = new IRS_Return_2290.PreparerFirm();
                    IRS_Return_2290.BusinessNameType PreparerBusinessName = new IRS_Return_2290.BusinessNameType();
                    PreparerBusinessName.BusinessNameLine1 = Preparer_Firm;

                    PreparerFirm.EIN = Preparer_EIN;
                    PreparerFirm.PreparerFirmBusinessName = PreparerBusinessName;
                    if (Preparer_Country == "")
                    {
                        #region Preparer US ADDRESS
                        IRS_Return_2290.USAddressType usAdd_Preparer = new IRS_Return_2290.USAddressType();


                        if (Preparer_Address.Length > 35)
                        {
                            string Preparer_AddressLine2 = string.Empty;
                            usAdd_Preparer.AddressLine1 = Preparer_Address.Substring(0, 35).Trim();
                            Preparer_AddressLine2 = Preparer_Address.Substring(35);
                            usAdd_Preparer.AddressLine2 = Preparer_AddressLine2.Trim();

                        }
                        else
                        {
                            usAdd_Preparer.AddressLine1 = Preparer_Address.Trim();
                        }
                        usAdd_Preparer.City = Preparer_City.Trim();

                        switch (Preparer_State)
                        {
                            #region Filer_State
                            case "AA": usAdd_Preparer.State = IRS_Return_2290.StateType.AA; break;
                            case "AE": usAdd_Preparer.State = IRS_Return_2290.StateType.AE; break;
                            case "AK": usAdd_Preparer.State = IRS_Return_2290.StateType.AK; break;
                            case "AL": usAdd_Preparer.State = IRS_Return_2290.StateType.AL; break;
                            case "AP": usAdd_Preparer.State = IRS_Return_2290.StateType.AP; break;
                            case "AR": usAdd_Preparer.State = IRS_Return_2290.StateType.AR; break;
                            case "AS": usAdd_Preparer.State = IRS_Return_2290.StateType.AS; break;
                            case "AZ": usAdd_Preparer.State = IRS_Return_2290.StateType.AZ; break;
                            case "CA": usAdd_Preparer.State = IRS_Return_2290.StateType.CA; break;
                            case "CO": usAdd_Preparer.State = IRS_Return_2290.StateType.CO; break;
                            case "CT": usAdd_Preparer.State = IRS_Return_2290.StateType.CT; break;
                            case "DC": usAdd_Preparer.State = IRS_Return_2290.StateType.DC; break;
                            case "DE": usAdd_Preparer.State = IRS_Return_2290.StateType.DE; break;
                            case "FL": usAdd_Preparer.State = IRS_Return_2290.StateType.FL; break;
                            case "FM": usAdd_Preparer.State = IRS_Return_2290.StateType.FM; break;
                            case "GA": usAdd_Preparer.State = IRS_Return_2290.StateType.GA; break;
                            case "GU": usAdd_Preparer.State = IRS_Return_2290.StateType.GU; break;
                            case "HI": usAdd_Preparer.State = IRS_Return_2290.StateType.HI; break;
                            case "IA": usAdd_Preparer.State = IRS_Return_2290.StateType.IA; break;
                            case "ID": usAdd_Preparer.State = IRS_Return_2290.StateType.ID; break;
                            case "IL": usAdd_Preparer.State = IRS_Return_2290.StateType.IL; break;
                            case "IN": usAdd_Preparer.State = IRS_Return_2290.StateType.IN; break;
                            case "KS": usAdd_Preparer.State = IRS_Return_2290.StateType.KS; break;
                            case "KY": usAdd_Preparer.State = IRS_Return_2290.StateType.KY; break;
                            case "LA": usAdd_Preparer.State = IRS_Return_2290.StateType.LA; break;
                            case "MA": usAdd_Preparer.State = IRS_Return_2290.StateType.MA; break;
                            case "MD": usAdd_Preparer.State = IRS_Return_2290.StateType.MD; break;
                            case "ME": usAdd_Preparer.State = IRS_Return_2290.StateType.ME; break;
                            case "MH": usAdd_Preparer.State = IRS_Return_2290.StateType.MH; break;
                            case "MI": usAdd_Preparer.State = IRS_Return_2290.StateType.MI; break;
                            case "MN": usAdd_Preparer.State = IRS_Return_2290.StateType.MN; break;
                            case "MO": usAdd_Preparer.State = IRS_Return_2290.StateType.MO; break;
                            case "MP": usAdd_Preparer.State = IRS_Return_2290.StateType.MP; break;
                            case "MS": usAdd_Preparer.State = IRS_Return_2290.StateType.MS; break;
                            case "MT": usAdd_Preparer.State = IRS_Return_2290.StateType.MT; break;
                            case "NC": usAdd_Preparer.State = IRS_Return_2290.StateType.NC; break;
                            case "ND": usAdd_Preparer.State = IRS_Return_2290.StateType.ND; break;
                            case "NE": usAdd_Preparer.State = IRS_Return_2290.StateType.NE; break;
                            case "NH": usAdd_Preparer.State = IRS_Return_2290.StateType.NH; break;
                            case "NJ": usAdd_Preparer.State = IRS_Return_2290.StateType.NJ; break;
                            case "NM": usAdd_Preparer.State = IRS_Return_2290.StateType.NM; break;
                            case "NV": usAdd_Preparer.State = IRS_Return_2290.StateType.NV; break;
                            case "NY": usAdd_Preparer.State = IRS_Return_2290.StateType.NY; break;
                            case "OH": usAdd_Preparer.State = IRS_Return_2290.StateType.OH; break;
                            case "OK": usAdd_Preparer.State = IRS_Return_2290.StateType.OK; break;
                            case "OR": usAdd_Preparer.State = IRS_Return_2290.StateType.OR; break;
                            case "PA": usAdd_Preparer.State = IRS_Return_2290.StateType.PA; break;
                            case "PR": usAdd_Preparer.State = IRS_Return_2290.StateType.PR; break;
                            case "PW": usAdd_Preparer.State = IRS_Return_2290.StateType.PW; break;
                            case "RI": usAdd_Preparer.State = IRS_Return_2290.StateType.RI; break;
                            case "SC": usAdd_Preparer.State = IRS_Return_2290.StateType.SC; break;
                            case "SD": usAdd_Preparer.State = IRS_Return_2290.StateType.SD; break;
                            case "TN": usAdd_Preparer.State = IRS_Return_2290.StateType.TN; break;
                            case "TX": usAdd_Preparer.State = IRS_Return_2290.StateType.TX; break;
                            case "UT": usAdd_Preparer.State = IRS_Return_2290.StateType.UT; break;
                            case "VA": usAdd_Preparer.State = IRS_Return_2290.StateType.VA; break;
                            case "VI": usAdd_Preparer.State = IRS_Return_2290.StateType.VI; break;
                            case "VT": usAdd_Preparer.State = IRS_Return_2290.StateType.VT; break;
                            case "WA": usAdd_Preparer.State = IRS_Return_2290.StateType.WA; break;
                            case "WI": usAdd_Preparer.State = IRS_Return_2290.StateType.WI; break;
                            case "WV": usAdd_Preparer.State = IRS_Return_2290.StateType.WV; break;
                            case "WY": usAdd_Preparer.State = IRS_Return_2290.StateType.WY; break;
                            #endregion
                        }

                        usAdd_Preparer.__StateSpecified = true;

                        usAdd_Preparer.ZIPCode = Preparer_Zip;

                        PreparerFirm.PreparerFirmUSAddress = usAdd_Preparer;
                        #endregion
                    }
                    else
                    {
                        #region Preparer FOREIGN ADDRESS
                        IRS_Return_2290.ForeignAddressType foreignAdd_Preparer = new IRS_Return_2290.ForeignAddressType();

                        if (Preparer_Address.Length > 35)
                        {
                            string Preparer_AddressLine2 = string.Empty;
                            foreignAdd_Preparer.AddressLine1 = Preparer_Address.Substring(0, 35).Trim();
                            Preparer_AddressLine2 = Preparer_Address.Substring(35);
                            foreignAdd_Preparer.AddressLine2 = Preparer_AddressLine2.Trim();

                        }
                        else
                        {
                            foreignAdd_Preparer.AddressLine1 = Preparer_Address.Trim();
                        }
                        foreignAdd_Preparer.City = Preparer_City.Trim();

                        if (Preparer_State.Length > 0)
                            foreignAdd_Preparer.ProvinceOrState = Preparer_State;

                        switch (Preparer_Country)
                        {
                            #region Preparer_Foreign_Country
                            case "AA": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.AA; break;
                            case "AC": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.AC; break;
                            case "AE": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.AE; break;
                            case "AF": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.AF; break;
                            case "AG": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.AG; break;
                            case "AJ": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.AJ; break;
                            case "AL": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.AL; break;
                            case "AM": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.AM; break;
                            case "AN": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.AN; break;
                            case "AO": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.AO; break;
                            case "AQ": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.AQ; break;
                            case "AR": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.AR; break;
                            case "AS": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.AS; break;
                            case "AT": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.AT; break;
                            case "AU": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.AU; break;
                            case "AV": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.AV; break;
                            case "AX": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.AX; break;
                            case "AY": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.AY; break;
                            case "BA": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.BA; break;
                            case "BB": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.BB; break;
                            case "BC": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.BC; break;
                            case "BD": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.BD; break;
                            case "BE": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.BE; break;
                            case "BF": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.BF; break;
                            case "BG": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.BG; break;
                            case "BH": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.BH; break;
                            case "BK": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.BK; break;
                            case "BL": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.BL; break;
                            case "BM": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.BM; break;
                            case "BN": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.BN; break;
                            case "BO": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.BO; break;
                            case "BP": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.BP; break;
                            case "BQ": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.BQ; break;
                            case "BR": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.BR; break;
                            case "BT": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.BT; break;
                            case "BU": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.BU; break;
                            case "BV": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.BV; break;
                            case "BX": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.BX; break;
                            case "BY": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.BY; break;
                            case "CA": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.CA; break;
                            case "CB": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.CB; break;
                            case "CD": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.CD; break;
                            case "CE": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.CE; break;
                            case "CF": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.CF; break;
                            case "CG": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.CG; break;
                            case "CH": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.CH; break;
                            case "CI": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.CI; break;
                            case "CJ": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.CJ; break;
                            case "CK": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.CK; break;
                            case "CM": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.CM; break;
                            case "CN": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.CN; break;
                            case "CO": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.CO; break;
                            case "CQ": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.CQ; break;
                            case "CR": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.CR; break;
                            case "CS": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.CS; break;
                            case "CT": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.CT; break;
                            case "CU": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.CU; break;
                            case "CV": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.CV; break;
                            case "CW": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.CW; break;
                            case "CY": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.CY; break;
                            case "DA": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.DA; break;
                            case "DJ": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.DJ; break;
                            case "DO": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.DO; break;
                            case "DR": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.DR; break;
                            case "DX": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.DX; break;
                            case "EC": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.EC; break;
                            case "EG": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.EG; break;
                            case "EI": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.EI; break;
                            case "EK": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.EK; break;
                            case "EN": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.EN; break;
                            case "ER": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.ER; break;
                            case "ES": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.ES; break;
                            case "ET": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.ET; break;
                            case "EZ": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.EZ; break;
                            case "FI": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.FI; break;
                            case "FJ": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.FJ; break;
                            case "FK": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.FK; break;
                            case "FM": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.FM; break;
                            case "FO": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.FO; break;
                            case "FP": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.FP; break;
                            case "FQ": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.FQ; break;
                            case "FR": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.FR; break;
                            case "FS": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.FS; break;
                            case "GA": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.GA; break;
                            case "GB": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.GB; break;
                            case "GG": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.GG; break;
                            case "GH": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.GH; break;
                            case "GI": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.GI; break;
                            case "GJ": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.GJ; break;
                            case "GK": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.GK; break;
                            case "GL": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.GL; break;
                            case "GM": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.GM; break;
                            case "GP": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.GP; break;
                            case "GQ": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.GQ; break;
                            case "GR": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.GR; break;
                            case "GT": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.GT; break;
                            case "GV": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.GV; break;
                            case "GY": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.GY; break;
                            case "HA": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.HA; break;
                            case "HK": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.HK; break;
                            case "HM": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.HM; break;
                            case "HO": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.HO; break;
                            case "HQ": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.HQ; break;
                            case "HR": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.HR; break;
                            case "HU": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.HU; break;
                            case "IC": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.IC; break;
                            case "ID": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.ID; break;
                            case "IM": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.IM; break;
                            case "IO": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.IO; break;
                            case "IP": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.IP; break;
                            case "IR": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.IR; break;
                            case "IS": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.IS; break;
                            case "IT": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.IT; break;
                            case "IV": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.IV; break;
                            case "IZ": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.IZ; break;
                            case "JA": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.JA; break;
                            case "JE": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.JE; break;
                            case "JM": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.JM; break;
                            case "JN": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.JN; break;
                            case "JO": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.JO; break;
                            case "JQ": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.JQ; break;
                            case "KE": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.KE; break;
                            case "KG": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.KG; break;
                            case "KN": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.KN; break;
                            case "KQ": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.KQ; break;
                            case "KR": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.KR; break;
                            case "KS": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.KS; break;
                            case "KT": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.KT; break;
                            case "KU": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.KU; break;
                            case "KZ": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.KZ; break;
                            case "LA": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.LA; break;
                            case "LE": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.LE; break;
                            case "LG": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.LG; break;
                            case "LH": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.LH; break;
                            case "LI": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.LI; break;
                            case "LO": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.LO; break;
                            case "LQ": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.LQ; break;
                            case "LS": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.LS; break;
                            case "LT": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.LT; break;
                            case "LU": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.LU; break;
                            case "LY": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.LY; break;
                            case "MA": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.MA; break;
                            case "MC": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.MC; break;
                            case "MD": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.MD; break;
                            case "MF": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.MF; break;
                            case "MG": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.MG; break;
                            case "MH": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.MH; break;
                            case "MI": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.MI; break;
                            case "MJ": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.MJ; break;
                            case "MK": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.MK; break;
                            case "ML": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.ML; break;
                            case "MN": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.MN; break;
                            case "MO": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.MO; break;
                            case "MP": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.MP; break;
                            case "MQ": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.MQ; break;
                            case "MR": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.MR; break;
                            case "MT": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.MT; break;
                            case "MU": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.MU; break;
                            case "MV": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.MV; break;
                            case "MX": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.MX; break;
                            case "MY": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.MY; break;
                            case "MZ": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.MZ; break;
                            case "NC": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.NC; break;
                            case "NE": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.NE; break;
                            case "NF": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.NF; break;
                            case "NG": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.NG; break;
                            case "NH": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.NH; break;
                            case "NI": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.NI; break;
                            case "NL": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.NL; break;
                            case "NO": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.NO; break;
                            case "NP": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.NP; break;
                            case "NR": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.NR; break;
                            case "NS": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.NS; break;
                            case "NT": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.NT; break;
                            case "NU": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.NU; break;
                            case "NZ": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.NZ; break;
                            case "PA": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.PA; break;
                            case "PC": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.PC; break;
                            case "PE": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.PE; break;
                            case "PF": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.PF; break;
                            case "PG": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.PG; break;
                            case "PK": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.PK; break;
                            case "PL": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.PL; break;
                            case "PM": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.PM; break;
                            case "PO": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.PO; break;
                            case "PP": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.PP; break;
                            case "PS": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.PS; break;
                            case "PU": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.PU; break;
                            case "QA": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.QA; break;
                            case "RM": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.RM; break;
                            case "RO": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.RO; break;
                            case "RP": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.RP; break;
                            case "RQ": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.RQ; break;
                            case "RS": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.RS; break;
                            case "RW": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.RW; break;
                            case "SA": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.SA; break;
                            case "SB": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.SB; break;
                            case "SC": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.SC; break;
                            case "SE": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.SE; break;
                            case "SF": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.SF; break;
                            case "SG": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.SG; break;
                            case "SH": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.SH; break;
                            case "SI": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.SI; break;
                            case "SL": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.SL; break;
                            case "SM": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.SM; break;
                            case "SN": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.SN; break;
                            case "SO": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.SO; break;
                            case "SP": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.SP; break;
                            case "ST": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.ST; break;
                            case "SU": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.SU; break;
                            case "SV": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.SV; break;
                            case "SW": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.SW; break;
                            case "SX": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.SX; break;
                            case "SY": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.SY; break;
                            case "SZ": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.SZ; break;
                            case "TD": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.TD; break;
                            case "TH": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.TH; break;
                            case "TI": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.TI; break;
                            case "TK": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.TK; break;
                            case "TL": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.TL; break;
                            case "TN": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.TN; break;
                            case "TO": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.TO; break;
                            case "TP": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.TP; break;
                            case "TS": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.TS; break;
                            case "TT": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.TT; break;
                            case "TU": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.TU; break;
                            case "TV": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.TV; break;
                            case "TW": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.TW; break;
                            case "TX": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.TX; break;
                            case "TZ": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.TZ; break;
                            case "UG": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.UG; break;
                            case "UK": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.UK; break;
                            case "UP": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.UP; break;
                            case "UV": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.UV; break;
                            case "UY": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.UY; break;
                            case "UZ": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.UZ; break;
                            case "VC": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.VC; break;
                            case "VE": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.VE; break;
                            case "VI": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.VI; break;
                            case "VM": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.VM; break;
                            case "VP": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.VP; break;
                            case "VQ": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.VQ; break;
                            case "VT": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.VT; break;
                            case "WA": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.WA; break;
                            case "WF": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.WF; break;
                            case "WI": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.WI; break;
                            case "WQ": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.WQ; break;
                            case "WS": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.WS; break;
                            case "WZ": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.WZ; break;
                            case "XA": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.XA; break;
                            case "XI": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.XI; break;
                            case "XZ": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.XZ; break;
                            case "YI": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.YI; break;
                            case "YM": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.YM; break;
                            case "ZA": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.ZA; break;
                            case "ZI": foreignAdd_Preparer.Country = IRS_Return_2290.CountryType.ZI; break;
                            #endregion
                        }

                        foreignAdd_Preparer.__CountrySpecified = true;

                        if (Filer_ZIPCode.Length > 0)
                            foreignAdd_Preparer.PostalCode = Filer_ZIPCode;

                        filer.ForeignAddress = foreignAdd_Preparer;
                        #endregion
                    }

                    returnHeaderType.PreparerFirm = PreparerFirm;
                }
                #endregion

                #region CONSENT_TO_VIN_DATA_DISCLOSURE
                IRS_Return_2290.ConsentToVINDataDisclosure consent2VINDataDisclosure = new IRS_Return_2290.ConsentToVINDataDisclosure();

                if (IsConsentToDiscloseYes)
                {
                    consent2VINDataDisclosure.ConsentToDiscloseYes = IRS_Return_2290.CheckboxType.X;

                    IRS_Return_2290.DisclosureFormSignatureInfo disclosureFormSignInfo = new IRS_Return_2290.DisclosureFormSignatureInfo();
                    disclosureFormSignInfo.EIN = Filer_EIN;

                    IRS_Return_2290.BusinessNameType disclosureFormSignInfo_businessAdd = new IRS_Return_2290.BusinessNameType();
                    disclosureFormSignInfo_businessAdd.BusinessNameLine1 = Filer_Name;
                    disclosureFormSignInfo.Name = disclosureFormSignInfo_businessAdd;
                    disclosureFormSignInfo.DateSigned = DateTime.Now.ToUniversalTime();

                    consent2VINDataDisclosure.SignatureOption = IRS_Return_2290.SignatureOption.PIN_Number;
                    consent2VINDataDisclosure.PIN = Officer_PIN;
                    consent2VINDataDisclosure.DisclosureFormSignatureInfo = disclosureFormSignInfo;
                }
                else
                {
                    consent2VINDataDisclosure.ConsentToDiscloseNo = IRS_Return_2290.CheckboxType.X;
                }

                returnHeaderType.ConsentToVINDataDisclosure = consent2VINDataDisclosure;
                #endregion

                returnHeaderType.TaxYear = TaxYear;

                returnHeaderType.binaryAttachmentCount = "0";
                returnHeaderType.SoftwareId = Variables.SoftwareID;
                //returnHeaderType.SoftwareVersion = Variables.SoftwareVersion;
                #endregion

                #region RETURN DATA

                IRS_Return_2290.ReturnData returnData = new IRS_Return_2290.ReturnData();

                #region IRS2290

                IRS_Return_2290.IRS2290 irs2290 = new IRS_Return_2290.IRS2290();

                #region IS APPLICATION FOR....

                if (IsAddressChange)
                    irs2290.AddressChange = IRS_Return_2290.CheckboxType.X;

                if (IsAmendedReturn)
                {
                    irs2290.AmendedReturn = IRS_Return_2290.CheckboxType.X;
                    irs2290.AmendedMonth = AmendedMonth; 
                }

                if (IsVINCorrection)
                    irs2290.VINCorrection = IRS_Return_2290.CheckboxType.X;

                if (IsFinalReturn)
                    irs2290.FinalReturn = IRS_Return_2290.CheckboxType.X;

                #endregion

                irs2290.TaxFromTaxComputation = Convert.ToDecimal(TaxFromTaxComputation.ToString("N2"));
                #region ADDITIONAL TAX
                IRS_Return_2290.AdditionalTax additionalTax = new IRS_Return_2290.AdditionalTax();
                additionalTax.MixedValue = Convert.ToDecimal(AdditionalTaxAmount.ToString("N2"));
                additionalTax.referenceDocumentId = "TGWIncreaseWorksheet";
                additionalTax.referenceDocumentName = "TGWIncreaseWorksheet";
                irs2290.AdditionalTax = additionalTax;
                #endregion
                irs2290.TotalTax = Convert.ToDecimal(TaxFromTaxComputation.ToString("N2")) + Convert.ToDecimal(AdditionalTaxAmount.ToString("N2")); ;
                #region CREDIT AMOUNT
                IRS_Return_2290.CreditsAmount creditAmt = new IRS_Return_2290.CreditsAmount();
                creditAmt.MixedValue = Convert.ToDecimal(CreditAmount.ToString("N2"));
                creditAmt.referenceDocumentId = "CreditsAmountStatement";
                creditAmt.referenceDocumentName = "CreditsAmountStatement";
                irs2290.CreditsAmount = creditAmt;
                #endregion
                irs2290.BalanceDue = (Convert.ToDecimal(TaxFromTaxComputation.ToString("N2")) + Convert.ToDecimal(AdditionalTaxAmount.ToString("N2"))) - Convert.ToDecimal(CreditAmount.ToString("N2"));

                if (irs2290.BalanceDue < 0)
                    irs2290.BalanceDue = 0.00M;

                #region IRS PAYMENT2

                Decimal BalanceDue = irs2290.BalanceDue;
                if (BalanceDue > 0)
                {
                    if (paymentType != null)
                    {
                        if (paymentType == "EFTPS")
                        {
                            irs2290.EFTPSPayment = IRS_Return_2290.CheckboxType.X;
                        }
                        else if (paymentType == "DirectDebit")
                        {
                            IRS_Return_2290.IRSPayment2Collection irsPmt2Collection = new IRS_Return_2290.IRSPayment2Collection();
                            IRS_Return_2290.IRSPayment2 irsPmt2 = new IRS_Return_2290.IRSPayment2();

                            irsPmt2.PaymentAmount = Convert.ToDecimal(BalanceDue.ToString("N2"));
                            irsPmt2.__PaymentAmountSpecified = true;

                            irsPmt2.RoutingTransitNumber = Payment_RoutingTransitNo;
                            irsPmt2.BankAccountNumber = Payment_Acc_No;

                            if (Payment_Acc_Type == "Checking")
                                irsPmt2.AccountType = IRS_Return_2290.BankAccountType._1;
                            else if (Payment_Acc_Type == "Saving")
                                irsPmt2.AccountType = IRS_Return_2290.BankAccountType._2;
                            irsPmt2.__AccountTypeSpecified = true;

                            irsPmt2.RequestedPaymentDate = Convert.ToDateTime(Payment_ReqPayDate);
                            irsPmt2.__RequestedPaymentDateSpecified = true;

                            irsPmt2.TaxpayerDaytimePhone = Payment_Txpyer_Ph;

                            irsPmt2.documentId = "IRSPayment2";
                            //irsPmt2.documentName = "IRSPayment2";
                            irsPmt2.softwareId = Variables.SoftwareID;
                            //irsPmt2.softwareVersion = Variables.SoftwareVersion;

                            irsPmt2Collection.Add(irsPmt2);

                            returnData.IRSPayment2Collection = irsPmt2Collection;
                            documentCount += 1;
                        }
                        else
                        {

                        }
                    }
                }
                #endregion


                #region TAX COMPUTATION

                if (dsTaxComputation.Tables.Count > 0)
                {
                    if (dsTaxComputation.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow ldr in dsTaxComputation.Tables[0].Rows)
                        {
                            IRS_Return_2290.TaxComputation taxComputation = new IRS_Return_2290.TaxComputation();
                            taxComputation.Category = Convert.ToString(ldr["Category"]);

                            IRS_Return_2290.TaxComputationColumnType taxCompColType = new IRS_Return_2290.TaxComputationColumnType();
                            taxCompColType.AmountOfTax = Convert.ToDecimal(ldr["Tax_Amount"]);

                            if (Convert.ToString(ldr["Logging_Partial_Tax"]) != "")
                                taxCompColType.LoggingPartialTax = Convert.ToDecimal(ldr["Logging_Partial_Tax"]);

                            if (Convert.ToString(ldr["Non_Logging_Partial_Tax"]) != "")
                                taxCompColType.NonLoggingPartialTax = Convert.ToDecimal(ldr["Non_Logging_Partial_Tax"]);

                            if (Convert.ToString(ldr["Logging_Vehicle_Count"]) != "0")
                            {
                                if (Convert.ToString(ldr["Logging_Vehicle_Count"]) != "")
                                    taxCompColType.NumberVehicleLogging = Convert.ToString(ldr["Logging_Vehicle_Count"]);
                                if (Convert.ToString(ldr["Logging_Vehicle_Count"]) != "")
                                    numTotalTaxableVeh += Convert.ToInt16(ldr["Logging_Vehicle_Count"]);
                            }
                            if (Convert.ToString(ldr["Non_Logging_Vehicle_Count"]) != "0")
                            {
                                if (Convert.ToString(ldr["Non_Logging_Vehicle_Count"]) != "")
                                    taxCompColType.NumberVehicleNonLogging = Convert.ToString(ldr["Non_Logging_Vehicle_Count"]);
                                if (Convert.ToString(ldr["Non_Logging_Vehicle_Count"]) != "")
                                    numTotalTaxableVeh += Convert.ToInt16(ldr["Non_Logging_Vehicle_Count"]);
                            }

                            taxComputation.ComputationColumns = taxCompColType;
                            irs2290.TaxComputationCollection.Add(taxComputation);
                        }

                        irs2290.TotalAmountOfTax = Convert.ToDecimal(TaxFromTaxComputation.ToString("N2"));

                        irs2290.TotalNumberOfVehicles = numTotalTaxableVeh.ToString();
                    }
                }

                #endregion

                #region SUSPENDED VIN

                if (dsSuspendedVehicles.Tables[0].Rows.Count > 0)
                {
                    IRS_Return_2290.SuspendedVINInfoTypeItemCollection sVINInfoTypeItemCollection = new IRS_Return_2290.SuspendedVINInfoTypeItemCollection();

                    foreach (DataRow ldr_suspended in dsSuspendedVehicles.Tables[0].Rows)
                    {
                        IRS_Return_2290.SuspendedVINInfoTypeItem suspendedVINInfoTypeItem = new IRS_Return_2290.SuspendedVINInfoTypeItem();
                        suspendedVINInfoTypeItem.VIN = Convert.ToString(ldr_suspended["VIN"]);
                        sVINInfoTypeItemCollection.Add(suspendedVINInfoTypeItem);

                        if (!Convert.ToBoolean(ldr_suspended["IsAgricultural"]))
                            irs2290.Checkbox7500Miles = IRS_Return_2290.CheckboxType.X;
                        else if (Convert.ToBoolean(ldr_suspended["IsAgricultural"]))
                            irs2290.Checkbox5000Miles = IRS_Return_2290.CheckboxType.X;

                        if (Convert.ToString(ldr_suspended["IsLogging"]) != "")
                        {
                            if (Convert.ToBoolean(ldr_suspended["IsLogging"]))
                                numTaxSuspended_LoggingVeh += 1;
                            else if (!Convert.ToBoolean(ldr_suspended["IsLogging"]))
                                numTaxSuspended_NonLoggingVeh += 1;
                        }
                        else
                        {
                            numTaxSuspended_NonLoggingVeh += 1;
                        }
                    }

                    IRS_Return_2290.SuspendedVINInfoType suspendedVINInfoType = new IRS_Return_2290.SuspendedVINInfoType();
                    suspendedVINInfoType.ItemCollection = sVINInfoTypeItemCollection;

                    IRS_Return_2290.SuspendedVIN sVIN = new IRS_Return_2290.SuspendedVIN();
                    sVIN.referenceDocumentId = "SuspendedVINStatement";
                    sVIN.referenceDocumentName = "SuspendedVINStatement";

                    if (numTaxSuspended_LoggingVeh != 0)
                        irs2290.NumberTaxSuspendedLoggingVeh = numTaxSuspended_LoggingVeh.ToString();
                    if (numTaxSuspended_NonLoggingVeh != 0)
                        irs2290.NumTaxSuspendedNonLoggingVeh = numTaxSuspended_NonLoggingVeh.ToString();

                    irs2290.SuspendedVIN = sVIN;

                }

                #endregion

                if (IsNotSubjectToTaxChecked)
                    irs2290.NotSubjectToTax = IRS_Return_2290.CheckboxType.X;

                irs2290.softwareId = Variables.SoftwareID;
                //irs2290.softwareVersion = Variables.SoftwareVersion;
                irs2290.documentId = "IRS2290";
                irs2290.documentName = "IRS2290";

                returnData.IRS2290 = irs2290;
                documentCount += 1;
                #endregion

                #region IRS 2290 SCHEDULE1

                IRS_Return_2290.IRS2290Schedule1 irs2290Schedule1 = new IRS_Return_2290.IRS2290Schedule1();


                if (Convert.ToInt16(numTotalTaxableVeh) > 0)
                    irs2290Schedule1.NumberOfTaxableVehicles = numTotalTaxableVeh.ToString();

                int Count_taxSuspendedLogVeh = Convert.ToInt16(numTaxSuspended_LoggingVeh);
                int Count_taxSuspendedNonLogVeh = Convert.ToInt16(numTaxSuspended_NonLoggingVeh);
                if (Count_taxSuspendedLogVeh + Count_taxSuspendedNonLogVeh != 0)
                    irs2290Schedule1.TotalNumberSuspendedVehicles = Convert.ToString(Count_taxSuspendedLogVeh + Count_taxSuspendedNonLogVeh);

                IRS_Return_2290.VehicleReportTaxItemCollection vrtic = new IRS_Return_2290.VehicleReportTaxItemCollection();


                if (dsTaxableVehicles.Tables.Count > 0)
                    foreach (DataRow ldr in dsTaxableVehicles.Tables[0].Rows)
                    {
                        IRS_Return_2290.VehicleReportTaxItem vrti = new IRS_Return_2290.VehicleReportTaxItem();
                        vrti.VehicleCategoryCode = Convert.ToString(ldr["WeightCategory"]);
                        vrti.VIN = Convert.ToString(ldr["VIN"]);
                        vrtic.Add(vrti);
                    }

                if (dsTGWIncreaseWorksheet.Tables.Count > 0)
                    foreach (DataRow ldr in dsTGWIncreaseWorksheet.Tables[0].Rows)
                    {
                        IRS_Return_2290.VehicleReportTaxItem vrti = new IRS_Return_2290.VehicleReportTaxItem();
                        vrti.VehicleCategoryCode = Convert.ToString(ldr["WeightCategoryNew"]);
                        vrti.VIN = Convert.ToString(ldr["VIN"]);
                        vrtic.Add(vrti);
                    }

                irs2290Schedule1.VehicleReportTaxItemCollection = vrtic;

                IRS_Return_2290.VehicleSuspendedTaxItemCollection vstic = new IRS_Return_2290.VehicleSuspendedTaxItemCollection();

                if (dsSuspendedVehicles.Tables.Count > 0)
                    foreach (DataRow ldr in dsSuspendedVehicles.Tables[0].Rows)
                    {
                        IRS_Return_2290.VehicleSuspendedTaxItem vsti = new IRS_Return_2290.VehicleSuspendedTaxItem();
                        vsti.VehicleCategoryCode = "W";
                        vsti.VIN = Convert.ToString(ldr["VIN"]);
                        vstic.Add(vsti);
                    }

                irs2290Schedule1.VehicleSuspendedTaxItemCollection = vstic;

                irs2290Schedule1.documentId = "IRS2290Schedule1";
                irs2290Schedule1.documentName = "IRS2290Schedule1";
                irs2290Schedule1.softwareId = Variables.SoftwareID;
                //irs2290Schedule1.softwareVersion = Variables.SoftwareVersion;

                returnData.IRS2290Schedule1 = irs2290Schedule1;
                documentCount += 1;

                #endregion


                #region CREDITS AMOUNT STATEMENT

                if (dsCreditVehicles.Tables[0].Rows.Count > 0)
                {
                    IRS_Return_2290.CreditsAmountStatementType cast = new IRS_Return_2290.CreditsAmountStatementType();

                    IRS_Return_2290.DisposalReportingItemCollection dric = new IRS_Return_2290.DisposalReportingItemCollection();

                    foreach (DataRow ldr in dsCreditVehicles.Tables[0].Rows)
                    {
                        IRS_Return_2290.DisposalReportingItem dri = new IRS_Return_2290.DisposalReportingItem();
                        dri.CreditsAmountExplanation = Convert.ToString(ldr["Reason"]);
                        dri.DisposalReportingAmount = Convert.ToString(ldr["TaxAmount"]);
                        dri.DisposalReportingDate = Convert.ToDateTime(ldr["EffectiveDate"]);
                        dri.DisposalReportingVIN = Convert.ToString(ldr["VIN"]);

                        dric.Add(dri);
                    }

                    IRS_Return_2290.CreditsAmountInfoType cait = new IRS_Return_2290.CreditsAmountInfoType();
                    cait.DisposalReportingItemCollection = dric;

                    IRS_Return_2290.CreditsAmountInfoTypeCollection caitc = new IRS_Return_2290.CreditsAmountInfoTypeCollection();
                    caitc.Add(cait);

                    IRS_Return_2290.CreditsAmountStatement cas = new IRS_Return_2290.CreditsAmountStatement();
                    cas.CreditsAmountInfoCollection = caitc;

                    cas.documentId = "CreditsAmountStatement";
                    cas.documentName = "CreditsAmountStatement";
                    cas.softwareId = Variables.SoftwareID;
                    //cas.softwareVersion = Variables.SoftwareVersion;

                    IRS_Return_2290.CreditsAmountStatementCollection casc = new IRS_Return_2290.CreditsAmountStatementCollection();
                    casc.Add(cas);

                    returnData.CreditsAmountStatementCollection = casc;
                    documentCount += 1;
                }
                #endregion

                #region STATEMENT IN SUPPORT OF SUSPENSION //Sold Suspended

                if (dsSoldSuspendedVehicles.Tables[0].Rows.Count > 0)
                {
                    IRS_Return_2290.StmtInSupportOfSuspension stmtInsSupportOfSuspension = new IRS_Return_2290.StmtInSupportOfSuspension();

                    IRS_Return_2290.StmtInSupportOfSuspensionInfoType sisosit = new IRS_Return_2290.StmtInSupportOfSuspensionInfoType();

                    IRS_Return_2290.ItemCollection ic = new IRS_Return_2290.ItemCollection();
                    foreach (DataRow ldr in dsSoldSuspendedVehicles.Tables[0].Rows)
                    {
                        IRS_Return_2290.Item i = new IRS_Return_2290.Item();
                        i.Date = Convert.ToDateTime(ldr["DateSold"]);
                        IRS_Return_2290.BusinessNameType bnt = new IRS_Return_2290.BusinessNameType();
                        bnt.BusinessNameLine1 = Convert.ToString(ldr["Buyer"]);
                        i.Name = bnt;
                        i.VIN = Convert.ToString(ldr["VIN"]);
                        ic.Add(i);
                    }
                    sisosit.ItemCollection = ic;

                    IRS_Return_2290.StmtInSupportOfSuspensionInfoTypeCollection sisostc = new IRS_Return_2290.StmtInSupportOfSuspensionInfoTypeCollection();
                    sisostc.Add(sisosit);

                    IRS_Return_2290.StmtInSupportOfSuspensionType sisost = new IRS_Return_2290.StmtInSupportOfSuspensionType();
                    sisost.StmtInSupportOfSuspensionInfoCollection = sisostc;

                    IRS_Return_2290.StmtInSupportOfSuspensionCollection sisosc = new IRS_Return_2290.StmtInSupportOfSuspensionCollection();
                    sisosc.Add(stmtInsSupportOfSuspension);

                    stmtInsSupportOfSuspension.documentId = "StatementInSupportOfSuspension";
                    stmtInsSupportOfSuspension.documentName = "StatementInSupportOfSuspension";
                    stmtInsSupportOfSuspension.softwareId = Variables.SoftwareID;
                    //stmtInsSupportOfSuspension.softwareVersion = Variables.SoftwareVersion;

                    returnData.StmtInSupportOfSuspensionCollection = sisosc;
                    documentCount += 1;
                }
                #endregion

                #region SUSPENDED VIN STATEMENT

                if (dsPriorYearMileageExceededVehicles.Tables[0].Rows.Count > 0)
                {
                    IRS_Return_2290.SuspendedVINStatement suspendedVINStmt = new IRS_Return_2290.SuspendedVINStatement();

                    IRS_Return_2290.SuspendedVINInfoTypeCollection svitc = new IRS_Return_2290.SuspendedVINInfoTypeCollection();
                    IRS_Return_2290.SuspendedVINInfoType svit = new IRS_Return_2290.SuspendedVINInfoType();

                    IRS_Return_2290.SuspendedVINInfoTypeItemCollection svitic = new IRS_Return_2290.SuspendedVINInfoTypeItemCollection();
                    foreach (DataRow ldr in dsPriorYearMileageExceededVehicles.Tables[0].Rows)
                    {
                        IRS_Return_2290.SuspendedVINInfoTypeItem sviti = new IRS_Return_2290.SuspendedVINInfoTypeItem();
                        sviti.VIN = Convert.ToString(ldr["VIN"]);
                        svitic.Add(sviti);
                    }
                    svit.ItemCollection = svitic;
                    svitc.Add(svit);

                    suspendedVINStmt.SuspendedVINInfoCollection = svitc;
                    suspendedVINStmt.documentId = "SuspendedVINStatement";
                    suspendedVINStmt.documentName = "SuspendedVINStatement";
                    suspendedVINStmt.softwareId = Variables.SoftwareID;
                    //suspendedVINStmt.softwareVersion = Variables.SoftwareVersion;

                    IRS_Return_2290.SuspendedVINStatementCollection svsc = new IRS_Return_2290.SuspendedVINStatementCollection();
                    svsc.Add(suspendedVINStmt);

                    returnData.SuspendedVINStatementCollection = svsc;
                    documentCount += 1;
                }
                #endregion

                #region TGW INCREASE WORKSHEET

                if (dsTGWIncreaseWorksheet.Tables[0].Rows.Count > 0)
                {
                    IRS_Return_2290.TGWIncreaseWorksheet tgwIncreaseWorksheet = new IRS_Return_2290.TGWIncreaseWorksheet();

                    IRS_Return_2290.TGWIncreaseInfoTypeCollection tgwiitc = new IRS_Return_2290.TGWIncreaseInfoTypeCollection();

                    foreach (DataRow ldr in dsTGWIncreaseWorksheet.Tables[0].Rows)
                    {
                        IRS_Return_2290.TGWIncreaseInfoType tgwiit = new IRS_Return_2290.TGWIncreaseInfoType();

                        tgwiit.AdditionalTaxAmount = Convert.ToDecimal(ldr["TaxAmount"]);
                        tgwiit.MonthOfTGWIncrease = Convert.ToString(ldr["IncreasedMonthValue"]);
                        tgwiit.NewTaxAmount = Convert.ToDecimal(ldr["NewTaxAmount"]);
                        tgwiit.PreviousTaxAmount = Convert.ToDecimal(ldr["PreviousTaxAmount"]);
                        tgwiit.TGWCategoryCode = Convert.ToString(ldr["WeightCategoryNew"]);
                        tgwiitc.Add(tgwiit);
                    }

                    tgwIncreaseWorksheet.TGWIncreaseInfoCollection = tgwiitc;

                    tgwIncreaseWorksheet.documentId = "TGWIncreaseWorksheet";
                    tgwIncreaseWorksheet.documentName = "TGWIncreaseWorksheet";
                    tgwIncreaseWorksheet.softwareId = Variables.SoftwareID;
                    //tgwIncreaseWorksheet.softwareVersion = Variables.SoftwareVersion;

                    IRS_Return_2290.TGWIncreaseWorksheetCollection tgwiwc = new IRS_Return_2290.TGWIncreaseWorksheetCollection();
                    tgwiwc.Add(tgwIncreaseWorksheet);

                    returnData.TGWIncreaseWorksheetCollection = tgwiwc;
                    documentCount += 1;
                }
                #endregion

                //returnData.documentCount = documentCount.ToString();

                #endregion

                #region RETURN

                IRS_Return_2290.Return return2290 = new IRS_Return_2290.Return();
                return2290.ReturnData = returnData;
                return2290.ReturnHeader = returnHeaderType;
                return2290.returnVersion = Variables.SoftwareVersion;

                #endregion

                #region Create XML file

                IFormatter ft = new BinaryFormatter();

                ft.Serialize(mStream, return2290);

                mStream.Position = 0;

                IRS_Return_2290.Return irsForm2 = (IRS_Return_2290.Return)ft.Deserialize(mStream);

                s.Serialize(fStream2, irsForm2);

                if (mStream != null) mStream.Close();

                if (fStream2 != null) fStream2.Close();

                if (fStream != null) fStream.Close();

                try
                {
                    XmlDocument xdoc = new XmlDocument();
                    xdoc.PreserveWhitespace = false;
                    xdoc.Load("C:\\xml\\submission.xml");
                    xdoc.DocumentElement.SetAttribute("xmlns:efile", "http://www.irs.gov/efile");
                    XmlTextWriter xmlWriter = new XmlTextWriter("C:\\" + @"\xml\submission.xml", new UTF8Encoding(false));
                    xdoc.Save(xmlWriter);
                    xmlWriter.Flush();
                    xmlWriter.Close();
                }
                catch { }

                #endregion

            }
            catch (Exception ex)
            {
                if (fStream != null) fStream.Close();
                if (fStream2 != null) fStream2.Close();
                if (mStream != null) mStream.Close();
                throw ex;
            }
        }

        #endregion

        #region Create Manifest XML

        private void createManifestXML(string TaxYear, DateTime TaxPeriodEndDate, DateTime TaxPeriodBeginDate, string EIN, string SubmissionType)
        {
            if (!Directory.Exists("C:\\" + @"\manifest"))
                Directory.CreateDirectory("C:\\" + @"\manifest");

            XmlTextWriter xmlWriter = new XmlTextWriter("C:\\" + @"\manifest\manifest.xml", new UTF8Encoding(false));

            xmlWriter.WriteStartDocument(true);

            xmlWriter.WriteStartElement("IRSSubmissionManifest");

            xmlWriter.WriteAttributeString("xmlns", "http://www.irs.gov/efile");

            xmlWriter.WriteAttributeString("xmlns:efile", "http://www.irs.gov/efile");

            //------------

            xmlWriter.WriteStartElement("SubmissionId");

            submissionID = "";

            BaseMethods BM = new BaseMethods();
            submissionID = Variables.EFIN + DateTime.Now.Year.ToString() + BM.getJulianDayOfYear() + BM.CreateRandomCode(7);

            xmlWriter.WriteString(submissionID);

            xmlWriter.WriteEndElement();

            xmlWriter.WriteStartElement("EFIN");

            xmlWriter.WriteString(Variables.EFIN);

            xmlWriter.WriteEndElement();

            xmlWriter.WriteStartElement("TaxYr");
            
            xmlWriter.WriteString(TaxYear);

            xmlWriter.WriteEndElement();

            xmlWriter.WriteStartElement("GovernmentCd");

            xmlWriter.WriteString("IRS");

            xmlWriter.WriteEndElement();

            xmlWriter.WriteStartElement("FederalSubmissionTypeCd");

            xmlWriter.WriteString(SubmissionType);

            xmlWriter.WriteEndElement();

            xmlWriter.WriteStartElement("TaxPeriodBeginDt");
            xmlWriter.WriteString(TaxPeriodBeginDate.ToString("yyyy-MM-dd"));

            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("TaxPeriodEndDt");
            xmlWriter.WriteString(TaxPeriodEndDate.ToString("yyyy-MM-dd"));

            xmlWriter.WriteEndElement();

            xmlWriter.WriteStartElement("TIN");

            xmlWriter.WriteString(EIN);

            xmlWriter.WriteEndElement();

            xmlWriter.WriteEndElement();

            xmlWriter.WriteEndDocument();

            xmlWriter.Flush();

            xmlWriter.Close();


        }

        public string createManifestXMLFile(string TaxYear, DateTime TaxPeriodEndDate, DateTime TaxPeriodBeginDate, string EIN, string SubmissionType)
        {
            string temp_submission_id = "";

            if (!Directory.Exists("C:\\" + @"\manifest"))
                Directory.CreateDirectory("C:\\" + @"\manifest");

            XmlTextWriter xmlWriter = new XmlTextWriter("C:\\" + @"\manifest\manifest.xml", new UTF8Encoding(false));
            xmlWriter.WriteStartDocument(true);
            xmlWriter.WriteStartElement("IRSSubmissionManifest");
            xmlWriter.WriteAttributeString("xmlns", "http://www.irs.gov/efile");
            xmlWriter.WriteAttributeString("xmlns:efile", "http://www.irs.gov/efile");

            xmlWriter.WriteStartElement("SubmissionId");

            BaseMethods BM = new BaseMethods();
            temp_submission_id = Variables.EFIN + DateTime.Now.Year.ToString() + BM.getJulianDayOfYear() + BM.CreateRandomCode(7);

            xmlWriter.WriteString(temp_submission_id);
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("EFIN");
            xmlWriter.WriteString(Variables.EFIN);
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("TaxYr");
            xmlWriter.WriteString(TaxYear);
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("GovernmentCd");
            xmlWriter.WriteString("IRS");
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("FederalSubmissionTypeCd");
            xmlWriter.WriteString(SubmissionType);
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("TaxPeriodBeginDt");
            xmlWriter.WriteString(TaxPeriodBeginDate.ToString("yyyy-MM-dd"));
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("TaxPeriodEndDt");
            xmlWriter.WriteString(TaxPeriodEndDate.ToString("yyyy-MM-dd"));
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("TIN");
            xmlWriter.WriteString(EIN);
            xmlWriter.WriteEndElement();
            xmlWriter.WriteEndElement();
            xmlWriter.WriteEndDocument();
            xmlWriter.Flush();
            xmlWriter.Close();

            return temp_submission_id;
        }

        #endregion

        static string createZip()
        {
            try
            {
                string SavePath = @"C:\";

                ZipFile TransmissionZip = ZipFile.Create(SavePath + "SendSubmission" + submissionID + ".zip");//name verification pending
                ZipFile SubmissionZip = ZipFile.Create(SavePath + submissionID + ".zip");
                SubmissionZip.BeginUpdate();
                SubmissionZip.Add(SavePath + "\\manifest\\manifest.xml");
                SubmissionZip.Add(SavePath + "\\xml\\Submission.xml");
                SubmissionZip.CommitUpdate();
                SubmissionZip.Close();

                File.Delete(path2CreateFile_4SendSubmission + @"\manifest\manifest.xml");
                File.Delete(path2CreateFile_4SendSubmission + @"\xml\Submission.xml");
                Directory.Delete(path2CreateFile_4SendSubmission + @"\manifest");
                Directory.Delete(path2CreateFile_4SendSubmission + @"\xml");
                TransmissionZip.BeginUpdate();
                TransmissionZip.Add(path2CreateFile_4SendSubmission + submissionID + ".zip", CompressionMethod.Stored);
                TransmissionZip.CommitUpdate();
                TransmissionZip.Close();
                File.Delete(path2CreateFile_4SendSubmission + submissionID + ".zip");

                return "Success";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public void MoveFiles(string SubmissionId, string ReferenceNumber)
        {
            try
            {
                if (!Directory.Exists("C:\\Live Submissions\\2290\\" + ReferenceNumber))
                    Directory.CreateDirectory("C:\\Live Submissions\\2290\\" + ReferenceNumber);

                string SubmissionFileName = "", ReturnSubmissionFileName = "", GetAckFileName = "", GetSchedule1FileName = "",
                    PathToSave = "C:\\Live Submissions\\2290\\" + ReferenceNumber + "\\";

                SubmissionFileName = "SendSubmission" + SubmissionId + ".zip";
                ReturnSubmissionFileName = "RetrunSubmission" + SubmissionId + ".zip";
                GetAckFileName = SubmissionId + "-Ack.zip";
                GetSchedule1FileName = SubmissionId + "-Sch1.zip";

                if (File.Exists("C:\\" + SubmissionFileName)
                    && File.Exists("C:\\" + ReturnSubmissionFileName)
                    && File.Exists("C:\\" + GetAckFileName)
                    && File.Exists("C:\\" + GetSchedule1FileName)
                    && File.Exists("C:\\" + SubmissionId + ".pdf"))
                {
                    File.Move("C:\\" + SubmissionFileName, PathToSave + SubmissionFileName);
                    File.Move("C:\\" + ReturnSubmissionFileName, PathToSave + ReturnSubmissionFileName);
                    File.Move("C:\\" + GetAckFileName, PathToSave + GetAckFileName);
                    File.Move("C:\\" + GetSchedule1FileName, PathToSave + GetSchedule1FileName);
                    File.Move("C:\\" + SubmissionId + ".pdf", PathToSave + SubmissionId + ".pdf");
                }
                else if (File.Exists("C:\\" + SubmissionFileName)
                    && File.Exists("C:\\" + ReturnSubmissionFileName)
                    && File.Exists("C:\\" + GetAckFileName))
                {
                    File.Move("C:\\" + SubmissionFileName, PathToSave + SubmissionFileName);
                    File.Move("C:\\" + ReturnSubmissionFileName, PathToSave + ReturnSubmissionFileName);
                    File.Move("C:\\" + GetAckFileName, PathToSave + GetAckFileName);
                }
                else if (File.Exists("C:\\" + GetSchedule1FileName) && File.Exists("C:\\" + SubmissionId + ".pdf"))
                {
                    File.Move("C:\\" + GetSchedule1FileName, PathToSave + GetSchedule1FileName);
                    File.Move("C:\\" + SubmissionId + ".pdf", PathToSave + SubmissionId + ".pdf");
                }
                else if (File.Exists("C:\\" + SubmissionFileName) && File.Exists("C:\\" + ReturnSubmissionFileName))
                {
                    File.Move("C:\\" + SubmissionFileName, PathToSave + SubmissionFileName);
                    File.Move("C:\\" + ReturnSubmissionFileName, PathToSave + ReturnSubmissionFileName);
                }
                else if (File.Exists("C:\\" + SubmissionFileName))
                {
                    File.Move("C:\\" + SubmissionFileName, PathToSave + SubmissionFileName);
                }

            }
            catch (Exception ex)
            {
                WriteLog.Log(ex.Message, EventLogEntryType.Error);
            }
        }
    }
}