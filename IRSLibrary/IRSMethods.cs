using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Web;
using System.Web.Services;
using System.Web.SessionState;
using MeFTransmitterServiceWse = IRSLibrary.MeFTransmitterServiceWse;
using Microsoft.Web.Services3;
using Microsoft.Web.Services3.Mime;
using Microsoft.Web.Services3.Security;
using Microsoft.Web.Services3.Security.Tokens;
using Microsoft.Web.Services3.Security.X509;
using ICSharpCode.SharpZipLib.Zip;
using IRSLibrary.MefMsiServices;
using IRSIApplication;
using CoreAPI;
using CoreAPI.SQLServer;

namespace IRSLibrary
{
    public class IRSMethods : MarshalByRefObject,IMethods
    {
        BaseMethods BM = new BaseMethods();
        protected DBService DB;

        public IRSMethods()
        {
            try
            {
                DB = new DBService(Variables.DBConnectionString);
                CommonObjects.gDB = DB;
            }
            catch (Exception ex)
            {
            }
        }

        #region IRS Service functions

        public string SendSubmission(ref DataTable dt)
        {
            return "";
        }

        public string GetSingleAcknowledgements(ref DataRow dr)
        {
            try
            {
                string Result=null;
            
                Result = BM._GetAck(ref dr);

                if (Result.Contains("Login Failure"))
                    return Result;

                if (Result.Contains("Rejected"))
                {
                    dr["Status"] = "V";
                    dr["RejectionErrorCodes"] = Result.Substring(9, Result.Length - 9).Replace('~', ',');
                }
                else if (Result.Contains("Accepted"))
                {
                    dr["Status"] = "A";
                }
 
                if (!dr.IsNull("Status"))
                {
                    DataTable dt = new DataTable();
                    dt.Columns.Add(new DataColumn("ReferenceNumber"));
                    dt.Columns.Add(new DataColumn("SubmissionID"));
                    dt.Columns.Add(new DataColumn("FormType"));
                    dt.Columns.Add(new DataColumn("TaxId"));
                    dt.Columns.Add(new DataColumn("FormCode"));
                    dt.Columns.Add(new DataColumn("Status"));
                    dt.Columns.Add(new DataColumn("FormKey"));
                    dt.Columns.Add(new DataColumn("StatusDate"));
                    dt.Columns.Add(new DataColumn("RejectionErrorCodes"));
                    dt.Columns.Add(new DataColumn("ProfileName"));
                    dt.Columns.Add(new DataColumn("FirstName"));
                    dt.Columns.Add(new DataColumn("Email"));
                    dt.Columns.Add(new DataColumn("FormSubCategory"));
                    dt.Columns.Add(new DataColumn("MailStatus"));
                    dt.AcceptChanges();
                    dt.Rows.Add(
                             Convert.ToInt32(dr["ReferenceNumber"]),
                             dr["SubmissionID"].ToString(),
                             dr["FormType"].ToString(),
                             dr["TaxId"].ToString(),
                             dr["FormCode"].ToString(),
                             dr["Status"].ToString(),
                             dr["FormKey"].ToString(),
                             CoreSafeNull.GetSafeNullString((object)dr["StatusDate"]).ToString(),
                             CoreSafeNull.GetSafeNullString((object)dr["RejectionErrorCodes"]).ToString(),
                             dr["ProfileName"].ToString(),
                             dr["FirstName"].ToString(),
                             dr["Email"].ToString(),
                             CoreSafeNull.GetSafeNullString((object)dr["FormSubCategory"]).ToString(),
                             CoreSafeNull.GetSafeNullString((object)dr["MailStatus"]).ToString());

                    dt.AcceptChanges();
                    SaveSubmissionResult(dt, Convert.ToString(dr["BatchNo"]), Convert.ToString(dr["Status"]));
                }

                return "Success";
               
            }
            catch
            {
                return "Exception";
            }
        }

        public string GetBulkAcknowledgements(ref string count)
        {
            try
            {
                string Result=null;

                if (BaseMethods.Req_Login.RequestSoapContext.Security.Tokens.Count == 0)
                {
                    Result = BM._Login();
                }

                if (Result != "Success" && Result != null)
                {
                    return "Login Failure";
                }


                Result = BM._GetNewAcks(ref count);

                return Result;
            }
            catch
            {
                return "Exception";
            }
        }
        
        public string Login()
        {
            try
            {
                string Result;
                Result = BM._Login();
                return Result;
            }
            catch
            {
                return "Exception";
            }
        
        }
        
        public string Logout()
        {
            try
            {
                string Result;
                Result = BM._Logout();
                return Result;
            }
            catch
            {
                return "Exception";
            }
        }

        #endregion

        #region BO Operation functions

        private void SaveSubmissionResult(DataTable dt, string BatchNo,string Status)
        {
            try
            {
                foreach (DataRow dr in dt.Rows)
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandText = "usp_SubModule_SaveSubmissionDetails";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@ref_no", Convert.ToString(dr["ReferenceNumber"]));
                    cmd.Parameters.AddWithValue("@SubmissionID", Convert.ToString(dr["SubmissionID"]));
                    cmd.Parameters.AddWithValue("@SubmissionType", Convert.ToString(dr["FormType"]));
                    cmd.Parameters.AddWithValue("@SubmissionStatus", Convert.ToString(Status));
                    cmd.Parameters.AddWithValue("@BatchNo", BatchNo);
                    cmd.Parameters.AddWithValue("@RejectionErrorCodes", Convert.ToString(dr["RejectionErrorCodes"]));
                    cmd.Parameters.AddWithValue("@StatusDate", Convert.ToString(dr["StatusDate"]));
                    DB.BeginTransaction();
                    DB.ExecuteNonQuery(cmd);
                    DB.CommitTransaction();
                }

            }
            catch
            { 
            
            }
        }

        public bool SendMail(string pTo, string pCC, string bCC, string pSubject, string pBody)
        {
            return true;
        }
        
        public DataTable GetLatestRecords(ref string clsRP)
        {
            DataTable functionReturnValue = default(DataTable);
            CoreRemoteParameters RP = new CoreRemoteParameters();
            functionReturnValue = null;
            try
            {
                RP.Deserialize(clsRP);

                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = RP["CommandText"].ToString();
                cmd.CommandType = CommandType.StoredProcedure;
                functionReturnValue = DB.GetDataTable(cmd, RP["TableName"].ToString());
                
                if (functionReturnValue != null)
                {
                    functionReturnValue.Namespace = "";
                }
                clsRP = RP.ToString();
            }
            catch (Exception ex)
            {
                RP.ErrorOccurred = true;
                clsRP = RP.ToString();
            }
            return functionReturnValue;
        }

        public int UpdateMailSentStatus(ref string clsRP)
        {
            CoreRemoteParameters RP = new CoreRemoteParameters();
            try
            {
                RP.Deserialize(clsRP);
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = RP["CommandText"].ToString();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@refno", RP["refno"].ToString());
                cmd.Parameters.AddWithValue("@MailStatus", RP["MailStatus"].ToString());
                clsRP = RP.ToString();
                DB.BeginTransaction();
                int i=DB.ExecuteNonQuery(cmd);
                DB.CommitTransaction();
                return i;
            }
            catch
            {
                RP.ErrorOccurred = true;
                clsRP = RP.ToString();
                return 0;
            }
        }

        public string CheckAuthentication(ref string clsRP)
        {
            CoreRemoteParameters RP = new CoreRemoteParameters();
            try
            {
                RP.Deserialize(clsRP);
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "usp_SubModule_CheckLogin";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@uid", RP["uid"].ToString());
                cmd.Parameters.AddWithValue("@pwd", RP["pwd"].ToString());
                clsRP = RP.ToString();
                return DB.ExecuteScalar(cmd).ToString();
            }
            catch
            {
                RP.ErrorOccurred = true;
                clsRP = RP.ToString();
                return null;
            }
        }

        public DataTable Search(ref string clsRP)
        {
            CoreRemoteParameters RP = new CoreRemoteParameters();
            try
            {
                RP.Deserialize(clsRP);
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "usp_SubModule_Search";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Category", RP["Category"].ToString());
                cmd.Parameters.AddWithValue("@Value", RP["Value"].ToString());
                clsRP = RP.ToString();
                return DB.GetDataTable(cmd,"SearchResult");
            }
            catch
            {
                RP.ErrorOccurred = true;
                clsRP = RP.ToString();
                return null;
            }
        }

        public DataSet DisplaySubmissionData(ref string clsRP)
        {
            CoreRemoteParameters RP = new CoreRemoteParameters();
            try
            {
                DataSet lds = new DataSet();
                RP.Deserialize(clsRP);
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "usp_SubModule_CollectSubmissionData";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@TaxId", RP["TaxId"].ToString());
                cmd.Parameters.AddWithValue("@FormKey", RP["FormKey"].ToString());
                cmd.Parameters.AddWithValue("@FormType", RP["FormType"].ToString());
                lds.Tables.Add(DB.GetDataTable(cmd,"Data"));

                cmd.CommandText = "usp_SubModule_GetError_RulesNumbers";
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("@RefNo", RP["RefNumber"].ToString());
                cmd.Parameters.AddWithValue("@FormType", RP["FormType"].ToString());
                lds.Tables.Add(DB.GetDataTable(cmd,"RuleNumbers"));

                if (RP["FormType"].ToString() == "7004")
                {
                    cmd.CommandText = "usp_SubModule_Collect7004MemberCompanyData";
                    cmd.Parameters.Clear();
                    cmd.Parameters.AddWithValue("@TaxId", RP["TaxId"].ToString());
                    lds.Tables.Add(DB.GetDataTable(cmd, "7004MemberData"));
                }

                clsRP = RP.ToString();
                return lds;
            }
            catch
            {
                RP.ErrorOccurred = true;
                clsRP = RP.ToString();
                return null;
            }
        }

        public DataSet NotifyUser(ref string clsRP)
        {
            CoreRemoteParameters RP = new CoreRemoteParameters();
            DataSet lds = new DataSet();
            try
            {
                RP.Deserialize(clsRP);
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "usp_SubModule_Cust_Notify_Operations";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@FormType", RP["FormType"].ToString());
                cmd.Parameters.AddWithValue("@TaxId", RP["TaxId"].ToString());
                cmd.Parameters.AddWithValue("@FormKey", RP["FormKey"].ToString());
                cmd.Parameters.AddWithValue("@RefNumber", RP["RefNumber"].ToString());
                if (RP["AllowResubmission"].ToString() == "True")
                    cmd.Parameters.AddWithValue("@AllowResubmission","True");
                              
                if(RP["FormCategory"].ToString()!="")
                {
                int num;
                bool isNumeric = int.TryParse(Convert.ToString(RP["FormCategory"]), out num);
                if (isNumeric)
                {
                    cmd.Parameters.AddWithValue("@FormCategory", RP["FormCategory"].ToString());
                }
                }

                lds = DB.GetDataSet(cmd);
                clsRP = RP.ToString();
            }
            catch
            {
                RP.ErrorOccurred = true;
                clsRP = RP.ToString();
                lds = null;
            }
            return lds;        
        }

        public string LockRecord(ref string clsRP)
        {
            CoreRemoteParameters RP = new CoreRemoteParameters();
            try
            {
                RP.Deserialize(clsRP);
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "usp_SubModule_LockRecord";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@RefNumber", RP["RefNumber"].ToString());
                string result = DB.ExecuteScalar(cmd).ToString();
                clsRP = RP.ToString();
                return result;
            }
            catch
            {
                RP.ErrorOccurred = true;
                clsRP = RP.ToString();
                return "Error"; 
            }
        
        }

        public int UnLockRecord(ref string clsRP)
        {
            CoreRemoteParameters RP = new CoreRemoteParameters();
            try
            {
                RP.Deserialize(clsRP);
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "usp_SubModule_UnLockRecord";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@RefNumber", RP["RefNumber"].ToString());
                DB.BeginTransaction();
                int i = DB.ExecuteNonQuery(cmd);
                DB.CommitTransaction();
                clsRP = RP.ToString();
                return i;
            }
            catch
            {
                RP.ErrorOccurred = true;
                clsRP = RP.ToString();
                return 0;
            }

        }

        public int BlockRecord(ref string clsRP)
        {
            CoreRemoteParameters RP = new CoreRemoteParameters();
            try
            {
                RP.Deserialize(clsRP);
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "usp_SubModule_BlockRecord";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@RefNumber", RP["ReferenceNumber"].ToString());
                DB.BeginTransaction();
                int i = DB.ExecuteNonQuery(cmd);
                DB.CommitTransaction();
                clsRP = RP.ToString();
                return i;
            }
            catch
            {
                RP.ErrorOccurred = true;
                clsRP = RP.ToString();
                return 0;
            }
        
        }
        #endregion
    }
}