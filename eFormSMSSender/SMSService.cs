﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Xml;

using System.Security.Cryptography.X509Certificates;
using System.Security.Authentication;

namespace eFormSMSSender
{
    public class SMSService
    {
       
        public static string Uid = Settings.Default.smsuid;
        public static string Pwd = Settings.Default.smspwd;

        public static string SendSingleSMS(string pApplicationName, string pMobNo, string pSubject, string pMessage, bool pIsExpress)
        {
            try
            {
                //return "-10";
                //const SslProtocols _Ssl3 = SslProtocols.Tls;
                const SslProtocols _Tls12 = (SslProtocols)0x00000C00; //Tls12
                const SecurityProtocolType Tls12 = (SecurityProtocolType)_Tls12;
                ServicePointManager.SecurityProtocol = Tls12;

                //client.SSLConfiguration.EnabledSslProtocols = SslProtocols.Tls11; // Tls123
                //client.SSLConfiguration.EnabledSslProtocols = SslProtocols.Tls; // TLS 1.0
                //client.SSLConfiguration.EnabledSslProtocols = SslProtocols.Ssl3;


                string ret = string.Empty;
                WebRequest w = WebRequest.Create("https://app.eztexting.com/api/sending/");
                w.Method = "POST";
                w.ContentType = "application/x-www-form-urlencoded";

                using (Stream writeStream = w.GetRequestStream())
                {
                    /*
                     *byte[] bytes = Encoding.UTF8.GetBytes(
                        "user=" + SMSService.Uid + "&pass=" + SMSService.Pwd + "&phonenumber=" + pMobNo + "&subject=" + pSubject + "&message=" + pMessage + "&express=" + pIsExpress); 
                    */
                    byte[] bytes = Encoding.UTF8.GetBytes(
                        "user=" + SMSService.Uid + "&pass=" + SMSService.Pwd + "&phonenumber=" + pMobNo + "&message=" + pMessage + "&express=" + pIsExpress);
                    writeStream.Write(bytes, 0, bytes.Length);
                }

                using (HttpWebResponse r = (HttpWebResponse)w.GetResponse())
                {
                    using (Stream responseStream = r.GetResponseStream())
                    {
                        using (StreamReader readStream = new StreamReader(responseStream, Encoding.UTF8))
                        {
                            ret = readStream.ReadToEnd();
                        }
                    }
                }

                string returnVal = GetReturnMessage(ret);

                if (ret == "1")
                    LogIt(pApplicationName, pMobNo, pSubject, pMessage);
                else
                    Logger.Write("", pApplicationName, returnVal);

                return returnVal; /* result of API call*/
            }
            catch (Exception ex)
            {
                Logger.Write("",pApplicationName, ex.ToString());
                return "error";
            }
        }

        public static string SendMultipleSMS(string pApplicationName, string[] pMobile, string[] pSubject, string[] pMsg)
        {
            try
            {
                WebClient client = new WebClient();
                string ret = string.Empty;
                string ret1 = string.Empty;
                for (int i = 0; i < pMobile.Length; i++)
                {
                    WebRequest w = WebRequest.Create("https://app.eztexting.com/api/sending/");
                    w.Method = "POST";
                    w.ContentType = "application/x-www-form-urlencoded";
                    using (Stream writeStream = w.GetRequestStream())
                    {
                        /*
                         *
                         * byte[] bytes = Encoding.UTF8.GetBytes("user=" + Uid + "&pass=" + Pwd + "&phonenumber=" + pMobile[i].ToString() +
                            "&subject=" + pSubject[i].ToString() + "&message=" + pMsg[i].ToString() + "&express=1");
                        */
                        byte[] bytes = Encoding.UTF8.GetBytes("user=" + Uid + "&pass=" + Pwd + "&phonenumber=" + pMobile[i].ToString() +
                            "&message=" + pMsg[i].ToString() + "&express=1");
                        writeStream.Write(bytes, 0, bytes.Length);
                    }

                    using (HttpWebResponse r = (HttpWebResponse)w.GetResponse())
                    {
                        using (Stream responseStream = r.GetResponseStream())
                        {
                            using (StreamReader readStream = new StreamReader(responseStream, Encoding.UTF8))
                            {
                                ret1 = readStream.ReadToEnd();
                            }
                        }
                    }

                    string returnVal = GetReturnMessage(ret);

                    if (ret1 == "1")
                        LogIt(pApplicationName, pMobile[i].ToString(), pSubject[i].ToString(), pMsg[i].ToString());
                    else
                        Logger.Write("", pApplicationName, returnVal);


                    ret += GetReturnMessage(ret1); /* result of API call*/
                } 
                
                return ret;
            }
            catch (Exception ex)
            {
                Logger.Write("", pApplicationName, ex.ToString());
                return "error";
            }
        }

        public static string ChkCreditBalance()
        {
            string ret = string.Empty;

            WebRequest w = WebRequest.Create("https://app.eztexting.com/api/credits/check/");

            w.Method = "POST";

            w.ContentType = "application/x-www-form-urlencoded";
            using (Stream writeStream = w.GetRequestStream())
            {
                byte[] bytes = Encoding.UTF8.GetBytes("User=" + Uid + "&Pass=" + Pwd);
                writeStream.Write(bytes, 0, bytes.Length);
            }

            using (HttpWebResponse r = (HttpWebResponse)w.GetResponse())
            {
                using (Stream responseStream = r.GetResponseStream())
                {
                    using (StreamReader readStream = new StreamReader(responseStream, Encoding.UTF8))
                    {
                        ret = readStream.ReadToEnd();
                    }
                }
            }

            return ret; /* result of API call*/

        }

        public static string CheckCredit()
        {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;

            bool isSuccesResponse = true;
            string ret = string.Empty;
            WebRequest w = WebRequest.Create("https://app.eztexting.com/billing/credits/get?format=xml&User=" + Uid + "&Password=" + Pwd);

            try
            {
                using (HttpWebResponse r = (HttpWebResponse)w.GetResponse())
                {
                    ret = GetResponseString(r);
                }
            }
            catch (System.Net.WebException ex)
            {
                isSuccesResponse = false;
                if (ex.Status == WebExceptionStatus.ProtocolError)
                {
                    ret = GetResponseString(ex.Response);
                }
            }
            XmlDocument response = new XmlDocument();
            response.LoadXml(ret);
           // System.Console.Out.WriteLine("Status: " + response.SelectSingleNode("Response/Status").InnerText);
          //  System.Console.Out.WriteLine("Code: " + response.SelectSingleNode("Response/Code").InnerText);
            if (isSuccesResponse)
            {
             //   System.Console.Out.WriteLine("Plan credits: " + response.SelectSingleNode("Response/Entry/PlanCredits").InnerText);
             //   System.Console.Out.WriteLine("Anytime credits: " + response.SelectSingleNode("Response/Entry/AnytimeCredits").InnerText);
              //  System.Console.Out.WriteLine("Total: " + response.SelectSingleNode("Response/Entry/TotalCredits").InnerText);
                return response.SelectSingleNode("Response/Entry/TotalCredits").InnerText;
            }
            else
            {
                return null;
               // System.Console.Out.WriteLine("Errors: " + ImplodeList(response.SelectNodes("Response/Errors/*")));
            }
           
        }

        static string ImplodeList(XmlNodeList list)
        {
            string ret = "";
            foreach (XmlNode node in list)
            {
                ret += ", " + node.InnerText;
            }
            return ret.Length > 2 ? ret.Substring(2) : ret;
        }

        static string GetResponseString(WebResponse response)
        {
            using (Stream responseStream = response.GetResponseStream())
            {
                using (StreamReader readStream = new StreamReader(responseStream, Encoding.UTF8))
                {
                    return readStream.ReadToEnd();
                }
            }
        }
   
        private static void LogIt(string pAppName, string pMobile, string pSubject, string pMsg)
        {
            try
            {
                string lFilePath = GetFileName();

                XmlDocument xlogDoc = new XmlDocument();
                xlogDoc.Load(lFilePath);
                XmlNode xnode = xlogDoc.GetElementsByTagName("SMSLogs")[0];

                XmlNode newNode = xlogDoc.CreateElement("SMSLogs");
                XmlAttribute time = xlogDoc.CreateAttribute("time");
                time.Value = DateTime.Today.Date.ToString("HH:mm:ss:ff");
                newNode.Attributes.Append(time);

                XmlNode appName = xlogDoc.CreateElement("application");
                appName.Value = pAppName;
                newNode.AppendChild(appName);

                XmlNode mobNo = xlogDoc.CreateElement("mobile");
                mobNo.Value = pMobile;
                newNode.AppendChild(mobNo);

                XmlNode subj = xlogDoc.CreateElement("subject");
                subj.Value = pSubject;
                newNode.AppendChild(subj);

                XmlNode msg = xlogDoc.CreateElement("message");
                msg.Value = pMsg;
                newNode.AppendChild(msg);

                XmlElement x = xlogDoc.DocumentElement;
                XmlNode y = xlogDoc.GetElementsByTagName("SMSLogs")[0];
                
                x.InsertBefore(newNode, y);


                xlogDoc.Save(lFilePath);

                //string LogEntry =
                //           "<Logs date='" + DateTime.Today.Date.ToString("yyyy-MM-dd") + "'>"+
                //            "<SMSLogs time='" + DateTime.Today.Date.ToString("HH:mm:ss:ff") + "'>" +
                //            "<application>" + pAppName + "</application>" +
                //            "<mobile>" + pMobile + "</mobile>" +
                //            "<subject>" + pSubject + "</subject>" +
                //            "<message>" + pMsg + "</message>" +
                //            "</SMSLogs></Logs>";
            }
            catch (Exception ex)
            {
                Logger.Write("",pAppName, ex.ToString());
            }
        }

        private static string GetFileName()
        {
            string lFile = @"~/Log/" + DateTime.Today.Date.ToString("yyyy-MM-dd") + ".xml";
            if (!File.Exists(lFile))
                File.Copy(@"~/Log/LogStructure.xml",lFile);

            return lFile;
        }

        private static string GetReturnMessage(string ret)
        {
            switch (ret)
            {
                case "1": return "Message sent";
                case "-1": return "Invalid user and/or password or API is not allowed for your account";
                case "-2": return "Credit limit reached";
                case "-5": return "Local opt out (the recipient/number is on your opt-out list.)";
                case "-7": return "Invalid message or subject (exceeds maximum number of characters and/or contains invalid characters - see a list of valid characters below)";
                case "-104": return "Globally opted out phone number (the phone number has been opted out from all messages sent from our short code)";
                case "-106": return "Incorrectly formatted phone number (number must be 10 digits)";
                case "-10": return "Unknown error (please contact our support dept.)";
                default: return null;
            }
        }
    }
}
