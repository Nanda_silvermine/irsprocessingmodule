﻿using System;
using System.Collections.Generic;
using System.Text;
using API;
using System.Diagnostics;

namespace eFormEmailSender
{
    public class eMailService
    {
        public static bool SendEmail(string pTo, string pCC, string bCC, string pSubject, string pBody, string pAttachment)
        {
            string senderEmail = string.Empty;
            string senderPwd = string.Empty;
            int senderPort = 0;
            string senderServer = string.Empty;

            if (Settings.Default.isProductionMode == true)
            {
                senderEmail = Settings.Default.eMailUser_Test;
                senderPwd = Settings.Default.eMailPwd_Test;
                senderPort = Settings.Default.eMailPort_Test;
                senderServer = Settings.Default.eMailServer_Test;
            }
            else
            {
                senderEmail = Settings.Default.eMailUser_Prod;
                senderPwd = Settings.Default.eMailPwd_Prod;
                senderPort = Settings.Default.eMailPort_Prod;
                senderServer = Settings.Default.eMailServer_Prod;
            }

            System.Net.Mail.MailMessage myMail = new System.Net.Mail.MailMessage(senderEmail, pTo);
            myMail.Subject = pSubject;
            myMail.IsBodyHtml = true;
            myMail.Body = pBody;

            if (pCC != "") { myMail.CC.Add(pCC); }
            if (bCC != "") { myMail.Bcc.Add(bCC); }

            if (pAttachment != "")
                myMail.Attachments.Add(new System.Net.Mail.Attachment(pAttachment));

            System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient(senderServer,senderPort);

            client.Credentials = new System.Net.NetworkCredential(senderEmail, senderPwd);

            client.EnableSsl = true;
            client.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
            client.Send(myMail);

            myMail.Dispose();

            return true;
        }

        public static bool SendEmailWithSchedule1(string pTo, string pCC, string bCC, string pSubject, string pBody, byte[] pSchedule1, string pSubmissionID)
        {
            try
            {
                string senderEmail = string.Empty;
                string senderPwd = string.Empty;
                int senderPort = 0;
                string senderServer = string.Empty;

                if (Settings.Default.isProductionMode == true)
                {
                    senderEmail = Settings.Default.eMailUser_Test;
                    senderPwd = Settings.Default.eMailPwd_Test;
                    senderPort = Settings.Default.eMailPort_Test;
                    senderServer = Settings.Default.eMailServer_Test;
                }
                else
                {
                    senderEmail = Settings.Default.eMailUser_Prod;
                    senderPwd = Settings.Default.eMailPwd_Prod;
                    senderPort = Settings.Default.eMailPort_Prod;
                    senderServer = Settings.Default.eMailServer_Prod;
                }

                System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage(senderEmail, pTo);
                msg.Subject = pSubject;
                msg.BodyEncoding = System.Text.Encoding.ASCII;
                msg.IsBodyHtml = true;
                msg.Body = pBody;
                
                if (pCC != "") { msg.CC.Add(pCC); }
                if (bCC != "") { msg.Bcc.Add(bCC); }
                


                string fileName = @"C:\Temp\" + pSubmissionID + ".pdf";


                using (System.IO.BinaryWriter binWriter =
                    new System.IO.BinaryWriter(System.IO.File.Open(fileName, System.IO.FileMode.Create)))
                {
                    binWriter.Write(pSchedule1);
                }

                System.Net.Mail.Attachment attachement = new System.Net.Mail.Attachment(fileName);

                msg.Attachments.Add(attachement);

                System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient(senderServer, senderPort);
                client.Credentials = new System.Net.NetworkCredential(senderEmail, senderPwd);
                client.EnableSsl = true;
                client.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;

                client.Send(msg);
                msg.Dispose();
                System.IO.File.Delete(@"C:\Temp\" + pSubmissionID + ".pdf");
                return true;
            }
            catch (Exception ex)
            {
                Log.Write("eForm2290: "+ex.Message, EventLogEntryType.Error);
                return false;
            }
        }
    }
}
