using System;
using System.Data;
using API;
using eFormEmailSender;
using DBOperations;
using System.Configuration;
using SMG_MailChimp;

public class eForm2290Operations 
{
    DBStorage db = new DBStorage();

    public eForm2290Operations()
    {
    }
  
    public DataSet GetPendingSubmissionList(string flag)
    {
        try
        {
            DataSet ds = new DataSet();
            string ProcedureName="",ParamName="",ParamValue="";

            if(flag!="")
            {
                ParamName = "@flag";
                ParamValue = flag;
                ProcedureName = "usp_Sub2290_Get_SubmissionList";
            }
            else
            {
                ParamName = "";
                ParamValue = "";
                ProcedureName = "usp_Sub2290_Get_PendingSubmission";
            }
            ds=db.SelectProcedureData(ProcedureName,ParamName,ParamValue);
            
            DataTable dt = ds.Tables[0];
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    dt.Rows[i]["TaxId"] = Crypt.Decrypt(dt.Rows[i]["TaxId"].ToString());
                }
                dt.AcceptChanges();
            }
            ds.Tables.Clear();
            ds.Tables.Add(dt);

            return ds;
        }
        catch
        {
            return null;
        }
    }
   
    public DataSet GetPendingMailingList()
    {
        try
        {
            DataSet ds = new DataSet();
            string ProcedureName = "";
            ProcedureName = "usp_Sub2290_Get_EmailListAccept";
            ds = db.SelectProcedureData(ProcedureName, "", "");

            DataTable dt = ds.Tables[0];
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    dt.Rows[i]["TaxID"] = Crypt.Decrypt(dt.Rows[i]["TaxID"].ToString());
                }
                dt.AcceptChanges();
            }
            ds.Tables.Clear();
            ds.Tables.Add(dt);

            return ds;
        }
        catch
        {
            return null;
        }
    }

    public DataSet GetPendingFAXList()
    {
        try
        {
            DataSet ds = new DataSet();
            string ProcedureName = "";
            ProcedureName = "usp_Sub2290_Get_FaxListAccept";
            ds = db.SelectProcedureData(ProcedureName, "", "");

            DataTable dt = ds.Tables[0];
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    dt.Rows[i]["TaxID"] = Crypt.Decrypt(dt.Rows[i]["TaxID"].ToString());
                }
                dt.AcceptChanges();
            }
            ds.Tables.Clear();
            ds.Tables.Add(dt);

            return ds;
        }
        catch
        {
            return null;
        }
    }

    public DataSet GetAllSubmissionData(string FormId, string UserId, string TaxId)
    {
        try
        {
            DataSet ds = new DataSet();
            ds = db.SelectProcedureData("usp_Sub2290_Get_AllSubmissionRecords", "@FormId,@UserId,@TaxId", FormId + "," + UserId + "," + Crypt.Encrypt(TaxId));

            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ds.Tables[0].Rows[i]["ur_sec_chars"] = Crypt.Decrypt(ds.Tables[0].Rows[i]["ur_sec_chars"].ToString());
                }
                ds.Tables[0].AcceptChanges();
            }

            if (ds.Tables[1].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                {
                    ds.Tables[1].Rows[i]["pd_tax_id"] = Crypt.Decrypt(ds.Tables[1].Rows[i]["pd_tax_id"].ToString());
                    ds.Tables[1].Rows[i]["pd_sig_pin"] = Crypt.Decrypt(ds.Tables[1].Rows[i]["pd_sig_pin"].ToString());
                }
                ds.Tables[1].AcceptChanges();
            }

            if (ds.Tables[2].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[2].Rows.Count; i++)
                {
                    string ttd_tax_id = Crypt.Decrypt(ds.Tables[2].Rows[i]["ttd_tax_id"].ToString());
                    if (ttd_tax_id.Length == 0)
                        ds.Tables[2].Rows[i]["ttd_tax_id"] = 0;
                    else
                        ds.Tables[2].Rows[i]["ttd_tax_id"] = ttd_tax_id;
                }
                ds.Tables[2].AcceptChanges();
            }

            if (ds.Tables[4].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[4].Rows.Count; i++)
                {
                    ds.Tables[4].Rows[i]["ip_tax_id"] = ds.Tables[4].Rows[i]["ip_tax_id"].ToString();
                    ds.Tables[4].Rows[i]["ip_bank_routing_no"] = Crypt.Decrypt(ds.Tables[4].Rows[i]["ip_bank_routing_no"].ToString());
                    ds.Tables[4].Rows[i]["ip_bank_account_no"] = Crypt.Decrypt(ds.Tables[4].Rows[i]["ip_bank_account_no"].ToString());
                }
                ds.Tables[4].AcceptChanges();
            }
            
            if (ds.Tables[5].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[5].Rows.Count; i++)
                {
                    if(ds.Tables[5].Rows[i]["ppd_EIN"]!=null)
                        ds.Tables[5].Rows[i]["ppd_EIN"] = Crypt.Decrypt(ds.Tables[5].Rows[i]["ppd_EIN"].ToString());
                    ds.Tables[5].Rows[i]["ppd_SSN_PTIN"] = Crypt.Decrypt(ds.Tables[5].Rows[i]["ppd_SSN_PTIN"].ToString());
                }
                ds.Tables[5].AcceptChanges();
            }

            if (ds.Tables[6].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[6].Rows.Count; i++)
                {
                    ds.Tables[6].Rows[i]["tpd_tax_id"] = Crypt.Decrypt(ds.Tables[6].Rows[i]["tpd_tax_id"].ToString());
                    ds.Tables[6].Rows[i]["tpd_designee_PIN"] = Crypt.Decrypt(ds.Tables[6].Rows[i]["tpd_designee_PIN"].ToString());
                }
                ds.Tables[6].AcceptChanges();
            }

            return ds;
        }
        catch
        {
            return null;
        }
    }
    
    public int UpdateSubmissionRecords(string TaxId,string FormId,string SubmissionId,string TaxBegindate,
        string TaxEndDate,string masterStatus,string SubmissionStatus,string RejectionErrorCodes,string TimeStamp)
    {
        try
        {
            string ParamNames, ParamValues = "";
            TaxId = Crypt.Encrypt(TaxId);

            ParamNames = "@sd_tax_ID,@sd_form_id,@sd_submission_ID,@sd_tax_begin_date,@sd_tax_end_date,@sd_submission_status,@sd_rejection_err_codes,@sd_timestamp,@master_status";
            ParamValues = TaxId + "," + FormId + "," + SubmissionId + "," + TaxBegindate + "," + TaxEndDate + "," + SubmissionStatus + "," + RejectionErrorCodes + "," + TimeStamp +","+ masterStatus;
            int i = db.UpdateProcedureData("usp_Sub2290_Update_SubmissionResult", ParamNames, ParamValues);
            return 1;
        }
        catch
        {
            return 0;
        }
    }
    
    public DataSet Select_Data(string ProcedureName, string ParamName, string ParamValue)
    {
        try
        {
            DataSet ds = new DataSet();
            ds = db.SelectProcedureData(ProcedureName, ParamName, ParamValue);
            return ds;
        }
        catch
        {
            return null;
        }
    }

    public DataSet Select_Data(string qry)
    {
        try
        {
            DataSet ds = new DataSet();
            ds = db.SelectQuery(qry);
            return ds;
        }
        catch
        {
            return null;
        }
    }

    public int Update_Data(string ProcedureName, string ParamName, string ParamValue)
    {
        try
        {
            int i;
            i = db.UpdateProcedureData(ProcedureName, ParamName, ParamValue);
            return i;
        }
        catch
        {
            return 0;
        }
    }
   
    public int Update_Schedule1(string reference_no, byte[] schedule1)
    {
        try
        {
            int i;
            i = db.UpdateSchedule1(reference_no,schedule1);
            return i;
        }
        catch
        {
            return 0;
        }
    }

    public bool Update_MailSentStatusAfterSubmission(string reference_no,out string error)
    {
        error = "";
        try
        {
            int i;
            i = db.UpdateMailSentStatus(reference_no);
            return Convert.ToBoolean(i);
        }
        catch(Exception ex)
        {
            error = ex.Message + ex.StackTrace;
            return false;
        }
    }

    public bool Update_MailSentStatusAfterSubmission(string reference_no)
    {
        try
        {
            int i;
            i = db.UpdateMailSentStatus(reference_no);
            return Convert.ToBoolean(i);
        }
        catch
        {
            return false;
        }
    }

    public bool SendSubmissionMailAcceptance(string BeginYear, string Name, string Email, string ReferenceID, string support, out string error)
    {
        error = "";
        try
        {
            Name = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(Name);
            byte[] Schedule1;
            string SubmissionID;
            DataTable dt = new DataTable();
            
            dt = db.SelectQuery("select S.submission_id, F.schedule1 FROM C_Submissions S LEFT JOIN [2290_Form_Binary] F ON S.FK_2290F_key=F.FK_2290F_key WHERE S.ref_no='" + ReferenceID + "'").Tables[0];
            if (dt.Rows.Count == 0)
                return false;
            else
            {
                Schedule1 = ((byte[])dt.Rows[0][1]);
                SubmissionID = Convert.ToString(dt.Rows[0][0]);
               // return SendMail_Acceptance(BeginYear, Name, Email, Schedule1, SubmissionID, support);
                return SendMail_Acceptance_New(BeginYear, Name, Email, Schedule1, SubmissionID, support, ReferenceID);
            }
        }
        catch(Exception ex)
        {
            error = ex.Message;
            return false;
        }

    }
    public bool SendSubmissionMailAcceptance(string BeginYear, string Name, string Email, string ReferenceID,string support)
    {
        try
        {
                Name = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(Name);
                byte[] Schedule1;
                string SubmissionID;
                DataTable dt = new DataTable();
                dt = db.SelectQuery("select S.irs_submission_id, F.stamped_sch_1 FROM C_Submissions S LEFT JOIN [2290_Form_Binary] F ON S.CFK_FD_form_key=F.FK_2290FD_form_key WHERE S.reference_no='" + ReferenceID + "'").Tables[0];
                if (dt.Rows.Count == 0)
                    return false;
                else
                {
                    Schedule1 = ((byte[])dt.Rows[0][1]);
                    SubmissionID = Convert.ToString(dt.Rows[0][0]);
                   // return SendMail_Acceptance(BeginYear, Name, Email, Schedule1, SubmissionID, support);
                    return SendMail_Acceptance_New(BeginYear, Name, Email, Schedule1, SubmissionID, support, ReferenceID);
                }
        }
        catch
        {
            return false;
        }
    
    }

    public bool SendSubmissionMailRejection(string pTo, string pCC, string bCC, string pSubject, string pBody)
    {
        try
        {
            //OLD EMAIL
            //return eMailService.SendEmail(pTo, pCC, bCC, pSubject, pBody, "");

            //NEW SMG_MAILCHIMP
            return SMG_Mailer.SendEmail(pTo, pCC, bCC, pSubject, pBody, "");
        }
        catch 
        {
            return false;
        }
    }

    private bool SendMail_Acceptance_New(string BeginYear, string Name, string Email, byte[] Schedule1, string SubmissionID, string support, string Ref_no)
    {
        try
        {
            string subject = "The IRS has accepted your 2290 return for the Tax Year ";
            subject += BeginYear + " - " + (Convert.ToInt16(BeginYear) + 1);

            System.Globalization.TextInfo textInfo = new System.Globalization.CultureInfo("", false).TextInfo;
            Name = textInfo.ToTitleCase(Name.ToLower());

            string body = "<!DOCTYPE html><html xmlns=http://www.w3.org/1999/xhtml xmlns:v=urn:schemas-microsoft-com:vml xmlns:o=urn:schemas-microsoft-com:office:office><head><title> Download Schedule-1</title><meta http-equiv=X-UA-Compatible content='IE=edge'><meta http-equiv=Content-Type content='text/html; charset=UTF-8'><meta name=viewport content='width=device-width, initial-scale=1'><style type=text/css>@import url(https://fonts.googleapis.com/css?family=Roboto:300,400,500,700);@media only screen and (min-width:480px){.mj-column-per-100{width:100%!important;max-width:100%}.mj-column-per-84{width:84%!important;max-width:84%}.mj-column-per-16{width:16%!important;max-width:16%}.mj-column-per-93{width:93%!important;max-width:93%}.mj-column-per-70{width:70%!important;max-width:70%}.mj-column-per-20{width:20%!important;max-width:20%}.mj-column-per-35{width:35%!important;max-width:35%}.mj-column-per-65{width:65%!important;max-width:65%}}@media only screen and (max-width:480px){table.mj-full-width-mobile{width:100%!important}td.mj-full-width-mobile{width:auto!important}}</style></head><body style=margin:0;padding:0;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;background-color:#ffffff><div style=background-color:#ffffff><div style='background:#fff;background-color:#fff;margin:0 auto;max-width:600px'><table align=center border=0 cellpadding=0 cellspacing=0 role=presentation style=border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;background:#ffffff;background-color:#ffffff;width:100%><tbody><tr><td style=border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;direction:ltr;font-size:0;padding:0;text-align:center><div class='mj-column-per-100 mj-outlook-group-fix' style=font-size:0;line-height:0;text-align:left;display:inline-block;width:100%;direction:ltr><div class='mj-column-per-84 mj-outlook-group-fix' style=font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:84%><table border=0 cellpadding=0 cellspacing=0 role=presentation width=100% style=border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt><tbody><tr><td style=border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;background-color:#006b8a;vertical-align:top;padding:0><table border=0 cellpadding=0 cellspacing=0 role=presentation style=border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt width=100%><tr><td style=border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;font-size:0px;word-break:break-word><div style=height:10px> &nbsp;</div></td></tr></table></td></tr></tbody></table></div><div class='mj-column-per-16 mj-outlook-group-fix' style=font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:16%><table border=0 cellpadding=0 cellspacing=0 role=presentation width=100% style=border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt><tbody><tr><td style=border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;vertical-align:top;padding:0><table border=0 cellpadding=0 cellspacing=0 role=presentation style=border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt width=100%><tr><td align=right style=border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;font-size:0;padding:0;word-break:break-word><table border=0 cellpadding=0 cellspacing=0 role=presentation style=border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px><tbody><tr><td style=border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;width:90px> <img alt=eForm2290 height=auto src=https://mcusercontent.com/f5f7a0d0b45e79dbdfab5be08/images/0c247da7-053c-483c-a370-d3d9079509d1.png style=border:0;height:auto;line-height:100%;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px width='90'/></td></tr></tbody></table></td></tr></table></td></tr></tbody></table></div></div></td></tr></tbody></table></div><div style='background:#fff;background-color:#fff;margin:0 auto;max-width:600px'><table align=center border=0 cellpadding=0 cellspacing=0 role=presentation style=border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;background:#ffffff;background-color:#ffffff;width:100%><tbody><tr><td style='border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;direction:ltr;font-size:0;padding:10px 0 30px 0;text-align:center'><div class='mj-column-per-100 mj-outlook-group-fix' style=font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%><table border=0 cellpadding=0 cellspacing=0 role=presentation width=100% style=border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt><tbody><tr><td style=border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;vertical-align:top;padding:0><table border=0 cellpadding=0 cellspacing=0 role=presentation style=border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt width=100%><tr><td align=center style='border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;font-size:0;padding:10px 25px;padding-top:0;padding-bottom:5px;word-break:break-word'><div style=font-family:Roboto,Arial,Helvetica;font-size:32px;font-weight:400;line-height:1.4;text-align:center;color:#000>Your return is accepted!</div></td></tr><tr><td align=center style='border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;font-size:0;padding:0 40px 0 40px;word-break:break-word'><div style=font-family:Roboto,Arial,Helvetica;font-size:14px;font-weight:400;line-height:1.4;text-align:center;color:#000>The IRS has accepted your 2290 filing, download updated Schedule-1</div></td></tr></table></td></tr></tbody></table></div></td></tr></tbody></table></div><div style='background:#fff;background-color:#fff;margin:0 auto;max-width:600px'><table align=center border=0 cellpadding=0 cellspacing=0 role=presentation style=border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;background:#ffffff;background-color:#ffffff;width:100%><tbody><tr><td style=border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;direction:ltr;font-size:0;padding:0;text-align:center><div class='mj-column-per-93 mj-outlook-group-fix' style=font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%><table border=0 cellpadding=0 cellspacing=0 role=presentation style=border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;background-color:#006B8A;border-radius:20px;vertical-align:top width=100%><tr><td style=border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;font-size:0px;word-break:break-word><div style=height:5px> &nbsp;</div></td></tr></table></div></td></tr></tbody></table></div><div style='background:#fff;background-color:#fff;margin:0 auto;max-width:600px'><table align=center border=0 cellpadding=0 cellspacing=0 role=presentation style=border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;background:#ffffff;background-color:#ffffff;width:100%><tbody><tr><td style='border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;direction:ltr;font-size:0;padding:20px 0;padding-bottom:0;padding-top:25px;text-align:center'><div class='mj-column-per-100 mj-outlook-group-fix' style=font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%><table border=0 cellpadding=0 cellspacing=0 role=presentation width=100% style=border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt><tbody><tr><td style=border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;vertical-align:top;padding:0><table border=0 cellpadding=0 cellspacing=0 role=presentation style=border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt width=100%><tr><td align=left style='border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;font-size:0;padding:10px 25px;padding-bottom:5px;word-break:break-word'><div style=font-family:Roboto,Arial,Helvetica;font-size:14px;font-weight:400;line-height:1.5;text-align:left;color:#000>Hello "+ Name
            + ",</div></td></tr><tr><td align=left style='border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;font-size:0;padding:10px 25px;padding-top:20px;word-break:break-word'><div style=font-family:Roboto,Arial,Helvetica;font-size:14px;font-weight:400;line-height:1.5;text-align:left;color:#000>Good news! Your Form 2290 submission has been accepted by the IRS.You can download your Schedule-1<b>attached with this email.</b> Alternatively, you can visit your <a href='https://www.eform2290.com/'>eForm2290 dashboard</a> to download the latest Schedule-1.</div></td></tr></table></td></tr></tbody></table></div></td></tr></tbody></table></div><div style='background:#fff;background-color:#fff;margin:0 auto;max-width:600px'><table align=center border=0 cellpadding=0 cellspacing=0 role=presentation style=border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;background:#ffffff;background-color:#ffffff;width:100%><tbody><tr><td style='border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;direction:ltr;font-size:0;padding:20px 0;padding-bottom:0;padding-top:10px;text-align:center'><div class='mj-column-per-100 mj-outlook-group-fix' style=font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%><table border=0 cellpadding=0 cellspacing=0 role=presentation width=100% style=border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt><tbody><tr><td style=border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;vertical-align:top;padding:0><table border=0 cellpadding=0 cellspacing=0 role=presentation style=border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt width=100%><tr><td align=left style='border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;font-size:0;padding:10px 25px;padding-bottom:5px;word-break:break-word'><div style=font-family:Roboto,Arial,Helvetica;font-size:14px;font-weight:400;line-height:1.5;text-align:left;color:#000>Reference Number: <b>"+ Ref_no
            + "</b></div></td></tr></table></td></tr></tbody></table></div></td></tr></tbody></table></div><div style='background:#ffffff;background-color:#ffffff;margin:0px auto;max-width:600px;'><table align='center' border='0' cellpadding='0' cellspacing='0' role='presentation' style='background:#ffffff;background-color:#ffffff;width:100%;'><tbody><tr><td style='direction:ltr;font-size:0px;padding:20px 0;padding-bottom:20px;padding-top:30px;text-align:center;'><!--[if mso | IE]><table role='presentation' border='0' cellpadding='0' cellspacing='0'><tr><td class='' style='vertical-align:top;width:558px;' ><![endif]--><div class='mj-column-per-93 mj-outlook-group-fix' style='font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;'><table border='0' cellpadding='0' cellspacing='0' role='presentation' width='100%'><tbody><tr><td style='background-color:#f2f2f2;vertical-align:top;padding:20px 0px 20px 0px;'><table border='0' cellpadding='0' cellspacing='0' role='presentation' width='100%'><tbody><tr><td align='center' style='font-size:0px;padding:10px 25px;padding-right:40px;padding-bottom:5px;padding-left:40px;word-break:break-word;'><div style='font-family:Roboto, Arial, Helvetica;font-size:20px;font-weight:400;line-height:1.5;text-align:center;color:#000000;'>Please note that your copy of Schedule-1 is attached with this email.</div></td></tr></tbody></table></td></tr></tbody></table></div></div></td></tr></tbody></table></div><div style='background:#fff;background-color:#fff;margin:0 auto;max-width:600px'><table align=center border=0 cellpadding=0 cellspacing=0 role=presentation style=border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;background:#ffffff;background-color:#ffffff;width:100%><tbody><tr><td style='border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;direction:ltr;font-size:0;padding:20px 0;padding-bottom:10px;padding-top:20px;text-align:center'><div class='mj-column-per-93 mj-outlook-group-fix' style=font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%><table border=0 cellpadding=0 cellspacing=0 role=presentation width=100% style=border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt><tbody><tr><td style='border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;background-color:#006b8a;vertical-align:top;padding:50px 0 40px 0'><table border=0 cellpadding=0 cellspacing=0 role=presentation style=border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt width=100%><tr><td align=center style='border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;font-size:0;padding:10px 25px;padding-top:0;padding-bottom:0;word-break:break-word'><div style=font-family:Roboto,Arial,Helvetica;font-size:20px;font-weight:500;line-height:1.4;text-align:center;color:#fff>Loved your 2290 e-filing experience?</div></td></tr><tr><td align=center style='border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;font-size:0;padding:10px 25px;padding-top:5px;padding-bottom:20px;word-break:break-word'><div style=font-family:Roboto,Arial,Helvetica;font-size:14px;font-weight:400;line-height:1.6;text-align:center;color:#fff>Rate our services and spread the word!</div></td></tr><tr><td align=center vertical-align=middle style='border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;font-size:0;padding:10px 25px;word-break:break-word'><table border=0 cellpadding=0 cellspacing=0 role=presentation style=border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:separate;line-height:100%><tr><td align=center bgcolor=#006B8A role=presentation style='border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;border:2px solid #fff;border-radius:5px;cursor:auto;mso-padding-alt:14px 28px;background:#006b8a' valign=middle> <a href=https://g.page/eform2290/review?rc style='display:inline-block;background:#006b8a;color:#fff;font-family:Roboto;font-size:16px;font-weight:500;line-height:120%;margin:0;text-decoration:none;text-transform:none;padding:14px 28px;mso-padding-alt:0;border-radius:5px' target=_blank> SUBMIT YOUR FEEDBACK </a></td></tr></table></td></tr></table></td></tr></tbody></table></div></td></tr></tbody></table></div><div style='background:#fff;background-color:#fff;margin:0 auto;border-radius:5px;max-width:600px'><table align=center border=0 cellpadding=0 cellspacing=0 role=presentation style=border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;background:#ffffff;background-color:#ffffff;width:100%;border-radius:5px><tbody><tr><td style='border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;direction:ltr;font-size:0;padding:20px 0;padding-bottom:0;padding-top:20px;text-align:center'><div class='mj-column-per-93 mj-outlook-group-fix' style=font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%><table border=0 cellpadding=0 cellspacing=0 role=presentation width=100% style=border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt><tbody><tr><td style='border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;background-color:#f2f2f2;vertical-align:top;padding:40px 10px 0 10px'><table border=0 cellpadding=0 cellspacing=0 role=presentation style=border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt width=100%><tr><td align=center style='border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;font-size:0;padding:10px 25px;padding-top:10px;word-break:break-word'><div style=font-family:Roboto,Arial,Helvetica;font-size:20px;font-weight:500;line-height:1.6;text-align:center;color:#000>Frequently Asked Questions (FAQs)</div></td></tr></table></td></tr></tbody></table></div></td></tr></tbody></table></div><div style='background:#fff;background-color:#fff;margin:0 auto;border-radius:5px;max-width:600px'><table align=center border=0 cellpadding=0 cellspacing=0 role=presentation style=border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;background:#ffffff;background-color:#ffffff;width:100%;border-radius:5px><tbody><tr><td style='border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;direction:ltr;font-size:0;padding:20px 0;padding-bottom:0;padding-top:0;text-align:center'><div class='mj-column-per-93 mj-outlook-group-fix' style=font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:center;width:100%><table border=0 cellpadding=0 cellspacing=0 role=presentation width=100% style=border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt><tbody><tr><td style='border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;background-color:#f2f2f2;vertical-align:center;padding:20px 20px 40px 20px'><table border=0 cellpadding=0 cellspacing=0 role=presentation style=border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt width=100%><tr><td align=left style='border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;font-size:0;padding:10px 25px;word-break:break-word'><div style=font-family:Roboto,Arial,Helvetica;font-size:16px;font-weight:400;line-height:1.6;text-align:left;text-decoration:underline;color:#000><a href=https://www.eform2290.com/instructions/schedule-1#div_block-13-447 style=text-decoration:none;color:#000000>1. What is <b>Schedule-1</b>?</a></div></td></tr><tr><td align=left style='border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;font-size:0;padding:10px 25px;word-break:break-word'><div style=font-family:Roboto,Arial,Helvetica;font-size:16px;font-weight:400;line-height:1.6;text-align:left;text-decoration:underline;color:#000><a href=https://www.eform2290.com/instructions/schedule-1#div_block-33-447 style=text-decoration:none;color:#000000>2. What is the <b>purpose of Schedule 1</b>?</a></div></td></tr><tr><td align=left style='border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;font-size:0;padding:10px 25px;word-break:break-word'><div style=font-family:Roboto,Arial,Helvetica;font-size:16px;font-weight:400;line-height:1.6;text-align:left;text-decoration:underline;color:#000><a href=https://www.eform2290.com/instructions/schedule-1#div_block-133-447 style=text-decoration:none;color:#000000>3. What to do <b>in case of loss</b> of Schedule 1 copy?</a></div></td></tr><tr><td align=left style='border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;font-size:0;padding:10px 25px;word-break:break-word'><div style=font-family:Roboto,Arial,Helvetica;font-size:16px;font-weight:400;line-height:1.6;text-align:left;text-decoration:underline;color:#000><a href=https://www.eform2290.com/instructions/schedule-1#div_block-133-447 style=text-decoration:none;color:#000000>4. How to <b>download Schedule 1</b>?</a></div></td></tr></table></td></tr></tbody></table></div></td></tr></tbody></table></div><div style='background:#fff;background-color:#fff;margin:0 auto;max-width:600px'><table align=center border=0 cellpadding=0 cellspacing=0 role=presentation style=border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;background:#ffffff;background-color:#ffffff;width:100%><tbody><tr><td style='border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;direction:ltr;font-size:0;padding:20px 0;padding-bottom:0;padding-top:50px;text-align:center'><div class='mj-column-per-100 mj-outlook-group-fix' style=font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%><table border=0 cellpadding=0 cellspacing=0 role=presentation width=100% style=border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt><tbody><tr><td style='border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;vertical-align:top;padding:0 10px 0 10px'><table border=0 cellpadding=0 cellspacing=0 role=presentation style=border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt width=100%><tr><td align=center style='border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;font-size:0;padding:10px 25px;padding-top:0;padding-bottom:0;word-break:break-word'><div style=font-family:Roboto,Arial,Helvetica;font-size:20px;font-weight:600;line-height:1.6;text-align:center;color:#000>Need help with your Schedule-1?</div></td></tr><tr><td align=center style='border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;font-size:0;padding:10px 25px;padding-top:5px;padding-bottom:0;word-break:break-word'><div style=font-family:Roboto,Arial,Helvetica;font-size:16px;font-weight:400;line-height:1.6;text-align:center;color:#000>Talk to our Form 2290 Expert, available 24x7 in English & Spanish</div></td></tr></table></td></tr></tbody></table></div></td></tr></tbody></table></div><div style='background:#fff;background-color:#fff;margin:0 auto;max-width:600px'><table align=center border=0 cellpadding=0 cellspacing=0 role=presentation style=border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;background:#ffffff;background-color:#ffffff;width:100%><tbody><tr><td style='border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;direction:ltr;font-size:0;padding:20px 0;padding-bottom:0;padding-top:30px;text-align:center'><div class='mj-column-per-93 mj-outlook-group-fix' style=font-size:0;line-height:0;text-align:left;display:inline-block;width:100%;direction:ltr><div class='mj-column-per-35 mj-outlook-group-fix' style=font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:middle;width:35%><table border=0 cellpadding=0 cellspacing=0 role=presentation width=100% style=border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt><tbody><tr><td style='border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;vertical-align:middle;padding:0 0 10px 0'><table border=0 cellpadding=0 cellspacing=0 role=presentation style=border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt width=100%><tr><td align=right style=border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;font-size:0;padding:0;word-break:break-word><table border=0 cellpadding=0 cellspacing=0 role=presentation style=border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px><tbody><tr><td style=border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;width:75px> <img alt='Form 2290 Expert' height=auto src=https://i.imgur.com/pM3FOgh.png style=border:0;height:auto;line-height:100%;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px width='75'/></td></tr></tbody></table></td></tr></table></td></tr></tbody></table></div><div class='mj-column-per-65 mj-outlook-group-fix' style=font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:middle;width:65%><table border=0 cellpadding=0 cellspacing=0 role=presentation width=100% style=border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt><tbody><tr><td style='border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;vertical-align:middle;padding:0 0 20px 0'><table border=0 cellpadding=0 cellspacing=0 role=presentation style=border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt width=100%><tr><td align=left style='border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;font-size:0;padding:0 10px 0 15px;word-break:break-word'><div style=font-family:Roboto,Arial,Helvetica;font-size:20px;font-weight:600;line-height:1.5;text-align:left;color:#000>Support Team</div></td></tr><tr><td align=left style='border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;font-size:0;padding:0 10px 0 15px;word-break:break-word'><div style=font-family:Roboto,Arial,Helvetica;font-size:14px;font-weight:400;line-height:1.5;text-align:left;color:#000> <img width='15' height='15' src='https://mail.google.com/mail/e/1f4de' alt='phone'/> <a href=tel:+18668816767 target=_blank><b>866-881-6767</b></a></div></td></tr><tr><td align=left style='border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;font-size:0;padding:0 10px 0 15px;word-break:break-word'><div style=font-family:Roboto,Arial,Helvetica;font-size:14px;font-weight:400;line-height:1.5;text-align:left;color:#000> <img width='18' height='15' src='https://thumbs.dreamstime.com/b/white-envelope-14503336.jpg' alt='email'/> <a href=team@support.eform2290.com target=_blank><b>support@eform2290.com</b></a></div></td></tr></table></td></tr></tbody></table></div></div></td></tr></tbody></table></div><div style='background:#fff;background-color:#fff;margin:0 auto;max-width:600px'><table align=center border=0 cellpadding=0 cellspacing=0 role=presentation style=border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;background:#ffffff;background-color:#ffffff;width:100%><tbody><tr><td style='border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;direction:ltr;font-size:0;padding:30px 0 0 0;text-align:center'><div class='mj-column-per-93 mj-outlook-group-fix' style=font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%><table border=0 cellpadding=0 cellspacing=0 role=presentation style=border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;background-color:#006B8A;border-radius:20px;vertical-align:top width=100%><tr><td style=border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;font-size:0px;word-break:break-word><div style=height:5px> &nbsp;</div></td></tr></table></div></td></tr></tbody></table></div><div style='background:#fff;background-color:#fff;margin:0 auto;max-width:600px'><table align=center border=0 cellpadding=0 cellspacing=0 role=presentation style=border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;background:#ffffff;background-color:#ffffff;width:100%><tbody><tr><td style='border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;direction:ltr;font-size:0;padding:30px 0 10px 0;text-align:center'><div class='mj-column-per-100 mj-outlook-group-fix' style=font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%><table border=0 cellpadding=0 cellspacing=0 role=presentation width=100% style=border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt><tbody><tr><td style='border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;vertical-align:top;padding:0 10px 0 10px'><table border=0 cellpadding=0 cellspacing=0 role=presentation style=border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt width=100%><tr><td align=center style=border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;font-size:0;padding:0;word-break:break-word><table border=0 cellpadding=0 cellspacing=0 role=presentation style=border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px><tbody><tr><td style=border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100px> <img alt=eForm2290 height=auto src=https://mcusercontent.com/f5f7a0d0b45e79dbdfab5be08/images/0c247da7-053c-483c-a370-d3d9079509d1.png style=border:0;height:auto;line-height:100%;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px width='100'/></td></tr></tbody></table></td></tr><tr><td align=center style='border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;font-size:0;padding:10px 25px;padding-top:15px;padding-bottom:5px;word-break:break-word'><div style=font-family:Roboto,Arial,Helvetica;font-size:18px;font-weight:600;line-height:1.3;text-align:center;color:#000>10 Years of e-Filing Excellence</div></td></tr><tr><td align=center style='border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;font-size:0;padding:10px 25px;padding-top:0;padding-bottom:30px;word-break:break-word'><div style=font-family:Roboto,Arial,Helvetica;font-size:14px;font-weight:400;line-height:1.3;text-align:center;color:#000>Rated &#x2B50;&#x2B50;&#x2B50;&#x2B50;&#x2B50; by people like you</div></td></tr><tr><td align=center style='border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;font-size:0;padding:10px 25px;padding-top:0;padding-bottom:5px;word-break:break-word'><div style=font-family:Roboto,Arial,Helvetica;font-size:11px;font-weight:400;line-height:1.3;text-align:center;color:#000>You are receiving this email because you have registered with your email, *|EMAIL|*, on eform2290.com.</div></td></tr><tr><td align=center style='border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;font-size:0;padding:10px 25px;padding-top:0;padding-bottom:5px;word-break:break-word'><div style=font-family:Roboto,Arial,Helvetica;font-size:11px;font-weight:400;line-height:1.3;text-align:center;color:#000>Silvermine Group of Wilton, LLC 1230 Rosecrans Ave, Suite 300, Manhattan Beach, CA 90266.</div></td></tr><tr><td align=center style='border-collapse:collapse;mso-table-lspace:0;mso-table-rspace:0;font-size:0;padding:10px 25px;padding-top:0;word-break:break-word'><div style=font-family:Roboto,Arial,Helvetica;font-size:11px;font-weight:400;line-height:1.3;text-align:center;color:#000>Copyright � *|CURRENT_YEAR|*, All rights reserved.</div></td></tr></table></td></tr></tbody></table></div></td></tr></tbody></table></div></div></body></html>";
            
            //NEW EMAIL SMG_MAILCHIMP
            return SMG_Mailer.SendEmailWithSchedule1(Email, "", support, subject, body, Schedule1, SubmissionID);
        }
        catch
        {
            return false;
        }
    }


    private bool SendMail_Acceptance(string BeginYear, string Name, string Email, byte[] Schedule1, string SubmissionID,string support)
    {
        try
        {
            string subject = "Congratulations! 2290 Tax Return Accepted for year ";
            subject += BeginYear + " - " + (Convert.ToInt16(BeginYear) + 1);

            System.Globalization.TextInfo textInfo = new System.Globalization.CultureInfo("", false).TextInfo;
            Name = textInfo.ToTitleCase(Name.ToLower());
            /* OLD --
            string body = "Dear " + Name + ":<br/><br/>";
            body += "Congratulations! Your heavy vehicle use tax return accepted by the IRS and ";
            body += "schedule 1 is attached with this email. ";
            body += "You can even download schedule 1 anytime from ";
            body += "<a href='http://www.eform2290.com'>eForm2290</a> website." + "<br/><br/>";
            body += "If you have questions or need further assistance, ";
            body += "please contact our support team." + "<br/><br/>";
            body += "Thank you for your business." + "<br/><br/>";
            body += "--<br/>With Regards,<br/>";
            body += "eForm2290 Support Team<br/>";
            body += "www.eForm2290.com<br/>";
            */
            /* New below */
            //NEW = https:/ /forms.gle/u1rrCrxSzxJheAY57
            //OLD = https:/ /goo.gl/KXVGU0
            /* OLD URL = https:/ /goo.gl/KXVGU0 */
            string ReviewTxt = "<table style='border:1px solid #000;font-family: calibri; font-size: 16px;'><tr style='border-bottom:1px solid #000'><td><p  style='text-align:justify'>If you are happy with our recent service, we ask that you <b>leave an online review</b> for our business helping us share our great service with others.</p></td></tr><tr style='text-align:center'><td><a href='https://www.facebook.com/eform2290/reviews/' target='_blank'><img src='https://www.eform2290.com/Images/r-fb.png' style='width:250px;' /></a></td></tr><tr><td><hr /><p  style='text-align:justify'>If we did not meet your expectations in any way, please leave us your feedback so we can address it with you and improve.</p></td></tr><tr style='text-align:center'><td><a href='https://forms.gle/u1rrCrxSzxJheAY57' target='_blank'><img src='https://www.eform2290.com/Images/gf.png' style='width:250px' /></a></td></tr></table>" +
          "<p style='font-family: calibri; font-size: 16px'>We look forward to seeing you again soon.</p>";

            string body = "<p style='font-family: calibri; font-size: 16px'>" +
                "Dear " + Name + ", <br /><br />" +
                "Greeting from eForm2290.com. <br /><br />" +
                "Congratulations! Your Heavy Vehicle Use Tax return (HVUT - 2290 Form), is accepted by the IRS and " +
                "Schedule 1 is attached with the email.<br />" +
                "You can download schedule 1 anytime from our website <a href='https://www.eform2290.com/'>www.eForm2290.com</a> .<br />" +
                "If you have questions or need further assistance, please contact our support team. <br/><br /> ";
            
            body = body + "Thank you for your business.<br/></p>";

            body = body + ReviewTxt;

            body = body + "<p style='font-family: calibri; font-size: 16px'>" +
                " Sincerely, <br />" +
                " eForm2290 Team <br /> " +
                " Email: <a href='mailto:support@eform2290.com'>support@eform2290.com</a> <br /> " +
                " <a href='https://www.eform2290.com/'>www.eForm2290.com</a>" +
                " </p>";

            /* ADP */
            /*string ADP = "<a href='http://www.adp.com/silvermine' target='_blank' style='display: inline-flex; text-decoration: none;'>" +
                    "<img src='https://www.eform2290.com/Images/ADP_New.jpg' style='height: 90px; border: 1px solid; border-right: none;' />" +
                    "<span style='background: #DF1E33; color: #fff; font-size: 13px; padding: 5px; text-align: justify; border: 1px solid #000; border-left: none;width:27%'>" +
                    "Through our partnership with ADP, we offer payroll, tax and HR solutions that can help improve the management of your people and your risk, so you can get" +
                    " back to focusing on your business.<br /></span>" +
                    "</a><br />";
             */
            string ADP = "<p style='border: 1px solid;text-align: center;padding: 3px;margin-bottom: -1.3%;width: auto;font-family:tahoma'></p><p style='border: 1px solid;padding: 5px;text-align: center;width: auto;'>" +
                    "<a href='https://www.adp.com/silvermine' target='_blank' style='display: inline-flex; text-decoration: none;height:100px;max-width:600px;width:100%;margin-top: 3px;margin-bottom: 3px;'>" +
                    "<img src='https://www.eform2290.com/Images/ADP_New.jpg' style='border: 1px solid; border-right: none' />" +
                    "<span style='background: #DF1E33; color: #fff; font-size: 13px; padding: 5px; text-align: justify; border: 1px solid #000; border-left: none;font-family:tahoma'>" +
                    "ADP is our preferred partner for payroll, tax and HR solutions.  " +
                    "This collaboration is our step towards a better eco-system for our clients and customers.  " +
                    "<br />Our partnership program empowers our customers to receive exclusive benefits towards services offered by ADP. </span>" +
                    "</a></p>";
            try
            {
                if (ConfigurationSettings.AppSettings["ADPDisplay"].ToString() == "1")
                {
                    body = body + ADP;
                }
            }
            catch (Exception e)
            { }

            /* RIGHauler Advertisment display */
            string RigHDisplay = "<p style='border: 1px solid;text-align: center;padding: 3px;margin-bottom: -1.3%;width: auto;font-family:tahoma'></p><p style='border: 1px solid;padding: 5px;text-align: center;width: auto;'>" +
                    "<a href='https://www.righauler.com' target='_blank' style='display: inline-flex; text-decoration: none;height:100px;max-width:600px;width:100%;margin-top: 3px;margin-bottom: 3px;'>" +
                    "<img src='https://www.eform2290.com/EmailImg/RHAd.gif' style='border: 1px solid; border-right: none' />" +
                    "</a></p>";
            try
            {
                if (ConfigurationSettings.AppSettings["RIGHaulerDisplay"].ToString() == "1")
                {
                    body = body + RigHDisplay;
                }
            }
            catch (Exception e)
            { }



            body = body + "<div style='color: gray'> " +
                " <hr /><p style='font-family: calibri; font-size: 10px;'>This is a computer-generated email. If you have any problems, questions or requests " +
                " regarding this message. Please email to <a style='font-size: 10px;' href='mailto:support@eform2290.com'>support@eform2290.com</a> and Support team will  " +
                " contact you at the earliest.</p> </div>";

            //OLD EMAIL Service
            //return eFormEmailSender.eMailService.SendEmailWithSchedule1(Email, "", support, subject, body, Schedule1, SubmissionID);

            //NEW EMAIL SMG_MAILCHIMP
            return SMG_Mailer.SendEmailWithSchedule1(Email, "", support, subject, body, Schedule1, SubmissionID);
        }
        catch
        {
            return false;
        }
    }

    private bool SendMail_Rejected(string BeginYear, string Name, string Email, string business, string reason, string SubmissionID, string support)
    {
        try
        {
            string subject = "2290 Tax Return Rejected for year ";
            subject += BeginYear + " - " + (Convert.ToInt16(BeginYear) + 1);

            System.Globalization.TextInfo textInfo = new System.Globalization.CultureInfo("", false).TextInfo;
            Name = textInfo.ToTitleCase(Name.ToLower());

            string body = "Dear " + Name + ":<br/><br/>";
            body += "Your heavy vehicle use tax return for ";
            body += business.ToUpper() + "has been rejected by the IRS due to following reason: ";

            body += "<ul><li>";
            body += reason;
            body+="</li></ul>";

            body += "You can contact our support team for further assistance.";
            
            body += "You can even download schedule 1 anytime from ";
            body += "<a href='https://www.eform2290.com'>eForm2290</a> website." + "<br/><br/>";
            body += "If you have questions or need further assistance, ";
            body += "please contact our support team." + "<br/><br/>";
            body += "Thank you for your business." + "<br/><br/>";
            body += "--<br/>With Regards,<br/>";
            body += "eForm2290 Support Team<br/>";
            body += "www.eForm2290.com<br/>";

            return false;
        }
        catch
        {
            return false;
        }
    }

    public DateTime GetServerDate()
    {
        DateTime dt = DateTime.Now;
        try
        {
            dt =Convert.ToDateTime(db.SelectQuery("select getdate()").Tables[0].Rows[0][0]);
         
        }
        catch {  }
        return dt;
    }

    public bool Update_FaxSentStatusAfterSubmission(string pRef_no, string tran_id)
    {
        try
        {
            int i;
            i = db.UpdateFaxSentStatus(pRef_no, tran_id);
            return Convert.ToBoolean(i);
        }
        catch
        {
            return false;
        }
    }
}
