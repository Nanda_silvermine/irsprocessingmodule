using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DBOperations;

namespace f2290Submission
{
    public partial class frmErrorList : Form
    {
        eForm2290Operations dbOperation = new eForm2290Operations();
        
        public frmErrorList(string FormId)
        {
            InitializeComponent();
            Entity entity = new Entity();
            DataTable dt = dbOperation.Select_Data("usp_Sub2290_Get_Error_RulesNumbers", "@FormId", FormId).Tables[0];
            grvErrorList.AutoGenerateColumns = false;
            grvErrorList.DataSource = dt;         
        }
    }
}